// dependencies
var express = require('express');
var http = require('http');
var https = require('https');
var fs = require("fs");

var app = express();

/* GET home page. */
app.get('/', function (req, res) {
    res.send("HELLO WORLD");
});

app.use(express.static('public'))


// file location of private key
var privateKey = fs.readFileSync( '../backend/keys/private.key' );

// file location of SSL cert
var certificate = fs.readFileSync( '../backend/keys/certificate.crt' );

// set up a config object
var server_config = {
    key : privateKey,
    cert: certificate
};

console.log("hello!");

var comparisonList1 = [

	{ 
		"attribute1": "poverty_rate", 
		"attribute2": null, 
		"filter": [ 
			{"attr": "region", "vals": ["Midwest"] }, 
			{"attr": "county_type", "vals": [ "Urban" ] }
			]
	},
	{ 
		"attribute1": "poverty_rate", 
		"attribute2": null, 
		"filter": [ 
			{"attr": "region", "vals": ["Midwest"] }, 
			{"attr": "county_type", "vals": [ "Rural" ] }
			]
	},
	{ 
		"attribute1": "poverty_rate", 
		"attribute2": null, 
		"filter": [ 
			{"attr": "region", "vals": ["Midwest"] }, 
			{"attr": "county_type", "vals": [ "Suburban" ] } 
			]
	}
];

var comparisonList2 = [

	{ 
		"attribute1": "poverty_rate", 
		"attribute2": null, 
		"filter": [ 
			{"attr": "region", "vals": ["Pacific", "Southwest"] }, 
			{"attr": "county_type", "vals": [ "Urban" ] }
			]
	},
	{ 
		"attribute1": "poverty_rate", 
		"attribute2": null, 
		"filter": [ 
			{"attr": "region", "vals": ["Pacific", "Southwest"] }, 
			{"attr": "county_type", "vals": [ "Rural" ] } 
			]
	},
	{ 
		"attribute1": "poverty_rate", 
		"attribute2": null, 
		"filter": [ 
			{"attr": "region", "vals": ["Pacific", "Southwest"] }, 
			{"attr": "county_type", "vals": [ "Suburban" ] }
			]
	}
];

var target1 = [
	{ 
		"attribute1": "poverty_rate", 
		"attribute2": null, 
		"filter": [ 
			{"attr": "county_type", "vals": [ "Rural" ] }
			]
		}
];

var multifaceted = [
	{ 
		"attribute1": "poverty_rate", 
		"attribute2": "cardiovascular_disease_rate", 
		"filter": [ 
			{"attr": "county_type", "vals": [ "Rural" ] }
			]
		}
];


var browseList1 = [
	{ 
		"attribute1": "poverty_rate", 
		"attribute2": null, 
		"filter": [ 
			{"attr": "county_type", "vals": [ "Rural" ] }
			]
	},
	{ 
		"attribute1": "cardiovascular_disease_rate", 
		"attribute2": null, 
		"filter": [ 
			{"attr": "county_type", "vals": [ "Rural" ] } 
			]
	},
	{ 
		"attribute1": "region", 
		"attribute2": null, 
		"filter": [ 
			{"attr": "county_type", "vals": [ "Rural" ] }
			]
	}

];

var browseList2 = [
	{ 
		"attribute1": "poverty_rate", 
		"attribute2": null, 
		"filter": [ 
			{"attr": "county_type", "vals": [ "Rural" ] },
			{"attr": "diabetes_rate", "vals": [ "very_high_diabetes_rate" ] }

			]
	},
	{ 
		"attribute1": "cardiovascular_disease_rate", 
		"attribute2": null, 
		"filter": [ 
			{"attr": "county_type", "vals": [ "Rural" ] } ,
			{"attr": "diabetes_rate", "vals": [ "very_high_diabetes_rate" ] }

			]
	},
	{ 
		"attribute1": "region", 
		"attribute2": null, 
		"filter": [ 
			{"attr": "county_type", "vals": [ "Rural" ] },
			{"attr": "diabetes_rate", "vals": [ "very_high_diabetes_rate" ] }

			]
	}

];


var mfBrowseList1 = [
	{ 
		"attribute1": "poverty_rate", 
		"attribute2": "diabetes_rate", 
		"filter": [ 
			{"attr": "county_type", "vals": [ "Rural" ] }
			]
	},
	{ 
		"attribute1": "cardiovascular_disease_rate", 
		"attribute2": "diabetes_rate", 
		"filter": [ 
			{"attr": "county_type", "vals": [ "Rural" ] } 
			]
	},
	{ 
		"attribute1": "region", 
		"attribute2": "diabetes_rate", 
		"filter": [ 
			{"attr": "county_type", "vals": [ "Rural" ] }
			]
	}

];

var mfBrowseList2 = [
	{ 
		"attribute1": "poverty_rate", 
		"attribute2": "diabetes_rate", 
		"filter": [ 
			{"attr": "county_type", "vals": [ "Rural" ] },
			{"attr": "diabetes_rate", "vals": [ "very_high_diabetes_rate" ] }

			]
	},
	{ 
		"attribute1": "cardiovascular_disease_rate", 
		"attribute2": "diabetes_rate", 
		"filter": [ 
			{"attr": "county_type", "vals": [ "Rural" ] } ,
			{"attr": "diabetes_rate", "vals": [ "very_high_diabetes_rate" ] }

			]
	},
	{ 
		"attribute1": "region", 
		"attribute2": "diabetes_rate", 
		"filter": [ 
			{"attr": "county_type", "vals": [ "Rural" ] },
			{"attr": "diabetes_rate", "vals": [ "very_high_diabetes_rate" ] }

			]
	}

];

var subPlusAttr1 = [
	{ 
		"attribute1": "poverty_rate", 
		"attribute2": "diabetes_rate", 
		"filter": [ 
			{"attr": "county_type", "vals": [ "Rural" ] },
			{"attr": "diabetes_rate", "vals": [ "very_high_diabetes_rate" ] }

			]
	},
	{ 
		"attribute1": "poverty_rate", 
		"attribute2": "cardiovascular_disease_rate", 
		"filter": [ 
			{"attr": "county_type", "vals": [ "Rural" ] } ,
			{"attr": "diabetes_rate", "vals": [ "very_high_diabetes_rate" ] }

			]
	},
	{ 
		"attribute1": "poverty_rate", 
		"attribute2": "region", 
		"filter": [ 
			{"attr": "county_type", "vals": [ "Rural" ] },
			{"attr": "diabetes_rate", "vals": [ "very_high_diabetes_rate" ] }

			]
	}

];


var mfBrowseBySubAttr = [
	{ 
		"attribute1": "poverty_rate", 
		"attribute2": "diabetes_rate", 
		"filter": [ 
			{"attr": "county_type", "vals": [ "Rural" ] },
			{"attr": "diabetes_rate", "vals": [ "very_high_diabetes_rate" ] }

			]
	},
	{ 
		"attribute1": "poverty_rate", 
		"attribute2": "cardiovascular_disease_rate", 
		"filter": [ 
			{"attr": "county_type", "vals": [ "Rural" ] } ,
			{"attr": "diabetes_rate", "vals": [ "very_high_diabetes_rate" ] }

			]
	},
	{ 
		"attribute1": "poverty_rate", 
		"attribute2": "region", 
		"filter": [ 
			{"attr": "county_type", "vals": [ "Rural" ] },
			{"attr": "diabetes_rate", "vals": [ "very_high_diabetes_rate" ] }

			]
	},
		{ 
		"attribute1": "uninsured_rate", 
		"attribute2": "diabetes_rate", 
		"filter": [ 
			{"attr": "county_type", "vals": [ "Rural" ] },
			{"attr": "diabetes_rate", "vals": [ "very_high_diabetes_rate" ] }

			]
	},
	{ 
		"attribute1": "uninsured_rate", 
		"attribute2": "cardiovascular_disease_rate", 
		"filter": [ 
			{"attr": "county_type", "vals": [ "Rural" ] } ,
			{"attr": "diabetes_rate", "vals": [ "very_high_diabetes_rate" ] }

			]
	},
	{ 
		"attribute1": "uninsured_rate", 
		"attribute2": "region", 
		"filter": [ 
			{"attr": "county_type", "vals": [ "Rural" ] },
			{"attr": "diabetes_rate", "vals": [ "very_high_diabetes_rate" ] }

			]
	}

];

// console.log("comparison 1 ");
// console.log( JSON.stringify( comparisonList1 ) ); 

// console.log("type: ");
// console.log( makeNewCollectionAndAssignType(comparisonList1) ); 

console.log("----------------------");
console.log("----------------------");

console.log("target 1 ");
console.log( JSON.stringify( target1 ) ); 
console.log("type: ");
console.log( makeNewCollectionAndAssignType(target1) ); 
console.log("----------------------");
console.log("----------------------");

console.log("mf 1 ");
console.log( JSON.stringify( multifaceted ) ); 
console.log("type: ");
console.log( makeNewCollectionAndAssignType(multifaceted) ); 
console.log("----------------------");
console.log("----------------------");

console.log("browse 1 ");
console.log( JSON.stringify( browseList1 ) ); 
console.log("type: ");
console.log( makeNewCollectionAndAssignType(browseList1) ); 
console.log("----------------------");
console.log("----------------------");


console.log("browse 2 ");
console.log( JSON.stringify( browseList2 ) ); 
console.log("type: ");
console.log( makeNewCollectionAndAssignType(browseList2) ); 
console.log("----------------------");
console.log("----------------------");



console.log("mf browse 1 ");
console.log( JSON.stringify( mfBrowseList1 ) ); 
console.log("type: ");
console.log( makeNewCollectionAndAssignType(mfBrowseList1) ); 
console.log("----------------------");
console.log("----------------------");



console.log("mf browse 2 ");
console.log( JSON.stringify( mfBrowseList2 ) ); 
console.log("type: ");
console.log( makeNewCollectionAndAssignType(mfBrowseList2) ); 
console.log("----------------------");
console.log("----------------------");



console.log("subPlusAttr ");
console.log( JSON.stringify( subPlusAttr1 ) ); 
console.log("type: ");
console.log( makeNewCollectionAndAssignType(subPlusAttr1) ); 
console.log("----------------------");
console.log("----------------------");

console.log("mfBrowseBySubAttr ");
console.log( JSON.stringify( mfBrowseBySubAttr ) ); 
console.log("type: ");
console.log( makeNewCollectionAndAssignType(mfBrowseBySubAttr) ); 
console.log("----------------------");
console.log("----------------------");



function makeNewCollectionAndAssignType ( viewList ){

    //how many filters

    filterList = [ "test" ]; 


    //how many attributes
    attributeList = []; 
    for( i = 0; i < viewList.length; i++ ){
        countAlreadyThere = 0; 
        for(j =0; j < attributeList.length; j++){
            if( viewList[i]["attribute1"] == attributeList[j] ){
                countAlreadyThere++; 
            } 
        }
        if( countAlreadyThere == 0 ){
            attributeList.push(viewList[i]["attribute1"]);
        }
    }

    //how many secondary attributes 
    secondaryAttributeList = []; 
    for( i = 0; i < viewList.length; i++ ){
        countAlreadyThere = 0; 
        for(j =0; j < secondaryAttributeList.length; j++){
            if( viewList[i]["attribute2"] == secondaryAttributeList[j] ){
                countAlreadyThere++; 
            } 
        }
        if( countAlreadyThere == 0 ){
        	if( viewList[i]["attribute2"] != null )
            	secondaryAttributeList.push(viewList[i]["attribute2"]);
        }
    }



    if( attributeList.length == 1 && secondaryAttributeList.length == 0 ){ //compare type



    }



    if( viewList.length == 1 && filterList.length == 1 && attributeList.length == 1 && secondaryAttributeList.length == 0 ){
    	return "target"; 
    }
    if( viewList.length == 1 && filterList.length == 1 && attributeList.length == 1 && secondaryAttributeList.length == 1 ){
    	return "multifaceted"; 
    }
    if( filterList.length == 1 && attributeList.length == viewList.length && secondaryAttributeList.length == 0 ){
    	return "browse"; 
    }
    if( filterList.length == 1 && attributeList.length == viewList.length && secondaryAttributeList.length == 1 ){
    	return "multifacetedBrowse"; 
    } 
    if( filterList.length == 1 && attributeList.length == 1  && secondaryAttributeList.length == viewList.length ){
    	return "sub+attr";
    }
    if( filterList.length == 1 && (attributeList.length * secondaryAttributeList.length)  == viewList.length ){
    	return "mfBrowseBySub+Attr";
    }

    return "Working on it...."


};





