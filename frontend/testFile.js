var listOfAllTests = [

  //INDEPENDENT
  //----- TARGETED
  { query: "I would just like to see UIC thefts by day", expected: "Independent, Targeted" },
  { query: "Can I  look at crimes near UIC in January by crime type?", expected: "Independent, Targeted" },
  { query: "Could you show me crimes that occurred on Monday near UIC by crime type?", expected: "Independent, Targeted" },
  { query: "How about burglaries in March by year?", expected: "Independent, Targeted" },
  { query: "Show me thefts that occur on streets by time of the day", expected: "Independent, Targeted" },
  { query: "What about crimes from 2014 in the Loop by month?", expected: "Independent, Targeted" },
  { query: "Can I see thefts in January 2012 by neighborhood?", expected: "Independent, Targeted" },
  { query: "Is there a distribution by crime types?", expected: "Independent, Targeted" },
  { query: "I want to look at assaults on streets in River North in June 2014 by time of the day", expected: "Independent, Targeted" },
  { query: "Can I see a breakdown of location types for all burglaries that take place on Saturday?", expected: "Independent, Targeted" },

  // Together
  { query: "What about crimes from 2014 in the Loop and UIC together by month?", expected: "Independent, Targeted, Together" },
  { query: "Can I see thefts and burglaries combined in January 2012 by neighborhood?", expected: "Independent, Targeted, Together" },
  { query: "I want to look at assaults on streets and sidewalks aggregated in River North in June 2014 by time of the day", expected: "Independent, Targeted, Together" },
  { query: "Can I see a breakdown of location types for all burglaries that take place on Saturday and Sunday combined together?", expected: "Independent, Targeted, Together" },


  //----- BROWSE
  { query: "Show me data about UIC", expected: "Independent, Create Many, Browse" },
  { query: "How about plots for both Loop and River North together", expected: "Independent, Create Many, Browse" },
  { query: "Can I get an overview of thefts that occurred in September", expected: "Independent, Create Many, Browse" },
  { query: "What about assaults near UIC and River North, combined together?", expected: "Independent, Create Many, Browse" },
  { query: "Can I see data about when thefts on streets happen", expected: "Independent, Create Many, Browse" },
  { query: "Where did theft crimes on Monday in 2014 happen?", expected: "Independent, Create Many, Browse" }, 
  { query: "When did Loop burglaries in 2014 occur?", expected: "Independent, Create Many, Browse" },


  //----- COMPARE
  { query: "I want to compare UIC and Loop crimes by day of the week that the crime occurred", expected: "Independent, Create Many, Compare" },
  { query: "Can I see theft, burglary and assault by neighborhood?", expected: "Independent, Create Many, Compare" },
  { query: "I want to see how 2012 and 2013 Loop crimes differ by crime type?", expected: "Independent, Create Many, Compare" },
  { query: "Is there a difference between January, March, July and October thefts on Friday by the time of the day that the crime occurred?", expected: "Independent, Create Many, Compare" },


  //----- Browse and Compare
  { query: "Show me information about UIC and the Loop", expected: "Independent, Create Many, Browse and Compare" },
  { query: "I want to compare when thefts and assaults occur", expected: "Independent, Create Many, Browse and Compare" },
   { query: "I want to see where June and January batteries happen", expected: "Independent, Create Many, Browse and Compare" },
  { query: "Can I see 2010, 2011, 2012, 2013 by neighborhood and day of the week", expected: "Independent, Create Many, Complex"},

  //------ Complex, Multifaceted
  { query: "I want to look at day of the week and neighborhood", expected: "Independent, Create Many, Complex" },
  { query: "What about March crimes by year and crime type", expected: "Independent, Create Many, Complex" },
  { query: "Can I see the relationship between crime type, neighborhood and month?", expected: "Independent, Create Many, Complex" },
  { query: "Can I see theft, battery and assault combined together by neighborhood and year", expected: "Independent, Create Many, Complex"}

  ////////////////////////////
  //TARGETED
  { query: "Show me 2012 crimes by time of the day", expected: "Referential queries on one view, Part 1 of 2: Create one view (targeted)" },
  { query: "This but 2013", expected: "Part 2, subset pivot, single" },

  { query: "Can I see crimes on streets by neighborhood", expected: "Referential queries on one view, Part 1 of 2: Create on view (targeted)" },
  { query: "This but sidewalks and restaurants", expected: "Part 2, subset pivot multi (One to many- sidewalks and restaurants)" },

  { query: "Can I see crimes in residences by day", expected: "Referential queries on one view, Part 1 of 2: Create one view (targeted)" },
  { query: "This but now with sidewalks and streets combined", expected: "Part 2, subset pivot single" },

  { query: "What about UIC thefts by day of the week?", expected: "Referential queries on one view, Part 1 of 2: Create one view (targeted)" },
  { query: "This but for the Loop", expected: "Part 2, Subset pivot partial (swap Loop for UIC, keep theft)" },

  { query: "Show me crimes in the Loop by year", expected: "Referential queries on one view, Part 1 of 2: Create one view (targeted)" },
  { query: "Now can I see this but for theft", expected: "Part 2, Add a filter criteria (theft)" },

  { query: "Show me January crimes by day", expected: "Referential queries on one view, Part 1 of 2: Create one view (targeted)" },
  { query: "This but by year", expected: "Part 2, pivot from one attribute to another" },

  { query: "Can I see Near West side crimes by day of the week?", expected:"Referential queries on one view, Part 1 of 2: Create on view (targeted)" },
  { query: "This but year and month", expected: "Part 2, pivot the attribute multiple times (one to many)" },

  { query: "I would just like to see UIC thefts by day", expected: "Referential queries on one view, Part 1 of 2: Create one view (targeted)" },
  { query: "Can I see this view but split by year", expected: "Part 2: Split" },

  { query: "Can I have a look at Saturday crimes by crime-type", expected: "Referential queries on one view, Part 1 of 2: Create one view (targeted)" },
  { query: "Take this view but split it by month", expected: "Part 2: Split" },

  { query: "Can I see the thefts by location type", expected:"Referential queries on one view, Part 1 of 2: Create one view (targeted)" },
  { query: "Now take this but split it by day", expected:  "Part 2: Split" },

  ///////////////////////////
  //BROWSE

  //--------COMPLETE
  { query: "Show me data about UIC", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: "Can I see this but for the Loop", expected: "Part 2: Subset collection single pivot complete" },

  { query: "I want to look at theft crimes", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: "Can we look at these views but with burglary and assault", expected: "Part 2: Subset collection, multi pivot complete" },

  { query: "Can I see views about Monday theft", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: "This but Tuesday burglary and Wednesday burglary", expected: "Part 2: Subset collection, multi pivot complete"  }, 

  { query: "Can I have a look at Friday crimes?", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: "What about this but for Thursday and Wednesday together?", expected: "Part 2: Subset collection, pivot with combined filter" },

  //PARTIAL
  { query: "Lets look at UIC theft", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: "Now can I see this but for battery", expected: "Part 2: Subset collection, single pivot partial"  },

  { query: "January crimes in the Loop", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: "This but May and October", expected: "Part 2: Subset collection, multi pivot partial" },

  { query: "Give me some plots for UIC theft", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: "Now can I get this but this time with some views with loop and river north combined together?", expected: "Part 2: Subset collection, partial pivot with combined filter" },

  //ADD
  { query: "Can I look that thefts that occur on the street", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: "This but just Monday", expected: "Part 2: Subset add filter single" },

  { query: "Can I see charts about UIC theft", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: " This but only for Monday and January", expected: "Part 2: Subset add filter single" },

  { query: "Could I see thefts", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: "This but now just for Monday and Tuesday together", expected: "Part 2: Subset collection, pivot with combined additional filter criteria" },

  { query: "Show me 2012 batteries", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: "This but charts for UIC and Loop", expected: "Part 2: Subset add filter multi" },

  { query: "Can I see statistics on UIC theft", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: " These views but for Monday and Tuesday and January", expected: "Part 1: Subset add filter multi"  },

  { query: "Can I See statistics on UIC theft", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: "This but Monday and Tuesday and January and February", expected: "Part 2: Subset add filter multi" },

  //EXTENDING
  { query: "Can I see when crimes occur near UIC", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: "Can I see these views but with crime type added", expected: "Part 2, Subset collection, extend" },

  { query: "I want to know when crimes occur near UIC", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: "Take these but add crime type and location type", expected: "Part 2: Subset collection, multi extend" },


  //   THESE OPERATE ON ATTRIBUTES, not the filters 
  //SPLIT
  { query: "Show me data about July crimes", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: "These views but split by year", expected: "Part 2: Subset collection, split" },

  { query: "Can I look at assaults on Saturday", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: "This but split by month", expected: "Part 2: Subset collection, split" },

  //MULTI-SPLIT
  { query: "Show me plots about the Loop", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: "Now can I see this but split by year and day", expected: "Part 2: Subset collection, split" },

  { query: "Is there a way to see data about crimes the occur on streets", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: "Ok, show me these but split them up by year and day", expected: "Part 2: Subset collection, split" },

]








var listOfTestQueriesIndependent = [


  //----- TARGETED
  { query: "I would just like to see UIC thefts by day", expected: "Independent, Targeted" },
  { query: "Can I  look at crimes near UIC in January by crime type?", expected: "Independent, Targeted" },
  { query: "Could you show me crimes that occurred on Monday near UIC by crime type?", expected: "Independent, Targeted" },
  { query: "How about burglaries in March by year?", expected: "Independent, Targeted" },
  { query: "Show me thefts that occur on streets by time of the day", expected: "Independent, Targeted" },
  { query: "What about crimes from 2014 in the Loop by month?", expected: "Independent, Targeted" },
  { query: "Can I see thefts in January 2012 by neighborhood?", expected: "Independent, Targeted" },
  { query: "Is there a distribution by crime types?", expected: "Independent, Targeted" },
  { query: "I want to look at assaults on streets in River North in June 2014 by time of the day", expected: "Independent, Targeted" },
  { query: "Can I see a breakdown of location types for all burglaries that take place on Saturday?", expected: "Independent, Targeted" },

  // Together
  { query: "What about crimes from 2014 in the Loop and UIC together by month?", expected: "Independent, Targeted, Together" },
  { query: "Can I see thefts and burglaries combined in January 2012 by neighborhood?", expected: "Independent, Targeted, Together" },
  { query: "I want to look at assaults on streets and sidewalks aggregated in River North in June 2014 by time of the day", expected: "Independent, Targeted, Together" },
  { query: "Can I see a breakdown of location types for all burglaries that take place on Saturday and Sunday combined together?", expected: "Independent, Targeted, Together" },


  //----- BROWSE
  { query: "Show me data about UIC", expected: "Independent, Create Many, Browse" },
  { query: "How about plots for both Loop and River North together", expected: "Independent, Create Many, Browse" },
  { query: "Can I get an overview of thefts that occurred in September", expected: "Independent, Create Many, Browse" },
  { query: "What about assaults near UIC and River North, combined together?", expected: "Independent, Create Many, Browse" },
  { query: "Can I see data about when thefts on streets happen", expected: "Independent, Create Many, Browse" },
  { query: "Where did theft crimes on Monday in 2014 happen?", expected: "Independent, Create Many, Browse" }, 
  { query: "When did Loop burglaries in 2014 occur?", expected: "Independent, Create Many, Browse" },


  //----- COMPARE
  { query: "I want to compare UIC and Loop crimes by day of the week that the crime occurred", expected: "Independent, Create Many, Compare" },
  { query: "Can I see theft, burglary and assault by neighborhood?", expected: "Independent, Create Many, Compare" },
  { query: "I want to see how 2012 and 2013 Loop crimes differ by crime type?", expected: "Independent, Create Many, Compare" },
  { query: "Is there a difference between January, March, July and October thefts on Friday by the time of the day that the crime occurred?", expected: "Independent, Create Many, Compare" },


  //----- Browse and Compare
  { query: "Show me information about UIC and the Loop", expected: "Independent, Create Many, Browse and Compare" },
  { query: "I want to compare when thefts and assaults occur", expected: "Independent, Create Many, Browse and Compare" },
   { query: "I want to see where June and January batteries happen", expected: "Independent, Create Many, Browse and Compare" },
  { query: "Can I see 2010, 2011, 2012, 2013 by neighborhood and day of the week", expected: "Independent, Create Many, Complex"},

  //------ Complex, Multifaceted
  { query: "I want to look at day of the week and neighborhood", expected: "Independent, Create Many, Complex" },
  { query: "What about March crimes by year and crime type", expected: "Independent, Create Many, Complex" },
  { query: "Can I see the relationship between crime type, neighborhood and month?", expected: "Independent, Create Many, Complex" },
  { query: "Can I see theft, battery and assault combined together by neighborhood and year", expected: "Independent, Create Many, Complex"}

  // { query: "I want to look at crime type and location type", expected: "Independent, Create Many, Complex" },
  // { query: "I want to look at crime type and year", expected: "Independent, Create Many, Complex" },
  // { query: "I want to look at crime type and time of the day", expected: "Independent, Create Many, Complex" },
  // { query: "I want to look at location type and year", expected: "Independent, Create Many, Complex" },


];


////////////////////////
///////////////////////
//////////////////////

var listOfTestTargetedPivots = [

  { query: "Show me 2012 crimes by time of the day", expected: "Referential queries on one view, Part 1 of 2: Create one view (targeted)" },
  { query: "This but 2013", expected: "Part 2, subset pivot, single" },

  { query: "Can I see crimes on streets by neighborhood", expected: "Referential queries on one view, Part 1 of 2: Create on view (targeted)" },
  { query: "This but sidewalks and restaurants", expected: "Part 2, subset pivot multi (One to many- sidewalks and restaurants)" },

  { query: "Can I see crimes in residences by day", expected: "Referential queries on one view, Part 1 of 2: Create one view (targeted)" },
  { query: "This but now with sidewalks and streets combined", expected: "Part 2, subset pivot single" },

  { query: "What about UIC thefts by day of the week?", expected: "Referential queries on one view, Part 1 of 2: Create one view (targeted)" },
  { query: "This but for the Loop", expected: "Part 2, Subset pivot partial (swap Loop for UIC, keep theft)" },

  { query: "Show me crimes in the Loop by year", expected: "Referential queries on one view, Part 1 of 2: Create one view (targeted)" },
  { query: "Now can I see this but for theft", expected: "Part 2, Add a filter criteria (theft)" },

  { query: "Show me January crimes by day", expected: "Referential queries on one view, Part 1 of 2: Create one view (targeted)" },
  { query: "This but by year", expected: "Part 2, pivot from one attribute to another" },

  { query: "Can I see Near West side crimes by day of the week?", expected:"Referential queries on one view, Part 1 of 2: Create on view (targeted)" },
  { query: "This but year and month", expected: "Part 2, pivot the attribute multiple times (one to many)" },

  //SPLIT
  { query: "I would just like to see UIC thefts by day", expected: "Referential queries on one view, Part 1 of 2: Create one view (targeted)" },
  { query: "Can I see this view but split by year", expected: "Part 2: Split" },

  { query: "Can I have a look at Saturday crimes by crime-type", expected: "Referential queries on one view, Part 1 of 2: Create one view (targeted)" },
  { query: "Take this view but split it by month", expected: "Part 2: Split" },

  { query: "Can I see the thefts by location type", expected:"Referential queries on one view, Part 1 of 2: Create one view (targeted)" },
  { query: "Now take this but split it by day", expected:  "Part 2: Split" },

];


//---- SUBSET PIVOTS
var listOfTestSubsetPivots = [

 //MULTI-SPLIT
  { query: "Show me plots about the Loop", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: "Now can I see this but split by year and day", expected: "Part 2: Subset collection, split" },

  { query: "Is there a way to see data about crimes the occur on streets", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: "Ok, show me these but split them up by year and day", expected: "Part 2: Subset collection, split" },



  //COMPLETE
  { query: "Show me data about UIC", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: "Can I see this but for the Loop", expected: "Part 2: Subset collection single pivot complete" },

  { query: "I want to look at theft crimes", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: "Can we look at these views but with burglary and assault", expected: "Part 2: Subset collection, multi pivot complete" },

  { query: "Can I see views about Monday theft", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: "This but Tuesday burglary and Wednesday burglary", expected: "Part 2: Subset collection, multi pivot complete"  }, 

  { query: "Can I have a look at Friday crimes?", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: "What about this but for Thursday and Wednesday together?", expected: "Part 2: Subset collection, pivot with combined filter" },

  //PARTIAL
  { query: "Lets look at UIC theft", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: "Now can I see this but for battery", expected: "Part 2: Subset collection, single pivot partial"  },

  { query: "January crimes in the Loop", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: "This but May and October", expected: "Part 2: Subset collection, multi pivot partial" },

  { query: "Give me some plots for UIC theft", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: "Now can I get this but this time with some views with loop and river north combined together?", expected: "Part 2: Subset collection, partial pivot with combined filter" },

  //ADD
  { query: "Can I look that thefts that occur on the street", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: "This but just Monday", expected: "Part 2: Subset add filter single" },

  { query: "Can I see charts about UIC theft", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: " This but only for Monday and January", expected: "Part 2: Subset add filter single" },

  { query: "Could I see thefts", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: "This but now just for Monday and Tuesday together", expected: "Part 2: Subset collection, pivot with combined additional filter criteria" },

  { query: "Show me 2012 batteries", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: "This but charts for UIC and Loop", expected: "Part 2: Subset add filter multi" },

  { query: "Can I see statistics on UIC theft", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: " These views but for Monday and Tuesday and January", expected: "Part 1: Subset add filter multi"  },

  { query: "Can I See statistics on UIC theft", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: "This but Monday and Tuesday and January and February", expected: "Part 2: Subset add filter multi" },

  //EXTENDING
  { query: "Can I see when crimes occur near UIC", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: "Can I see these views but with crime type added", expected: "Part 2, Subset collection, extend" },

  { query: "I want to know when crimes occur near UIC", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: "Take these but add crime type and location type", expected: "Part 2: Subset collection, multi extend" },


  //   THESE OPERATE ON ATTRIBUTES, not the filters 
  //SPLIT
  { query: "Show me data about July crimes", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: "These views but split by year", expected: "Part 2: Subset collection, split" },

  { query: "Can I look at assaults on Saturday", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: "This but split by month", expected: "Part 2: Subset collection, split" },

  //MULTI-SPLIT
  { query: "Show me plots about the Loop", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: "Now can I see this but split by year and day", expected: "Part 2: Subset collection, split" },

  { query: "Is there a way to see data about crimes the occur on streets", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: "Ok, show me these but split them up by year and day", expected: "Part 2: Subset collection, split" },

];



var listOfTestComparisonPivots = [

  //COMPARISON ATTRIBUTE PIVOT SINGLE 
  { query: "I want to compare UIC and Loop by day of the week", expected: "Referential queries on a comparison set, Part 1 of 2: Create comparison set" },
  { query: "This but year", expected: "Part 2: copy views and pivot attribute, single" },

  { query: "Show me theft and burglary by month", expected: "Referential queries on a comparison set, Part 1 of 2: Create comparison set"  },
  { query: "This but location type", expected: "Part 2: copy views and pivot attribute, single" },

   { query: "Can I see thefts in 2012 and 2013 by location type", expected: "Referential queries on a comparison set, Part 1 of 2: Create comparison set"  },
  { query: "This but by month", expected: "Part 2: copy views and pivot attribute, single" },

  //COMPARISON ATTRIBUTE PIVOT MULTI
  { query: "UIC and Loop restaurant thefts by time of the day", expected: "Referential queries on a comparison set, Part 1 of 2: Create comparison set"  },
  { query: "This but day of the week and month", expected: "Part 2: copy views and pivot attribute, multi" },

  { query: "Crimes near UIC and on Monday and Tuesday by year", expected: "Referential queries on a comparison set, Part 1 of 2: Create comparison set"  },
  { query: "This but crime type and month", expected: "Part 2: copy views and pivot attribute, multi" },

   { query: "Show me UIC and Loop by location type", expected: "Referential queries on a comparison set, Part 1 of 2: Create comparison set" },
  { query: "This but year and crime-type", expected: "Part 2: copy views and pivot attribute, multi" },

  //SPLIT SINGLE
  { query: "UIC and Loop by crime-type", expected: "Referential queries on a comparison set, Part 1 of 2: Create comparison set"  },
  { query: "This but split by year", expected: "Part 2: Split, single" },

  { query: "Can I see theft and battery by day", expected: "Referential queries on a comparison set, Part 1 of 2: Create comparison set"  },
  { query: "This but split by year", expected: "Part 2: Split, single" },

  { query: "January and March and October by day", expected: "Referential queries on a comparison set, Part 1 of 2: Create comparison set"  },
  { query: "This but split by crime type", expected: "Part 2: Split, single"},

  //SPLIT MULTI
  { query: "UIC and Loop and River-North by crime-type", expected: "Referential queries on a comparison set, Part 1 of 2: Create comparison set"  },
  { query: "This but split by year and day", expected: "Part 2: Split, multi"},

  { query: "Street and sidewalk thefts on Monday by time of the day", expected: "Referential queries on a comparison set, Part 1 of 2: Create comparison set"  },
  { query: "This but split by year and month", expected: "Part 2: Split, multi" },

  { query: "2012 and 2014 crimes by day", expected: "Referential queries on a comparison set, Part 1 of 2: Create comparison set"  },
  { query: "This but split by crime type and month", expected: "Part 2: Split, multi" },

  //FILTER
  { query: "I want to compare UIC and Loop by day", expected: "Referential queries on a comparison set, Part 1 of 2: Create comparison set"  },
  { query: "Show me these views but now for just thefts", expected: "Part 2, filter single" },

  { query: "Show me Monday and Tuesday and Wednesday by time of the day", expected: "Referential queries on a comparison set, Part 1 of 2: Create comparison set"  },
  { query: "These but with theft and burglary", expected: "Part 2, filter single" },

  { query: "Can I see crimes from June, July and August visualized by day", expected: "Referential queries on a comparison set, Part 1 of 2: Create comparison set"  },
  { query: "These views but theft and street", expected: "Part 2, filter single" },

  { query: "Show me crimes on streets and sidewalks by day", expected: "Referential queries on a comparison set, Part 1 of 2: Create comparison set"  },
  { query: "Can I see this but theft and burlary together in the UIC area", expected: "Part 2, filter single" },

  //EXTEND- complete 
  { query: "Show me crimes near UIC and the Loop by month", expected: "Referential queries on a comparison set, Part 1 of 2: Create comparison set"  },
  { query: "This but for River North", expected: "Part 2, Extend comparison collection" },

  { query: "Can I see UIC and Loop thefts by year", expected: "Referential queries on a comparison set, Part 1 of 2: Create comparison set"  },
  { query: "This but for River North and only thefts", expected: "Part 2, Extend comparison collection" },

  { query: "Can I see UIC and Loop theft and burglary by day", expected: "Referential queries on a comparison set, Part 1 of 2: Create comparison set"  },
  { query: "These views but with River North assault", expected: "Part 2, Extend comparison collection" },

  //EXTEND- partial 
  { query: "Show me UIC and Loop thefts by day", expected:"Referential queries on a comparison set, Part 1 of 2: Create comparison set"  },
  { query: "These plots but for River North", expected: "Part 2, Extend comparison collection" },

  { query: "Show me UIC and Loop theft on streets by month", expected: "Referential queries on a comparison set, Part 1 of 2: Create comparison set"  },
  { query: "This but River North", expected: "Part 2, Extend comparison collection" },

  { query: "March and April 2012 thefts by time of the day", expected: "Referential queries on a comparison set, Part 1 of 2: Create comparison set"  },
  { query: "This but now with 2013 thefts", expected: "Part 2, Extend comparison collection" },

  { query: "Can I see UIC and Loop theft on streets by day", expected: "Referential queries on a comparison set, Part 1 of 2: Create comparison set"  },
  { query: "This but River North theft", expected: "Part 2, Extend comparison collection" },

];




var listOfBrowseAndComparePivots = [

  //pivot
  { query: "Show me information about thefts near UIC and the Loop", expected: "Part 1 of 2: Creates a Browse and Compare Collection" },
  { query: "These views but for burglary", expected: "Part 2: Copy and pivot the collection" },

  { query: "Show me information about thefts on Monday near UIC and the Loop", expected: "Part 1 of 2: Creates a Browse and Compare Collection" },
  { query: "These views but for burglary", expected: "Part 2: Copy and pivot the collection" },

  { query: "Show me information about thefts on Monday near UIC and the Loop", expected: "Part 1 of 2: Creates a Browse and Compare Collection" },
  { query: "These views but for burglary and Tuesday", expected: "Part 2: Copy and pivot the collection" },

  { query: "Show me information about thefts near UIC and the Loop", expected: "Part 1 of 2: Creates a Browse and Compare Collection" },
  { query: "These views but for burglary and assault together", expected: "Part 2: Copy and pivot the collection" },

  { query: "Show me information about thefts near UIC and the Loop", expected: "Part 1 of 2: Creates a Browse and Compare Collection" },
  { query: "These views but for burglary and assault", expected: "Part 2: Copy and pivot the collection" },


  { query: "Show me information about Monday thefts in January and the July", expected: "Part 1 of 2: Creates a Browse and Compare Collection" },
  { query: "These views but for burglary", expected: "Part 2: Copy and pivot the collection" },


  //can't do
  //   { query: "Show me information about thefts and batteries together near UIC and the Loop", expected: "Part 1 of 2: Creates a Browse and Compare Collection" },
  // { query: "These views but for burglary", expected: "Part 2: Copy and pivot the collection" },

  //Extend the collection- subset
  { query: "Show me information about thefts near UIC and the Loop", expected: "Part 1 of 2: Creates a Browse and Compare Collection" },
  { query: "These views but for River North", expected: "Part 2: Extend the collection" },

  { query: "I want to look at thefts on Monday in 2012 in January and March and June", expected: "Part 1 of 2: Creates a Browse and Compare Collection" },
  { query: "These views but for October", expected: "Part 2: Extend the collection" },


  //Extend one dimension- attribute
  { query: "I want to compare when crimes occur near UIC and the Loop", expected: "Part 1 of 2: Creates a Browse and Compare Collection" },
  { query: "These views but for crime type as well", expected: "Part 2: Extend to a new attribute" },

  { query: "I want to compare when crimes occur near UIC and the Loop and Near West side", expected: "Part 1 of 2: Creates a Browse and Compare Collection" },
  { query: "These views but for crime type as well", expected: "Part 2: Extend to a new attribute" },

  { query: "I want to compare when crimes occur near UIC and the Loop and Near West side", expected: "Part 1 of 2: Creates a Browse and Compare Collection" },
  { query: "These views but for crime type and location type as well", expected: "Part 2: Extend to a new attribute" },

  { query: "I want to compare when crimes occur near UIC and the Loop and Near West side and River East", expected: "Part 1 of 2: Creates a Browse and Compare Collection" },
  { query: "These views but for crime type and location type as well", expected: "Part 2: Extend to a new attribute" },

  { query: "I want to compare where crimes occur on Monday, Tuesday, Wednesday and Thursday", expected: "Part 1 of 2: Creates a Browse and Compare Collection" },
  { query: "These views but for month and year and time of the day as well", expected: "Part 2: Extend to a new attribute" },


  //split
  { query: "Show me information about UIC and the Loop", expected: "Part 1 of 2: Creates a Browse and Compare Collection" },
  { query: "These views but split by year", expected: "Part 2: Split" },

    { query: "Show me information about January and March", expected: "Part 1 of 2: Creates a Browse and Compare Collection" },
  { query: "These views but split by neighborhood", expected: "Part 2: Split" },
  
  //filter
  { query: "Show me information about UIC and the Loop", expected: "Part 1 of 2: Creates a Browse and Compare Collection" },
  { query: "These views but for theft", expected: "Part 2: Add a filter criteria" },

  { query: "Show me information about January and March", expected: "Part 1 of 2: Creates a Browse and Compare Collection" },
  { query: "These views but for theft and streets", expected: "Part 2: Add a filter criteria" },

  { query: "Show me information about January and March", expected: "Part 1 of 2: Creates a Browse and Compare Collection" },
  { query: "These views but for theft and burglary together and streets", expected: "Part 2: Add a filter criteria" },

  { query: "Show me information about January and March crimes near UIC", expected: "Part 1 of 2: Creates a Browse and Compare Collection" },
  { query: "These views but for theft and burglary together and streets", expected: "Part 2: Add a filter criteria" },

  
];



var listOfTestComplexPivots = [

  //subset pivot
  //   forms the same multidimensional comparison grid
  { query: "What about March crimes by year and crime type", expected: "Part 1 of 2: Creates a Complex Collection" },
  { query: "These views but for July", expected: "Part 2: Add a filter criteria" },

  //   multi
  //     forms a grid 
  { query: "What about March crimes by year and crime type", expected: "Part 1 of 2: Creates a Complex Collection" },
  { query: "These views but for July and December", expected: "Part 2: Add a filter criteria" },


  //add filter criteria
  //---- single (forms a hierarchy)

  { query: "I want to look at day of the week and neighborhood", expected: "Part 1 of 2: Creates a Complex Collection" },
  { query: "These views but for just theft", expected: "Part 2: Add a filter criteria" },

  { query: "Can I see the relationship between crime type, neighborhood and month?", expected: "Part 1 of 2: Creates a Complex Collection" },
  { query: "Can we take these views and only show the data for 2012?", expected: "Part 1 of 2: Add a filter criteria" },

  { query: "What about March crimes by year and crime type", expected: "Part 1 of 2: Creates a Complex Collection" },
  { query: "These views but for crimes that took place in restaurants", expected: "Part 2: Add a filter criteria" },

  { query: "What about March crimes by year and crime type", expected: "Part 1 of 2: Creates a Complex Collection" },
  { query: "These views but for crimes that took place in 2012", expected: "Part 2: Add a filter criteria" },


  //-----multi to compare
  // ----- forms a multidimensional comparison grid with parents
  { query: "Can I see the relationship between month and neighborhood?", expected: "Part 1 of 2: Creates a Complex Collection" },
  { query: "Can we take these views and show them for 2012, 2013 and 2014?", expected: "Part 2: Multiple pivots, to compare in complex ways" },

  //   { query: "Can I see the relationship between crime type, neighborhood and month?", expected: "Part 1 of 2: Creates a Complex Collection" },


  //   { query: "Can I see theft, battery and assault combined together by neighborhood and year", expected: "Part 1 of 2: Creates a Complex Collection"}

  //  { query: "Show me information about January and March crimes near UIC", expected: "Part 1 of 2: Creates a Complex Collection" },
  // { query: "These views but for theft and burglary together and streets", expected: "Part 2: Add a filter criteria" },

];

var listOfPivotTheSubset = [

  //COMPLETE

  //TARGET
  { query: "Show me 2012 crimes by time of the day", expected: "Referential queries on one view, Part 1 of 2: Create one view (targeted)" },
  { query: "This but 2013", expected: "Part 2, subset pivot, single" },

  { query: "Can I see crimes on streets by neighborhood", expected: "Referential queries on one view, Part 1 of 2: Create on view (targeted)" },
  { query: "This but sidewalks and restaurants", expected: "Part 2, subset pivot multi (One to many- sidewalks and restaurants)" },

  { query: "Can I see crimes in residences by day", expected: "Referential queries on one view, Part 1 of 2: Create one view (targeted)" },
  { query: "This but now with sidewalks and streets combined", expected: "Part 2, subset pivot single" },


  //BROWSE COLLECTIONS
  { query: "Show me data about UIC", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: "Can I see this but for the Loop", expected: "Part 2: Subset collection single pivot complete" },

  { query: "I want to look at theft crimes", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: "Can we look at these views but with burglary and assault", expected: "Part 2: Subset collection, multi pivot complete" },

  { query: "Can I see views about Monday theft", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: "This but Tuesday burglary and Wednesday burglary", expected: "Part 2: Subset collection, multi pivot complete"  }, 

  { query: "Can I have a look at Friday crimes?", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: "What about this but for Thursday and Wednesday together?", expected: "Part 2: Subset collection, pivot with combined filter" },


  //COMPLEX
  { query: "What about March crimes by year and crime type", expected: "Part 1 of 2: Creates a Complex Collection" },
  { query: "These views but for July", expected: "Part 2: Add a filter criteria" },

  { query: "What about March crimes by year and crime type", expected: "Part 1 of 2: Creates a Complex Collection" },
  { query: "These views but for July and December", expected: "Part 2: Add a filter criteria" },

  // //CxC
  // { query: "Can I see UIC and Loop theft and burglary in 2012 by day", expected: "Referential queries on a comparison by comparison grid, Part 1 of 2: Create cxc grid"  },
  // { query: "These views but 2013", expected: "Part 2, Attribute pivot" },

  // { query: "Can I see UIC and Loop theft and burglary on streets by day", expected: "Referential queries on a comparison by comparison grid, Part 1 of 2: Create cxc grid"  },
  // { query: "These views but with restaurants", expected: "Part 2, Attribute pivot" },


  // //Multifaceted Browse
  // { query: "Can I see when thefts occur", expected: "Part 1 of 3: Creates a Browse Collection" },
  // { query: "These views but split by year", expected: "Part 2 or 3: Create multifaceted browse" },
  // { query: "These views but assault", expected: "Part 3 or 3: Subset pivot" },

  // { query: "Can I see when thefts occur", expected: "Part 1 of 3: Creates a Browse Collection" },
  // { query: "These views but split by year", expected: "Part 2 or 3: Create multifaceted browse" },
  // { query: "These views but assault and battery", expected: "Part 3 or 3: Subset pivot" },

  // //.   PARTIAL
  // { query: "Can I see when UIC thefts occur", expected: "Part 1 of 3: Creates a Browse Collection" },
  // { query: "These views but split by year", expected: "Part 2 or 3: Create multifaceted browse" },
  // { query: "These views but Loop", expected: "Part 3 or 3: Subset pivot" },


  // //Multifaceted compare
  //   //SUBSET PIVOT
  // { query: "Can I see UIC and River North thefts by day ", expected: "Part 1 of 3: Creates a Comparison Collection" },
  // { query: "These views but split by year", expected: "Part 2 or 3: Create multifaceted browse" },
  // { query: "These views but burglaries", expected: "Part 3 or 3: Subset pivot" },

  // { query: "Can I see UIC and River North thefts by day ", expected: "Part 1 of 3: Creates a Comparison Collection" },
  // { query: "These views but split by year", expected: "Part 2 or 3: Create multifaceted browse" },
  // { query: "These views but burglaries and assaults", expected: "Part 3 or 3: Subset pivot" },


  // //Multi b+C SUBSET PIVOT
  // { query: "Show me information when thefts occur near UIC and River North", expected: "Part 1 of 3: Creates a Browse and Compare Collection" },
  // { query: "These views but split by year", expected: "Part 2 of 3: Split to make multifaceted b+c" },
  // { query: "These views but burglaries", expected: "Part 3 of 3: filter" },


  // { query: "Show me thefts by day and year", expected: "Part 1 of 3: Creates a Complex Collection" },
  // { query: "These views but January and March only", expected: "Part 2 of 3: filter to make a complex x compare grid" },
  // { query: "These views but burglaries", expected: "Part 3 of 3: partial subset pivot" },


];

var listOfPartialPivotTheSubset = [

  //  targeted
  { query: "What about UIC thefts by day of the week?", expected: "Referential queries on one view, Part 1 of 2: Create one view (targeted)" },
  { query: "This but for the Loop", expected: "Part 2, Subset pivot partial (swap Loop for UIC, keep theft)" },

  //   browse
  { query: "Lets look at UIC theft", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: "Now can I see this but for battery", expected: "Part 2: Subset collection, single pivot partial"  },

  { query: "January crimes in the Loop", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: "This but May and October", expected: "Part 2: Subset collection, multi pivot partial" },

  { query: "Give me some plots for UIC theft", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: "Now can I get this but with some views with loop and river north combined together?", expected: "Part 2: Subset collection, partial pivot with combined filter" },

  //   complex 

  //   compare   FAIL
  { query: "Show me UIC and Loop thefts by day", expected:"Referential queries on a comparison set, Part 1 of 2: Create comparison set"  },
  { query: "These plots but for burglary", expected: "Part 2, Extend comparison collection" },

  { query: "March and April 2012 thefts by time of the day", expected: "Referential queries on a comparison set, Part 1 of 2: Create comparison set"  },
  { query: "This but for 2013", expected: "Part 2, Extend comparison collection" },


  //  b+c 
  { query: "Show me information about thefts near UIC and the Loop", expected: "Part 1 of 2: Creates a Browse and Compare Collection" },
  { query: "These views but for burglary", expected: "Part 2: Copy and pivot the collection" },

  { query: "Show me information about thefts on Monday near UIC and the Loop", expected: "Part 1 of 2: Creates a Browse and Compare Collection" },
  { query: "These views but for burglary", expected: "Part 2: Copy and pivot the collection" },

  { query: "Show me information about thefts on Monday near UIC and the Loop", expected: "Part 1 of 2: Creates a Browse and Compare Collection" },
  { query: "These views but for burglary and Tuesday", expected: "Part 2: Copy and pivot the collection" },

  { query: "Show me information about thefts near UIC and the Loop", expected: "Part 1 of 2: Creates a Browse and Compare Collection" },
  { query: "These views but for burglary and assault together", expected: "Part 2: Copy and pivot the collection" },

  { query: "Show me information about thefts near UIC and the Loop", expected: "Part 1 of 2: Creates a Browse and Compare Collection" },
  { query: "These views but for burglary and assault", expected: "Part 2: Copy and pivot the collection" },


  { query: "Show me information about Monday thefts in January and the July", expected: "Part 1 of 2: Creates a Browse and Compare Collection" },
  { query: "These views but for burglary", expected: "Part 2: Copy and pivot the collection" },


];

var listOfFilterActions = [


  //Targeted
 { query: "Show me crimes in the Loop by year", expected: "Referential queries on one view, Part 1 of 2: Create one view (targeted)" },
  { query: "Now can I see this but for theft", expected: "Part 2, Add a filter criteria (theft)" },

  //ADD
  { query: "Can I look that thefts that occur on the street", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: "This but just Monday", expected: "Part 2: Subset add filter single" },

  { query: "Can I see charts about UIC theft", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: " This but only for Monday and January", expected: "Part 2: Subset add filter single" },

  { query: "Could I see thefts", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: "This but now just for Monday and Tuesday together", expected: "Part 2: Subset collection, pivot with combined additional filter criteria" },

  { query: "Show me 2012 batteries", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: "This but charts for UIC and Loop", expected: "Part 2: Subset add filter multi" },

  { query: "Can I see statistics on UIC theft", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: " These views but for Monday and Tuesday and January", expected: "Part 1: Subset add filter multi"  },

  { query: "Can I See statistics on UIC theft", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: "This but Monday and Tuesday and January and February", expected: "Part 2: Subset add filter multi" },

  //comparison
  { query: "I want to compare UIC and Loop by day", expected: "Referential queries on a comparison set, Part 1 of 2: Create comparison set"  },
  { query: "Show me these views but now for just thefts", expected: "Part 2, filter single" },

  { query: "Show me Monday and Tuesday and Wednesday by time of the day", expected: "Referential queries on a comparison set, Part 1 of 2: Create comparison set"  },
  { query: "These but with theft and burglary", expected: "Part 2, filter single" },

  { query: "Can I see crimes from June, July and August visualized by day", expected: "Referential queries on a comparison set, Part 1 of 2: Create comparison set"  },
  { query: "These views but theft and street", expected: "Part 2, filter single" },

  { query: "Show me crimes on streets and sidewalks by day", expected: "Referential queries on a comparison set, Part 1 of 2: Create comparison set"  },
  { query: "Can I see this but theft and burlary together in the UIC area", expected: "Part 2, filter single" },

  //browse and compare
  { query: "Show me information about UIC and the Loop", expected: "Part 1 of 2: Creates a Browse and Compare Collection" },
  { query: "These views but for theft", expected: "Part 2: Add a filter criteria" },

  { query: "Show me information about January and March", expected: "Part 1 of 2: Creates a Browse and Compare Collection" },
  { query: "These views but for theft and streets", expected: "Part 2: Add a filter criteria" },

  { query: "Show me information about January and March", expected: "Part 1 of 2: Creates a Browse and Compare Collection" },
  { query: "These views but for theft and burglary together and streets", expected: "Part 2: Add a filter criteria" },

  { query: "Show me information about January and March crimes near UIC", expected: "Part 1 of 2: Creates a Browse and Compare Collection" },
  { query: "These views but for theft and burglary together and streets", expected: "Part 2: Add a filter criteria" },


  //complex
  //---- single (forms a hierarchy)

  { query: "I want to look at day of the week and neighborhood", expected: "Part 1 of 2: Creates a Complex Collection" },
  { query: "These views but for just theft", expected: "Part 2: Add a filter criteria" },

  { query: "Can I see the relationship between crime type, neighborhood and month?", expected: "Part 1 of 2: Creates a Complex Collection" },
  { query: "Can we take these views and only show the data for 2012?", expected: "Part 1 of 2: Add a filter criteria" },

  { query: "What about March crimes by year and crime type", expected: "Part 1 of 2: Creates a Complex Collection" },
  { query: "These views but for crimes that took place in restaurants", expected: "Part 2: Add a filter criteria" },

  { query: "What about March crimes by year and crime type", expected: "Part 1 of 2: Creates a Complex Collection" },
  { query: "These views but for crimes that took place in 2012", expected: "Part 2: Add a filter criteria" },

];

listOfSplitTheCollection = [

 //target
  { query: "I would just like to see UIC thefts by day", expected: "Referential queries on one view, Part 1 of 2: Create one view (targeted)" },
  { query: "Can I see this view but split by year", expected: "Part 2: Split" },

  { query: "Can I have a look at Saturday crimes by crime-type", expected: "Referential queries on one view, Part 1 of 2: Create one view (targeted)" },
  { query: "Take this view but split it by month", expected: "Part 2: Split" },

  { query: "Can I see the thefts by location type", expected:"Referential queries on one view, Part 1 of 2: Create one view (targeted)" },
  { query: "Now take this but split it by day", expected:  "Part 2: Split" },

  //   BROWSE
  //SPLIT
  { query: "Show me data about July crimes", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: "These views but split by year", expected: "Part 2: Subset collection, split" },

  { query: "Can I look at assaults on Saturday", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: "This but split by month", expected: "Part 2: Subset collection, split" },

  //MULTI-SPLIT--- fail
  { query: "Show me plots about the Loop", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: "Now can I see this but split by year and day", expected: "Part 2: Subset collection, split" },

  { query: "Is there a way to see data about crimes the occur on streets", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: "Ok, show me these but split them up by year and day", expected: "Part 2: Subset collection, split" },

 //Comparison
 //single
  { query: "UIC and Loop by crime-type", expected: "Referential queries on a comparison set, Part 1 of 2: Create comparison set"  },
  { query: "This but split by year", expected: "Part 2: Split, single" },

  { query: "Can I see theft and battery by day", expected: "Referential queries on a comparison set, Part 1 of 2: Create comparison set"  },
  { query: "This but split by year", expected: "Part 2: Split, single" },

  { query: "January and March and October by day", expected: "Referential queries on a comparison set, Part 1 of 2: Create comparison set"  },
  { query: "This but split by crime type", expected: "Part 2: Split, single"},

  //SPLIT MULTI
  { query: "UIC and Loop and River-North by crime-type", expected: "Referential queries on a comparison set, Part 1 of 2: Create comparison set"  },
  { query: "This but split by year and day", expected: "Part 2: Split, multi"},

  { query: "Street and sidewalk thefts on Monday by time of the day", expected: "Referential queries on a comparison set, Part 1 of 2: Create comparison set"  },
  { query: "This but split by year and month", expected: "Part 2: Split, multi" },

  { query: "2012 and 2014 crimes by day", expected: "Referential queries on a comparison set, Part 1 of 2: Create comparison set"  },
  { query: "This but split by crime type and month", expected: "Part 2: Split, multi" },

  //Browse and compare
  { query: "Show me information about UIC and the Loop", expected: "Part 1 of 2: Creates a Browse and Compare Collection" },
  { query: "These views but split by year", expected: "Part 2: Split" },

    { query: "Show me information about January and March", expected: "Part 1 of 2: Creates a Browse and Compare Collection" },
  { query: "These views but split by neighborhood", expected: "Part 2: Split" }
];


var listOfAttributePivots = [

  //targeted
 { query: "Show me January crimes by day", expected: "Referential queries on one view, Part 1 of 2: Create one view (targeted)" },
  { query: "This but by year", expected: "Part 2, pivot from one attribute to another" },

  { query: "Can I see Near West side crimes by day of the week?", expected:"Referential queries on one view, Part 1 of 2: Create on view (targeted)" },
  { query: "This but year and month", expected: "Part 2, pivot the attribute multiple times (one to many)" },

  //COMPARISON ATTRIBUTE PIVOT SINGLE 
  { query: "I want to compare UIC and Loop by day of the week", expected: "Referential queries on a comparison set, Part 1 of 2: Create comparison set" },
  { query: "This but year", expected: "Part 2: copy views and pivot attribute, single" },

  { query: "Show me theft and burglary by month", expected: "Referential queries on a comparison set, Part 1 of 2: Create comparison set"  },
  { query: "This but location type", expected: "Part 2: copy views and pivot attribute, single" },

   { query: "Can I see thefts in 2012 and 2013 by location type", expected: "Referential queries on a comparison set, Part 1 of 2: Create comparison set"  },
  { query: "This but by month", expected: "Part 2: copy views and pivot attribute, single" },

  //COMPARISON ATTRIBUTE PIVOT MULTI
  { query: "UIC and Loop restaurant thefts by time of the day", expected: "Referential queries on a comparison set, Part 1 of 2: Create comparison set"  },
  { query: "This but day of the week and month", expected: "Part 2: copy views and pivot attribute, multi" },

  { query: "Crimes near UIC and on Monday and Tuesday by year", expected: "Referential queries on a comparison set, Part 1 of 2: Create comparison set"  },
  { query: "This but crime type and month", expected: "Part 2: copy views and pivot attribute, multi" },

   { query: "Show me UIC and Loop by location type", expected: "Referential queries on a comparison set, Part 1 of 2: Create comparison set" },
  { query: "This but year and crime-type", expected: "Part 2: copy views and pivot attribute, multi" },

];

var listExtendPivots = [


  //B+C
  //Extend the collection- subset
  { query: "Show me information about thefts near UIC and the Loop", expected: "Part 1 of 2: Creates a Browse and Compare Collection" },
  { query: "These views but for River North", expected: "Part 2: Extend the collection" },

  { query: "I want to look at thefts on Monday in 2012 in January and March and June", expected: "Part 1 of 2: Creates a Browse and Compare Collection" },
  { query: "These views but for October", expected: "Part 2: Extend the collection" },


  //Extend one dimension- attribute
  { query: "I want to compare when crimes occur near UIC and the Loop", expected: "Part 1 of 2: Creates a Browse and Compare Collection" },
  { query: "These views but for crime type as well", expected: "Part 2: Extend to a new attribute" },

  { query: "I want to compare when crimes occur near UIC and the Loop and Near West side", expected: "Part 1 of 2: Creates a Browse and Compare Collection" },
  { query: "These views but for crime type as well", expected: "Part 2: Extend to a new attribute" },

  { query: "I want to compare when crimes occur near UIC and the Loop and Near West side", expected: "Part 1 of 2: Creates a Browse and Compare Collection" },
  { query: "These views but for crime type and location type as well", expected: "Part 2: Extend to a new attribute" },

  { query: "I want to compare when crimes occur near UIC and the Loop and Near West side and River East", expected: "Part 1 of 2: Creates a Browse and Compare Collection" },
  { query: "These views but for crime type and location type as well", expected: "Part 2: Extend to a new attribute" },

  { query: "I want to compare where crimes occur on Monday, Tuesday, Wednesday and Thursday", expected: "Part 1 of 2: Creates a Browse and Compare Collection" },
  { query: "These views but for month and year and time of the day as well", expected: "Part 2: Extend to a new attribute" },

  //BROWSE
  { query: "Can I see when crimes occur near UIC", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: "Can I see these views but with crime type added", expected: "Part 2, Subset collection, extend" },

  { query: "I want to know when crimes occur near UIC", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: "Take these but add crime type and location type", expected: "Part 2: Subset collection, multi extend" },

  ////////////////

  //COMPARE EXTEND- complete 
  { query: "Show me crimes near UIC and the Loop by month", expected: "Referential queries on a comparison set, Part 1 of 2: Create comparison set"  },
  { query: "This but for River North", expected: "Part 2, Extend comparison collection" },

  { query: "Can I see UIC and Loop thefts by year", expected: "Referential queries on a comparison set, Part 1 of 2: Create comparison set"  },
  // { query: "This but for River North and only thefts", expected: "Part 2, Extend comparison collection" },
  { query: "This but for River North", expected: "Part 2, Extend comparison collection" },


  ///**** THIS IS NOT COMPARISON BUT cxc grid 
  // { query: "Can I see UIC and Loop theft and burglary by day", expected: "Referential queries on a comparison set, Part 1 of 2: Create comparison set"  },
  // { query: "These views but with River North assault", expected: "Part 2, Extend comparison collection" },

    //COMPARE EXTEND- partial 
  { query: "Show me UIC and Loop thefts by day", expected:"Referential queries on a comparison set, Part 1 of 2: Create comparison set"  },
  { query: "These plots but for River North", expected: "Part 2, Extend comparison collection" },

  { query: "Show me UIC and Loop theft on streets by month", expected: "Referential queries on a comparison set, Part 1 of 2: Create comparison set"  },
  { query: "This but River North", expected: "Part 2, Extend comparison collection" },

  //------- NOT EXTEND- but a pivot
  { query: "March and April 2012 thefts by time of the day", expected: "Referential queries on a comparison set, Part 1 of 2: Create comparison set"  },
  { query: "This but now with May", expected: "Part 2, Extend comparison collection" },

  { query: "Can I see UIC and Loop theft on streets by day", expected: "Referential queries on a comparison set, Part 1 of 2: Create comparison set"  },
  { query: "This but River North", expected: "Part 2, Extend comparison collection" },


  //B+C
  //Extend the collection- subset
  { query: "Show me information about thefts near UIC and the Loop", expected: "Part 1 of 2: Creates a Browse and Compare Collection" },
  { query: "These views but for River North", expected: "Part 2: Extend the collection" },

  { query: "I want to look at thefts on Monday in 2012 in January and March and June", expected: "Part 1 of 2: Creates a Browse and Compare Collection" },
  { query: "These views but for October", expected: "Part 2: Extend the collection" },


  //Extend one dimension- attribute
  { query: "I want to compare when crimes occur near UIC and the Loop", expected: "Part 1 of 2: Creates a Browse and Compare Collection" },
  { query: "These views but for crime type as well", expected: "Part 2: Extend to a new attribute" },

  { query: "I want to compare when crimes occur near UIC and the Loop and Near West side", expected: "Part 1 of 2: Creates a Browse and Compare Collection" },
  { query: "These views but for crime type as well", expected: "Part 2: Extend to a new attribute" },

  { query: "I want to compare when crimes occur near UIC and the Loop and Near West side", expected: "Part 1 of 2: Creates a Browse and Compare Collection" },
  { query: "These views but for crime type and location type as well", expected: "Part 2: Extend to a new attribute" },

  { query: "I want to compare when crimes occur near UIC and the Loop and Near West side and River East", expected: "Part 1 of 2: Creates a Browse and Compare Collection" },
  { query: "These views but for crime type and location type as well", expected: "Part 2: Extend to a new attribute" },

  { query: "I want to compare where crimes occur on Monday, Tuesday, Wednesday and Thursday", expected: "Part 1 of 2: Creates a Browse and Compare Collection" },
  { query: "These views but for month and year and time of the day as well", expected: "Part 2: Extend to a new attribute" },
];

var listOfCxCPivots = [



  //Extend
  { query: "Can I see UIC and Loop and River North theft and burglary by day", expected: "Referential queries on a comparison by comparison grid, Part 1 of 2: Create cxc grid"  },
  { query: "These views but Near West", expected: "Part 2, Extend" },

    { query: "Can I see UIC and Loop theft and burglary and assault by day", expected: "Referential queries on a comparison by comparison grid, Part 1 of 2: Create cxc grid"  },
  { query: "These views but Near West", expected: "Part 2, Extend" },

    { query: "Can I see UIC and Loop theft and burglary by day", expected: "Referential queries on a comparison by comparison grid, Part 1 of 2: Create cxc grid"  },
  { query: "These views but assault", expected: "Part 2, Extend" },

  { query: "Can I see UIC and Loop theft and burglary and assault by day", expected: "Referential queries on a comparison by comparison grid, Part 1 of 2: Create cxc grid"  },
  { query: "These views but battery", expected: "Part 2, Extend" },

  { query: "Can I see UIC and Loop and River North theft and burglary and assault by day", expected: "Referential queries on a comparison by comparison grid, Part 1 of 2: Create cxc grid"  },
  { query: "These views but battery", expected: "Part 2, Extend" },

    { query: "Can I see UIC and Loop and River North theft and burglary and assault by day", expected: "Referential queries on a comparison by comparison grid, Part 1 of 2: Create cxc grid"  },
  { query: "These views but Near West", expected: "Part 2, Extend" },



  //PARTIAL SUBSET PIVOT
  { query: "Can I see UIC and Loop theft and burglary in 2012 by day", expected: "Referential queries on a comparison by comparison grid, Part 1 of 2: Create cxc grid"  },
  { query: "These views but 2013", expected: "Part 2, Attribute pivot" },

  { query: "Can I see UIC and Loop theft and burglary on streets by day", expected: "Referential queries on a comparison by comparison grid, Part 1 of 2: Create cxc grid"  },
  { query: "These views but with restaurants", expected: "Part 2, Attribute pivot" },

  //ATTRIBUTE PIVOT
  { query: "Can I see UIC and Loop theft and burglary by day", expected: "Referential queries on a comparison by comparison grid, Part 1 of 2: Create cxc grid"  },
  { query: "These views but with month", expected: "Part 2, Attribute pivot" },

  { query: "Can I see UIC and Loop theft and burglary by day", expected: "Referential queries on a comparison by comparison grid, Part 1 of 2: Create cxc grid"  },
  { query: "These views but with location type", expected: "Part 2, Attribute pivot" },


  //FILTER
  { query: "Can I see UIC and Loop theft and burglary by day", expected: "Referential queries on a comparison by comparison grid, Part 1 of 2: Create cxc grid"  },
  { query: "These views but just 2012", expected: "Part 2, Attribute pivot" },

  { query: "Can I see UIC and Loop theft and burglary by day", expected: "Referential queries on a comparison by comparison grid, Part 1 of 2: Create cxc grid"  },
  { query: "These views but just streets", expected: "Part 2, Attribute pivot" },

  //SPLIT
  { query: "Can I see UIC and Loop theft and burglary by day", expected: "Referential queries on a comparison by comparison grid, Part 1 of 2: Create cxc grid"  },
  { query: "These views but split by year", expected: "Part 2, Attribute pivot" },

  { query: "Can I see UIC and Loop theft and burglary by day", expected: "Referential queries on a comparison by comparison grid, Part 1 of 2: Create cxc grid"  },
  { query: "These views but split by month", expected: "Part 2, Attribute pivot" },



  //CREATE
  { query: "Can I see UIC and Loop and River North theft and burglary by day", expected: "Referential queries on a comparison by comparison grid, Part 1 of 2: Create cxc grid"  },

  { query: "Can I see UIC and Loop and River North theft and burglary in 2012 by day", expected: "Referential queries on a comparison by comparison grid, Part 1 of 2: Create cxc grid"  },

  { query: "Can I see UIC and Loop and River North theft and burglary in 2012 and 2013 by day", expected:"Referential queries on a comparison by comparison grid, Part 1 of 2: Create cxc grid"   },

  { query: "Can I see UIC and Loop theft in 2012 by day", expected: "Referential queries on a comparison by comparison grid, Part 1 of 2: Create cxc grid"   },

    { query: "Can I see UIC and Loop by day", expected: "Referential queries on a comparison by comparison grid, Part 1 of 2: Create cxc grid"  },


  //THIS IS NOT COMPARISON BUT cxc grid 
  // { query: "Can I see UIC and Loop theft and burglary by day", expected: "Referential queries on a comparison set, Part 1 of 2: Create comparison set"  },
  // { query: "These views but with River North assault", expected: "Part 2, Extend comparison collection" }
];

var listOfMultifacetedBrowse = [
  //EXTEND
  { query: "Can I see when thefts occur", expected: "Part 1 of 3: Creates a Browse Collection" },
  { query: "These views but split by year", expected: "Part 2 or 3: Create multifaceted browse" },
  { query: "These views but with neighborhood", expected: "Part 3 or 3: Subset pivot" },

  //SUBSET PIVOT
  { query: "Can I see when thefts occur", expected: "Part 1 of 3: Creates a Browse Collection" },
  { query: "These views but split by year", expected: "Part 2 or 3: Create multifaceted browse" },
  { query: "These views but assault", expected: "Part 3 or 3: Subset pivot" },

  { query: "Can I see when thefts occur", expected: "Part 1 of 3: Creates a Browse Collection" },
  { query: "These views but split by year", expected: "Part 2 or 3: Create multifaceted browse" },
  { query: "These views but assault and battery", expected: "Part 3 or 3: Subset pivot" },

  //PARTIAL
  { query: "Can I see when UIC thefts occur", expected: "Part 1 of 3: Creates a Browse Collection" },
  { query: "These views but split by year", expected: "Part 2 or 3: Create multifaceted browse" },
  { query: "These views but Loop", expected: "Part 3 or 3: Subset pivot" },


    //FILTER
  { query: "Can I see when thefts occur", expected: "Part 1 of 3: Creates a Browse Collection" },
  { query: "These views but split by year", expected: "Part 2 or 3: Create multifaceted browse" },
  { query: "These views but UIC only", expected: "Part 3 or 3: Filter" },

  { query: "Can I see when thefts occur", expected: "Part 1 of 3: Creates a Browse Collection" },
  { query: "These views but split by year", expected: "Part 2 or 3: Create multifaceted browse" },
  { query: "These views but UIC and Loop", expected: "Part 3 or 3: Filter" },

];

var listOfMultifacetedCompare = [

  //ATTRIBUTE PIVOT
  // { query: "Can I see UIC and River North thefts by day ", expected: "Part 1 of 3: Creates a Comparison Collection" },
  // { query: "These views but split by year", expected: "Part 2 or 3: Create multifaceted browse" },
  // { query: "These views but month", expected: "Part 3 or 3: Subset pivot" },

  // { query: "Can I see UIC and River North thefts by day ", expected: "Part 1 of 3: Creates a Comparison Collection" },
  // { query: "These views but split by year", expected: "Part 2 or 3: Create multifaceted browse" },
  // { query: "These views but burglaries and assaults", expected: "Part 3 or 3: Subset pivot" },

  //PARTIAL EXTEND
  { query: "Can I see UIC and River North thefts by day ", expected: "Part 1 of 3: Creates a Comparison Collection" },  
  { query: "These views but split by year", expected: "Part 2 or 3: Create multifaceted browse" },
  { query: "These views but Loop", expected: "Part 3 or 3: Subset pivot" },

  { query: "Can I see UIC and River North thefts by day ", expected: "Part 1 of 3: Creates a Comparison Collection" },  
  { query: "These views but split by year", expected: "Part 2 or 3: Create multifaceted browse" },
  { query: "These views but Loop and Near West Side", expected: "Part 3 or 3: Subset pivot" },

    //EXTEND
  { query: "Can I see UIC and River North by day ", expected: "Part 1 of 3: Creates a Comparison Collection" },
  { query: "These views but split by year", expected: "Part 2 or 3: Create multifaceted browse" },
  { query: "These views but Loop", expected: "Part 3 or 3: Subset pivot" },

  { query: "Can I see UIC and River North by day ", expected: "Part 1 of 3: Creates a Comparison Collection" },  
  { query: "These views but split by year", expected: "Part 2 or 3: Create multifaceted browse" },
  { query: "These views but Loop and Near West Side", expected: "Part 3 or 3: Subset pivot" },

    //FILTER
  { query: "Can I see theft and burglary by day ", expected: "Part 1 of 3: Creates a Comparison Collection" },  
  { query: "These views but split by year", expected: "Part 2 or 3: Create multifaceted browse" },
  { query: "These views but UIC only", expected: "Part 3 or 3: Filter" },

  { query: "Can I see theft and burglary by day ", expected: "Part 1 of 3: Creates a Comparison Collection" },
  { query: "These views but split by year", expected: "Part 2 or 3: Create multifaceted browse" },
  { query: "These views but UIC and Loop", expected: "Part 3 or 3: Filter" },


  //SUBSET PIVOT
  { query: "Can I see UIC and River North thefts by day ", expected: "Part 1 of 3: Creates a Comparison Collection" },
  { query: "These views but split by year", expected: "Part 2 or 3: Create multifaceted browse" },
  { query: "These views but burglaries", expected: "Part 3 or 3: Subset pivot" },

  { query: "Can I see UIC and River North thefts by day ", expected: "Part 1 of 3: Creates a Comparison Collection" },
  { query: "These views but split by year", expected: "Part 2 or 3: Create multifaceted browse" },
  { query: "These views but burglaries and assaults", expected: "Part 3 or 3: Subset pivot" },


];


var listOfMultifacetedBAndC  = [

  //EXTEND-SUBSET

  { query: "Show me information when thefts occur near UIC and River North", expected: "Part 1 of 3: Creates a Browse and Compare Collection" },
  { query: "These views but split by year", expected: "Part 2 of 3: Split to make multifaceted b+c" },
  { query: "These views but also for Loop", expected: "Part 3 of 3: Extend subset" },

  //EXTEND-ATTR
  { query: "Show me information when thefts occur near UIC and River North", expected: "Part 1 of 3: Creates a Browse and Compare Collection" },
  { query: "These views but split by year", expected: "Part 2 of 3: Split to make multifaceted b+c" },
  { query: "These views but also for location type", expected: "Part 3 of 3: extend attr" },

  //SUBSET PIVOT
  { query: "Show me information when thefts occur near UIC and River North", expected: "Part 1 of 3: Creates a Browse and Compare Collection" },
  { query: "These views but split by year", expected: "Part 2 of 3: Split to make multifaceted b+c" },
  { query: "These views but burglaries", expected: "Part 3 of 3: filter" },

  //FILTER PIVOT
  { query: "Show me information when thefts occur near UIC and River North", expected: "Part 1 of 3: Creates a Browse and Compare Collection" },
  { query: "These views but split by year", expected: "Part 2 of 3: Split to make multifaceted b+c" },
  { query: "These views but only ones on streets", expected: "Part 3 of 3: filter" },



];

var listOfComplexAndCompareGrid = [

  //random:
  // { query: "Can I see UIC and River North thefts by day ", expected: "Part 1 of 3: Creates a Comparison Collection" },  
  // { query: "These views but split by year", expected: "Part 2 or 3: Create multifaceted compare" },
  // { query: "These views but Loop and Near West Side", expected: "Part 3 or 3: Subset pivot" },
  // //EXTEND-SUBSET
  // { query: "Show me information when thefts occur near UIC and River North", expected: "Part 1 of 3: Creates a Browse and Compare Collection" },
  // { query: "These views but split by year", expected: "Part 2 of 3: Split to make multifaceted b+c" },
  // { query: "These views but also for Loop", expected: "Part 3 of 3: Extend subset" },

  // //EXTEND-ATTR
  // { query: "Show me information when thefts occur near UIC and River North", expected: "Part 1 of 3: Creates a Browse and Compare Collection" },
  // { query: "These views but split by year", expected: "Part 2 of 3: Split to make multifaceted b+c" },
  // { query: "These views but also for location type", expected: "Part 3 of 3: extend attr" },

  //SUBSET PIVOT
  //"What about March crimes by year and crime type"
  { query: "Show me thefts by day and year", expected: "Part 1 of 3: Creates a Complex Collection" },
  { query: "These views but January and March and July only", expected: "Part 2 of 3: filter to make a complex x compare grid" },
  { query: "These views but burglaries", expected: "Part 3 of 3: partial subset pivot" },


  { query: "Show me thefts by day and year and neighborhood", expected: "Part 1 of 3: Creates a Complex Collection" },
  { query: "These views but January and March and July only", expected: "Part 2 of 3: filter to make a complex x compare grid" },
  //{ query: "These views but burglaries", expected: "Part 3 of 3: partial subset pivot" },

  //FILTER PIVOT
  { query: "Show me thefts day and year", expected: "Part 1 of 3: Creates a Complex Collection" },
  { query: "These views but January and March only", expected: "Part 2 of 3: Subset pivot to make a complex by compare grid" },
  { query: "These views but just for 2012", expected: "Part 3 of 3: partial subset pivot" },


  //EXTEND 
  { query: "Show me thefts by day and year", expected: "Part 1 of 3: Creates a Complex Collection" },
  { query: "These views but for January and March", expected: "Part 2 of 3: Subset pivot to make a complex by compare grid" },
  { query: "These views but for July", expected: "Part 3 of 3: partial subset pivot" },

  { query: "Show me thefts by day and year", expected: "Part 1 of 3: Creates a Complex Collection" },
  { query: "These views but for assault and burglary", expected: "Part 2 of 3: Subset pivot to make a complex by compare grid" },
  { query: "These views but homicide", expected: "Part 3 of 3: Extend" },

];


var systematicTest = {

  ///////////////////////////  TARGET
  //--Subset pivot

  //--------Single - produces one targeted view

  //--------Multi - produces a comparison set

  //--Attribute pivot

  //--------Single - produces one targeted view

  //--------Multi - produces a comparison set

  //--Split

  //--------Single - produces a multifaceted single view

  //--------Multi- produces a subset+attribute set

  //--Filter 

  //--------Single - produces a targeted view

  //--------Multi- produces a comparison set

  


}



var listOfSubsetPlusAttrPivots = [

  // ----- Target, multi-split, and then ...

  // Attribute pivot, single  
  { query: "Can I see UIC thefts by day", expected: "Part 1 of 3: Target" },
  { query: "This but split by year and month", expected: "Part 2 of 3: Create sub+attr" },
  { query: "This but for time of the day", expected: "Part 3 of 3: Attribute pivotmulti" },

  // Attribute pivot, multi 
  { query: "Can I see UIC thefts by day", expected: "Part 1 of 3: Target" },
  { query: "This but split by year and month", expected: "Part 2 of 3: Create sub+attr" },
  { query: "This but for time of the day and location type", expected: "Part 3 of 3: Attribute pivot, multi" },

  //Filter - single
  { query: "Can I see UIC crimes by day", expected: "Part 1 of 3: Target" },
  { query: "This but split by year and month", expected: "Part 2 of 3: Create sub+attr" },
  { query: "This but for only for thefts", expected: "Part 3 of 3: Filter, single" },

  //Filter- multi
  { query: "Can I see UIC crimes by day", expected: "Part 1 of 3: Target" },
  { query: "This but split by year and month", expected: "Part 2 of 3: Create sub+attr" },
  { query: "This but for only for thefts and burglaries", expected: "Part 3 of 3: Filter, single" },






  // Complete subset pivot, single
  { query: "Can I see UIC crimes by day", expected: "Part 1 of 3: Target" },
  { query: "This but split by year and month", expected: "Part 2 of 3: Create sub+attr" },
  { query: "This but for River North", expected: "Part 3 of 3: Subset pivot, complete, single" },

  // Partial subset pivot, single
  { query: "Can I see UIC thefts by day", expected: "Part 1 of 3: Target" },
  { query: "This but split by year and month", expected: "Part 2 of 3: Create sub+attr" },
  { query: "This but for River North", expected: "Part 3 of 3: Subset pivot, partial, single" },

   // Complete subset pivot, multi
  { query: "Can I see UIC crimes by day", expected: "Part 1 of 3: Target" },
  { query: "This but split by year and month", expected: "Part 2 of 3: Create sub+attr" },
  { query: "This but for River North and Near West Side", expected: "Part 3 of 3: Subset pivot, complete, multi" },

  // Partial subset pivot, multi
  { query: "Can I see UIC thefts by day", expected: "Part 1 of 3: Target" },
  { query: "This but split by year and month", expected: "Part 2 of 3: Create sub+attr" },
  { query: "This but for River North and Near West Side", expected: "Part 3 of 3: Subset pivot, partial, multi" },

  // Attribute pivot, single  
  { query: "Can I see UIC thefts by day", expected: "Part 1 of 3: Target" },
  { query: "This but split by year and month", expected: "Part 2 of 3: Create sub+attr" },
  { query: "This but for time of the day", expected: "Part 3 of 3: Attribute pivotmulti" },

  // Attribute pivot, multi 
  { query: "Can I see UIC thefts by day", expected: "Part 1 of 3: Target" },
  { query: "This but split by year and month", expected: "Part 2 of 3: Create sub+attr" },
  { query: "This but for time of the day and location type", expected: "Part 3 of 3: Attribute pivot, multi" },

  //Filter - single
  { query: "Can I see UIC crimes by day", expected: "Part 1 of 3: Target" },
  { query: "This but split by year and month", expected: "Part 2 of 3: Create sub+attr" },
  { query: "This but for only for thefts", expected: "Part 3 of 3: Filter, single" },

  //Filter- multi
  { query: "Can I see UIC crimes by day", expected: "Part 1 of 3: Target" },
  { query: "This but split by year and month", expected: "Part 2 of 3: Create sub+attr" },
  { query: "This but for only for thefts and burglaries", expected: "Part 3 of 3: Filter, single" },


];




// NOT POSSIBLE!
// var listOfMultifacetedComplexAndCompareGrid = [

//   //SUBSET PIVOT
//   //"What about March crimes by year and crime type"
//   { query: "Show me thefts by day and year", expected: "Part 1 of 4: Creates a Complex and Compare Collection" },
//   { query: "These views but January and March only", expected: "Part 2 of 4: Multi filter to make a grid" },
//   { query: "These views but split by neighborhood", expected: "Part 3 of 4: Split to make it listOfMultifacetedBrowse" },  
//   { query: "These views but burglary", expected: "Part 4 of 4: Subset pivot" },


//   //Filter
//   { query: "Show me thefts by day and year", expected: "Part 1 of 4: Creates a Complex and Compare Collection" },
//   { query: "These views but January and March only", expected: "Part 2 of 4: Multi filter to make a grid" },
//   { query: "These views but split by neighborhood", expected: "Part 3 of 4: Split to make it listOfMultifacetedBrowse" },  
//   { query: "These views but streets", expected: "Part 4 of 4: Subset pivot" },



// ];


var listOfMultifacetedCAndCGrid = [
  //Subset pivot
  { query: "Can I see UIC and Loop theft and assault on streets by day", expected: "Part 1 of 3: Create cxc grid"  },
  { query: "These views but split by year", expected: "Part 2 of 3: split pivot" },

  { query: "These views but sidewalks", expected: "Part 3 of 3: subset pivot" },


  { query: "These views but River North", expected: "Part 3 of 3: extend" },
    { query: "These views but restaurants", expected: "Part 3 of 3: subset pivot" },

  { query: "These views but January", expected: "Part 3 of 3: filter pivot" },   ///FAIL




  //filter pivot
  { query: "Can I see UIC and Loop theft and assault on streets by day", expected: "Part 1 of 3: Create cxc grid"  },
  { query: "These views but split by year", expected: "Part 2 of 3: split pivot" },
  { query: "These views but January", expected: "Part 3 of 3: filter pivot" },

    //extenf
  { query: "Can I see UIC and Loop theft and assault on streets by day", expected: "Part 1 of 3: Create cxc grid"  },
  { query: "These views but split by year", expected: "Part 2 of 3: split pivot" },
  { query: "These views but River North", expected: "Part 3 of 3: filter pivot" },


];

var listOfSPAandMultiCompare = [

  //Extend
  { query: "Can I see UIC theft by day", expected: "Part 1 of 3: Target" },
  { query: "This but split by year and month", expected: "Part 2 of 3: Create sub+attr" },
  { query: "This but for River North and Near West Side", expected: "Part 3 of 3: Subset pivot, complete, multi" },
  { query: "This but for Loop", expected: "Part 3 of 3: Subset pivot, complete, multi" },

  //Subset pivot
  { query: "Can I see UIC theft by day", expected: "Part 1 of 3: Target" },
  { query: "This but split by year and month", expected: "Part 2 of 3: Create sub+attr" },
  { query: "This but for River North and Near West Side", expected: "Part 3 of 3: Subset pivot, complete, multi" },
  { query: "This but for assault", expected: "Part 3 of 3: Subset pivot, complete, multi" },

  //Filter
  { query: "Can I see UIC by day", expected: "Part 1 of 3: Target" },
  { query: "This but split by year and month", expected: "Part 2 of 3: Create sub+attr" },
  { query: "This but for River North and Near West Side", expected: "Part 3 of 3: Subset pivot, complete, multi" },
  { query: "This but for theft", expected: "Part 3 of 3: Subset pivot, complete, multi" },

  //Extend
  { query: "Can I see UIC theft by day", expected: "Part 1 of 3: Target" },
  { query: "This but split by year and month", expected: "Part 2 of 3: Create sub+attr" },
  { query: "This but for River North and Near West Side", expected: "Part 3 of 3: Subset pivot, complete, multi" },
  { query: "This but for Loop", expected: "Part 3 of 3: Subset pivot, complete, multi" },

];


var listOfSPAandMultiBrowse = [

  //Extend
  { query: "Can I see UIC by day", expected: "Part 1 of 3: Target" },
  { query: "This but split by year and month", expected: "Part 2 of 3: Create sub+attr" },
  { query: "This but for time of the day and location type", expected: "Part 3 of 3: Subset pivot, complete, multi" },
  { query: "This but for crime type", expected: "Part 3 of 3: Subset pivot, complete, multi" },

  //Subset pivot
  { query: "Can I see UIC thefts by day", expected: "Part 1 of 3: Target" },
  { query: "This but split by year and month", expected: "Part 2 of 3: Create sub+attr" },
  { query: "This but for time of the day and location type", expected: "Part 3 of 3: Attribute pivot, multi" },
  { query: "This but for assault", expected: "Part 3 of 3: Subset pivot, complete, multi" },

  //filter
  { query: "Can I see UIC thefts by day", expected: "Part 1 of 3: Target" },
  { query: "This but split by year and month", expected: "Part 2 of 3: Create sub+attr" },
  { query: "This but for time of the day and location type", expected: "Part 3 of 3: Attribute pivot, multi" },
  { query: "This but for Tuesday", expected: "Part 3 of 3: Subset pivot, complete, multi" },

];
  
//test
var listOfAttrPivotsRevised = [

  //SPAandMultiBrowse EXTEND- add new multifaceted browse collection  DONE
  { query: "Can I see UIC by day", expected: "Part 1 of 4: Target" },
  { query: "This but split by year and month", expected: "Part 2 of 4: Create sub+attr" },
  { query: "This but for time of the day and location type", expected: "Part 3 of 4: Attribute pivot, multi, make grid" },
  { query: "This but split by crime type also", expected: "Part 3 of 4: Extend split" },

  //SPAandMultiBrowse EXTEND- add new sub+attr collection
  { query: "Can I see UIC by day", expected: "Part 1 of 4: Target" },
  { query: "This but split by year and month", expected: "Part 2 of 4: Create sub+attr" },
  { query: "This but for time of the day and location type", expected: "Part 3 of 4: Attribute pivot, multi, make grid" },
  { query: "This but for crime type also", expected: "Part 3 of 4: Extend attr" },


  //SPAandMultiCompare attribute pivot-- duplicate entire collection with new attribute  DONE
  { query: "Can I see UIC theft by day", expected: "Part 1 of 3: Target" },
  { query: "This but split by year and month", expected: "Part 2 of 3: Create sub+attr" },
  { query: "This but for River North and Near West Side", expected: "Part 3 of 3: Subset pivot, complete, multi" },
  { query: "This but for time of the day", expected: "Part 3 of 4: Extend split" },

  //SPAandMultiCompare split pivot- add in a row with the same subsets, same attr, new secondary attr   DONE
  { query: "Can I see UIC theft by day", expected: "Part 1 of 3: Target" },
  { query: "This but split by year and month", expected: "Part 2 of 3: Create sub+attr" },
  { query: "This but for River North and Near West Side", expected: "Part 3 of 3: Subset pivot, complete, multi" },
  { query: "This but split by time of the day", expected: "Part 3 of 4: Extend split" },

  //cxc grid - attr pivot- DONE
  { query: "Can I see UIC and Loop theft and assault on streets by day", expected: "Part 1 of 3: Create cxc grid"  },
  { query: "These views but split by year", expected: "Part 2 of 3: split pivot" },
  { query: "These views but by month", expected: "Part 2 of 3: split pivot" },

  //DONE
  { query: "Can I see UIC and Loop theft and assault on streets by day", expected: "Part 1 of 3: Create cxc grid"  },
  { query: "These views but split by year", expected: "Part 2 of 3: split pivot" },
  { query: "These views but by month and time of the day", expected: "Part 2 of 3: split pivot" },

  //cxc grid - split pivot  DONE
  { query: "Can I see UIC and Loop theft and assault on streets by day", expected: "Part 1 of 3: Create cxc grid"  },
  { query: "These views but split by year", expected: "Part 2 of 3: split pivot" },
  { query: "These views but split by month", expected: "Part 2 of 3: split pivot" },

  //spa extend via split - DONE 
  { query: "Can I see UIC crimes by day", expected: "Part 1 of 3: Target" },
  { query: "This but split by year and month", expected: "Part 2 of 3: Create sub+attr" },
  { query: "This but split by time of the day and location type", expected: "Part 2 of 3: add new view with new split" },

  { query: "Can I see UIC crimes by location type", expected: "Part 1 of 3: Target" },
  { query: "This but split by year and month", expected: "Part 2 of 3: Create sub+attr" },
  { query: "This but split by time of the day and day of the week", expected: "Part 2 of 3: add new view with new split" },


  //multifaceted browse

  ///multifaceted compare

];


//Note- I rearranged the order and I edited the language
var subject15 = [
  { query: "Could I get the information for the UIC crime statistics?", expected: "q1" },
  { query: "Could I get the same graphs but for River North?", expected: "q2" },
  { query: "Could I see these but now for Near West Side and the Loop?", expected: "q2" },
  { query: "Is there a graph for the number of crime for all four neighborhoods", expected: "q3"},

  //layout queries ....
]; 

var subject6 = [
  { query: "Could I get crimes by neighborhood", expected: "q1" },
  { query: "Can I see crimes by crime type?", expected: "q2" },
  { query: "Could I see this but for River North?", expected: "q3" },
  { query: "Can I see this but for Loop, UIC and Near West?", expected: "q4"},
  { query: "Can I see by year the crime rates?", expected: "q5"},
  { query: "Can I see this but split by neighborhood?", expected: "q5"},
  { query: "Can I see this but for month of the year?", expected: "q6"},
  { query: "Can I see this but for day of the week?", expected: "q7"},
  { query: "Can I see this but for time of the day?", expected: "q8"},
  { query: "Can I see these views but just for 2010, 2011, 2012, 2013 and 2014?", expected: "q9"},
  { query: "Can I see crimes by location type near UIC and River North?", expected: "q10"},
  { query: "Can I see these views but for 2010, 2011, 2012, 2013 and 2014", expected: "q11"},
]; 

var subject19 = [
  { query: "Could I see how many crimes occurred in each neighborhood", expected: "q1" },
  { query: "Could I look at the Near West side by crime type?", expected: "q1" },
  { query: "Of the theft on the Near West side, show it by month", expected: "q1" },
  { query: "Can I see this but for Loop, UIC and River North", expected: "q1" },
  { query: "Can I see these charts but for battery?", expected: "q1" },
  { query: "I want to look at the location types for theft and battery", expected: "q1" },
  { query: "Can I see these but by time of the day", expected: "q1" },

];


// var myExample = [
//   { query: "Can I see all crimes broken down by crime type?", expected: "q1" },
//   { query: "Show me some information about thefts", expected: "q2" },
//   { query: "Can I see these charts but for assault", expected: "q3" },
//   { query: "Can I see these charts but just for UIC", expected: "q4"},
//   { query: "How about these graphs but looking at the Loop", expected: "q5"},
//   { query: "Can I compare Loop and UIC in 2013 by ", expected: "q5"},
//   { query: "Can I see this but for month of the year?", expected: "q6"},
//   { query: "Can I see this but for day of the week?", expected: "q7"},
//   { query: "Can I see this but for time of the day?", expected: "q8"},
//   { query: "Can I see these views but just for 2010, 2011, 2012, 2013 and 2014?", expected: "q9"},
//   { query: "Can I see crimes by location type near UIC and River North?", expected: "q10"},
//   { query: "Can I see these views but for 2010, 2011, 2012, 2013 and 2014", expected: "q11"},

// ];

///TESTING

var testDelete =  [

  //Compare DONE
  //Compare: UIC Tod, Loop Tod, Near West Tod, Delete Loop
  { query: "Show me June vs July vs March vs October theft by neighborhood ", expected: "Part 1 of 2: Compare, Creates a Subset Collection" },
  { query: "These views but delete March and October", expected: "Part 2: Delete" },

  //Browse DONE
  //Browse: UIC Tod, UIC month, UIC year, UIC dotw; Delete year and month
  { query: "Show me data about July crimes", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: "These views but delete crime type and year", expected: "Part 2: Delete" },



  //B+C :  UIC vs Loop vs Near West by when crimes occur;  Delete Near West 
  { query: "Show me information about UIC and Loop and Near West", expected: "Part 1 of 2: B+C, Creates a Subset Collection" },
  { query: "These views but delete Near West and Loop", expected: "Part 2: Delete" }, 


  //Complex x Compare: UIC and Loop tod, dow, moy.  Delete tod.  Same as Complex
  { query: "Show me thefts by day and year and month", expected: "Part 1 of 3: Creates a Complex Collection" },
  { query: "These views but UIC and River North and Loop only", expected: "Part 2 of 3: filter to make a complex x compare grid" },
  { query: "These views but delete month", expected: "Part 3 of 3: delete" },

  //Browse DONE
  //Browse: UIC Tod, UIC month, UIC year, UIC dotw; Delete year and month
  { query: "Show me data about July crimes", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: "These views but delete crime type and neighborhood", expected: "Part 2: Delete" },

  //Multifaceted browse DONE
  { query: "Can I see when thefts occur", expected: "Part 1 of 3: Creates a Browse Collection" },
  { query: "These views but split by year", expected: "Part 2 or 3: Create multifaceted browse" },
  { query: "These views but delete month", expected: "Part 3 or 3: Delete" },

  //Multifaceted Compare  DONE
  { query: "Can I see UIC and River North and Loop thefts by day ", expected: "Part 1 of 3: Creates a Comparison Collection" },  
  { query: "These views but split by year", expected: "Part 2 or 3: Create multifaceted browse" },
  { query: "These views but delete Loop", expected: "Part 3 or 3: Delete" },

  //Complex: UIC by month, day and year.  Delete day   DONE
  { query: "Show me UIC crimes by month day and year ", expected: "Part 1 of 2: Complex, Creates a Subset Collection" },
  { query: "These views but delete day", expected: "Part 2: Delete" }, 

  //S+A:  UIC Tod year, UIC Tod month, UIC Tod Dow; Delete Dow   DONE  
  { query: "Can I see UIC crimes by day", expected: "Part 1 of 3: Target" },
  { query: "This but split by year and month and time of the day", expected: "Part 2 of 3: Create sub+attr" },
  { query: "This but delete time of the day", expected: "Part 3 of 3: Delete" },

  //B+C :  UIC vs Loop vs Near West by when crimes occur;  Delete Near West   
  { query: "Show me information about UIC and Loop and Near West", expected: "Part 1 of 2: B+C, Creates a Subset Collection" },
  { query: "These views but delete Near West and Loop", expected: "Part 2: Delete" }, 

  //B+C:   UIC vs Loop vs Near West by when crimes occur;  Delete year 
  { query: "Show me information about UIC and Loop and Near West", expected: "Part 1 of 2: B+C, Creates a Subset Collection" },
  { query: "These views but delete crime type and location type", expected: "Part 2: Delete" }, 

  //Delete CxC:  UIC theft, Loop theft, UIC burglary, Loop burglary by tod.  Delete UIC.
  { query: "Can I see UIC and Loop and River North theft and burglary by day", expected: "Part 1 of 2: CxC Grid"  },
  { query: "These views but delete River North", expected: "Part 2, Delete" },


  //Complex x Compare: UIC and Loop tod, dow, moy.  Delete tod.  Same as Complex
  { query: "Show me thefts by day and year and month", expected: "Part 1 of 3: Creates a Complex Collection" },
  { query: "These views but UIC and River North and Loop only", expected: "Part 2 of 3: filter to make a complex x compare grid" },
  { query: "These views but delete month", expected: "Part 3 of 3: delete" },

  //Complex x Compare: UIC and Loop and Near West Tod, dow, may.  Delete Near West. 
  { query: "Show me thefts by day and year", expected: "Part 1 of 3: Creates a Complex Collection" },
  { query: "These views but January and March and June only", expected: "Part 2 of 3: filter to make a complex x compare grid" },
  { query: "These views but delete June", expected: "Part 3 of 3: delete" },

];



var test2Delete =  [

  //Delete CxC:  UIC theft, Loop theft, UIC burglary, Loop burglary by tod.  Delete UIC.
  { query: "Can I see UIC and Loop and River North theft and burglary by day", expected: "Part 1 of 2: CxC Grid"  },
  { query: "These views but delete River North and UIC", expected: "Part 2, Delete" },

  //Complex x Compare: UIC and Loop tod, dow, moy.  Delete tod.  Same as Complex  DONE
  { query: "Show me thefts by day and year and month", expected: "Part 1 of 3: Creates a Complex Collection" },
  { query: "These views but UIC and River North and Loop only", expected: "Part 2 of 3: filter to make a complex x compare grid" },
  { query: "These views but delete month and year and River North and Loop", expected: "Part 3 of 3: delete" },


  //DONE
  { query: "Show me thefts by day and year and month", expected: "Part 1 of 3: Creates a Complex Collection" },
  { query: "These views but UIC and River North and Loop only", expected: "Part 2 of 3: filter to make a complex x compare grid" },
  { query: "These views but delete month and year", expected: "Part 3 of 3: delete" },


    //Compare DONE
  //Compare: UIC Tod, Loop Tod, Near West Tod, Delete Loop
  { query: "Show me June vs July vs March vs October theft by neighborhood ", expected: "Part 1 of 2: Compare, Creates a Subset Collection" },
  { query: "These views but delete March and October and June", expected: "Part 2: Delete" },

  //Browse DONE
  //Browse: UIC Tod, UIC month, UIC year, UIC dotw; Delete year and month
  { query: "Show me when July crimes occur", expected: "Part 1 of 2: Browse, Creates a Subset Collection" },
  { query: "These views but delete year and time of the day", expected: "Part 2: Delete" },



  //B+C :  UIC vs Loop vs Near West by when crimes occur;  Delete Near West 
  { query: "Show me information about UIC and Loop and Near West", expected: "Part 1 of 2: B+C, Creates a Subset Collection" },
  { query: "These views but delete Near West and Loop", expected: "Part 2: Delete" }, 




  //Multifaceted browse DONE
  { query: "Can I see when thefts occur", expected: "Part 1 of 3: Creates a Browse Collection" },
  { query: "These views but split by year", expected: "Part 2 or 3: Create multifaceted browse" },
  { query: "These views but delete month and day", expected: "Part 3 or 3: Delete" },



  //Multifaceted Compare  DONE  
  { query: "Can I see UIC and River North and Loop thefts by day ", expected: "Part 1 of 3: Creates a Comparison Collection" },  
  { query: "These views but split by year", expected: "Part 2 or 3: Create multifaceted browse" },
  { query: "These views but delete Loop and UIC", expected: "Part 3 or 3: Delete" },

  //Complex: UIC by month, day and year.  Delete day   DONE
  { query: "Show me UIC crimes by month day and year ", expected: "Part 1 of 2: Complex, Creates a Subset Collection" },
  { query: "These views but delete day and month", expected: "Part 2: Delete" }, 

  //S+A:  UIC Tod year, UIC Tod month, UIC Tod Dow; Delete Dow   DONE  
  { query: "Can I see UIC crimes by day", expected: "Part 1 of 3: Target" },
  { query: "This but split by year and month and time of the day", expected: "Part 2 of 3: Create sub+attr" },
  { query: "This but delete time of the day and month", expected: "Part 3 of 3: Delete" },

  // //B+C :  UIC vs Loop vs Near West by when crimes occur;  Delete Near West   
  // { query: "Show me information about UIC and Loop and Near West", expected: "Part 1 of 2: B+C, Creates a Subset Collection" },
  // { query: "These views but delete Near West and Loop", expected: "Part 2: Delete" }, 

  // //B+C:   UIC vs Loop vs Near West by when crimes occur;  Delete year 
  // { query: "Show me information about UIC and Loop and Near West", expected: "Part 1 of 2: B+C, Creates a Subset Collection" },
  // { query: "These views but delete crime type and location type", expected: "Part 2: Delete" }, 




  //Complex x Compare: UIC and Loop tod, dow, moy.  Delete tod.  Same as Complex
  { query: "Show me thefts by day and year and month", expected: "Part 1 of 3: Creates a Complex Collection" },
  { query: "These views but UIC and River North and Loop only", expected: "Part 2 of 3: filter to make a complex x compare grid" },
  { query: "These views but delete month and day", expected: "Part 3 of 3: delete" },

  //Complex x Compare: UIC and Loop and Near West Tod, dow, may.  Delete Near West. 
  { query: "Show me thefts by day and year", expected: "Part 1 of 3: Creates a Complex Collection" },
  { query: "These views but January and March and June only", expected: "Part 2 of 3: filter to make a complex x compare grid" },
  { query: "These views but delete June and March", expected: "Part 3 of 3: delete" },


];


var listOfAttributePivots = [

  //targeted
 { query: "Show me January crimes by day", expected: "Referential queries on one view, Part 1 of 2: Create one view (targeted)" },
  { query: "This but by year", expected: "Part 2, pivot from one attribute to another" },

  { query: "Can I see Near West side crimes by day of the week?", expected:"Referential queries on one view, Part 1 of 2: Create on view (targeted)" },
  { query: "This but year and month", expected: "Part 2, pivot the attribute multiple times (one to many)" },

  //COMPARISON ATTRIBUTE PIVOT SINGLE 
  { query: "I want to compare UIC and Loop by day of the week", expected: "Referential queries on a comparison set, Part 1 of 2: Create comparison set" },
  { query: "This but year", expected: "Part 2: copy views and pivot attribute, single" },

  { query: "Show me theft and burglary by month", expected: "Referential queries on a comparison set, Part 1 of 2: Create comparison set"  },
  { query: "This but location type", expected: "Part 2: copy views and pivot attribute, single" },

   { query: "Can I see thefts in 2012 and 2013 by location type", expected: "Referential queries on a comparison set, Part 1 of 2: Create comparison set"  },
  { query: "This but by month", expected: "Part 2: copy views and pivot attribute, single" },

  //COMPARISON ATTRIBUTE PIVOT MULTI
  { query: "UIC and Loop restaurant thefts by time of the day", expected: "Referential queries on a comparison set, Part 1 of 2: Create comparison set"  },
  { query: "This but day of the week and month", expected: "Part 2: copy views and pivot attribute, multi" },

  { query: "Crimes near UIC and on Monday and Tuesday by year", expected: "Referential queries on a comparison set, Part 1 of 2: Create comparison set"  },
  { query: "This but crime type and month", expected: "Part 2: copy views and pivot attribute, multi" },

   { query: "Show me UIC and Loop by location type", expected: "Referential queries on a comparison set, Part 1 of 2: Create comparison set" },
  { query: "This but year and crime-type", expected: "Part 2: copy views and pivot attribute, multi" },

];

var selectTests = [

  //MULTIFACETED SINGLE VIEW then select 
  // { query: "I would just like to see UIC thefts by day", expected: "Independent, Targeted" },
  // { query: "This but split by year", expected: "Independent, Targeted" },
  // { query: "This but select 2014 and show it by month", expected: "Independent, Targeted" },



  //TARGET then select
  //----- single selection, single pivot: produces target and overview
  { query: "I would just like to see UIC thefts by day", expected: "Independent, Targeted" },
  { query: "This but select Monday and show by month", expected: "Select" }, //creates a new target, with an overview
  { query: "This but Tuesday", expected: "Test" }, //creates a new target, with an overview
  { query: "This but location type", expected: "Test" }, //creates a new target, with an overview

  //----- single selection, multiple pivots : produces browse set with overview
  { query: "I would just like to see UIC thefts by day", expected: "Independent, Targeted" },
  { query: "This but select Monday and show by month and year", expected: "Multi select" },   // creates a browse set

  //----- multiples selection, single pivots : produces comparison set with overview 
  { query: "I would just like to see UIC thefts by day", expected: "Independent, Targeted" },
  { query: "This but select Monday and Tuesday and show by month", expected: "Select" }, //creates a comparison set with overview

  //----- multiples selection, multiple pivots : produces b+c with overview
  { query: "I would just like to see UIC thefts by day", expected: "Independent, Targeted" },
  { query: "This but select Monday and Tuesday and show by month and year", expected: "Multi select" },   // creates a b+c with overview

  
  //COMPARE then select 
  //----- single selection, single pivot: produces comparison set with overview set
  { query: "I want to compare UIC and Loop thefts by the day of the week that the crime occurred", expected: "Independent, Create Many, Compare" },
  { query: "This but select Monday and show by month", expected: "Select" }, //creates a comparison set

  //----- single selections, multiple pivot: 
  { query: "I want to compare UIC and Loop thefts by the day of the week that the crime occurred", expected: "Independent, Create Many, Compare" },
  { query: "This but select Monday and show by month and year", expected: "Select" }, //creates b+c with overview collection

  //----- multiple selection, single pivot: produces comparison set with overview set
  { query: "I want to compare UIC and Loop thefts by the day of the week that the crime occurred", expected: "Independent, Create Many, Compare" },
  { query: "This but select Monday and Tuesday and show by month", expected: "Select" }, //creates a cxc set

  //----- multiple selections, multiple pivot: 
  { query: "I want to compare UIC and Loop thefts by the day of the week that the crime occurred", expected: "Independent, Create Many, Compare" },
  { query: "This but select Monday and Tuesday and show by month and year", expected: "Select" }, //creates multiple b+c or multiple cxc (sister b+c becomes cxc?)







  //MULTIFACETED COMPARE then select 


  //CxC then select


  //?? multifaceted compare, multifaceted cxc, sub+attr 

  


];

var listOfSubsetPlusAttrPivots2 = [

  // ----- Target, multi-split, and then ...

  // Attribute pivot, single  
  // { query: "Can I see UIC thefts by day", expected: "Part 1 of 3: Target" },
  // { query: "This but split by year and month", expected: "Part 2 of 3: Create sub+attr" },
  // { query: "This but for time of the day", expected: "Part 3 of 3: Attribute pivotmulti" },

  // Attribute pivot, multi 
  { query: "Can I see UIC thefts by day", expected: "Part 1 of 3: Target" },
  { query: "Can I see UIC thefts by day", expected: "Part 1 of 3: Target" },
  { query: "This but for time of the day and location type", expected: "Part 3 of 3: Attribute pivot, multi" },

  //Filter - single
  { query: "Can I see UIC crimes by day", expected: "Part 1 of 3: Target" },
  { query: "This but split by year and month", expected: "Part 2 of 3: Create sub+attr" },
  { query: "This but for only for thefts", expected: "Part 3 of 3: Filter, single" },

  //Filter- multi
  { query: "Can I see UIC crimes by day", expected: "Part 1 of 3: Target" },
  { query: "This but split by year and month", expected: "Part 2 of 3: Create sub+attr" },
  { query: "This but for only for thefts and burglaries", expected: "Part 3 of 3: Filter, single" },
];

var testGrouped = [
  // { query: "Can I see UIC thefts by location type", expected: "Part 1 of 3: Target" },
  // { query: "This but split by year", expected: "Part 1 of 3: Target" },

    { query: "Can I see UIC crimes by day", expected: "Part 1 of 3: Target" },
  { query: "This but for location type and time of the day", expected: "Part 3 of 3: Attribute pivot, multi" },
    { query: "This but split by crime type", expected: "Part 3 of 3: Attribute pivot, multi" },
        // { query: "This but assault and battery", expected: "Part 3 of 3: Attribute pivot, multi" },



];

var listOfCxCPivots2 = [



  //Extend
  { query: "Can I see UIC and Loop and River North theft and burglary by day", expected: "Referential queries on a comparison by comparison grid, Part 1 of 2: Create cxc grid"  },
  { query: "These views but Near West", expected: "Part 2, Extend" },

    { query: "Can I see UIC and Loop theft and burglary and assault by day", expected: "Referential queries on a comparison by comparison grid, Part 1 of 2: Create cxc grid"  },
  { query: "These views but Near West", expected: "Part 2, Extend" },

    { query: "Can I see UIC and Loop theft and burglary by day", expected: "Referential queries on a comparison by comparison grid, Part 1 of 2: Create cxc grid"  },
  { query: "These views but assault", expected: "Part 2, Extend" },

  { query: "Can I see UIC and Loop theft and burglary and assault by day", expected: "Referential queries on a comparison by comparison grid, Part 1 of 2: Create cxc grid"  },
  { query: "These views but battery", expected: "Part 2, Extend" },

  { query: "Can I see UIC and Loop and River North theft and burglary and assault by day", expected: "Referential queries on a comparison by comparison grid, Part 1 of 2: Create cxc grid"  },
  { query: "These views but battery", expected: "Part 2, Extend" },

    { query: "Can I see UIC and Loop and River North theft and burglary and assault by day", expected: "Referential queries on a comparison by comparison grid, Part 1 of 2: Create cxc grid"  },
  { query: "These views but Near West", expected: "Part 2, Extend" },



  //PARTIAL SUBSET PIVOT
  { query: "Can I see UIC and Loop theft and burglary in 2012 by day", expected: "Referential queries on a comparison by comparison grid, Part 1 of 2: Create cxc grid"  },
  { query: "These views but 2013", expected: "Part 2, Attribute pivot" },

  { query: "Can I see UIC and Loop theft and burglary on streets by day", expected: "Referential queries on a comparison by comparison grid, Part 1 of 2: Create cxc grid"  },
  { query: "These views but with restaurants", expected: "Part 2, Attribute pivot" },

  //ATTRIBUTE PIVOT
  { query: "Can I see UIC and Loop theft and burglary by day", expected: "Referential queries on a comparison by comparison grid, Part 1 of 2: Create cxc grid"  },
  { query: "These views but with month", expected: "Part 2, Attribute pivot" },

  { query: "Can I see UIC and Loop theft and burglary by day", expected: "Referential queries on a comparison by comparison grid, Part 1 of 2: Create cxc grid"  },
  { query: "These views but with location type", expected: "Part 2, Attribute pivot" },


  //FILTER
  { query: "Can I see UIC and Loop theft and burglary by day", expected: "Referential queries on a comparison by comparison grid, Part 1 of 2: Create cxc grid"  },
  { query: "These views but just 2012", expected: "Part 2, Attribute pivot" },

  { query: "Can I see UIC and Loop theft and burglary by day", expected: "Referential queries on a comparison by comparison grid, Part 1 of 2: Create cxc grid"  },
  { query: "These views but just streets", expected: "Part 2, Attribute pivot" },

  //SPLIT
  { query: "Can I see UIC and Loop theft and burglary by day", expected: "Referential queries on a comparison by comparison grid, Part 1 of 2: Create cxc grid"  },
  { query: "These views but split by year", expected: "Part 2, Attribute pivot" },

  { query: "Can I see UIC and Loop theft and burglary by day", expected: "Referential queries on a comparison by comparison grid, Part 1 of 2: Create cxc grid"  },
  { query: "These views but split by month", expected: "Part 2, Attribute pivot" },



  //CREATE
  { query: "Can I see UIC and Loop and River North theft and burglary by day", expected: "Referential queries on a comparison by comparison grid, Part 1 of 2: Create cxc grid"  },

  { query: "Can I see UIC and Loop and River North theft and burglary in 2012 by day", expected: "Referential queries on a comparison by comparison grid, Part 1 of 2: Create cxc grid"  },

  { query: "Can I see UIC and Loop and River North theft and burglary in 2012 and 2013 by day", expected:"Referential queries on a comparison by comparison grid, Part 1 of 2: Create cxc grid"   },

  { query: "Can I see UIC and Loop theft in 2012 by day", expected: "Referential queries on a comparison by comparison grid, Part 1 of 2: Create cxc grid"   },

    { query: "Can I see UIC and Loop by day", expected: "Referential queries on a comparison by comparison grid, Part 1 of 2: Create cxc grid"  },


  //THIS IS NOT COMPARISON BUT cxc grid 
  // { query: "Can I see UIC and Loop theft and burglary by day", expected: "Referential queries on a comparison set, Part 1 of 2: Create comparison set"  },
  // { query: "These views but with River North assault", expected: "Part 2, Extend comparison collection" }
];

var listOfComplexAndCompareGrid2 = [

  //random:
  // { query: "Can I see UIC and River North thefts by day ", expected: "Part 1 of 3: Creates a Comparison Collection" },  
  // { query: "These views but split by year", expected: "Part 2 or 3: Create multifaceted compare" },
  // { query: "These views but Loop and Near West Side", expected: "Part 3 or 3: Subset pivot" },
  // //EXTEND-SUBSET
  // { query: "Show me information when thefts occur near UIC and River North", expected: "Part 1 of 3: Creates a Browse and Compare Collection" },
  // { query: "These views but split by year", expected: "Part 2 of 3: Split to make multifaceted b+c" },
  // { query: "These views but also for Loop", expected: "Part 3 of 3: Extend subset" },

  // //EXTEND-ATTR
  // { query: "Show me information when thefts occur near UIC and River North", expected: "Part 1 of 3: Creates a Browse and Compare Collection" },
  // { query: "These views but split by year", expected: "Part 2 of 3: Split to make multifaceted b+c" },
  // { query: "These views but also for location type", expected: "Part 3 of 3: extend attr" },

  //SUBSET PIVOT
  //"What about March crimes by year and crime type"
  { query: "Show me thefts by day and year", expected: "Part 1 of 3: Creates a Complex Collection" },
  { query: "These views but January and March and July only", expected: "Part 2 of 3: filter to make a complex x compare grid" },
  { query: "These views but burglaries", expected: "Part 3 of 3: partial subset pivot" },


  { query: "Show me thefts by day and year and neighborhood", expected: "Part 1 of 3: Creates a Complex Collection" },
  { query: "These views but January and March and July only", expected: "Part 2 of 3: filter to make a complex x compare grid" },
  //{ query: "These views but burglaries", expected: "Part 3 of 3: partial subset pivot" },
];






var listOfTests = [ 
 {"name": "listOfTestQueriesIndependent", "array":  listOfTestQueriesIndependent},
 {"name": "listOfTestTargetedPivots", "array":  listOfTestTargetedPivots},
  {"name": "listOfTestSubsetPivots", "array":  listOfTestSubsetPivots},
 {"name": "listOfTestComparisonPivots", "array":  listOfTestComparisonPivots},
  {"name": "listOfTestComplexPivots", "array":  listOfTestComplexPivots},
 {"name": "listOfBrowseAndComparePivots", "array":  listOfBrowseAndComparePivots},
  {"name": "listOfCxCPivots", "array":  listOfCxCPivots},
 {"name": "listOfMultifacetedBrowse", "array":  listOfMultifacetedBrowse},
  {"name": "listOfMultifacetedCompare", "array":  listOfMultifacetedCompare},
 {"name": "listOfMultifacetedBAndC", "array":  listOfMultifacetedBAndC},
  {"name": "listOfMultifacetedCAndCGrid", "array":  listOfMultifacetedCAndCGrid},
 {"name": "listOfPivotTheSubset", "array":  listOfPivotTheSubset},
  {"name": "listOfPartialPivotTheSubset", "array":  listOfPartialPivotTheSubset},
 // {"name": "listOfAttributePivots", "array":  listOfAttributePivots},
  {"name": "listOfSplitTheCollection", "array":  listOfSplitTheCollection},
 // {"name": "listOfExtendPivots", "array":  listOfExtendPivots},
  ];
