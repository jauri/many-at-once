
const port = 9101;//9101;//3000;
var connection = null; //connection to the display

var switchData = "covid19";//"chicago_crime";// "covid19";//"chicago_crime";//"covid19";//"chicago_crime";//"covid19";//"chicago_crime";//"covid19";//"chicago_crime";//"covid19";//"chicago_crime"; //"covid19";//"chicago_crime";//"covid19";//"chicago_crime";//"chicago_crime";//"covid19";//"chicago_crime";//"covid19";//"chicago_crime";//"covid19";//"covid19";//"chicago_crime"; //"chicago_crime";//"covid19"; //"chicago_crime";//"covid19";//"chicago_crime";//"covid19";//"chicago_crime"; //"covid19";//"chicago_crime";//"covid19";//"chicago_crime";//"covid19";//"chicago_crime";//

var switchPlatform = "desktop";//"sage2"; //"desktop";//"desktop";//"sage2"; //"desktop"; 

var dataDescFile; 
var attributeFile;
var dataFiles; 
var whatDataContains;

//if you need to reload from a prior saved state
var reloadFromPrior = false;
var reloadFile = "log/participant6.json";//"log/Abeer-pilotMain.json";//"log/1617989698503.json";//"log/p0.json";  //"log/testTheUndo.json";//"log/p0.json"; //"log/1617989698503.json";//"log/Abeer-pilotMain.json";//"log/Abari-maybe.json";//"log/1616992386517.json";//"log/test4.json";//"log/simpleRefTest.json";//"log/1616894418091.json";//"log/Abeer-pilotMain.json";//"log/bigStarterTest.json"; 
	//"log/nlpTestMonovariate.json"; 
var reloadList = [];

var theInfo = {
	"visCount": 0,
	"queryCount": 0,
	"visQuestionCount": 0,
	"indepCount": 0,
	"refCount": 0,
	"nonResponseCount": 0,

	"targetCount": 0,
	"multifacetedCount": 0,
	"sub_attrCount": 0,
	"browseCount": 0,
	"comparisonCount": 0,
	"b_cCount": 0,
	"multifacetedB_C_Count": 0,
	"multifacetedCompareCount": 0,
	"multifacetedBrowseCount": 0,
	"multifacetedCompareCompareCount": 0,
	"multiBrowse_SubAttrCount": 0,
	"multifacetedCompare_SubAttrCount": 0,
	"complexCount": 0,
	"compare_compare_gridCount": 0,
	"pivotTheSubsetCount": 0,
	"filterTheCollectionCount": 0,
	"splitTheCollectionCount": 0,
	"pivotTheDataAttributeCount": 0,
	"extendTheCollectionCount": 0,
	"splitPivotCount": 0,
	"splitPivotExtendCount": 0,

	"undoCount": 0,
	"deleteCount": 0,
	"moveToSideCount": 0,

	"totalViewsReferenced": 0,
	"maxViewsReferenced": 0,
	"maxVisViewsReferenced": 0,
	"totalVisViewsReferenced": 0,
	"undoViewCount": 0
}; 

queryRefPairs = {

	"list": [] 
}

var dataToPrint = []; 

if(switchData == "chicago_crime"){
	dataFiles = [ 'data/chicago_crime/crimes.txt' ];
	whatDataContains = ["crimes"];
	dataDescFile = 'data/chicago_crime/dataDescription.txt';
	attributeFile = 'data/chicago_crime/attributeDescription.txt';
}
if(switchData == "covid19"){
	dataFiles = ['data/covid19_2/counties.csv', 'data/covid19_2/cases.csv']; //for now....
	whatDataContains = ["counties", "cases"];
	dataDescFile = 'data/covid19_2/dataDescription.txt';
	attributeFile = 'data/covid19_2/attributeDescription.txt';
}
//////////////////////////////////////////////////////////////////////////////////
//NODE MODULES 
//---basics
const http = require('http');
const https = require('https');

const csv = require('csv-parser');
const fs = require('fs');
var WebSocketServer = require('websocket').server;

var loadash = require('lodash');


var prevReturnObj = null;

// // Loadash....
// var loadash = require('lodash');
//--- Load the core build.
// var _ = require('lodash/core');
//---- Load the FP build for immutable auto-curried iteratee-first data-last methods.
// var fp = require('lodash/fp');

//---nlp 
const { NerManager } = require('node-nlp');
const manager = new NerManager({ threshold: 0.9 });
var pluralize = require('pluralize')

//---custom modules
var interpreter = require('./interpreter.js' );
var vcCreator = require('./view_collections_creator.js' );
  
//////////////////////////////////////////////////////////////////////////////////
//DATA 
var datasets = {};
var theDataset = []; // the rows and columns of the crime data
var dataDescription = []; // the features of the data used for nlp
var attributeAndValues = [];  // the data ontology- what are the fields and values

//Variables for storing the views and collections
var collectionCount = 0;  //number of collections created

var referencedViews = [];

var recentQuery = "loop"; 

var inputDataStorage = { "data": [], "user-id": Date.now() };

const createCsvWriter = require('csv-writer').createObjectCsvWriter; 
const csvWriter = createCsvWriter({

  path: ""+ inputDataStorage["user-id"] + '-csvOutput.csv',
  header: [
    {id: 'number', title: 'Number'},
    {id: 'query', title: 'Query'},
    {id: 'dir_ref_undo_err', title: 'Direct,Ref,Undo,Error'},
    {id: 'new_mod_ext_na', title: 'New,Mod,Extend,NA'},
    {id: 'subtype', title: 'Subtype'},
    // {id: 'resultType', title: 'Result Type'},
    // {id: 'resultList', title: 'Result List'},
    { id: "refType", title: "Reference col type"},
    {id: 'referencedViews', title: 'Referenced Views'},
    {id: 'producedList', title: 'Produced Views'},
    {id: 'producedType', title: 'Produced Type'}
  ]

}); 

//Example
// { number: 1, query: "can i see...", category: "referential", "new_or_mod": "mod", "subcategory": "subsetPivot"}

var outputCSVData = [ ]; 

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

countReads = 0;
numberOfReads = dataFiles.length;
dataFile = dataFiles[0];
datasets[dataFile] = [];

console.log(dataFiles);

readData( dataFiles[0] );


//FIRST read in the dataset
function readData(file){

	fs.createReadStream(file)  
	  .pipe(csv())
	  .on('data', (row) => {
	    // console.log(row);
	    //theDataset.push(row);
	    datasets[file].push(row)
	  })
	  .on('end', () => {

	  	console.log("-------------------");
	    console.log('Data file ' + file + ' successfully processed');
	    countReads++;

	    if( countReads < dataFiles.length ){
	    	console.log("begin reading " + dataFiles[countReads] );
	    	datasets[ dataFiles[countReads] ] = [];
	    	//fs.createReadStream(dataFiles[countReads]);
	    	readData(dataFiles[countReads]);
	    }


	    else{ 
	    	readDataDescription(); //TURN BACK ON

	    }
	});
}



//THEN Read in the data description 
function readDataDescription(){

	fs.createReadStream(dataDescFile)  
	  .pipe(csv({ separator: '\t' }))
	  .on('data', (row) => {
	    // console.log(row);
	    dataDescription.push(row);
	  })
	  .on('end', () => {
	  	console.log("-------------------");
	    console.log('Data description file successfully processed');

	    readAttributeAndValues();
	});

};

//FINALLY Read in the attributes 
function readAttributeAndValues(){
	fs.createReadStream(attributeFile)  
	  .pipe(csv({ separator: '\t' }))
	  .on('data', (row) => {
	    // console.log(row);
	    attributeAndValues.push(row);
	  })
	  .on('end', () => {
	  	console.log("-------------------");
	    console.log('Attribute file successfully processed');

	    //testDataRetrieval(); 
	  	console.log("-------------------");


	  	if(!reloadFromPrior)
	    	runEverything();
	    else
	    	reloadPriorState();
	});

};

function reloadPriorState(){
	fs.readFile(reloadFile, (err, data) => {
    	if (err) throw err;
    	let file = JSON.parse(data);
    	console.log(file);


    	dataArr = file["data"];
    	
    	//HERE
    	for(var i =0; i < dataArr.length; i++){
 			if( dataArr[i]["targetCollection"] != null){
 				targetCollection = dataArr[i]["targetCollection"]; 
 				referencedViews = [];
 				for(var j = 0; j < targetCollection.views.length; j++){
 					referencedViews.push(targetCollection.views[j].id );
 				}

    			obj = {"query": dataArr[i]["query"], "referencedViews": referencedViews};//targetCollection.views };
 			}
    		else
    			obj = {"query": dataArr[i]["query"], "referencedViews": [] };

    		reloadList.push(obj);
    		console.log(obj.query);
    	}


    	console.log("-------------------");
	    console.log('RELOAD file');


	    //testDataRetrieval(); 
	  	console.log("-------------------"); 

	  	
	    runEverything();

	});
	// fs.createReadStream(reloadFile)  
	//   .pipe(csv({ separator: '\t' }))
	//   .on('data', (row) => {
	//     // console.log(row);
	//     attributeAndValues.push(row);
	//   })
	//   .on('end', () => {
	  	

	// });

};



// THEN RUN EVERYTHING
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
var reloadCount = 0;

function runEverything() {
	console_debug("-------------------");
	console_debug(theDataset);
	console_debug("-------------------");
	console_debug("-------------------");
	console_debug("-------------------");
	console_debug(dataDescription);
	console_debug("-------------------");
	console_debug("-------------------");
	console_debug("-------------------");
	console_debug(attributeAndValues);
	console_debug("-------------------");
	console_debug("-------------------");
	console_debug("-------------------");


	//--------INIT STUFF - turn back on
	init_nlp();   
	init_creator(); 


	//-----START SERVER AND COMMUNICATION TO CLIENT
	const requestHandler = (request, response) => {  //not sure when this is invoked exactly.... 
	  console.log(request.url)
	  // response.end('Hello Node.js Server!')
	}

	// const server = http.createServer(requestHandler);
		const options = {
		  key: fs.readFileSync('keys/private.key'),
		  cert: fs.readFileSync('keys/certificate.crt')
		};
	const server = https.createServer(options, requestHandler); 



	// https.createServer(options, function (req, res) {
	//   res.writeHead(200);
	//   res.end("hello world\n");
	// }).listen(8000);




	/// NOW LISTEN AND RESPOND 
	//////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////

	server.listen(port, (err) => {

		//ERROR HANDLING- at the moment errors are not handled
		if (err) {
			return console.log('something bad happened', err);
		}

		//Console reporting what is happenning
		console.log(`server is listening on ${port}`);

		console.log("-------------------");
		console.log("-------------------");
		console.log("-------------------");
		console.log("START");

		wsServer = new WebSocketServer({
			httpServer: server
		});


		wsServer.on('error', (error) => {
		  //handle error
		  console.log("THE ERROR!!!!");
		});

		// WebSocket server
		wsServer.on('request', function(request) {  //when the 
			connection = request.accept(null, request.origin);

			// handle
			// all messages from users here.
			connection.on('message', function(message) {

				var obj = JSON.parse(message.utf8Data);// this has the query

				console.log("MESSAGE RECIEVED:");
				console.log("----Type:");
				console.log(obj.message); 
				console.log("----Query:");
				console.log(obj.query);
				console.log("----Views:");
				console.log(obj.referencedViews);

				if(obj.query == "query"){
					//do nothing --- means nothing is sent 

					//send attr list
					returnObj = {
			            "type": "attributeList",
			            "attributeList": attributeAndValues 
			        };
					connection.send( JSON.stringify( returnObj ) );

					// if( reloadFromPrior ){
					// 	// reloadCount = 0; 
					// 	reloadingFromPriorState();
					// }


				}
				else if( obj.query == "done"){
					//do nothing at the moment.... 

					//if reload from prior state, open the next one
					if( reloadFromPrior ){
						if( reloadCount < reloadList.length ){
							reloadingFromPriorState();
							// reloadCount++;x
						}
						else 
							reloadingFromPrior = false; //STOP 
					}

					//if reload from prior state and none are available, then stop reloading 

				}
				else if( obj.query == "undo" ){
					console.log("UNDO");

					theInfo["undoCount"]++;
					theInfo["queryCount"]++;

					queryRefPairs["list"].push( {"query": obj.query, "ref": "" }); 

					inputDataStorage["data"].push( { 
						"number": inputDataStorage["data"].length, 
						"query": "undo"
					});

					//Example
					// { number: 1, query: "can i see...", category: "referential", "new_or_mod": "mod", "subcategory": "subsetPivot"}
					obj = {
						 "number": outputCSVData.length,
						 "query": "undo",
						 "dir_ref_undo_err": "undo",
						 "new_mod_ext_na": "na",
						 "subtype": "na",
						 // "resultType": "na",
						 // "resultList": "na",
						 "refType": "na",
						 "referencedViews": "na",
						 "producedList": "na",
						 "producedType" : "na"
						}
					outputCSVData.push( obj );


					filename = "log/"+inputDataStorage["user-id"] + ".json"; 
					const jsonString = JSON.stringify(inputDataStorage)
					fs.writeFile(filename, jsonString, function (err,data) {
						if (err) {
					    return console.log(err);
					  }
					  console.log(inputDataStorage);

						// csvWriter
						//   .writeRecords(outputCSVData)
						//   .then( ()=> executeUndoCommand() );
					  
					  // connection.send( JSON.stringify( returnObj ) );
					  executeUndoCommand(); 


					});




				}
				else if( obj.query == "closedView"){
					if( switchPlatform == "sage2"){
						if( referencedViews.length >= 1){
							view = referencedViews[0]; 

							//delete view from collection

						}
					}
	

				}
				else { //figure out what they are asking 


					theInfo["queryCount"]++;

					if( obj.referencedViews )
						queryRefPairs["list"].push( {"query": obj.query, "ref": obj.referencedViews.toString() }); 
					else
						queryRefPairs["list"].push( {"query": obj.query, "ref": "" }); 

					//log
					inputDataStorage["data"].push( { 
						"number": inputDataStorage["data"].length, 
						"query": obj.query
					});

					obj = {
						 "number": outputCSVData.length,
						 "query": obj.query,
						 "dir_ref_undo_err": "unknown",
						 "new_mod_ext_na": "unknown",
						 "subtype": "unknown",
						 // "resultType": "unkonwn",
						 // "resultList": "uknonwn",
						 "refType": "unknown",
						 "referencedViews": queryRefPairs["list"][ queryRefPairs["list"].length-1 ]["ref"],
						 "producedList": "unknown",
						 "producedType": "unknown"
						}
					outputCSVData.push( obj );

					console.log( "input data storage: ");
					console.log( inputDataStorage );


					console.log("Store any referenced views");
					console.log(obj.referencedViews); 
					referencedViews = obj.referencedViews;


					// obj = {
					// 	 "number": outputCSVData.length,
					// 	 "query": obj.query,
					// 	 "category": "unknown",
					// 	 "new_or_mod": "unknown",
					// 	 "subcategory": "unknown" 
					// 	}
					// outputCSVData.push( obj );	

					console.log("Begin nlp interpretation");
					
					//NORMAL PIPELINE
					nlp_to_entities(obj.query); //turn back on...

				}


			});

			connection.on('close', function(connection) {
				// close user connection
			});

			connection.on('error', async () => {
				console.log("ERROR!!!!!!")
			  // await subscriptionServer.disconnect(connectionId);
			 });
		});

	})//end listen

};// end run everything


function reloadingFromPriorState(){

	current = reloadList[ reloadCount ]; 

	console.log("Store any referenced views");
	console.log(current.referencedViews); 
	referencedViews = current.referencedViews;
	query = current.query; 

	console.log("Begin nlp interpretation");

	//log
	inputDataStorage["data"].push( { 
		"number": inputDataStorage["data"].length, 
		"query": query
	});

	reloadCount ++;

	theInfo["queryCount"]++;

	if( referencedViews )
		queryRefPairs["list"].push( {"query": query, "ref": referencedViews.toString() }); 
	else
		queryRefPairs["list"].push( {"query": query, "ref": "" }); 

	obj = {
						 "number": outputCSVData.length,
						 "query": obj.query,
						 "dir_ref_undo_err": "unknown",
						 "new_mod_ext_na": "unknown",
						 "subtype": "unknown",
						 // "resultType": "unkonwn",
						 // "resultList": "uknonwn",
						 "refType": "unknown",
						 "referencedViews": queryRefPairs["list"][ queryRefPairs["list"].length-1 ]["ref"],
						 "producedList": "unknown",
						 "producedType": "unknown"

						}
					outputCSVData.push( obj );

	
	//NORMAL PIPELINE
	nlp_to_entities(query); //turn back on...
};


//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
////////////////////////////-- NLP PIPELINE -- ///////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////


function init_nlp(){

	for(var i = 0; i< dataDescription.length; i++){
		console_debug(dataDescription[i]);

		textArr = dataDescription[i]["List"].split(",")

		for(j = 0; j < textArr.length; j++){
			newText = textArr[j].trim(); 
			textArr[j] = newText;
		}

		// for(j =0; j < textArr.length; j++){
		// 	tokens = textArr[j].split(" "); 
		// 	newSentence = "";

		// 	for(k =0; k < tokens.length; k++){
		// 		singularWord = pluralize.singular(tokens[k]);
		// 		newSentence = newSentence + " " + singularWord;
		// 	}
		// 	textArr[i] = newSentence;

		// }

		manager.addNamedEntityText( 
			dataDescription[i]["Entity"],
			dataDescription[i]["Option"],
			['en'],
			textArr
			);
	}

	interpreter.init(dataDescription, attributeAndValues); 

};

function nlp_to_entities(query){

	//remove plurals
	tokens = query.split(/,| |-/);//" "); 
	newQuery = "";
	for(i =0; i < tokens.length; i++){
		newToken = pluralize.singular(tokens[i]);
		newQuery = newQuery + " " + newToken;
	}

	query = newQuery;
	console.log(query);


		manager.findEntities(
	  query,
	  'en',
	).then(entities => entities_to_attr(entities)) //entities => console.log(entities));
	.catch(error => {

   	 	console.error('onRejected function called: ' + error.message);
   	 	theInfo["nonResponseCount"]++;

		returnObj = {
			            "type": "error",
			            "error": "Sorry. Did not understand. Please try again, with a different wording, or try a new question."
			        };
		connection.send( JSON.stringify( returnObj ) );

  	});

};

// distinguish time and time of the day from day of the week.... 
function disambiguateEntities(entities){


	//remove entities that overlap
	for( var i = 0; i < entities.length; i++){
		for(var j =i+1; j < entities.length; j++){
			if(entities[i]["end"] > entities[j]["start"] ){

			}
		}
	}


	for( var i = 0; i < entities.length; i++){
		if( entities[i].option == "time"){
			entities[i].option = "time-of-the-day"; 
		}
		else if( entities[i].option == "day"){
			entities[i].option = "day-of-the-week"; 
		}
		else if( entities[i].option == "month"){
			entities[i].option = "month-of-the-year"; 
		}

		else if( entities[i].entity == "set"){
			if( entities[i].sourceText.includes("on Mondays") ){
				entities[i].option = "monday";
				entities[i].entity = "day-of-the-week";
			}
			else if( entities[i].sourceText.includes("on Tuesdays") ){
				entities[i].option = "tuesday";
				entities[i].entity = "day-of-the-week";
			}
			else if( entities[i].sourceText.includes("on Wednesdays") ){
				entities[i].option = "wednesday";
				entities[i].entity = "day-of-the-week";
			}
			else if( entities[i].sourceText.includes("on Thursdays") ){
				entities[i].option = "thursday";
				entities[i].entity = "day-of-the-week";
			}
			else if( entities[i].sourceText.includes("on Fridays") ){
				entities[i].option = "friday";
				entities[i].entity = "day-of-the-week";
			}
			else if( entities[i].sourceText.includes("on Saturdays") ){
				entities[i].option = "saturday";
				entities[i].entity = "day-of-the-week";
			}
			else if( entities[i].sourceText.includes("on Sundays") ){
				entities[i].option = "sunday";
				entities[i].entity = "day-of-the-week";
			}
		}

			//weekends -> 
		if( entities[i].entity == "request-info" ){

			if( entities[i].option == "weekends") {
				console.log("weekends!");
				satObj = { 
					"start": entities[i].start,
					"end": entities[i].end,
					"len": 10,
					"levenshtein": 0,
					"accuracy": 1, 
					"option": "saturday",
					"sourceText": "saturday",
					"entity": "day-of-the-week",
					"utteranceText": "saturday"
				};
				sunObj = { 
					"start": entities[i].start,
					"end": entities[i].end,
					"len": 10,
					"levenshtein": 0,
					"accuracy": 1, 
					"option": "sunday",
					"sourceText": "sunday",
					"entity": "day-of-the-week",
					"utteranceText": "sunday"
				};
				togetherObj = {
					"start": entities[i].start,
					"end": entities[i].end,
					"len": 10,
					"levenshtein": 0,
					"accuracy": 1, 
					"option": "aggregate",
					"sourceText": "together",
					"entity": "request-info",
					"utteranceText": "together"
				}
				entities.splice(i, 1, satObj , sunObj, togetherObj);
				i += 3;
			}
			else if( entities[i].option == "weekdays") {
				console.log("weekdays!");
				monObj = { 
					"start": entities[i].start,
					"end": entities[i].end,
					"len": 10,
					"levenshtein": 0,
					"accuracy": 1, 
					"option": "monday",
					"sourceText": "monday",
					"entity": "day-of-the-week",
					"utteranceText": "monday"
				};
				tuesObj = { 
					"start": entities[i].start,
					"end": entities[i].end,
					"len": 10,
					"levenshtein": 0,
					"accuracy": 1, 
					"option": "tuesday",
					"sourceText": "tuesday",
					"entity": "day-of-the-week",
					"utteranceText": "tuesday"
				};
				wedObj = { 
					"start": entities[i].start,
					"end": entities[i].end,
					"len": 10,
					"levenshtein": 0,
					"accuracy": 1, 
					"option": "wednesday",
					"sourceText": "wednesday",
					"entity": "day-of-the-week",
					"utteranceText": "wednesday"
				};
				thursObj = { 
					"start": entities[i].start,
					"end": entities[i].end,
					"len": 10,
					"levenshtein": 0,
					"accuracy": 1, 
					"option": "thursday",
					"sourceText": "thursday",
					"entity": "day-of-the-week",
					"utteranceText": "thursday"
				};
				friObj = { 
					"start": entities[i].start,
					"end": entities[i].end,
					"len": 10,
					"levenshtein": 0,
					"accuracy": 1, 
					"option": "friday",
					"sourceText": "friday",
					"entity": "day-of-the-week",
					"utteranceText": "friday"
				};
				togetherObj = {
					"start": entities[i].start,
					"end": entities[i].end,
					"len": 10,
					"levenshtein": 0,
					"accuracy": 1, 
					"option": "aggregate",
					"sourceText": "together",
					"entity": "request-info",
					"utteranceText": "together"
				};
				entities.splice(i, 1, monObj , tuesObj, wedObj, thursObj, friObj, togetherObj);
				i += 6;

			}

		}

	}
	console.log("added weekends");
	console.log(entities);



	return entities; 
};



function isEntityAnAttributeValue(entity){
	//console.log(attributeAndValues);
	//console.log("is entity an attribute value")
	for( var i = 0; i < attributeAndValues.length; i++){
		//console.log(entity)
		//console.log(attributeAndValues[i]);
		//console.log( entity == attributeAndValues[i]["name"] );
		if( entity ==   attributeAndValues[i]["name"] )
			return true;
	}
	return false;

};

function entities_to_attr(entities){
	console.log("npl output: ")
	console.log(entities);

	// Example:  "Can I see theft by time of the day? "
	var attributeList = []; // time
	var filters = [];
	var filterValues = []; // theft 
	var filterAttributes =  [];  // crime-type 
	var viewTypeParams = [];
	

	// Example "Can I see this but for the loop"
	var requestInfo = [];  // this-but 

	entities = disambiguateEntities(entities); // distinguish time of the day from day of the week (both have 'day')


	// capture the attributes and filters and request-info that are to be used in making visualization 
	for(var i = 0; i < entities.length; i++){
		console_debug("ENTITY: ")
		console_debug( entities[i]);
		if( entities[i]["entity"] == "attribute" ){
			attributeList.push(entities[i]["option"]); //time 
		}
		else if( entities[i]["entity"] == "request-info" ){

			requestInfo.push(entities[i]["option"]); // this-but, compare....

			if( entities[i]["option"] == "high" || entities[i]["option"] == "very-high"
				|| entities[i]["option"] == "low" || entities[i]["option"] == "very-low"
					|| entities[i]["option"] == "moderate"){
				
				//is there a 'where the'?
				whereThe = false;
				for(var j = 0; j < requestInfo.length; j++){
					if( requestInfo[j] == "whereThe"){
						whereThe = true; 
					}
				}

				console.log(whereThe);

				if( !whereThe ){
					console.log("not where the");
					if( i != entities.length-1 ){
						console.log("i != entities.length-1 ");
						shouldContinue = true;
						if( i > 0 ){ //very high covid vulnerabilty rank, very high, poverty rate 
							if( entities[i-1]["start"] >= entities[i]["start"] && entities[i-1]["end"] >=  entities[i]["end"]){
								if( entities[i-1]["sourceText"].includes(entities[i]["sourceText"]) ){
									shouldContinue = false;
									console.log("shouldContinue = false ");

								}
							}
						}

						console.log("checking entities");
						console.log(entities);
						if( shouldContinue ){
							console.log("should continue");
							found = false;
							for(var j = i+1; j < entities.length && !found; j++){
								console.log("i : " + i + " j : " + j);
								console.log( entities[j]["entity"] ); 
								if( entities[j]["entity"] == "attribute"){ //grab next attribute
									filterValues.push(entities[i]["option"] + "-" + entities[j]["option"] ); // theft
									filterAttributes.push(entities[j]["option"]); // crime-type
									found = true;
									console.log("found! " + entities[j]["option"])
								}
								else if( entities[j]["entity"] != "request-info"){ //grab next value
									filterValues.push(entities[i]["option"] + "-" + entities[j]["entity"] ); // theft
									filterAttributes.push(entities[j]["entity"]); // crime-type
									found = true;
									console.log("found! " + entities[j]["option"])
								}
							}
						}

					}		
				} 
				else{
					console.log("WHERE THE")
					found = false;
					for(var j = i-1; j >= 0  && !found; j--){
						if( entities[j]["entity"] == "attribute"){ //grab next attribute
								filterValues.push(entities[i]["option"] + "-" + entities[j]["option"] ); // theft
								filterAttributes.push(entities[j]["option"]); // crime-type
								found = true;
						}
					}
				}
							
			}
			
	
		}
		else if( entities[i]["entity"] == "view-type" ){
			viewTypeParams.push(entities[i]["option"]); 
		}
		else if( isEntityAnAttributeValue(entities[i]["entity"]) ) {
			filterValues.push(entities[i]["option"]); // theft
			filterAttributes.push(entities[i]["entity"]); // crime-type
		}
		else if( entities[i]["entity"] == "unclear" ){

			
         
		}
	}

	//remove duplicates - used to be above ....
	// for( var i = 0; i < entities.length; i++){
	// 	for( var j = i+1; j < entities.length; j++){
	// 		if(entities[i].option == entities[j].option ){
	// 			entities.splice(j, 1);
	// 			j--;
	// 		}
	// 	}
	// }

	//remove duplicates
	for(i = 0; i < attributeList.length; i++){
		for(j = i+1; j < attributeList.length; j++){
			if( attributeList[i] == attributeList[j] ){
				attributeList.splice(j, 1);	
				j--;
			}
		}
	}


	//remove duplicates
	for(i = 0; i < filterValues.length; i++){
		for(j = i+1; j < filterValues.length; j++){
			if( filterValues[i] == filterValues[j] && filterAttributes[i] == filterAttributes[j] ){
				filterValues.splice(j, 1);
				filterAttributes.splice(j, 1);
				j--;
			}
		}
	}

	//make sure ot account for situations with no filter
	if(filterValues.length == 0 )
		filterValues.push("all");
	if(filterAttributes.length == 0 )
		filterAttributes.push("all");


	console_debug("attributes: "); 
	console_debug(attributeList); //eg. location_type
	console_debug("filter values: "); 
	console_debug(filterValues);  //eg.  uic, theft
	console_debug("filter attributes: "); 
	console_debug(filterAttributes);  //eg.  neighborhood, crime_type
	console_debug("view type params: "); 
	console_debug(viewTypeParams);  //eg.  neighborhood, crime_type


	//log


	//this is just for testing... 

	console_debug( "TESTING DO I SEE THE REFERENCED VIEWS");
	console_debug(referencedViews);

	// console.log("REFERENCED VIEWS");
	// console.log(referencedViews);
	//vcCreator.fromViewsToCollection(referencedViews); 

	if( requestInfo.length > 0 && requestInfo[0] == "undo" ){
		executeUndoCommand();
		return;
	}

	var targetCollection = null;

	if( switchPlatform == "sage2"){

		//first question- are they all in the same collection? 
		if( referencedViews && referencedViews.length > 0 ){
			areAllInSameCollection = vcCreator.areAllInSameCollection(referencedViews);

			if( areAllInSameCollection["allInOne"] ){
				targetCollection = areAllInSameCollection["theCollection"];
			} 
		}

		
	}
	else if( switchPlatform == "desktop"){
		targetCollection = vcCreator.fromViewsToCollection(referencedViews);  //vcCreator.getMostRecentCollection(); 
		console_debug(targetCollection);

	}

	interpretObj = interpreter.attr_to_vis(filterAttributes, filterValues, attributeList, requestInfo, viewTypeParams, targetCollection, entities);  

	if( inputDataStorage["data"].length >= 1 ){
		idx = inputDataStorage["data"].length-1;

		// storageObj = inputDataStorage["data"][ idx ];
		// storageObj["isIndependent"] = interpretObj["isIndependent"];
		// storageObj["type"] = interpretObj["type"];
		// storageObj["attributeList"] = interpretObj["attributeList"];
		// storageObj["filterList"] = interpretObj["filterList"];
		// storageObj["requestInfo"] = interpretObj["requestInfo"];
		// storageObj["viewTypeParams"] = interpretObj["viewTypeParams"];
		// if( interpretObj["targetCollection"] )
		// 	storageObj["targetCollection"] = interpretObj["targetCollection"];
		// else
		// 	storageObj["targetCollection"] = null;


		// inputDataStorage["data"][ idx ] = storageObj;

		inputDataStorage["data"][idx]["isIndependent"] = interpretObj["isIndependent"];
		inputDataStorage["data"][idx]["type"] = interpretObj["type"];
		inputDataStorage["data"][idx]["attributeList"] = interpretObj["attributeList"];
		inputDataStorage["data"][idx]["filterList"] = interpretObj["filterList"];
		inputDataStorage["data"][idx]["requestInfo"] = interpretObj["requestInfo"];
		inputDataStorage["data"][idx]["viewTypeParams"] = interpretObj["viewTypeParams"];
		if( interpretObj["targetCollection"] )
			inputDataStorage["data"][idx]["targetCollection"] = interpretObj["targetCollection"];
		else
			inputDataStorage["data"][idx]["targetCollection"] = null;



					console.log( "input data storage: ");
					console.log( inputDataStorage );

		//inputDataStorage["data"][ idx ] = storageObj;
	}



	//TURN BACK ON!!!!
	make_vis(interpretObj); //TURN THIS BACK ON!!!!!!
};

//////////////////////////////////////////////////////////////////////////

function executeUndoCommand(){

	if( lastCommandExecuted == null || lastCommandExecuted["list"] == null ){
		console.log("doing nothing, because you can only undo once"); 
		return;
	}
	
	console.log("undoing the previous command");

	for(i = 0; i < lastCommandExecuted["list"].length; i++){
		if( lastCommandExecuted["list"][i]["type"] == "new_collection"){
			console.log("delete collection");

			theInfo["undoViewCount"] += vcCreator.listOfCollections[ vcCreator.listOfCollections.length-1 ].views.length; //prevCollection.views.length; 


			//for now keep it in the server...?

			//store the undo command 

			connection.send( JSON.stringify( {"delete": true, "collectionId": lastCommandExecuted["list"][i]["collectionId"] } ) ); 
		}
		else if( lastCommandExecuted["list"][i]["type"] == "modify_collection" ){
			console.log("modify collection to prior state");
			console.log( lastCommandExecuted.previousReturnObj );

			lastCommandExecuted.previousReturnObj["list"][0]["type"] = "modify_collection_delete";

	  		connection.send( JSON.stringify( lastCommandExecuted.previousReturnObj ) );
			theInfo["undoViewCount"] += vcCreator.listOfCollections[ vcCreator.listOfCollections.length-1 ].views.length; //prevCollection.views.length; 

	  		vcCreator.updateCollection( lastCommandExecuted.prevCollection );
			//priorCollection = lastCommandExecuted.prevCollection; 

			//connection.send( JSON.stringify( {"deleteModify": true, "collectionId": priorCollection.id, "collection": priorCollection.  } ) ); 


			// returnObj = {
   //          "count": 1, 
   //          "list": [ 
   //              { 
   //                  "vis_collection": this.getViewsFromCollection(collection), 
   //                  "collectionId": collection.number,
   //                   "type": "modify_collection", 
   //                   "collection": this.getViewsFromCollection(collection), 
   //                   "collectionType": collection.type,
   //                   "collectionFilterList": collection.filterList,
   //                   "collectionAttributeList": collection.attributeList,
   //                   "collectionSecondaryAttributeList": collection.secondaryAttributeList,
   //                   "collectionAggrFilterList": collection.aggregatedFilter,
   //                   "replaceCollection": false,
   //                   "gridSpec": collection.gridSpec
   //              },
   //          ]
        // };

	  		// connection.send( JSON.stringify( returnObj ) );

			//connection.send( JSON.stringify( {"delete": true, "collectionId": lastCommandExecuted["list"][i]["collectionId"] } ) ); 

		}
	}


}


//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
////////////////////////////-- VIS PIPELINE -- ///////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

function init_creator(){
	console.log("init creator");
	vcCreator.init( datasets, attributeAndValues, dataFiles, whatDataContains); //theDataset, attributeAndValues, ); 

};

function make_vis(interpretObj){

	var prevCollection = null;

	console.log("in make vis! ");
	console.log(theInfo);
	console.log(queryRefPairs);
	var returnObj; 

	theInfo["visQuestionCount"]++;
	// queryRefPairs["list"][ queryRefPairs["list"].length-1 ]["type"] = interpretObj["type"];

	var category = "referential";
	var new_or_mod = "extend";

	if( interpretObj.isIndependent ){
		new_or_mod = "new";
		category = "direct"; 
	}

	var subcategory = "unknown"; 

	if( interpretObj.isIndependent ){
		console_debug("INDEPENDENT");

		theInfo["indepCount"]++;
		queryRefPairs["list"][ queryRefPairs["list"].length-1 ]["ref"] = "";
		
		outputCSVData[ outputCSVData.length-1 ]["dir_ref_undo_err"] = "direct";	
		outputCSVData[ outputCSVData.length-1 ]["new_mod_ext_na"] = "new";		
		outputCSVData[ outputCSVData.length-1 ]["refType"] = "na";
		outputCSVData[ outputCSVData.length-1]["referencedViews"] =  "";

		if( interpretObj.type == "target" ){

			returnObj = vcCreator.createTargeted(interpretObj.filterList[0], interpretObj.attributeList, interpretObj.viewTypeParams, interpretObj.secondaryAttributeList, theInfo); 
			theInfo = returnObj.theInfo;
			subcategory = returnObj["list"][0]["category"]; 

		}
		else if( interpretObj.type == "browse" ){


			if( interpretObj.attributeList.length == 0 ){
				 returnObj = vcCreator.createSubsetComplete(interpretObj.filterList[0], interpretObj.viewTypeParams, interpretObj.secondaryAttributeList, theInfo );  	
				theInfo = returnObj.theInfo;
				subcategory = returnObj["list"][0]["category"]; 
			}
			else{
				 returnObj = vcCreator.createSubsetPartial(interpretObj.filterList[0], interpretObj.attributeList, interpretObj.viewTypeParams, interpretObj.secondaryAttributeList, theInfo);	
				theInfo = returnObj.theInfo;
				subcategory = returnObj["list"][0]["category"]; 
			}
		}
		else if( interpretObj.type == "comparison" ){

			returnObj = vcCreator.createComparisonSet(interpretObj.filterList, interpretObj.attributeList, interpretObj.viewTypeParams, interpretObj.secondaryAttributeList, theInfo);
			theInfo = returnObj.theInfo;
			subcategory = returnObj["list"][0]["category"]; 
		}
		else if (interpretObj.type == "b+c" ){
						// theInfo["b_cCount"]++;

			 returnObj = vcCreator.createBrowseAndCompare(interpretObj.filterList, interpretObj.attributeList, interpretObj.viewTypeParams, interpretObj.secondaryAttributeList, theInfo); //note- shouldn't there be a case for b+c complete?
			theInfo = returnObj.theInfo;
			subcategory = returnObj["list"][0]["category"]; 
		}
		else if( interpretObj.type == "complex" ){
						// theInfo["complexCount"]++;

			returnObj  = vcCreator.createComplex(interpretObj.filterList, interpretObj.attributeList, interpretObj.viewTypeParams, theInfo);	
			theInfo = returnObj.theInfo;
			subcategory = returnObj["list"][0]["category"]; 
		}
		else if( interpretObj.type == "compare_compare_grid" ){

			returnObj = vcCreator.createCompareByCompare(interpretObj.filterList, interpretObj.attributeList, interpretObj.aggregatedFilter, interpretObj.viewTypeParams, interpretObj.secondaryAttributeList, theInfo);	
			theInfo = returnObj.theInfo;
			subcategory = returnObj["list"][0]["category"]; 
		}
		else {
			//error?
			theInfo["nonResponseCount"]++;
			subcategory = "non-response"; 
			
			returnObj = {
			            "type": "error",
			            "error": "Sorry. Did not understand. Please try again, with a different wording, or try a new question."
			        };
		}

		outputCSVData[ outputCSVData.length-1 ]["producedType"] = subcategory;
		outputCSVData[ outputCSVData.length-1 ]["subtype"] = subcategory;
		if( returnObj["list"] ){
			col = returnObj["list"][0]["vis_collection"];
			list = [];
			for(i = 0; i < col.length; i++){
				list.push( col[i]["id"] ); 
			}
			outputCSVData[ outputCSVData.length-1 ]["producedList"] = list.toString();
		}


	} else {
		console_debug("REFERENTIAL");
		theInfo["refCount"]++;

		subcategory = interpretObj.subtype;
		
		theInfo["totalViewsReferenced"]+= referencedViews.length; 
		theInfo["totalVisViewsReferenced"] += referencedViews.length;
		if( referencedViews.length > theInfo["maxViewsReferenced"] )
			theInfo["maxViewsReferenced"] = referencedViews.length; 
		if( referencedViews.length > theInfo["maxVisViewsReferenced"] )
			theInfo["maxVisViewsReferenced"] = referencedViews.length; 

		queryRefPairs["list"][ queryRefPairs["list"].length-1 ]["refCollectionType"] = interpretObj.targetCollection.type;
		queryRefPairs["list"][ queryRefPairs["list"].length-1 ]["pivotType"] = interpretObj.subtype;

		outputCSVData[ outputCSVData.length-1 ]["dir_ref_undo_err"] = "ref";		
		outputCSVData[ outputCSVData.length-1 ]["subtype"] = interpretObj.subtype;
		outputCSVData[ outputCSVData.length-1 ]["refType"] = interpretObj.targetCollection.type;


		prevCollection = loadash.cloneDeep( interpretObj.targetCollection );

		if( interpretObj.subtype == "pivotTheSubset" ){
			theInfo["pivotTheSubsetCount"]++;

			 returnObj  = vcCreator.pivotTheSubset(interpretObj.filterList, interpretObj.attributeList, interpretObj.targetCollection, interpretObj.pivotCount);
		} 
		else if( interpretObj.subtype == "filterTheCollection" ){

			theInfo["filterTheCollectionCount"]++;

			 returnObj = vcCreator.filterTheCollection(interpretObj.filterList, interpretObj.attributeList, interpretObj.targetCollection, interpretObj.pivotCount);
		} 
		else if( interpretObj.subtype == "splitTheCollection" ){
			theInfo["splitTheCollectionCount"]++;

			 returnObj = vcCreator.splitTheCollection(interpretObj.filterList, interpretObj.attributeList, interpretObj.targetCollection, interpretObj.pivotCount);	
		} 
		else if( interpretObj.subtype == "pivotTheDataAttribute" ){
			theInfo["pivotTheDataAttributeCount"]++;

			 returnObj =  vcCreator.pivotTheDataAttribute(interpretObj.filterList, interpretObj.attributeList, interpretObj.targetCollection, interpretObj.pivotCount);
		} 
		else if( interpretObj.subtype == "extendTheCollection" ){
			theInfo["extendTheCollectionCount"]++;

			 returnObj =   vcCreator.extendTheCollection(interpretObj.filterList, interpretObj.attributeList, interpretObj.targetCollection, interpretObj.pivotCount);
		}
		else if( interpretObj.subtype == "splitPivot" ){
			theInfo["splitPivotCount"]++;

			 returnObj =  vcCreator.splitPivot(interpretObj.filterList, interpretObj.attributeList, interpretObj.targetCollection, interpretObj.pivotCount);
		}
		else if( interpretObj.subtype == "splitPivotExtend" ){
			theInfo["splitPivotExtendCount"]++;

			 returnObj =  vcCreator.splitPivotExtend(interpretObj.filterList, interpretObj.attributeList, interpretObj.targetCollection, interpretObj.pivotCount);
		}
		else if( interpretObj.subtype == "deleteFromCollection" ){
			 returnObj = vcCreator.deleteFromCollection(interpretObj.filterList, interpretObj.attributeList, interpretObj.targetCollection, interpretObj.pivotCount);
		}
		else if( interpretObj.subtype == "selectFromCollection" ){
			 returnObj = vcCreator.selectFromCollection(interpretObj.filterList, interpretObj.attributeList, interpretObj.targetCollection, interpretObj.pivotCount);
		}
		else{
			//error
			theInfo["nonResponseCount"]++;
			subcategory = "non-response";
			
			returnObj = {
			            "type": "error",
			            "error": "Sorry. Did not understand. Please try again, with a different wording, or try a new question."
			        };
		}

		outputCSVData[ outputCSVData.length-1 ]["new_mod_ext_na"] = returnObj["list"][0]["type"]; 
		outputCSVData[ outputCSVData.length-1 ]["producedType"] = returnObj["list"][0]["collectionType"] //subcategory;
		if( returnObj["list"] ){
			col = returnObj["list"][0]["vis_collection"];
			list = [];
			for(i = 0; i < col.length; i++){
				list.push( col[i]["id"] ); 
			}
			outputCSVData[ outputCSVData.length-1 ]["producedList"] = list.toString();
		}
	}

	// if( returnObj["error"] ){
	// 	newReturnObj = {
	// 		            "type": "error",
	// 		            "error": returnObj["error"]; 
	// 		        };
	// 	returnObj = newReturnObj;
	// }

	if( returnObj["error"] ){

		outputCSVData[ outputCSVData.length-1 ]["dir_ref_undo_err"] = "err";
		outputCSVData[ outputCSVData.length-1 ]["new_mod_ext_na"] = "na";
		outputCSVData[ outputCSVData.length-1 ]["subtype"] = "na";
		// outputCSVData[ outputCSVData.length-1 ]["resultType"] = "na";
		// outputCSVData[ outputCSVData.length-1 ]["resultList"] = "na";
		outputCSVData[ outputCSVData.length-1 ]["referencedViews"] = "na";
		outputCSVData[ outputCSVData.length-1 ]["refType"] = "na";
		outputCSVData[ outputCSVData.length-1 ]["producedList"] = "na";
		outputCSVData[ outputCSVData.length-1 ]["producedType"] = "na";

		//do nothing....?
	}
	else if( returnObj["list"][0]["type"] == "new_collection" || returnObj["list"][0]["type"] == "modify_collection"){
		returnObj["query"] = inputDataStorage["data"][ inputDataStorage["data"].length-1 ]["query"];
		returnObj["isIndependent"] = inputDataStorage["data"][ inputDataStorage["data"].length-1 ]["isIndependent"];
	} 
	
	queryRefPairs["list"][ queryRefPairs["list"].length-1 ]["type"] = subcategory; //interpretObj["type"];

	// vcCreator.printAllCollections();
	theInfo["visCount"]= vcCreator.listOfViews.length; 


	collectionCount = collectionCount + 1;


	//LOG
	// fs.write( inputDataStorage )

	if( returnObj["error"] ){
		//do nothing
	} else {
		lastCommandExecuted = loadash.cloneDeep( returnObj );
		lastCommandExecuted.prevCollection = prevCollection; //add this ...
		lastCommandExecuted.previousReturnObj = loadash.cloneDeep( prevReturnObj );

		prevReturnObj = loadash.cloneDeep( returnObj ); 	
	}


	filename = "log/"+inputDataStorage["user-id"] + ".json"; 
	const jsonString = JSON.stringify(inputDataStorage)
	fs.writeFile(filename, jsonString, function (err,data) {
		if (err) {
	    return console.log(err);
	  }
	  console.log(inputDataStorage);

	  	console.log("----------------------- ");
		console.log("----------------------- ");
		console.log("----------------------- ");
		console.log("----------------------- ");
		console.log("QUERY REF PAIRS ------------------ ");
		console.log(queryRefPairs);
		console.log("----------------------- ");
		console.log("----------------------- ");
		console.log("----------------------- ");
		console.log("----------------------- ");


		console.log("----------------------- ");
		console.log("----------------------- ");
		console.log("----------------------- ");
		console.log("----------------------- ");
		console.log("INFO ------------------ ");
		console.log(theInfo);
		console.log("----------------------- ");
		console.log("----------------------- ");
		console.log("----------------------- ");
		console.log("----------------------- ");


		console.log("----------------------- ");
		console.log("----------------------- ");
		console.log("----------------------- ");
		console.log("----------------------- ");
		console.log("INFO ------------------ ");
		console.log(outputCSVData);
		console.log("----------------------- ");
		console.log("----------------------- ");
		console.log("----------------------- ");
		console.log("----------------------- ");




		// var category = "referential"; 
		// if( inputDataStorage["data"][idx]["isIndependent"] )
		// 	category = "direct";
			
			
		// 	obj = {
		// 	 "number": outputCSVData.length,
		// 	 "query": obj.query,
		// 	 "category": ,
		// 	 "new_or_mod": "unknown",
		// 	 "subcategory": "unknown" 
		// 	}
		// outputCSVData.push( obj );	



	  connection.send( JSON.stringify( returnObj ) );

	});



}; 

const logging = true;

const debug = true;  // error messages or not

function console_debug(string){
	if(debug )
		console.log(string);

};

