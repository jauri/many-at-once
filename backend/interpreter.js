// Loadash....
var loadash = require('lodash');

module.exports = {

	dataDescription: null, 
	attributeAndValues: null,

	init: function(dataDescription, attributeAndValues) { 

		console.log("Init interpreter");

		this.dataDescription = dataDescription;
		this.attributeAndValues = attributeAndValues;

	},

	checkForSplit: function(attributeList, requestInfo, entities){
		var revisedAttributeList = [];
		var secondaryAttributeList = []; 

		// if( !this.isIndependent ){
		// 	return { "attributeList": attributeList, "secondaryAttributeList": []  };
		// }

		for( var i = 0 ; i < entities.length; i++){

			if( entities[i]["option"] == "versus" ){
				if( attributeList.length >= 2 )
					entities[i]["option"] = "split";
			}
		}

		for( var i = 0 ; i < requestInfo.length; i++){

			if( requestInfo[i] == "versus" ){
				if( attributeList.length >= 2 )
					requestInfo[i] = "split";
			}
		}

		if( this.determineIsSplit( attributeList, requestInfo ) ){ //&& attributeList.length > 2 ){ //what was this for?
			splitEnd = 99999; 
			for(i = 0; i < entities.length; i++){
				if( entities[i]["option"] == "split" ){
					splitEnd = entities[i]["end"]; 
				}
				if( entities[i]["entity"] == "attribute" && entities[i]["start"] > splitEnd ){
					secondaryAttributeList.push( entities[i]["option"] );
				} else if( entities[i]["entity"] == "attribute" && entities[i]["start"] < splitEnd ){
					revisedAttributeList.push( entities[i]["option"] );
				}
			}

			console.log("attribute list revised " +  revisedAttributeList);
			console.log( "secondaryAttributeList " + secondaryAttributeList );
			return { "attributeList": revisedAttributeList, "secondaryAttributeList": secondaryAttributeList };


		} else{
			return { "attributeList": attributeList, "secondaryAttributeList": []  };
		}

	},

	attr_to_vis: function(filterAttributes, filterValues, attributeList, requestInfo, viewTypeParams,  targetCollection, entities){
		console.log("attribute to vis"); 



		// if( viewTypeParams.length > 0 ){
		// 	isMap = false;
		// 	isLine = false;

		// 	for(var i = 0; i < viewTypeParams.length; i++){
		// 		if( viewTypeParams[i] == "map"){
		// 			isMap = true;
		// 		}
		// 		if( viewTypeParams[i] == "line"){
		// 			isLine = true;
		// 		}
		// 	}

		// 	// attrCount = 0;
		// 	// for( )

		// 	if( isMap || isLine )
		// 		requestInfo.push("split");

		// }
		
		if( this.isAttrCatchall( attributeList, requestInfo ) ) {
			attributeList = this.fillInAttributeCatchall( attributeList, filterAttributes, requestInfo  );
		}
		if( attributeList.length == 0 ){ //I don't think I need this- I handle this later 

		} 


		console_debug("check for split");
		console_debug(filterAttributes);
		console_debug(filterValues);
		console_debug(attributeList);
		// console_debug(secondaryAttributeList);
		obj = this.checkForSplit(attributeList, requestInfo, entities); 
 
		attributeList = obj["attributeList"];
		var secondaryAttributeList = obj["secondaryAttributeList"];

		console_debug("resolved");
		console_debug(filterAttributes);
		console_debug(filterValues);
		console_debug(attributeList);
		console_debug(secondaryAttributeList);

		// if( attributeList == 0 ){
		// 	attributeList = secondaryAttributeList;
		// }

		// if no attr, fill in all 
		if( filterValues.length == 0 )
		{
			filterValues.push("all");
			filterAttributes.push("all");
		}

		console_debug("filled in missing info:");
		console_debug(filterAttributes);
		console_debug(filterValues);
		console_debug(attributeList);
		console_debug(secondaryAttributeList);

		//remove cases of 'where the poverty rate is low'  poverty rate low is filter
		this.fixFilterAndAttributeConflict(filterAttributes, attributeList); 
		console_debug("removed attr that conflict with filters");
		console_debug(filterAttributes);
		console_debug(filterValues);
		console_debug(attributeList);
		console_debug(secondaryAttributeList);

		
		//first aggregate filters together
		aggregatedFilter = this.aggregateFilters( filterAttributes, filterValues ); 

		aggregateTogetherInVis =  this.shouldWeAggregateAll( requestInfo ); //false; //assume always false for now
		var filterList = [ aggregatedFilter ]; 
		if( !aggregateTogetherInVis )
			filterList = this.makeFilterObjects( aggregatedFilter ); 


			console.log("secondary");
			console.log(secondaryAttributeList);

		//request type
		var type = "target";

		if( this.isIndependent(requestInfo) ){
			type = this.determineIndependentType(filterList, attributeList, requestInfo, aggregatedFilter, viewTypeParams, entities);
			console.log("TYPE: " + type);

			//accomodate maps and color by
			obj = this.adaptToMultifacetedViewTypes( type, filterList, attributeList, requestInfo, aggregatedFilter, viewTypeParams, entities, secondaryAttributeList);
			console.log("REVISED TYPE " + obj.revisedType);
			type = obj.revisedType;
			secondaryAttributeList = obj.secondaryAttributeList;
			attributeList = obj.attributeList;


			if( type == "compare_compare_grid")
				return { isIndependent: true, type: type, filterList: filterList, attributeList: attributeList, requestInfo: requestInfo, aggregatedFilter: aggregatedFilter, viewTypeParams: viewTypeParams, secondaryAttributeList: secondaryAttributeList };


			return { isIndependent: true, type: type, filterList: filterList, attributeList: attributeList, requestInfo: requestInfo, viewTypeParams: viewTypeParams, secondaryAttributeList: secondaryAttributeList };
		}
		else {
			var returnObj; 
			var pivotCount;

			console.log("secondary");
			console.log(secondaryAttributeList);


			//---- what kind of pivot is it? 
			pivotType = this.determineThePivotType(targetCollection, filterList, attributeList, requestInfo, viewTypeParams, secondaryAttributeList); 

			//adapt to multifaceted types
			obj = this.adaptPivotsToMultifacetedViewTypes(pivotType, targetCollection, filterList, attributeList, requestInfo, aggregatedFilter, viewTypeParams, entities, secondaryAttributeList);
			pivotType = obj.revisedType;
			secondaryAttributeList = obj.secondaryAttributeList;


			console.log( "PIVOT TYPE = " + pivotType);
			if( pivotType == "pivotTheSubsetComplete" ){

				//do nothing?
				pivotCount = filterList.length; 
				pivotType = "pivotTheSubset"; 

			}
			if( pivotType == "pivotTheSubsetPartial" ){

				pivotCount = filterList.length; 
				if( targetCollection.filterList.length == 1 ){

					newFilterList = [] ; 
					for( var i = 0; i < filterList.length; i++ ){
						filter = this.partialPivotFilterFix(filterList[i], targetCollection.filterList[0]);   //
						newFilterList.push( filter ); 
					}	
					filterList = newFilterList;
				}
				else{

					newFilterList = loadash.cloneDeep( targetCollection.filterList ); 
					finalFilterList =[];

					for(i =0 ; i < filterList.length; i++){
						for(j =0; j < filterList[i].length; j++){

							for(k = 0; k < newFilterList.length; k++){
								newFilter = []; 
								for(l = 0; l < newFilterList[k].length; l++){
									if( newFilterList[k][l]["attr"] == filterList[i][j]["attr"])
									{
										newFilterList[k][l] = filterList[i][j]; 
									}
									// else{
									// 	newFilter.push(targetCollection.filterList[k][l]);
									// }
								}
								// newFilterList.push(newFilter);
							}

						}
							finalFilterList = finalFilterList.concat(newFilterList);
							newFilterList = loadash.cloneDeep(targetCollection.filterList); 
					}	
					filterList = finalFilterList;//newFilterList;
					console.log( newFilterList );	

				}

				pivotType = "pivotTheSubset"; 
			}
			if( pivotType == "filterTheCollection"){
				//do nothing
				pivotCount = filterList.length;
			}
			if( pivotType == "pivotTheDataAttribute"){
				filterList = targetCollection.filterList; 
				pivotCount = attributeList.length; 
			}
			if( pivotType == "extendTheCollectionPartial"){ //don't think it will work for browse+compare

				console_debug("comparisonCollectionExtendPartial");
				pivotCount = filterList.length;
				// if( targetCollection.type == "b+c" ){
				// 	countMatch = 0; 
				// 	for(i = 0; i < targetCollection.filterList[0].length; i++){
				// 		for( j = 0; j < filterList[0].length; j++){
				// 			if( filterList[0][j]["attr"] == targetCollection.filterList[0][i]["attr"] )
				// 				countMatch = countMatch +1; 
				// 		}
				// 		if( countMatch == 0 ){
				// 			for(j = 0; j < filterList.length; j++){
				// 				filterList[j].push( targetCollection.filterList[0][i] );
				// 			}
				// 		}
				// 		countMatch = 0;
				// 	}
				// }
				if( targetCollection.type == "comparison" || targetCollection.type == "multifacetedCompare" 
					|| targetCollection.type == "b+c" || targetCollection.type == "multifacetedB+C" 
					|| targetCollection.type == "complexAndCompareGrid" || targetCollection.type == "multifacetedCompareBySubAttr" 
					|| targetCollection.type == "multifacetedBrowseBySubAttr" ||  targetCollection.type == "subAttrByMultifacetedCompare"
					||  targetCollection.type == "subAttrByMultifacetedBrowse"){
					countMatch = 0; 
					for(i = 0; i < targetCollection.filterList[0].length; i++){
						for( j = 0; j < filterList[0].length; j++){
							if( filterList[0][j]["attr"] == targetCollection.filterList[0][i]["attr"] )
								countMatch = countMatch +1; 
						}
						if( countMatch == 0 ){
							for(j = 0; j < filterList.length; j++){
								filterList[j].push( targetCollection.filterList[0][i] );
							}
						}
						countMatch = 0;
					}
				}
				else if( targetCollection.type == "compare_compare_grid"  || targetCollection.type == "multifaceted_compare_compare_grid"){


					//MAKE FILTER LIST HERE first 
					//for all vals in aggregated filter list not in the same attr, make the pairs
					// newFilterList = []; 
					// for(i =0 ; i < filterList.length; i++){
		   //          	console_debug(filterList[i]);
		   //          	for(k = 0; k < filterList[i].length; k++){
			  //               for(j = 0; j < targetCollection.aggregatedFilter.length; j++ ){
			  //               	console_debug(targetCollection.aggregatedFilter[j])
			  //               	console_debug(filterList[i][k]["attr"] + " " +  targetCollection.aggregatedFilter[j]["attr"]);
			  //                   if(filterList[i][k]["attr"] == targetCollection.aggregatedFilter[j]["attr"])
			  //                   {
			  //                      //SKIP
			  //                   }
			  //                   else{

			  //                   	newFilterList.push( targetCollection.aggregatedFilter[j].( filterList[i][k] ));
			  //                   }
			  //               }
			  //           }
		   //          }

		   		

					//first insert new filters into the appropriate place in the aggregated filter
		            for(i =0 ; i < filterList.length; i++){
		            	console_debug(filterList[i]);
		            	for(k = 0; k < filterList[i].length; k++){
			                for(j = 0; j < targetCollection.aggregatedFilter.length; j++ ){
			                	console_debug(targetCollection.aggregatedFilter[j])
			                	console_debug(filterList[i][k]["attr"] + " " +  targetCollection.aggregatedFilter[j]["attr"]);
			                    if(filterList[i][k]["attr"] == targetCollection.aggregatedFilter[j]["attr"])
			                    {
			                        //insert it into the aggregated filter
			                        console_debug("adding" + filterList[i][k]["vals"][0])
			                        targetCollection.aggregatedFilter[j]["vals"].push(filterList[i][k]["vals"][0]);
			                    }
			                }
			            }
		            }
		                	console_debug(targetCollection.aggregatedFilter)


		            filterList = this.makeFilterObjects(targetCollection.aggregatedFilter);		//oops.... 


		            //only keep filter list items not from the original collection
		            //for all items in the new filter list 
		            //loop through the old and check to see if any match
		            //if any match, don't keep
		            newFilterList = [];


				}

				pivotType = "extendTheCollection";//no need to remember this as partial

			}
			if(pivotType == "extendTheCollection"){
				if(attributeList.length > 1)
					pivotCount = attributeList.length; 
				else
					pivotCount = filterList.length;

			}
			if( pivotType == "splitTheCollection" ){
				attributeList = secondaryAttributeList; //fix

				pivotCount = attributeList.length;
				console.log(targetCollection.secondaryAttributeList.length);
				if( targetCollection.secondaryAttributeList.length != 0 ){ //if it already has a split
					pivotType = "splitPivot"; 
				}

				if( targetCollection.type == "sub+attr" || targetCollection.type == "multifacetedBrowseBySubAttr" 
					|| targetCollection.type == "multifacetedCompareBySubAttr" ||  targetCollection.type == "subAttrByMultifacetedCompare"
					||  targetCollection.type == "subAttrByMultifacetedBrowse"){//multifacetedCompareBySubAttr??
					pivotType = "splitPivotExtend"; 
				}
				console.log(pivotType);
			}

			returnObj = {
					isIndependent: false, 
					type: "referential", 
					subtype: pivotType, //"subsetMultiPivotComplete", 
					filterList: filterList, 
					attributeList: attributeList, 
					requestInfo: requestInfo,
					targetCollection: targetCollection,
					pivotCount: pivotCount,
					viewTypeParams: viewTypeParams,
					secondaryAttributeList: secondaryAttributeList
			};
		}

		return returnObj;

	},


	adaptToMultifacetedViewTypes: function(type, filterList, attributeList, requestInfo, aggregatedFilter, viewTypeParams, entities, secondaryAttributeList){
		console.log("adapt to multifaceted view types ");
		newType = type;
		containsMap = false;
		mapIdx = -1; 

		for( var i = 0; i < attributeList.length; i++){
			if( attributeList[i] == "county-map"){
				containsMap = true; 
				mapIdx = i; 
			}
		}

		if( containsMap ){
			if( type == "complex" && attributeList.length == 2 ){
				newType = "target"; //will become multifaceted in view collection creator
				attributeList.splice(mapIdx, 1); 
				secondaryAttributeList = attributeList; 
				attributeList = [ "county-map" ]; 
			}
			else if( type == "complex" && attributeList.length > 2){
				newType = "target"; //will become sub+attr in view collection creator
				attributeList.splice(mapIdx, 1); 
				secondaryAttributeList = attributeList; 
				attributeList = [ "county-map" ]; 
			} 
			else if( type == "b+c"){
				newType = "comparison"; //will become multifaceted compare in view collection creator
				attributeList.splice(mapIdx, 1); 
				secondaryAttributeList = attributeList; 
				attributeList = [ "county-map" ]; 
			}

		}

		else { 
			containsLine = false;
			lineIdx = -1; 

			for( var i = 0; i < attributeList.length; i++){
				if( attributeList[i] == "date"){
					containsLine = true; 
					lineIdx = i; 
				}
			}

			if( containsLine ){
				if( type == "complex" && attributeList.length == 2 ){
					newType = "target"; //will become multifaceted in view collection creator
					attributeList.splice(lineIdx, 1); 
					secondaryAttributeList = attributeList; 
					attributeList = [ "date" ]; 
				}
				else if( type == "complex" && attributeList.length > 2){
					newType = "target"; //will become sub+attr in view collection creator
					attributeList.splice(lineIdx, 1); 
					secondaryAttributeList = attributeList; 
					attributeList = [ "date" ]; 
				} 
				else if( type == "b+c"){
					newType = "comparison"; //will become multifaceted compare in view collection creator
					attributeList.splice(lineIdx, 1); 
					secondaryAttributeList = attributeList; 
					attributeList = [ "date" ]; 
				}

			}

			//this toggles to a different interpretation of complex questions
			else{ //not contains line

				//does it for the first attr.... =/  

				// if( type == "complex" && attributeList.length == 2 ){
				// 	newType = "target"; //will become multifaceted in view collection creator
				// 	firstAttr = attributeList[0]; //save it
				// 	attributeList.splice(0, 1); //remove it 
				// 	secondaryAttributeList = loadash.cloneDeep( attributeList ); //copy
				// 	attributeList = [ firstAttr ]; //overwrite
				// 	// attributeList = [ "date" ]; 
				// }
				// else if( type == "complex" && attributeList.length > 2){
				// 	newType = "target"; //will become multifaceted in view collection creator
				// 	firstAttr = attributeList[0]; //save it
				// 	attributeList.splice(0, 1); //remove it 
				// 	secondaryAttributeList = loadash.cloneDeep( attributeList ); //copy
				// 	attributeList = [ firstAttr ]; //overwrite
				// } 

			}

		}
		
		return { "revisedType": newType, "attributeList": attributeList, "secondaryAttributeList": secondaryAttributeList };

	},

	adaptPivotsToMultifacetedViewTypes: function(type, targetCollection, filterList, attributeList, requestInfo, aggregatedFilter, viewTypeParams, entities, secondaryAttributeList){
		
		newType = type;
		targetContainsMap = false;
		mapIdx = -1; 


		// newType = type;
		targetContainsLine = false;
		lineIdx = -1; 

		//current type includes maps 
		for( var i = 0; i < targetCollection.attributeList.length; i++){
			if( targetCollection.attributeList[i] == "county-map"){
				targetContainsMap = true; 
				mapIdx = i; 
			}
			if( targetCollection.attributeList[i] == "date"){
				targetContainsLine = true; 
				lineIdx = i; 
			}
		}



		if( targetContainsMap && type == "pivotTheDataAttribute" ){
			//pivot attr includes date
			attrContainsLine = false;
			for( var j = 0; j < attributeList.length; j++ ){
				if( attributeList[j] == "date"){
					attrContainsLine = true;
				}
			}

			if( !attrContainsLine ){
				if( targetCollection.type == "multifaceted" ){
					newType = "splitTheCollection";
					secondaryAttributeList = attributeList;
				} 
				if( targetCollection.type == "sub+attr"){
					newType = "splitTheCollection";
					secondaryAttributeList = attributeList;
				}
				if( targetCollection.type == "multifacetedCompare"){
					newType = "splitTheCollection";
					secondaryAttributeList = attributeList;
				}
				if( targetCollection.type == "multifacetedCompareBySubAttr"){
					newType = "splitTheCollection";
					secondaryAttributeList = attributeList;
				}
			}

		}

		if( targetContainsLine && type == "pivotTheDataAttribute" ){
			//pivot attr includes date
			attrContainsMap = false;
			for( var j = 0; j < attributeList.length; j++ ){
				if( attributeList[j] == "county-map"){
					attrContainsMap = true;
				}
			}

			if( !attrContainsMap ){
				if( targetCollection.type == "multifaceted" ){
					newType = "splitTheCollection";
					secondaryAttributeList = attributeList;
				} 
				if( targetCollection.type == "sub+attr"){
					newType = "splitTheCollection";
					secondaryAttributeList = attributeList;
				}
				if( targetCollection.type == "multifacetedCompare"){
					newType = "splitTheCollection";
					secondaryAttributeList = attributeList;
				}
				if( targetCollection.type == "multifacetedCompareBySubAttr"){
					newType = "splitTheCollection";
					secondaryAttributeList = attributeList;
				}
			}

		}



		return { "revisedType": newType, "attributeList": attributeList, "secondaryAttributeList": secondaryAttributeList };

	},

	fixFilterAndAttributeConflict: function(filterAttributes, attributeList){
		for(i = 0; i < filterAttributes.length; i++){
			for(j = 0; j< attributeList.length; j++){
				//console.log(filterAttributes[i]);
				//console.log(attributeList[j]);
				if(filterAttributes[i] == attributeList[j]){
					attributeList.splice(j, 1,)
					j--;
				}
			}
		}
	},



	determineThePivotType3: function(targetCollection, filterList, attributeList, requestInfo){



	},


	determineThePivotType: function(targetCollection, filterList, attributeList, requestInfo, viewTypeParams, secondaryAttributeList){
		console.log("secondary attr list")
		console.log( secondaryAttributeList );
		//---- SPLIT? 
		var isSplit = this.determineIsSplit( secondaryAttributeList, requestInfo ); 

		if( isSplit && secondaryAttributeList.length >= 1 ){
			return "splitTheCollection";
		}

		var isDelete = this.determineIsDelete( targetCollection, filterList, attributeList, requestInfo ); 
		if(isDelete){
			return "deleteFromCollection";
		}

		var isSelect = this.determineIsSelect(targetCollection, filterList, attributeList, requestInfo ); 
		if(isSelect){
			return "selectFromCollection";
		}

		console.log(targetCollection.attributeList);

		//one attribute conserved in the collection, attributes are included in the request, no subsets
		if( targetCollection.attributeList.length == 1 && attributeList.length >= 1 && filterList[0][0]["attr"] == "all" ){
			return "pivotTheDataAttribute";
		}
		if( targetCollection.attributeList.length > 1 && attributeList.length >= 1 ){ //such as a browse collection, and new attr requested
			console.log(targetCollection.attributeList); 
			console.log(targetCollection.attributeList.length); 
			console.log(targetCollection);

			return "extendTheCollection"; // extend the collection 
		}


		// //for things with multiple attributes?  
		// if( filterList[0][0]["attr"] == "all" && attributeList.length >= 1 ){
		// 	return "extendTheAttribute";
		// }

		//////////////////
		//Is the filter being modified in some way?  Filter list not empty-> not 'all'
		//      Complete subset pivot
		//      Partial subset pivot
		//      Add a filter
		//      Extend the collection 
		var isSubsetPivot = false; 
		var isSingleSubsetPivot = true; 
		var isMultiSubsetPivot = false;

		var isCompleteSubsetPivot = false;
		var	isPartialSubsetPivot = false;
		var	isAddFilter = false;
		var	isExtendSubset = false;
		var	isExtendAttr = false;

		//------- FILTER IS SPECIFIED 
		if( filterList.length > 0 && filterList[0][0]["attr"] != "all" ){ //a filter is specified
			console.log("one subset of the data");

			//how many pivots are mentioned "Can I see this but Loop and River North"
			isSingleSubsetPivot = this.determineIsSinglePivot( filterList ); 
			if( isSingleSubsetPivot ){
				isMultiSubsetPivot = false; 
			} else{
				isMultiSubsetPivot = true; 
			}

			// Complete vs Partial:  (only applies to subset conserving collections)
			//  UIC , this but Loop 										VS    UIC theft, this but Loop 
			//  UIC theft, this but Loop battery  							VS    UIC theft, this but Loop 
			//  UIC, this but Loop and River north 							VS    UIC theft, this but Loop and River North 
			//  UIC theft, this but Loop burglary and River north burglary  VS    UIC theft, this but Loop and River North 
			//  ????  UIC theft, this but UIC battery and UIC assault ???
			isCompleteSubsetPivot = false;
			isPartialSubsetPivot = false;
			isAddFilter = false;  
			isExtend = false; 

			//---- BROWSE type collections
			if( targetCollection.filterList.length == 1 ) { //one filter for the whole collection
				console.log("FILTER LIST LENGTH = 1");
				// count matches of Filter attr values.  
				countMatch = 0;
				for( i = 0; i < filterList.length ; i++){ 
					for( j = 0; j < filterList[i].length; j++){

						for( k =0 ; k < targetCollection.filterList[0].length ; k++){

							//if the target collection filter attr matches the filter list attr, count match
							if( filterList[i][j]["attr"] == targetCollection.filterList[0][k]["attr"] ){
								countMatch = countMatch + 1; 
							}
						}
					}
				}

				// number of matches should equal the number of filters in the target collection * number of filter list entries
				if( countMatch < targetCollection.filterList[0].length * filterList.length ){
					isCompleteSubsetPivot = false;
					isPartialSubsetPivot = true; 
				} else{
					isCompleteSubsetPivot = true;
					isPartialSubsetPivot = false; 
				}

				if( isCompleteSubsetPivot )
					console_debug("COMPLETE");
				// if( isPartial )
				// 	console_debug("PARTIAL");


				//add new filter criteria? 
				if( isPartialSubsetPivot ){  //may be an 'add', not a partial pivot 
					//simple case, not completely right (eg. UIC theft, this but UIC Monday is an add technically)
					if( countMatch == 0 ){
						isAddFilter = true; 
						console_debug("ADD");
					}
					else
						console_debug("PARTIAL");
				}




				if( isAddFilter ){
					return "filterTheCollection";
				}
				if( isPartialSubsetPivot ){
					return "pivotTheSubsetPartial"; 
				}
				if( isCompleteSubsetPivot){
					return "pivotTheSubsetComplete"; 
				}
				// if( isExtend ){
				// 	return "extendTheSubset";
				// }



			}//end if single subset of the data
			else //not a single subset of the data, but multiple -> can either be partial or add or extend
			{
				console.log("multiple subsets of the data");
				//figure out if any portion of the filter list is conserved
				conservedFilterList = []; 
				for(i = 0; i < targetCollection.filterList.length; i++){
		            for(j = 0; j < targetCollection.filterList[i].length; j++){
		                for(k = i+1; k < targetCollection.filterList.length; k++){
		                    for(l = 0; l < targetCollection.filterList[k].length; l++){
		                        if( targetCollection.filterList[i][j]["vals"][0] == targetCollection.filterList[k][l]["vals"][0] && targetCollection.filterList[i][j]["attr"] == targetCollection.filterList[k][l]["attr"] ){
		                            conservedFilterList.push(targetCollection.filterList[i][j]); 
		                        }
		                    }
		                }
		            }
		        }
		        console_debug (conservedFilterList);

		        //prune
		        reallyConserved = []; 
		        for(i =0; i < conservedFilterList.length; i++){
		        	countMatch = 0; 
		        	for(j =0; j < targetCollection.filterList.length; j++){
		        		for(k = 0; k < targetCollection.filterList[j].length; k++){
		               		if( conservedFilterList[i]["vals"][0] == targetCollection.filterList[j][k]["vals"][0] && targetCollection.filterList[j][k]["attr"] == conservedFilterList[i]["attr"] ){
		               			countMatch++;
							}
		        		}

		        	}
		        	if( countMatch == targetCollection.filterList.length ){
		        		reallyConserved.push( conservedFilterList[i] );
		        	}
		        }

		        conservedFilterList = reallyConserved;		        
		        console_debug (conservedFilterList);


				conservedIsPivotedCount = 0; 
				for(i =0; i < conservedFilterList.length; i++){
					for( j =0 ; j < filterList.length; j++){
						for(k = 0; k < filterList[j].length; k++){
							if( conservedFilterList[i]["attr"] == filterList[j][k]["attr"] ){
								conservedIsPivotedCount++; 
							}

						}
					}
				}

				countMatch = 0; 
				for(var i = 0; i < filterList.length; i++ ){
					for( j = 0; j < filterList[i].length; j++){

						for( k =0 ; k < targetCollection.filterList.length ; k++){

							for(l = 0; l < targetCollection.filterList[k].length ; l++){
								if( targetCollection.filterList[k][l]["attr"] == filterList[i][j]["attr"] )
									countMatch ++;	
							}
	 					}
					}
				}

				isExtendComplete = true; 

				if( countMatch == 0 ){
					console_debug("ADD"); 
					isAddFilter = true; 
					isExtend = false; 
				}
				else{
					if(conservedIsPivotedCount > 0){
						isPartialSubsetPivot = true; 
						console_debug("PARTIAL PIVOT"); 
						// isExtendComplete = false;
					}
					if( countMatch > 0 ){
						isExtend = true;
						console.log("EXTEND SUBSET");

						//is extend partial? 
						//does this break everything?

						// for( i = 0; i < filterList.length ; i++){ 
							countMatch = 0;
							for( j = 0; j < filterList[0].length; j++){

								for( k =0 ; k < targetCollection.filterList[0].length ; k++){
									if( filterList[0][j]["attr"] == targetCollection.filterList[0][k]["attr"] ){
										countMatch++;
									}

								}
							}

						// }




						// //THIS IS AWFUL, I'M SORRY.... 
						//  // count matches of Filter attr values.  
						// countMatch = 0;
						// // isExtend = true; 
						// isExtendIsComplete = true; 
						// for( i = 0; i < filterList.length ; i++){ 
						// 	for( j = 0; j < filterList[i].length; j++){

						// 		for( k =0 ; k < targetCollection.filterList[0].length ; k++){

						// 			//if the target collection filter attr matches the filter list attr, count match
						// 			if( filterList[i][j]["attr"] == targetCollection.filterList[0][k]["attr"] ){
						// 				countMatch = countMatch + 1; 
						// 			}
						// 		}
						// 		if( countMatch == 0 )
						// 			isExtend = false; 
						// 		if( countMatch < targetCollection.filterList[0].length )
						// 			isExtendIsComplete = false; //if the number of matches at any point is not the same
						// 	}
						// }

						if( countMatch < targetCollection.filterList[0].length ){
							console.log("EXTEND PARTIAL");
							isExtendComplete = false; 
						}
					}
				}

				if( isAddFilter ){
					return "filterTheCollection";
				}
				if( isSplit && secondaryAttributeList.length == 1){//attributeList.length == 1 ){
					return "splitTheCollection";
				}
				if( isPartialSubsetPivot ){
					return "pivotTheSubsetPartial"; 
				}
				if( isExtend || attributeList.length > 0  || secondaryAttributeList.length > 0 ){
					console.log(isExtendComplete + " " + countMatch + " "  + conservedIsPivotedCount );
					if(isExtendComplete)
						return "extendTheCollection";
					else
						return "extendTheCollectionPartial";
				}
				// if( attributeList.length > 0 ){
				// 	return "extendAttribute"; 
				// }

			} //end multiple subsets


		}	  




	},

	determineIsSplit: function( attributeList, requestInfo ){
		var isSplit = false; 
		for(i =0; i < requestInfo.length; i++){
			if( requestInfo[i] == "split" && attributeList.length >= 1 )
				isSplit = true;
		}
		return isSplit;
	},

	determineIsDelete: function( targetCollection, filterList, attributeList, requestInfo){
		isDelete = false; 
		for(i =0; i < requestInfo.length; i++){
			if( requestInfo[i] == "delete" )
				isDelete = true;
		}
		return isDelete;
	},

	determineIsSelect: function( targetCollection, filterList, attributeList, requestInfo){
		isSelect = false; 
		for(i =0; i < requestInfo.length; i++){
			if( requestInfo[i] == "select" )
				isSelect = true;
		}

		if( filterList.length == 0 )// || attributeList.length == 0 )
			return false; 

		//select Monday... means that the attribute list must contain monday 
		foundFilter = true;//false;
		// for(i =0; i < filterList.length; i++){
		// 	for( j =0; j < targetCollection.attributeList.length; j++){
		// 		if( filterList[i]["attr"] == targetCollection.attributeList[j] )
		// 			foundFilter = true; 
		// 	}

		// }

		//note, not dealing with secondary attributes

		return isSelect && foundFilter;
	},

	determineIsSinglePivot: function(filterList){
		isSingleSubsetPivot = false;
		isMultiSubsetPivot = false; 

		//how many pivots? 
		for( i = 0; i < filterList.length ; i++){
			for( j = 0; j < filterList[i].length; j++){
				for( k = i+1; k < filterList.length; k++){
					for( l = 0; l < filterList[k].length; l++){
						if( filterList[i][j]["attr"] ==  filterList[k][l]["attr"] ){
							isSingleSubsetPivot = false; 
							isMultiSubsetPivot = true; 
						}
					}
				}
			}	
		}

		if( isSingleSubsetPivot )
			console_debug("SINGLE PIVOT");
		if( isMultiSubsetPivot )
			console_debug("MULTI PIVOT");

		return isSingleSubsetPivot;
	},

	determineThePivotType2: function(targetCollection, filterList, attributeList, requestInfo){

		isComplete = true; 
		isPartial = false; 

		isSplit = false; 
		for(i = 0; i < requestInfo.length; i++){
			if(requestInfo[i] == "split")
				isSplit = true; 
		}


	
		isFilter = false; 
		filterSingle = false;
		filterMulti = false;
		
		filterForExtend = false; 


		isPivot = false; 
		isExtend = false;

		// RESOLVE CASES WHERE FILTER CRITERIA ARE SPECIFIED 
		if( filterList.length != 0 && filterList[0][0]["attr"] != "all"){	//is there a new filter?

			//determine which part of the filter from the target collection is conserved 
			var conservedFilterList = [];

			if( targetCollection.type == "browse" || targetCollection.type == "targeted" || targetCollection.type == "complex" ){
				conservedFilterList = targetCollection.filterList; 
			}
			else{ //find any part of the filter list that is conserved 

		        for(i = 0; i < filterList.length; i++){
		            for(j = 0; j < filterList[i].length; j++){
		                for(k = i+1; k < filterList.length; k++){
		                    for(l = 0; l < filterList[k].length; l++){
		                        if( filterList[i][j]["vals"][0] == filterList[k][l]["vals"][0] && filterList[i][j]["attr"] == filterList[k][l]["attr"] ){
		                            conservedFilterList.push(filterList[i][j]); 
		                        }
		                    }
		                }

		            }
		        }

			}


			//first check to see if any part of the filter criteria is conserved
			// accross views in comparison collections and others (b+c, c+c, complex x compare, other grids...) 
			var conservedIsPivotedCount = 0; 
			if( targetCollection.type == "comparison" || targetCollection.type == "b+c" ){  //no conserved filter 

				//is any part of the filter conserved?
				// console.log(targetCollection.conservedFilterList);
				for(i =0; i < targetCollection.conservedFilterList.length; i++){
					for( j =0 ; j < filterList.length; j++){
						for(k = 0; k < filterList[j].length; k++){
							if( targetCollection.conservedFilterList[i]["attr"] == filterList[j][k]["attr"] ){
								conservedIsPivotedCount++; 
							}

						}
					}
				}

				console.log("is this a complete copy+pivot?  Yes if count match > 0");
				console.log(conservedIsPivotedCount)

			}


			// count matches of Filter attr values.  
			countMatch = 0;
			for( i = 0; i < filterList.length ; i++){ 
				for( j = 0; j < filterList[i].length; j++){

					for( k =0 ; k < targetCollection.filterList[0].length ; k++){

						//if the target collection filter attr matches the filter list attr, count match
						if( filterList[i][j]["attr"] == targetCollection.filterList[0][k]["attr"] ){
							countMatch = countMatch + 1; 
						}
					}
				}
			}

			// number of matches should equal the number of filters in the target collection * number of filter list entries
			if( countMatch < targetCollection.filterList[0].length * filterList.length ){
				isComplete = false;
				isPartial = true; 
			}
	
		}

		if( isComplete ){
			return "pivotTheSubsetComplete";
		}
		if( isPartial ){
			return "pivotTheSubsetPartial";
		}
	},



	/////////////////////
	/////////////////////
	///////////////////


	attr_to_vis2: function(filterAttributes, filterValues, attributeList, requestInfo, targetCollection){
			// factor in 'all'
		// if catchall used, fill in missing attr
		console.log("attribute to vis"); 

		if( this.isAttrCatchall( attributeList, requestInfo ) ) {
			attributeList = this.fillInAttributeCatchall( attributeList, filterAttributes, requestInfo  );
		}
		if( attributeList.length == 0 ){ //I don't think I need this- I handle this later 

		}

		// if no attr, fill in all 
		if( filterValues.length == 0 )
		{
			filterValues.push("all");
			filterAttributes.push("all");
		}

		console_debug("filled in missing info:");
		console_debug(filterAttributes);
		console_debug(filterValues);
		console_debug(attributeList);

		
		//first aggregate filters together
		aggregatedFilter = this.aggregateFilters( filterAttributes, filterValues ); 

		aggregateTogetherInVis =  this.shouldWeAggregateAll( requestInfo ); //false; //assume always false for now
		var filterList = [ aggregatedFilter ]; 
		if( !aggregateTogetherInVis )
			filterList = this.makeFilterObjects( aggregatedFilter ); 

		//request type
		var type = "target";

		if( this.isIndependent(requestInfo) ){
			type = this.determineIndependentType(filterList, attributeList, requestInfo);
			console.log("TYPE: " + type);

			return { isIndependent: true, type: type, filterList: filterList, attributeList: attributeList, requestInfo: requestInfo };
		}
		else {

			//------ SUBSET SINGLE AND MULTI PIVOT COMPLETE 
			var returnObj; 

			//------ ACTIONS PERFORMED ON SUBSET COLLECTIONS 
			if( targetCollection.type == "subsetComplete"  || targetCollection.type == "subsetPartial" ){ // single subset being pivoted 

				//---- TYPES OF ACTIONS 
				//"subsetSinglePivotComplete" 
				//"subsetMultiPivotComplete" 
				//"subsetSinglePivotPartial"
				//"subsetMultiPivotPartial" 
				//"subsetAddFilterSingle"
				//"subsetAddFilterMulti"
				//"subsetRemoveFilterSingle"

				//"subsetCollectionSingleExtend" //same
				//"subsetCollectionMultiExtend" //same
				//"subsetCollectionSplit"


				//JUST FOR TESTING 
				subsetPivotType = this.determineSubsetPivotType(targetCollection, filterList, attributeList, requestInfo); //"subsetCollectionSplit"; //"subsetCollectionSingleExtend"; //determineSubsetPivotType( filterList, attributeList, targetCollection.filterList); //"subsetAddFilterMulti" ;//"subsetAddFilterSingle"; //"subsetMultiPivotPartial"; //determineSubsetPivotType(); //TO DO  
				console_debug(subsetPivotType); 
				if( subsetPivotType == "subsetSinglePivotPartial" ){
					 
					filter = this.partialPivotFilterFix(filterList[0], targetCollection.filterList[0]);  
					filterList = [ filter ];
				}
				else if( subsetPivotType == "subsetMultiPivotPartial" ){ /// uic theft, this but loop and river north
					newFilterList = [] ; 
					for( var i = 0; i < filterList.length; i++ ){
						filter = this.partialPivotFilterFix(filterList[i], targetCollection.filterList[0]);   //
						newFilterList.push( filter ); 
					}	
					filterList = newFilterList;
					console.log( newFilterList );					
				}
				else if( subsetPivotType == "subsetAddFilterSingle" || subsetPivotType == "subsetAddFilterMulti" ){ //UIC theft, this but just Mondays 
					 
					 console.log("--------subset add filter single");
					 console.log(targetCollection.filterList);
					 console.log(filterList);

					 newFilterList = [];  


					 //newFilterList.push( targetCollection.filterList[0] ); 
					 for(var i = 0; i < filterList.length; i++){
					 	newFilterList.push( [] );
					 	for(var j = 0; j < targetCollection.filterList[0].length; j++){
					 		newFilterList[i].push(targetCollection.filterList[0][j]) ;
					 	}

					 	for(var j = 0; j < filterList[i].length; j++){
					 		newFilterList[i].push( filterList[i][j] );
					 

					 					 // for(var i = 0; i < targetCollection.filterList.length; i++){
					 	}
					 }

					 filterList = newFilterList;

				}
				else if( subsetPivotType == "subsetCollectionSingleExtend" || subsetPivotType == "subsetCollectionMultiExtend" ){

					//note I am just going to make the new view 

					console.log("--------subset extend attributes");
					console.log(targetCollection.filterList);
					console.log(attributeList);

					filterList = targetCollection.filterList; //stays the same
					//newAttributeList = attributeList;   //attrbute list is just the attribute list

				}
				else if( subsetPivotType == "subsetCollectionSplit"){

					// nothing?  no filter specified, attribute goes in attribute list 
				}

				console.log( subsetPivotType );

				returnObj = {
					isIndependent: false, 
					type: "referential", 
					subtype: subsetPivotType, //"subsetMultiPivotComplete", 
					filterList: filterList, 
					attributeList: attributeList, 
					requestInfo: requestInfo,
					targetCollection: targetCollection
				};

			}
			//------ ACTIONS PERFORMED ON COMPARISON COLLECTIONS 
			else if( targetCollection.type == "comparison" ){ // single subset being pivoted 

				//---- TYPES OF ACTIONS 
				//"attributePivotSingle"  UIC and Loop by day, this but year  
				//"attributePivotMulti"  UIC and Loop by day, this but year and month 
				//"attributePivotSingle"   UIC and Loop by day, this but year and month together 
				//"subsetAddFilterSingle"  UIC and Loop theft by day, this but battery 
				//"subsetAddFilterMulti" UIC and Loop theft by day, this but battery and assault

				//"singleExtendComplete"  UIC and Loop by day, this but River North 
				//"multiExtendComplete"  UIC and Loop by day, this but River North and Near West
				//"singleExtendPartial"  UIC and Loop theft by day, this but River North
				//"multiExtendPartial"  UIC and Loop theft by day, this but River North and Near West


				//TESTS
				comparisonPivotType = this.determineComparisonPivotType(targetCollection, filterList, attributeList, requestInfo); // "comparisonAttributePivotSingle"; //determineSubsetPivotType( filterList, attributeList, targetCollection.filterList); //"subsetAddFilterMulti" ;//"subsetAddFilterSingle"; //"subsetMultiPivotPartial"; //determineSubsetPivotType(); //TO DO  

				if( comparisonPivotType == "comparisonAttributePivotSingle" || comparisonPivotType == "comparisonAttributePivotMulti"){ //UIC and Loop by day, this but year   

					filterList = targetCollection.filterList; 

				}
				if( comparisonPivotType == "comparisonCollectionPivotFilterSingle" || comparisonPivotType == "comparisonCollectionPivotFilterMulti" ){
					newFilterList = [] ; 
					for(j = 0; j < targetCollection.filterList.length; j++) { 
						for( var i = 0; i < filterList.length; i++ ){
							filter = targetCollection.filterList[j].concat( filterList[i] );// this.partialPivotFilterFix(filterList[i], targetCollection.filterList[j]);   //
							newFilterList.push( filter ); 
						}
					}	
					filterList = newFilterList;
					console.log( newFilterList );

				}
				if( comparisonPivotType == "comparisonCollectionExtendPartial"){
					console_debug("comparisonCollectionExtendPartial");
					countMatch = 0; 
					for(i = 0; i < targetCollection.filterList[0].length; i++){
						for( j = 0; j < filterList[0].length; j++){
							if( filterList[0][j]["attr"] == targetCollection.filterList[0][i]["attr"] )
								countMatch = countMatch +1; 
						}
						if( countMatch == 0 ){
							for(j = 0; j < filterList.length; j++){
								filterList[j].push( targetCollection.filterList[0][i] );
							}
						}
						countMatch = 0;
					}

					comparisonPivotType = "comparisonCollectionExtend";//no need to remember this as partial

				}


				returnObj = {
					isIndependent: false, 
					type: "referential", 
					subtype: comparisonPivotType, 
					filterList: filterList, 
					attributeList: attributeList, 
					requestInfo: requestInfo,
					targetCollection: targetCollection
				};
			}
			else if( targetCollection.type == "targeted" ){

				console_debug( "TARGETED COLLECTION PIVOT");
				targetedCollectionPivotType = this.determineTargetedPivotType(targetCollection, filterList, attributeList, requestInfo);
				
				if( targetedCollectionPivotType == "targetedCollectionAttributePivot"){
					targetCollection.type = "subsetPartial"; // switch it to a subset collection. NOTE_ maybe it is complete, but no check
															

					targetedCollectionPivotType = "subsetCollectionMultiExtend";//might be single, but doesn't matter
 
					//check to make sure attributes aren't added twice
					skip = [];
					for(i = 0; i < attributeList.length; i++ ){
						skip.push( false );
						if(attributeList[i] == targetCollection.attributeList[0] )
							skip[skip.length-1] = true;
						if( attributeList[i] == targetCollection.attributeList[1] )
							skip[skip.length-1] = true;
					}

					newAttributeList = [];
					for( i = 0; i < skip.length; i++){
						if( ! skip[i] )
							newAttributeList.push(attributeList[i]);
						//else don't add it 
					}

					attributeList = newAttributeList; 

					filterList = targetCollection.filterList;
				} 
				else if(targetedCollectionPivotType == "subsetSinglePivotComplete" || targetedCollectionPivotType == "subsetSinglePivotPartial" 
							|| targetedCollectionPivotType == "subsetMultiPivotComplete" || targetedCollectionPivotType == "subsetMultiPivotPartial" 
							|| targetedCollectionPivotType == "subsetAddFilterSingle" || targetedCollectionPivotType == "subsetAddFilterMulti" ){
				
					targetCollection.type = "comparison";

					if( targetedCollectionPivotType == "subsetSinglePivotPartial" ){
						filter = this.partialPivotFilterFix(filterList[0], targetCollection.filterList[0]);  
						filterList = [ filter ];

						targetedCollectionPivotType = "comparisonCollectionExtend";//might be single, but doesn't matter

					}
					else if ( targetedCollectionPivotType == "subsetSinglePivotPartial" ){
						newFilterList = [] ; 
						for( var i = 0; i < filterList.length; i++ ){
							filter = this.partialPivotFilterFix(filterList[i], targetCollection.filterList[0]);   //
							newFilterList.push( filter ); 
						}	
						filterList = newFilterList;

						targetedCollectionPivotType = "comparisonCollectionExtend";//might be single, but doesn't matter

					}
					//NOTE- change this to be hierarchical once we have these functions
					//UIC this but theft should make overview+focus tree but now it makes comparison collection
					else if( targetedCollectionPivotType == "subsetAddFilterSingle" || targetedCollectionPivotType == "subsetAddFilterMulti" ){
						console_debug("HELLO!")
						 newFilterList = [];  


						 //newFilterList.push( targetCollection.filterList[0] ); 
						 for(var i = 0; i < filterList.length; i++){
						 	newFilterList.push( [] );
						 	for(var j = 0; j < targetCollection.filterList[0].length; j++){
						 		newFilterList[i].push(targetCollection.filterList[0][j]) ;
						 	}

						 	for(var j = 0; j < filterList[i].length; j++){
						 		newFilterList[i].push( filterList[i][j] );
						 

						 					 // for(var i = 0; i < targetCollection.filterList.length; i++){
						 	}
						 }

						 filterList = newFilterList;

					 	targetedCollectionPivotType = "comparisonCollectionExtend";//Note- need a different collection type 

					}

					 	targetedCollectionPivotType = "comparisonCollectionExtend"; 


				}

				returnObj = {
					isIndependent: false, 
					type: "referential", 
					subtype: targetedCollectionPivotType, 
					filterList: filterList, 
					attributeList: attributeList, 
					requestInfo: requestInfo,
					targetCollection: targetCollection
				};

			}
			else if( targetCollection.type == "grid"){


				returnObj = {
					isIndependent: false, 
					type: "referential", 
					subtype: targetedCollectionPivotType, 
					filterList: filterList, 
					attributeList: attributeList, 
					requestInfo: requestInfo,
					targetCollection: targetCollection
				};
			}
			else if( targetCollection.type == "b+c" ){

				console_debug( "B+C pivot");
				bnCCollectionPivotType = this.determineBnCPivotType(targetCollection, filterList, attributeList, requestInfo);

				if( bnCCollectionPivotType == "browseAndCompareAddFilter"){
					// newFilterList = [] ; 
					// for(var j = 0; j < targetCollection.filterList.length; j++) { 
					// 	for( var i = 0; i < filterList.length; i++ ){
					// 		filter = targetCollection.filterList[j].concat( filterList[i] );// this.partialPivotFilterFix(filterList[i], targetCollection.filterList[j]);   //
					// 		newFilterList.push( filter ); 
					// 	}
					// }	
					// filterList = newFilterList;
					// console.log( newFilterList );

					//do nothing it is handled in the creator
				}
				if( bnCCollectionPivotType == "browseAndCompareSplit"){
					//do nothing?

					// filterList = targetCollection.filterList; 

				}
				if( bnCCollectionPivotType == "browseAndComparePivotSubset" ){

					newFilterList = targetCollection.filterList; 
					
					for(i =0 ; i < filterList.length; i++){
						for(j =0; j < filterList[i].length; j++){

							for(k = 0; k < newFilterList.length; k++){
								newFilter = []; 
								for(l = 0; l < newFilterList[k].length; l++){
									if( newFilterList[k][l]["attr"] == filterList[i][j]["attr"])
									{
										newFilterList[k][l] = filterList[i][j]; 
									}
									// else{
									// 	newFilter.push(targetCollection.filterList[k][l]);
									// }
								}
								// newFilterList.push(newFilter);
							}
						}
					}	
					filterList = newFilterList;
					console.log( newFilterList );	
				}
				if( bnCCollectionPivotType =="browseAndCompareSubsetExtend"){

					newFilterList = [];
					for(i = 0; i < filterList.length; i++){
						newFilter= targetCollection.filterList[0];

						for(j = 0; j < filterList[i].length; j++){
							for(k = 0; k < newFilter.length; k++){
								if(newFilter[k]["attr"] == filterList[i][j]["attr"]){
									newFilter[k] = filterList[i][j];
								}
							}
						}
						newFilterList.push(newFilter);

					}

					filterList = newFilterList;
				}

				returnObj = {
					isIndependent: false, 
					type: "referential", 
					subtype: bnCCollectionPivotType, 
					filterList: filterList, 
					attributeList: attributeList, 
					requestInfo: requestInfo,
					targetCollection: targetCollection
				};



			}

			if( targetCollection.type == "complex" ){

				complexPivotType = this.determineComplexType(targetCollection, filterList, attributeList, requestInfo);
			


				//complexAddFilter


				returnObj = {
					isIndependent: false, 
					type: "referential", 
					subtype: complexPivotType, 
					filterList: filterList, 
					attributeList: attributeList, 
					requestInfo: requestInfo,
					targetCollection: targetCollection
				};

			}

			// if(isCompletePivot ){ 
			// 	if( filterList.length == 1 ){
			// 		returnObj = {
			// 			isIndependent: false, 
			// 			type: "referential", 
			// 			subtype: "subsetSinglePivotComplete", 
			// 			filterList: filterList, 
			// 			attributeList: attributeList, 
			// 			requestInfo: requestInfo,
			// 			targetCollection: targetCollection
			// 		}; 
			// 	}
			// 	else {
			// 		returnObj = {
			// 					isIndependent: false, 
			// 					type: "referential", 
			// 					subtype: "subsetMultiPivotComplete", 
			// 					filterList: filterList, 
			// 					attributeList: attributeList, 
			// 					requestInfo: requestInfo,
			// 					targetCollection: targetCollection
			// 				}; 
			// 	}
			// }
			// else {


			// }	

			return returnObj;

		}
	},

	shouldWeAggregateAll: function (requestInfo){

		for(i = 0; i < requestInfo.length; i++ ){
			if(requestInfo[i] == "aggregate" ){
				return true;
			}
		}
		return false; 

	},

	partialPivotFilterFix: function(filter, tcFilter){
		console.log("partial pivot filter fix");

		//scenario: UIC theft, this but Loop 
		//resolution: Loop theft
		var newFilterToAdd = []; // eg add theft  
		for(var i = 0; i < tcFilter.length; i++ ){ //loop through all the filters in the filter list
			newFilterToAdd.push(true); //default- got to add it 
		}
		console.log( newFilterToAdd );

		//turn off addition of ones already there 
		for(var i = 0; i < tcFilter.length; i++ ){
			for(var j = 0; j < filter.length; j++){
				if( tcFilter[i]["attr"] == filter[j]["attr"] ) 
					newFilterToAdd[i] = false;  //already there (Eg. attr= neighborhood, for diff vals.)
			}
		}

		console.log( newFilterToAdd );

		for(var i = 0; i < tcFilter.length; i++ ){
			if( newFilterToAdd[i] ){ //if need to add it (eg theft)
				console.log("ADDING ");
				filter.push( tcFilter[i] ); 
			}
		}

		return filter;

	}, 

	//TO DO
	//---- TYPES OF ACTIONS 
				//"subsetSinglePivotComplete"  , UIC, this but loop
				//"subsetMultiPivotComplete"   , UIC, this but loop and river north
				//"subsetSinglePivotPartial"   , UIC theft, this but battery
				//"subsetMultiPivotPartial"    , UIC theft, this but loop and river north
				//"subsetAddFilterSingle"      , UIC theft, this but just Mondays   AND  UIC theft, this but Monday and January
				//"subsetAddFilterMulti"       , UIC theft, this but Mondays and Tuesdays
				//"subsetRemoveFilterSingle"

				//"subsetCollectionSingleExtend" //same
				//"subsetCollectionMultiExtend" //same
				//"subsetCollectionSplit"
	determineSuxbsetPivotType: function(targetCollection, filterList, attributeList, requestInfo){
		//return "subsetSinglePivotComplete";


		//extend? split?  or change filter?
		console_debug("WHICH");
		console_debug(filterList[0][0]["attr"]);
		if( filterList[0][0]["attr"] == "all" ){

			for(i =0; i < requestInfo.length; i++){
				if( requestInfo[i] == "split")
					return "subsetCollectionSplit";
			}

			if( attributeList.length == 1 )
				return "subsetCollectionSingleExtend";
			
			return "subsetCollectionMultiExtend";

		}
 

		// Multi vs single:  UIC, this but Loop  VS    UIC, this but Loop and River North 
		//       for all entries in the new filter list, one of them has multiple vals 

		var isSingle = true; 
		var isMulti = false; 
		console_debug("DETERMINE SUBSET PIVOT TYPE"); 
		for( i = 0; i < filterList.length ; i++){
			for( j = 0; j < filterList[i].length; j++){


				for( k = i+1; k < filterList.length; k++){
					for( l = 0; l < filterList[k].length; l++){
						if( filterList[i][j]["attr"] ==  filterList[k][l]["attr"] ){
							isSingle = false; 
							isMulti = true; 
						}
					}
				}
			}	
		}

		if( isSingle )
			console_debug("SINGLE");
		if( isMulti )
			console_debug("MULTI");


		// Complete vs Partial:  
		//  UIC , this but Loop 										VS    UIC theft, this but Loop 
		//  UIC theft, this but Loop battery  							VS    UIC theft, this but Loop 
		//  UIC, this but Loop and River north 							VS    UIC theft, this but Loop and River North 
		//  UIC theft, this but Loop burglary and River north burglary  VS    UIC theft, this but Loop and River North 
		//  ????  UIC theft, this but UIC battery and UIC assault ???
		isComplete = true;
		isPartial = false; 

		// count matches of Filter attr values.  
		countMatch = 0;
		for( i = 0; i < filterList.length ; i++){ 
			for( j = 0; j < filterList[i].length; j++){

				for( k =0 ; k < targetCollection.filterList[0].length ; k++){

					//if the target collection filter attr matches the filter list attr, count match
					if( filterList[i][j]["attr"] == targetCollection.filterList[0][k]["attr"] ){
						countMatch = countMatch + 1; 
					}
				}
			}
		}

		// number of matches should equal the number of filters in the target collection * number of filter list entries
		if( countMatch < targetCollection.filterList[0].length * filterList.length ){
			isComplete = false;
			isPartial = true; 
		}

		if( isComplete )
			console_debug("COMPLETE");
		if( isPartial )
			console_debug("PARTIAL");

		//  NOTE- shows up partial on adds  
		isAdd = false; 
		if( isPartial ){  //may be an 'add', not a partial pivot 
			//simple case, not completely right (eg. UIC theft, this but UIC Monday is an add technically)
			if( countMatch == 0 )
				isAdd = true; 
		}


		if( isAdd )
			console_debug("correction : ADD-- NOT PARTIAL");

		if( isAdd )
		{
			if( isSingle )
				return "subsetAddFilterSingle";
			else
				return "subsetAddFilterMulti";
		}
		else if( isPartial ){
			if( isSingle )
				return "subsetSinglePivotPartial";
			else
				return "subsetMultiPivotPartial";
		}
		else if( isComplete ){
			if( isSingle )
				return "subsetSinglePivotComplete";
			else
				return "subsetMultiPivotComplete";
		}
		return "notDetermined";




			// var isCompletePivot = true; 
			// var isPartialPivot = false; 

			// if( filterList.length == 1 && targetCollection.filterList.length == 1 ){ // one subset 
			// 	if( filterList[0].length < targetCollection.filterList[0].length ){ //eg. from UIC theft, 'this but loop'
			// 		isCompletePivot = false; 
			// 		isPartialPivot = true; 

			// 		//Fix the filter 
			// 		var newFilterToAdd = []; 
			// 		for(var i = 0; i < targetCollection.filterList.length; i++ ){
			// 			newFilterToAdd.push(true); //default- gotta add it 
			// 		}

			// 		for(var i = 0; i < targetCollection.filterList.length; i++ ){
			// 			for(var j = 0; j < filterList.length; j++){
			// 				if( targetCollection.filterList[i]["attr"] == filterList[i]["attr"] )
			// 					newFilterToAdd[i] = false;  //already there (Eg. attr= neighborhood, for diff vals.)
			// 			}
			// 		}

			// 		for(var i = 0; i < targetCollection.filterList.length; i++ ){
			// 			if( newFilterToAdd ){

			// 			}
			// 		}





			// 	}
			// }									// isTargetACollection = true,
				
	},


	determineComparisonPivotType: function( targetCollection, filterList, attributeList, requestInfo ){

		console_debug("DETERMINE COMPARISON PIVOT TYPE"); 
		isSingle = true;
		isMulti = false;

		if( attributeList.length > 1 ){
			isSingle = false;
			isMulti = true; 
		}


		isSplit = false; 
		for(i = 0; i < requestInfo.length; i++){
			if(requestInfo[i] == "split")
				isSplit = true; 
		}


		isFilter = false; 
		filterSingle = false;
		filterMulti = false;
		
		filterForExtend = false; 
		isPartial = false;
		isComplete = false;

		// count matches of Filter attr values.  
		countMatch = 0;
		isExtend = true; 
		ifExtendIsComplete = true; 
		for( i = 0; i < filterList.length ; i++){ 
			for( j = 0; j < filterList[i].length; j++){

				for( k =0 ; k < targetCollection.filterList[0].length ; k++){

					//if the target collection filter attr matches the filter list attr, count match
					if( filterList[i][j]["attr"] == targetCollection.filterList[0][k]["attr"] ){
						countMatch = countMatch + 1; 
					}
				}
				if( countMatch == 0 )
					isExtend = false; 
				if( countMatch < targetCollection.filterList[0].length )
					ifExtendIsComplete = false; //if the number of matches at any point is not the same
			}
		}

		console_debug("COUNT MATCH " + countMatch);
		console_debug(filterList[0][0]["attr"]);
		if( filterList[0][0]["attr"] == "all"){
			console_debug("NO FILTER");
			isFilter = false;
		}
		else if( filterList[0][0]["attr"] != "all" ){  //extend or filter
			//assume filter for now
			isFilter = true;
			if( filterList.length > 1 ){
				filterMulti = true;
			}
			else{
				filterSingle = true; 
			}
		}

		if( isFilter ){

			if( isExtend )
			{
				if( ifExtendIsComplete )
					return "comparisonCollectionExtend";
				else{
					//do something! 
					return "comparisonCollectionExtendPartial";
				}
			}

			if(filterSingle)
				return "comparisonCollectionPivotFilterSingle";//"comparisonCollectionFilterSingle";
			else
				return "comparisonCollectionPivotFilterMulti";//"comparisonCollectionFilterMulti";
		}

		if( isSingle ){
			if( isSplit )
				return "comparisonCollectionSplit";
			else
				return "comparisonAttributePivotSingle";
		}
		if( isMulti ){ 
			if( isSplit )
				return "comparisonCollectionSplit";
			else
				return "comparisonAttributePivotMulti";
		}
	},

	determineTargetedPivotType: function( targetCollection, filterList, attributeList, requestInfo ){
		//if filter list has entries, it is a targetedCollectionSubsetPivot forming a comparison collection
		if( filterList[0][0]["attr"] != "all" ){
			type = this.determineSubsetPivotType(targetCollection, filterList, attributeList, requestInfo); 

			return type;
			
		}

		isSplit = false; 
		for(i = 0; i < requestInfo.length; i++){
			if(requestInfo[i] == "split")
				isSplit = true; 
		}

		//if attribute list has entries, it is a targetedCollectionAttributePivot forming a subset collection
		// isSingle = true;
		// isMulti = false;

		// if( attributeList.length > 1 ){
		// 	isSingle = false;
		// 	isMulti = true; 
		// }

		// //if split by
		// isSplit = false; 
		// for(i = 0; i < requestInfo.length; i++){
		// 	if(requestInfo[i] == "split")
		// 		isSplit = true; 
		// }
		if( !isSplit )
			return "targetedCollectionAttributePivot";
		else
			return "targetCollectionSplit";
	},

	determineBnCPivotType: function(targetCollection, filterList, attributeList, requestInfo ){

		isSplit = false; 
		for(i = 0; i < requestInfo.length; i++){
			if(requestInfo[i] == "split")
				isSplit = true; 
		}


	
		isFilter = false; 
		filterSingle = false;
		filterMulti = false;
		
		filterForExtend = false; 
		isPartial = false;
		isComplete = false;

			isPivot = false; 
			isExtend = false;

		if( filterList.length != 0 && filterList[0][0]["attr"] != "all"){	//is there a new filter

			conservedIsPivotedCount = 0; 
			console.log(targetCollection.conservedFilterList);
			for(i =0; i < targetCollection.conservedFilterList.length; i++){
				for( j =0 ; j < filterList.length; j++){
					for(k = 0; k < filterList[j].length; k++){
						if( targetCollection.conservedFilterList[i]["attr"] == filterList[j][k]["attr"] ){
							conservedIsPivotedCount++; 
						}

					}
				}
			}

			console.log("is this a complete copy+pivot?  Yes if count match > 0");
			console.log(conservedIsPivotedCount)
			
			countMatch = 0; 
			for(var i = 0; i < filterList.length; i++ ){
				for( j = 0; j < filterList[i].length; j++){

					for( k =0 ; k < targetCollection.filterList.length ; k++){

						for(l = 0; l < targetCollection.filterList[k].length ; l++){
							if( targetCollection.filterList[k][l]["attr"] == filterList[i][j]["attr"] )
								countMatch ++;

						}

 					}

				}

			}

			console.log(countMatch);


// count matches of Filter attr values.  
		// countMatch = 0;
		// isExtend = true; 
		// ifExtendIsComplete = true; 
		// for( i = 0; i < filterList.length ; i++){ 
		// 	for( j = 0; j < filterList[i].length; j++){

		// 		for( k =0 ; k < targetCollection.filterList[0].length ; k++){

		// 			//if the target collection filter attr matches the filter list attr, count match
		// 			if( filterList[i][j]["attr"] == targetCollection.filterList[0][k]["attr"] ){
		// 				countMatch = countMatch + 1; 
		// 			}
		// 		}
		// 		if( countMatch == 0 )
		// 			isExtend = false; 
		// 		if( countMatch < targetCollection.filterList[0].length )
		// 			ifExtendIsComplete = false; //if the number of matches at any point is not the same
		// 	}
		// }



			if( countMatch == 0 ){//must be a new filter 
				isFilter = true;
				if( filterList.length > 1 )
					filterMulti = true;
				else
					filterSingle = true;
			}
			else{
				if(conservedIsPivotedCount > 0){
					isPivot = true; 
				}
				if( countMatch > 0 ){
					isExtend = true;
				}

			}


			//is it adding a new filter criteria? 

			// type = this.determineSubsetPivotType( targetCollection, filterList, attributeList, requestInfo );
			console.log("DETERMINE BROWSE AND COMPARE TYPE");
			// console.log(type);
		}


		if( isFilter ){
			return "browseAndCompareAddFilter";
		}
		if( isSplit && attributeList.length == 1 ){
			return "browseAndCompareSplit";
		}
		if( isPivot ){
			return "browseAndComparePivotSubset"; 
		}
		if( isExtend ){
			return "browseAndCompareSubsetExtend";
		}
		if( attributeList.length > 0 ){
			return "browseAndCompareAttrExtend";
		}

	},

	determineComplexType: function(targetCollection, filterList, attributeList, requestInfo ){

		// isSplit = false; 
		// for(i = 0; i < requestInfo.length; i++){
		// 	if(requestInfo[i] == "split")
		// 		isSplit = true; 
		// }

		isFilter = false; 
		filterSingle = false;
		filterMulti = false;

		if( filterList[0][0]["attr"] != "all" ){
			type = this.determineSubsetPivotType(targetCollection, filterList, attributeList, requestInfo); 


			console.log("complex type: " + type);
			if( type == "subsetAddFilterSingle"  )
				return "complexAddFilter";
			if( type == "subsetAddFilterMulti")
				return "complexAddFilterMulti";
			if( type == "subsetSinglePivotComplete"){
				return "complexSinglePivotComplete";
			}
			if( type == "subsetMultiPivotComplete"){
				return "complexMultiPivotComplete";
			}
			
		}
		


		// if( isSplit && attributeList.length == 1 ){
		// 	return "browseAndCompareSplit";
		// }

	},


	//   create an aggregated filter .  Eg filter1 = [ {"attr": "neighborhood", "vals": ["uic", "loop"] }, {"attr": "crime-type", "vals": ["battery", "theft"] } ];
	//   from: filterAttributes = [ "neighborhood", "neighborhood", "crime-type", "crime-type"]
	aggregateFilters: function(filterAttributes, filterValues ){

		console_debug("aggregating filters together");

		newAggregatedFilterList = [] ; 
		for( i = 0; i < filterAttributes.length; i++ ){ //for all filter attributes
			encountered = false; 
			for( j = 0; j < newAggregatedFilterList.length; j++){
				if( newAggregatedFilterList[j]["attr"] == filterAttributes[i] ){
					encountered = true;
					newAggregatedFilterList[j]["vals"].push( filterValues[i] ); 
				}
			}
			if( !encountered ){
				newAggregatedFilterList.push( { "attr": filterAttributes[i], "vals": [filterValues[i] ] }); 
			}
		}

		console_debug("aggregated filter");
		console_debug(newAggregatedFilterList); 

		return newAggregatedFilterList;
	},

	

	//from: aggregatedFilter = [ {"attr": "neighborhood", "vals": ["uic", "loop"] }, {"attr": "crime-type", "vals": ["battery", "theft"] } ];
	//to: 
	// filter1 =  [ {"attr": "neighborhood", "vals": ["uic"] }, {"attr": "crime-type", "vals": ["battery"] } ]
	// filter2 = [ {"attr": "neighborhood", "vals": ["loop"] }, {"attr": "crime-type", "vals": ["battery"] } ]
	// filter3 =  [ {"attr": "neighborhood", "vals": ["uic"] }, {"attr": "crime-type", "vals": ["theft"] } ]
	// filter4 =  [ {"attr": "neighborhood", "vals": ["loop"] }, {"attr": "crime-type", "vals": ["theft"] } ]
	makeFilterObjects: function( aggregatedFilter ){
		console_debug("in make filter objects"); 

		// x = ["1", "2"]; 
		// y = ["A", "B", "C"];
		// z = ["a", "b", "c", "d", "e"];
		// // zz = ["3", "4", "5", "6", "7"];
		// lists = [ y , x, z]; 

		///////////////

		var lists = [];
		for(i = 0; i < aggregatedFilter.length; i++){
			lists.push( aggregatedFilter[i]["vals"] );
		}

		var result = [];
		this.generatePermutations( lists, result, 0, "" );

		console_debug(result); 

		var resultArr = [];
		for(i = 0; i< result.length; i++){
			tokens = result[i].substring(0,result[i].length-1).split(",");
			resultArr.push( tokens ); 
		}

		console_debug(resultArr);

		filterList = []; 
		for(i = 0; i < resultArr.length; i++){
			filter = []; 
			for(j = 0; j < resultArr[i].length; j++){
				filter.push( { "attr": aggregatedFilter[j]["attr"],  "vals": [ resultArr[i][j] ]});
			}
			filterList.push( filter );
		}

		for(i = 0; i <filterList.length; i++)
			console_debug(filterList[i]);

		return filterList;

	},

	//RECURSION!
	// given the aggregated filters generate all combinations
	generatePermutations: function( lists, result, depth, current){
		if( depth == lists.length  ){
			//console_debug( "terminating" ); 
			//console_debug(current);
			result.push( current ); 
			return; 
		}

		for(var i = 0; i < lists[depth].length; i++){
			//console_debug( "         " + lists[depth][i] ); 
			newDepth = depth+1; 
			this.generatePermutations( lists, result, newDepth, current + lists[depth][i] + "," );
		}

	},

	isThereOneSubset: function(filterAttributes){
		var filterOnAttr = [];
		for(var i = 0; i < this.attributeAndValues.length; i++){
			filterOnAttr.push(0); 
			for(var j = 0; j < filterAttributes.length; j++){
				if( this.attributeAndValues[i]["name"] == filterAttributes[j] ){
					filterOnAttr[i]++; 
					if( filterOnAttr[i] > 1 )
						return false; 
				}
			}
		}

		return true; 

	},

	isAttrCatchall: function( attributeList, requestInfo ){

		// if( requestInfo == null )
		// 	return false; 

		for(i = 0; i < requestInfo.length; i++ ){
			if(requestInfo[i] == "where" ){
				//console.log("CATCHALL");
				return true;
			}
			if( requestInfo[i] == "when"){
				//console.log("CATCHALL");

				return true; 
			}
		}

		return false; 
	},


	fillInAttributeCatchall: function( attributeList, filterAttributes, requestInfo ){
		candidateAttributes = []; 
		for(i = 0; i < requestInfo.length; i++ ){
			if(requestInfo[i] == "where" ){
				candidateAttributes.push("neighborhood");
				candidateAttributes.push("location-type");
				//candidateAttributes.push("map");			
			}
			if( requestInfo[i] == "when"){
				candidateAttributes.push("year");
				candidateAttributes.push("month-of-the-year");
				candidateAttributes.push("day-of-the-week");
				candidateAttributes.push("time-of-the-day");
			}
		}

		//console.log(candidateAttributes);

		//if it is the same as a filter attribute, skip it. 
		for(i = 0; i < filterAttributes.length; i++){
			for(j =0; j < candidateAttributes.length; j++){
				if( candidateAttributes[j] == filterAttributes[i] ){
					candidateAttributes[j] = "skip"; 
				}
			}
		}

		for(i = 0; i < attributeList.length; i++){
			for(j =0; j < candidateAttributes.length; j++){
				if( candidateAttributes[j] == attributeList[i] ){
					candidateAttributes[j] = "skip"; 
				}
			}
		}

		var attributeList = []; 
		for(i = 0; i < candidateAttributes.length; i++){
			for(j =0; j < this.attributeAndValues.length; j++){
				if( candidateAttributes[i] == this.attributeAndValues[j]["name"]){
					attributeList.push( this.attributeAndValues[j]["name"] );
				}
			}
		}

		//console.log(attributeList);
		return attributeList;
	},


	//TODO 
	isIndependent: function(requestInfo){
		console_debug("request info: ");
		console_debug(requestInfo);

		for( i = 0; i < requestInfo.length; i++){
			if( requestInfo[i] == "this-but")
				return false;
			if( requestInfo[i] == "these-but")
				return false;
		}
		return true;
	},


	determineIndependentType: function( filterList, attributeList, requestInfo, aggregatedFilter,  viewTypeParams ) { //filterAttributes, filterValues, attributeList, requestInfo){

		console.log("REQUEST INFO");
		console.log(requestInfo);

		console.log(filterList);
		oneSubset = false;
		if( filterList.length == 1 ) 
			oneSubset = true;

		console_debug("IS THERE ONE SUBSET?");
		console_debug(oneSubset);  

		console_debug("filter list");
		for(var i =0; i < filterList.length; i++)
			console_debug(filterList[i]);  


		console_debug("attribute list");
		console_debug(attributeList);  

		console_debug("request info");
		console_debug(requestInfo);  


		isSplit = this.determineIsSplit(attributeList, requestInfo);

		//one attribute, 1 or no subsets
		if( oneSubset &&  ( attributeList.length == 1 || (attributeList.length == 2 && isSplit) ) ){
			return "target";
		}
		if( oneSubset &&  ( attributeList.length == 0 || this.isAttrCatchall(attributeList, requestInfo) ) ){
			return "browse";
		}
		if( oneSubset && attributeList.length > 1 ){
			return "complex";//"browse";//"complex";//CHANGE 
		}
		if( !oneSubset && ( attributeList.length == 1 || (attributeList.length == 2 && isSplit) ) ){
			//if at least 2 attr have 2+ vals in aggr filter AND not 'together'
			if( !this.shouldWeAggregateAll( requestInfo )){
				countHasTwoOrMoreVals = 0;
				for(i = 0; i < aggregatedFilter.length; i++){
					if( aggregatedFilter[i]["vals"].length >= 2 )
						countHasTwoOrMoreVals++; 
				}
				if( countHasTwoOrMoreVals >= 2 ){
					console.log("COMPARE BY COMPARE GRID");
					return "compare_compare_grid";
				}
			}
			return "comparison"; 
		}
		if( !oneSubset && attributeList.length > 1 ){
			return "b+c"; 
		}
		if( !oneSubset && attributeList.length == 0 ){
			return "b+c"; 
		}


		return "error"; 
	}

};

var debug = true; 

function console_debug(string){
	if(debug )
		console.log(string);

};

var debug2 = false;
function console_debug2(string){
	if(debug2 )
		console.log(string);

};



