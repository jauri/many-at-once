// Loadash....
var loadash = require('lodash');
const colorbrewer = require('colorbrewer');

module.exports = {

 	attributeAndValues: null, //these are the attributes and values 
        //format:  name type    list    plotType            
    datasets: null, //this is where all the data is 
    filenames: null,
    whatDataContains: null,

	listOfCollections: [],
    collectionCount: 0,
    lastCreatedCollection: 0,

    listOfViews : [],
    viewCount : 0,

    hashFromIdToIdx : {},
    hashFromIdToIdxInitialized : false,

    colorAssignment : {}, 

    colorSchemes : {

        "classic20": [ "#1f77b4", "#aec7e8", "#ff7f0e", "#ffbb78", "#2ca02c", "#98df8a", 
        "#d62728", "#ff9896", "#9467bd", "#c5b0d5", "#8c564b", "#c49c94", "#e377c2", "#f7b6d2", "#7f7f7f", 
        "c7c7c7", "#bcbd22", "#dbdb8d", "#17becf", "#9edae5" ],

        "classic40": ["#1f77b4", "#aec7e8", "#ff7f0e", "#ffbb78", "#2ca02c", "#98df8a", 
        "#d62728", "#ff9896", "#9467bd", "#c5b0d5", "#8c564b", "#c49c94", "#e377c2", "#f7b6d2", "#7f7f7f", 
        "c7c7c7", "#bcbd22", "#dbdb8d", "#17becf", "#9edae5", "#1f77b4", "#aec7e8", "#ff7f0e", "#ffbb78", "#2ca02c", "#98df8a", 
        "#d62728", "#ff9896", "#9467bd", "#c5b0d5", "#8c564b", "#c49c94", "#e377c2", "#f7b6d2", "#7f7f7f", 
        "c7c7c7", "#bcbd22", "#dbdb8d", "#17becf", "#9edae5"  ],

        "classicGreenOrange": [ "#32a251", "#ff7f0f", "#3cb7cc", "#ffd94a", "#39737c"]


    },


    init: function(datasets, attributeAndValues, filenames, whatDataContains){
        // console.log(theDataset[0]); 
        this.attributeAndValues = attributeAndValues; 
        this.datasets = datasets; 
        this.filenames = filenames;
        this.whatDataContains = whatDataContains;

        this.assignColors(this.attributeAndValues);


    },

    assignColors: function( attrAndValues ){

        this.colorAssignment[ "null" ] = "lightgray"; 

        for( var i = 0; i < attrAndValues.length; i++){
            list = attrAndValues[i]["listOfLabels"].split(",");
            
            if( attrAndValues[i]["color"] == "Tableau20" ){
                colorArr = this.colorSchemes["classic20"]; 
            }
            else if( attrAndValues[i]["color"] == "classic20" ){
                colorArr = this.colorSchemes["classic20"]; 
            }
            else if( attrAndValues[i]["color"] == "classic40" ){
                colorArr = this.colorSchemes["classic40"]; 
            }
            else{
                colorArr = this.getColorArrayByColorBrewerName( attrAndValues[i]["color"], list.length, false  ); 
            }

            for( var j = 0; j < list.length; j++){
                console.log("color: " + list[j] );
                this.colorAssignment[ this.getColorKey( attrAndValues[i]["name"] , list[j] ) ] = colorArr[j]; 
                console.log("color: " + colorArr[j] );

            }
        }

        console.log("COLORS " );
        console.log( this.colorAssignment );
    },

    getColorKey( attrName, attrVal ){

        return (attrName + "-" + attrVal);
    },

    colorLookupForList: function ( name, domainArr ){
        var arrToReturn = []; 
        for(var i =0 ; i < domainArr.length; i++){
            key = this.getColorKey( name, domainArr[i]  );
            console.log("key " + key);
            console.log("COLOR! " + this.colorAssignment[ key ] );
            arrToReturn.push( this.colorAssignment[ key ] );
        }

        console.log(arrToReturn);
        return arrToReturn;
    },

    ////////////////////////////////////////////////////////////////////////////////

    initCollection: function(type, filterList, attributeList, secondaryAttributeList){
        col = { 
        	"views": [], 
        	"id": "collectionId"+this.collectionCount,
            "number":  this.collectionCount,
        	"type": type,
        	"filterList": filterList,
        	"attributeList": attributeList,
            "secondaryAttributeList": secondaryAttributeList,
        	"isStandalone": true,
        	"isAParent": false,
        	"isAChild": false,
        	"parentId": null,
        	"childCollectionList": []
        };

        if( secondaryAttributeList == null )
            col.secondaryAttributeList = [];


        this.lastCreatedCollection = this.collectionCount;
        this.collectionCount = this.collectionCount + 1;
        this.storeCollection(col);
        return col;
    },

    storeCollection: function(collection){
        this.listOfCollections.push(collection); 
        // console.log("PRINT COLLECTION");
        // console.log("##############################");
        // console.log("##############################");
        // console.log(this.listOfCollections);
    }, 

    printAllCollections: function(){
    	 console_debug("Printing all collections");
    	 for(var i = 0; i < this.listOfCollections.length; i++){
            console_debug(this.listOfCollections[i]);
        }
    },

   
    getCollection: function (collectionId) {
        for(var i = 0; i < this.listOfCollections.length; i++){
            if( this.listOfCollections[i]["id"] == collectionId )
                return this.listOfCollections[i];
        }
        return null;
    },

    getCollectionByNumber: function(collectionNumber){
        if( this.listOfCollections.length <= collectionNumber )
            return null;
        return this.listOfCollections[ collectionNumber ];
    },

    getMostRecentCollection: function(){
    	if( this.listOfCollections.length == 0 )
    		return null;
    	return this.listOfCollections[ this.listOfCollections.length-1 ];
    },

    fromViewsToCollection: function(viewList){
        console_debug3("FROM VIEWS TO COLLECTION");
        console_debug3(viewList);
        console_debug3(this.listOfCollections.length);
        countCollection = []; 
        for(var x = 0; x < this.listOfCollections.length; x++ ){
            countCollection.push(0); 
            for(var y = 0; y < this.listOfCollections[x]["views"].length; y++){
                for(var z = 0; z < viewList.length; z++){
                    console_debug3(viewList[z]);
                    console_debug3( this.listOfCollections[x]["views"][y]["id"] );
                    if( viewList[z] == this.listOfCollections[x]["views"][y]["id"] ){
                        console_debug3("VIEW:");
                        console_debug3(viewList[z]);
                        console_debug3("collection:");
                        console_debug3(this.listOfCollections[x]["id"]);
                        console_debug3("x: " + x + " y " + y + " z "+ z);
                        countCollection[ countCollection.length-1 ]++; //incremenet 
                    }
                }
            }
        }

        console_debug3(countCollection);


        // countCollection = []; 
        // for(var x = 0; x < this.listOfCollections.length; x++ ){
        //     countCollection.push(0); 
        //     for(var y = 0; y < viewList.length; y++){
        //         for(var z = 0; z < viewList[y]["collectionList"].length; z++){
        //             if( viewList[y]["collectionList"][z] == this.listOfCollections[x]["id"] )
        //                 countCollection[ countCollection.length-1 ] = countCollection[ countCollection.length-1 ] +1; //incremenet 
        //         }
        //     }
        // }

        //find most common
        mostCollection = null;
        maxCount = -99999; 
        for(var x = 0; x < countCollection.length; x++){
            if( countCollection[x] > maxCount ){
                maxCount = countCollection[x];
                mostCollection = this.listOfCollections[x]; 
            }

        }
        console_debug3("most: ");
        console_debug3( mostCollection);
        return mostCollection;//just return the most frequent collection
        
    },

    areAllInSameCollection: function( viewList ){
        console_debug3("ARE ALL IN THE SAME COLLECTION");
        console_debug3(viewList);
        console_debug3(this.listOfCollections.length);

        countCollection = []; 
        for(var x = 0; x < this.listOfCollections.length; x++ ){
            countCollection.push(0); //init 0
            for(var y = 0; y < this.listOfCollections[x]["views"].length; y++){//for all views in collection
                for(var z = 0; z < viewList.length; z++){ //for all views referenced
                    console_debug3(viewList[z]);
                    console_debug3( this.listOfCollections[x]["views"][y]["id"] );
                    if( viewList[z] == this.listOfCollections[x]["views"][y]["id"] ){ //if the ref view is in the collection
                        console_debug3("VIEW:");
                        console_debug3(viewList[z]);
                        console_debug3("collection:");
                        console_debug3(this.listOfCollections[x]["id"]);
                        console_debug3("x: " + x + " y " + y + " z "+ z);
                        countCollection[ countCollection.length-1 ]++; //incremenet count
                    }
                }
            }
        }

        //at this point, if all views are in a collection, the county for the collection will be equal to the number of ref views 

        console_debug3(countCollection);


        // countCollection = []; 
        // for(var x = 0; x < this.listOfCollections.length; x++ ){
        //     countCollection.push(0); 
        //     for(var y = 0; y < viewList.length; y++){
        //         for(var z = 0; z < viewList[y]["collectionList"].length; z++){
        //             if( viewList[y]["collectionList"][z] == this.listOfCollections[x]["id"] )
        //                 countCollection[ countCollection.length-1 ] = countCollection[ countCollection.length-1 ] +1; //incremenet 
        //         }
        //     }
        // }

        //find most common
        mostCollection = null;
        maxCount = -99999; 
        for(var x = 0; x < countCollection.length; x++){
            if( countCollection[x] > maxCount ){
                maxCount = countCollection[x];
                mostCollection = this.listOfCollections[x]; 
            }

        }

        allInOne = ( mostCollection.views.length == viewList.length ); 

        return {"allInOne": allInOne, "theCollection": mostCollection }; 

    }, 

    // makeNewCollectionAndAssignType: function( viewList ){

    //     //how many filters

    //     //how many attributes
    //     attributeList = []; 
    //     for( i = 0; i < viewList.length; i++ ){
    //         countAlreadyThere = 0; 
    //         for(j =0; j < attributeList.length; j++){
    //             if( viewList[i]["attribute1"] == attributeList[j] ){
    //                 countAlreadyThere++; 
    //             } 
    //         }
    //         if( countAlreadyThere == 0 ){
    //             attributeList.push(viewList[i]["attribute1"]);
    //         }
    //     }

    //     //how many secondary attributes 
    //     secondaryAttributeList = []; 
    //     for( i = 0; i < viewList.length; i++ ){
    //         countAlreadyThere = 0; 
    //         for(j =0; j < secondaryAttributeList.length; j++){
    //             if( viewList[i]["attribute2"] == secondaryAttributeList[j] ){
    //                 countAlreadyThere++; 
    //             } 
    //         }
    //         if( countAlreadyThere == 0 ){
    //             secondaryAttributeList.push(viewList[i]["attribute2"]);
    //         }
    //     }



    //     if( attributeList.length == 1 && secondaryAttributeList.length == 0 ){ //compare type

    //     }


    // },

    updateCollection: function( priorCollection ){

        this.listOfCollections[ this.listOfCollections.length -1 ] = loadash.cloneDeep( priorCollection ) ;
    },

    addViewToCollection: function(collection, view){
        collection["views"].push(view);
        // view["   "].push( collection.id ); 
    },

    addViewToFrontOfCollection: function(collection, view){
        collection["views"].splice(0, 0, view);
    },

    insertViewInCollection: function( collection, view, insertIdx){
        if( insertIdx >= collection["views"].length ){
            this.addViewToCollection(collection, view);
        }
        else{
            collection["views"].splice(insertIdx, 0, view);
        }
    },

    getViewsFromCollection: function(collection){
        return collection["views"];
    },


        //subset collection: conserved Filter, no conserved attributes 
        //comparison collection: conserved attr1, maybe conserved attr2
        //


    setCollectionConserved: function(collection, conservedFilter, conservedAttr1, conservedAttr2){
        collection.conserved = { 
            "filter": conservedFilter,
            "attribute1": conservedAttr1,
            "attribute2": conservedAttr2
        };

    },

    setCollectionVaried: function(collection, variedFilterList, variedAttr1, variedAttr2){
        collection.varied = { 
            "filterList": variedFilterList,
            "attribute1_list": variedAttr1,
            "attribute2_list": variedAttr2
        };

    },

//////////////////// -----  HELPER FUNCTIONS ------  /////////////////////////////////////     

    getAttributeName: function(attr){
        if(attr == "all")
            return "all";

        return attr["name"]; 
    },

    getAttributeList: function(attr){
        if(attr == "all")
            return "all";

        return attr["list"].split(",");
    },

    getAttributeType: function(attr){

        if(attr == "all")
            return "all";

        return attr["type"]; 
    },

    getAttributePlotType: function(attr){
        if(attr == "all")
            return "all";

        return attr["plotType"]; 
    },

    getAttributeFromName: function(attrName){
        if(attrName == "all")
            return "all";
        for(var i = 0; i < this.attributeAndValues.length; i++){
            if( this.attributeAndValues[i]["name"] == attrName )
                return this.attributeAndValues[i]; 
        }
    },

    getAttributeListFromName: function(attrName){

        if(attrName == "all")
            return "all";

        attr = this.getAttributeFromName( attrName );
        return this.getAttributeList( attr ); 
    },

    getAttributeTypeFromName: function(attrName){

        if(attrName == "all")
            return "all";

        attr = this.getAttributeFromName( attrName );
        return this.getAttributeType(attr); 
    },

    getAttributePlotTypeFromName: function(attrName){

        if(attrName == "all")
            return "all";

        attr = this.getAttributeFromName( attrName );
        return this.getAttributePlotType(attr); 
    },




//////////////////// -----  CREATE collections ------  ///////////////////////////////////// 


    createTargeted(filter, attributeList, viewTypeParams, secondaryAttributeList, theInfo){//, secondaryAttributeList, viewTypeParams){
        console.log("create targeted");
        console.log("filter:"); 
        console.log(filter);
        console.log("attributeList:"); 
        console.log(attributeList);
        console.log("secondaryAttribute:"); 

        console.log(secondaryAttributeList); 

        collection = null;
        if( attributeList.length == 1 && secondaryAttributeList.length == 0){
    	   collection = this.initCollection("targeted", [ filter ], attributeList );//[0], attributeList[1]); 
            if( theInfo )
                theInfo["targetCount"]++; 
        }
        else if( attributeList.length == 2 ){
            attList1 = [ attributeList[0] ];
            attList2 = [attributeList[1]];
            collection = this.initCollection("multifaceted", [ filter ], attList1, attList2 );//[0], attributeList[1]); 
            if( theInfo )
                theInfo["multifacetedCount"]++; 
        } else if( secondaryAttributeList.length == 1 ){
            attList1 = [ attributeList[0] ];
            attList2 = [secondaryAttributeList[0]];
            console.log("secondaryAttributeList.length == 1 " )
            console.log(attList1);
            console.log(attList2);
            if( theInfo )
                theInfo["multifacetedCount"]++; 
            collection = this.initCollection("multifaceted", [ filter ], attList1, attList2 );
        } else if( secondaryAttributeList.length > 1 ){
            var collection = this.initCollection( "sub+attr", [ filter ], [attributeList[0]], secondaryAttributeList );
            if(theInfo)
                theInfo["sub_attrCount"]++; 

            for(var i = 0 ; i < secondaryAttributeList.length; i++){
                col = this.createTargeted( filter, attributeList, viewTypeParams, [ secondaryAttributeList[i]] );

                console.log( col["list"][0]["vis_collection"][0] );
                this.addViewToCollection(collection, col["list"][0]["vis_collection"][0] ); 
            }

            this.fixAggFilter( filterList, attributeList, collection ); 
            this.layoutViews(collection);

            if( !theInfo )
                theInfo = {}; 

            return {
                "count": 1, 
                "theInfo": theInfo,
                "list": [ 
                    { 
                        "vis_collection": this.getViewsFromCollection(collection), 
                        "collectionId": collection.number,
                         "type": "new_collection", 
                         "collection": this.getViewsFromCollection(collection), 
                         "collectionType": collection.type,
                         "collectionFilterList": collection.filterList,
                         "collectionAttributeList": collection.attributeList,
                         "collectionSecondaryAttributeList": collection.secondaryAttributeList,
                         "collectionAggrFilterList": collection.aggregatedFilter,
                         "replaceCollection": false,
                         "gridSpec": collection.gridSpec,
                         "category": collection.type,


                    },
                ]
            };

        }

        console.log(viewTypeParams);
        console.log(attributeList);
        viewType = null;

        //not using..        
        // this.setCollectionConserved(collection, filter, attributeList[0], attributeList[1]); 
        // collection.conservedFilter = true;
        // collection.conservedAttr1 = true;
        // collection.conservedAttr2 = true; 

       // collection["type"] = "targeted";
        // collection["filterList"] = [ ];
        // collection["filterList"].push(filter); 
        // collection["attributeList"] = attributeList; 
        // collection["isStandalone"] = true; 
        // collection["isAParent"] = false; 
        // collection["isAChild"] = false; 
        // collection["parentId"] = null; 
        // collection["childCollectionList"] = [];

        // console_debug(filter);
        //collection.push ( this.makeChart(filterAttributes, filterValues, this.getAttributeFromName(attributeList[0]), this.getAttributeFromName(attributeList[1]) ) ); 

        // primaryAttribute = attributeList[0];
        // secondaryAttribute = null;
        // if( attributeList.length > 1 ){
        //     secondaryAttribute = attributeList[1];
        //     collection["type"] = "multifaceted";
        //     collection.secondaryAttributeList.push(secondaryAttribute);
        // } else if( secondaryAttributeList && secondaryAttributeList.length == 1 ){
        //     secondaryAttribute = secondaryAttributeList[0];
        //     collection["type"] = "multifaceted";
        //     collection.secondaryAttributeList.push(secondaryAttribute);
        // }

        primaryAttribute = collection.attributeList[0];
        secondaryAttribute = null;
        if( collection.secondaryAttributeList.length > 0 )
            secondaryAttribute = collection.secondaryAttributeList[0];


        viewType = null;
        if( viewTypeParams.length > 0 ){
            viewType = viewTypeParams[0];
        }

        this.addViewToCollection(collection, this.makeChart(filter, this.getAttributeFromName(primaryAttribute), this.getAttributeFromName(secondaryAttribute) , viewType ));
       
        this.fixAggFilter( filterList, attributeList, collection ); 
        this.layoutViews(collection);

        if( !theInfo )
            theInfo = {}; 

        return {
            "count": 1, 
             "theInfo": theInfo,
            "list": [ 
                { 
                    "vis_collection": this.getViewsFromCollection(collection), 
                    "collectionId": collection.number,
                     "type": "new_collection", 
                     "collection": this.getViewsFromCollection(collection), 
                     "collectionType": collection.type,
                     "collectionFilterList": collection.filterList,
                     "collectionAttributeList": collection.attributeList,
                     "collectionSecondaryAttributeList": collection.secondaryAttributeList,
                     "collectionAggrFilterList": collection.aggregatedFilter,
                     "replaceCollection": false,
                     "gridSpec": collection.gridSpec,
                     "category": collection.type,

                },
            ]
        };

    },

    // createMultifaceted(filter, attributeList){

    // },

    defineAttrList: function(filter, viewTypeParams, secondaryAttributeList, theInfo){
        var attrList = []; 
        //which variables remain?  
        var includeVariables = []; //which ones should have a chart
        for( var i = 0; i < this.attributeAndValues.length; i++){
            includeVariables.push(true);  //start withall 
        }

        for( var i = 0; i < this.attributeAndValues.length; i++){
            //console.log("i = " + i + " " + this.attributeAndValues[i]["name"]);
            for( var j = 0; j < filter.length; j++){
                //console.log("j = " + j + " " +  variables[j]);
                if( this.attributeAndValues[i]["name"] == filter[j]["attr"] ){
                    includeVariables[i] = false; 

                }
            }
        }

        for( var i = 0; i < this.attributeAndValues.length; i++ ){
            if( includeVariables[i] ){
                console_debug("----- make a chart for attribute: ")
                console_debug(this.attributeAndValues[i]);
            
                attrList.push( this.attributeAndValues[i]["name"] ); //add it 
            }
            else
                console_debug("----- skip " + this.attributeAndValues[i]["name"]);
        }
        return attrList;

    },

    createSubsetComplete: function(filter, viewTypeParams, secondaryAttributeList, theInfo){ 

    	//var collection = this.initCollection("browse", [ filter ], [] ); 
        console.log("SUBSET COMPLETE!"); 

        console.log(secondaryAttributeList); 
        var collection = null;
        attrList2 = [];
        if( secondaryAttributeList.length == 0 ){//attributeList.length == 1 ){
           collection = this.initCollection("browse", [ filter ], [] );//[0], attributeList[1]); 
            if( theInfo )
                theInfo["browseCount"]++;
        } else if( secondaryAttributeList.length == 1 ){
            //attList1 = [ attributeList[0] ];
            attrList2 = [secondaryAttributeList[0]];
            collection = this.initCollection("multifacetedBrowse", [ filter ], [], attrList2 );
            if( theInfo )
                theInfo["multifacetedBrowseCount"]++;
        } else if( secondaryAttributeList.length > 1 ){
            var collection = this.initCollection( "multifacetedBrowseBySubAttr", [ filter ], [], secondaryAttributeList );
            collection.attributeList = this.defineAttrList( filter, viewTypeParams, secondaryAttributeList);
            if( theInfo )
                theInfo["multiBrowse_SubAttrCount"]++; 

            for(var i = 0 ; i < secondaryAttributeList.length; i++){
                col = this.createSubsetComplete( filter, viewTypeParams, [ secondaryAttributeList[i] ] );
                console.log( col["list"][0]["vis_collection"][0] );
                for(v = 0; v < col["list"][0]["vis_collection"].length; v++)
                    this.addViewToCollection(collection, col["list"][0]["vis_collection"][v] ); 
                // theInfo["browseCount"]--; 
            }


            this.fixAggFilter( filterList, collection.attributeList, collection ); 
            this.layoutViews(collection);

            if( !theInfo )
                theInfo = {}; 

            return {
                "count": 1, 
                "theInfo": theInfo,
                "list": [ 
                    { 
                        "vis_collection": this.getViewsFromCollection(collection), 
                        "collectionId": collection.number,
                         "type": "new_collection", 
                         "collection": this.getViewsFromCollection(collection), 
                         "collectionType": collection.type,
                         "collectionFilterList": collection.filterList,
                         "collectionAttributeList": collection.attributeList,
                         "collectionSecondaryAttributeList": collection.secondaryAttributeList,
                         "collectionAggrFilterList": collection.aggregatedFilter,
                         "replaceCollection": false,
                         "gridSpec": collection.gridSpec,
                         "category": collection.type,
                    },
                ]
            };

        }


        this.setCollectionConserved(collection, filter, null, null); 
        collection.conservedFilter = true;
        collection.conservedAttr1 = false;
        collection.conservedAttr2 = false; 

        // collection["type"] = "subsetComplete"; 
        // collection["filterList"] = [ ];
        // collection["filterList"].push(filter); 
        // collection["attributeList"] = null; 
        // collection["isStandalone"] = true; 
        // collection["isAParent"] = false; 
        // collection["isAChild"] = false; 
        // collection["parentId"] = null;  
        // collection["childCollectionList"] = [];

        console.log("CREATE SUBSET COMPLETE");
        console.log("-- filter by ");
        console.log(filter);


        console.log("-- producing views...... ");  

        //which variables remain?  
        var includeVariables = []; //which ones should have a chart
        for( var i = 0; i < this.attributeAndValues.length; i++){
            includeVariables.push(true);  //start withall 
        }

        for( var i = 0; i < this.attributeAndValues.length; i++){
            //console.log("i = " + i + " " + this.attributeAndValues[i]["name"]);
            for( var j = 0; j < filter.length; j++){
                //console.log("j = " + j + " " +  variables[j]);
                if( this.attributeAndValues[i]["name"] == filter[j]["attr"] ){
                    includeVariables[i] = false; 

                }
            }
        }

        for( var i = 0; i < this.attributeAndValues.length; i++ ){
            if( includeVariables[i] ){
                console_debug("----- make a chart for attribute: ")
                console_debug(this.attributeAndValues[i]);
                view = this.makeChart( filter, this.attributeAndValues[i], this.getAttributeFromName(attrList2[0]) ); 
                this.addViewToCollection(collection, view);

                collection["attributeList"].push( this.attributeAndValues[i]["name"] ); //add it 
            }
            else
                console_debug("----- skip " + this.attributeAndValues[i]["name"]);
        }

        this.setCollectionVaried(collection, null, collection["attributeList"], null ); 


        // console.log(collection.length);
        //console.log(JSON.stringify(collection));
        console.log("done!"); 

        this.fixAggFilter( filter, [], collection ); 
        this.layoutViews(collection);

        if( !theInfo )
            theInfo = {}; 

        // return this.getViewsFromCollection(collection);
        // return {"vis_collection": this.getViewsFromCollection(collection), "collectionNumber": collection.number};
        return {
            "count": 1, 
            "theInfo": theInfo,
            "list": [ 
                { 
                    "vis_collection": this.getViewsFromCollection(collection), 
                    "collectionId": collection.number,
                     "type": "new_collection", 
                     "collection": this.getViewsFromCollection(collection), 
                     "collectionType": collection.type,
                     "collectionFilterList": collection.filterList,
                     "collectionAttributeList": collection.attributeList,
                     "collectionSecondaryAttributeList": collection.secondaryAttributeList,
                     "collectionAggrFilterList": collection.aggregatedFilter,
                     "replaceCollection": false,
                    "gridSpec": collection.gridSpec,
                     "category": collection.type,
                },
            ]
        };


    },


    createSubsetPartial: function(filter, attributeList, viewTypeParams, secondaryAttributeList, theInfo){  
    	console.log("CREATE SUBSET PARTIAL");

    	console.log(filter);
    	console.log( attributeList );
        //var collection = this.initCollection("subsetPartial", [filter], attributeList);
        var collection = this.initCollection("browse", [filter], attributeList);
        if( theInfo )
            theInfo["browseCount"]++; 

        this.setCollectionConserved(collection, filter, null, null); 
        this.setCollectionVaried(collection, null, attributeList, null); 
        collection.conservedFilter = true;
        collection.conservedAttr1 = false;
        collection.conservedAttr2 = false; 


        // collection["type"] = "subsetPartial"; 
        // collection["filterList"] = [ ];
        // collection["filterList"].push(filter); 
        // collection["attributeList"] = attributeList;
        // collection["isStandalone"] = true; 
        // collection["isAParent"] = false; 
        // collection["isAChild"] = false; 
        // collection["parentId"] = null;  
        // collection["childCollectionList"] = [];

        console.log("-- filter by attributes:");
        console.log(filter);

        console.log("-- producing views...... ");  
        console.log(attributeList);


         for(var i = 0; i < attributeList.length; i++ ){
            console_debug("----- make a chart for attribute: ")
            console_debug(attributeList[i]);
            attr1 = this.getAttributeFromName (attributeList[i]);

            view = this.makeChart( filter, attr1, null ); 
            console_debug(view);
            this.addViewToCollection(collection, view);         
        }
 
        console.log("done!"); 

        this.fixAggFilter( filterList, attributeList, collection ); 
        this.layoutViews(collection);

        if( !theInfo )
            theInfo = {}; 

        // return this.getViewsFromCollection(collection);
        //return {"vis_collection": this.getViewsFromCollection(collection), "collectionNumber": collection.number};
        return {
            "count": 1, 
            "theInfo": theInfo,
            "list": [ 
                { 
                    "vis_collection": this.getViewsFromCollection(collection), 
                    "collectionId": collection.number,
                     "type": "new_collection", 
                     "collection": this.getViewsFromCollection(collection), 
                     "collectionType": collection.type,
                     "collectionFilterList": collection.filterList,
                     "collectionAttributeList": collection.attributeList,
                     "collectionSecondaryAttributeList": collection.secondaryAttributeList,
                     "collectionAggrFilterList": collection.aggregatedFilter,
                     "replaceCollection": false,
                    "gridSpec": collection.gridSpec,
                    "category": collection.type,

                },
            ]
        };

    },


    //are any parameters in the filter list conserved?  
    //ex- uic theft vs uic burglary, both are about uic
    determineConservedAndVaried: function(type, collection, filterList, attributeList){

        var conservedFilterList = [];
        var variedFilterList = [];
        console.log(filterList);

        for(i = 0; i < filterList.length; i++){
            for(j = 0; j < filterList[i].length; j++){
                for(k = i+1; k < filterList.length; k++){
                    for(l = 0; l < filterList[k].length; l++){
                        if( filterList[i][j]["vals"][0] == filterList[k][l]["vals"][0] && filterList[i][j]["attr"] == filterList[k][l]["attr"] ){
                            conservedFilterList.push(filterList[i][j]); 
                        }
                    }
                }

            }
        }

        if( type == "comparison" ){
            this.setCollectionConserved(collection, null, attributeList[0], null); 
            this.setCollectionVaried(collection, filterList, null, null); 
            collection.conservedFilter = false;
            if(conservedFilterList.length > 0)
                collection.conservedFilter = true;

            collection.conservedAttr1 = true;
            collection.conservedAttr2 = false; 

            collection.conservedFilterList = conservedFilterList;

        }
        if( type == "b+c"){
            this.setCollectionConserved(collection, null, null, null); 
            this.setCollectionVaried(collection, filterList, attributeList, null); 
            collection.conservedFilter = false;
            if(conservedFilterList.length > 0)
                collection.conservedFilter = true;

            collection.conservedAttr1 = false;
            collection.conservedAttr2 = false; 
            collection.conservedFilterList = conservedFilterList;

        }

    },


    createComparisonSet: function( filterList, attributeList, viewTypeParams, secondaryAttributeList, theInfo){

        //var collection = this.initCollection("comparison", filterList, attributeList);
        console.log(secondaryAttributeList.length);
        var collection;
        var attList1 = [];
        var attList2 = [];
        if( attributeList.length == 1 && secondaryAttributeList.length == 0){
           collection = this.initCollection("comparison", filterList, attributeList );//[0], attributeList[1]); 
            if( theInfo )
              theInfo["comparisonCount"]++; 
        }
        else if( attributeList.length == 2 && secondaryAttributeList.length != 1 ){
            attList1 = [ attributeList[0] ];
            attList2 = [attributeList[1]];
            if( theInfo )
                theInfo["multifacetedCompareCount"]++;
           collection = this.initCollection("multifacetedCompare", filterList, attList1, attList2 );//[0], attributeList[1]); 
        } else if( secondaryAttributeList.length == 1 ){
            attList1 = [ attributeList[0] ];
            attList2 = [secondaryAttributeList[0]];
            if( theInfo )
                theInfo["multifacetedCompareCount"]++;
           collection = this.initCollection("multifacetedCompare", filterList, attList1, attList2 );//[0], attributeList[1]); 
        } else if( secondaryAttributeList.length > 1 ){
            attList1 = [ attributeList[0] ];
            collection = this.initCollection("multifacetedCompareBySubAttr", filterList, attList1, secondaryAttributeList );//[0], attributeList[1]); 
            if( theInfo )
                theInfo["multifacetedCompare_SubAttrCount"]++;
            for(var i =0 ; i < secondaryAttributeList.length ; i++){
                attList2 = [secondaryAttributeList[i]];
                col = this.createComparisonSet( filterList, attributeList, viewTypeParams, [ secondaryAttributeList[i]] );
                for(var j =0; j < col["list"][0]["vis_collection"].length; j++){
                    this.addViewToCollection(collection, col["list"][0]["vis_collection"][j] ); 
                }

                // theInfo["comparisonCount"]--; 

            }


            this.fixAggFilter( filterList, attributeList, collection ); 


            //don't layout... force it here
            columns = collection.filterList.length;
            collection.gridSpec = "repeat( " + columns + ", 1fr)";


            if( !theInfo )
                theInfo = {}; 

            return {
                "count": 1, 
                "theInfo": theInfo,
                "list": [ 
                    { 
                        "vis_collection": this.getViewsFromCollection(collection), 
                        "collectionId": collection.number,
                         "type": "new_collection", 
                         "collection": this.getViewsFromCollection(collection), 
                         "collectionType": collection.type,
                         "collectionFilterList": collection.filterList,
                         "collectionAttributeList": collection.attributeList,
                         "collectionSecondaryAttributeList": collection.secondaryAttributeList,
                         "collectionAggrFilterList": collection.aggregatedFilter,
                         "replaceCollection": false,
                         "gridSpec": collection.gridSpec,
                           "category": collection.type,

                    },
                ]
            };
        }

        //this.determineConservedAndVaried("comparison", collection, filterList, attributeList); 


        // this.setCollectionConserved(collection, null, attributeList[0], null); 
        // this.setCollectionVaried(collection, filterList, null, null); 
        // collection.conservedFilter = false;
        // collection.conservedAttr1 = true;
        // collection.conservedAttr2 = false; 

        // collection["type"] = "comparison"; 
        // collection["filterList"] = filterList;
        // collection["attributeList"] = attributeList;
        // collection["isStandalone"] = true; 
        // collection["isAParent"] = false; 
        // collection["isAChild"] = false; 
        // collection["parentId"] = null; 
        // collection["childCollectionList"] = [];


        console.log("CREATE COMPARISON");
        console.log("-- producing views...... ");  


        for(var i = 0; i < filterList.length; i++ ){
            console_debug("----- make a chart for filter: ")
            console_debug(filterList[i]);

            attr1 = this.getAttributeFromName (attributeList[0]);
            attr2 = null;
            console.log(attList2);
            if( attributeList.length == 2 || secondaryAttributeList.length == 1)
                attr2 = this.getAttributeFromName (attList2[0])
            console.log(attr2);
            view = this.makeChart( filterList[i], attr1, attr2 ); 
            console_debug(view);
            this.addViewToCollection(collection, view);    

        }

        this.fixAggFilter( filterList, attributeList, collection ); 

        this.layoutViews(collection);

        // return this.getViewsFromCollection(collection);
        // return {"vis_collection": this.getViewsFromCollection(collection), "collectionNumber": collection.number};
    
        if( !theInfo )
            theInfo = {};

        return {
            "count": 1, 
            "theInfo": theInfo,
            "list": [ 
                { 
                    "vis_collection": this.getViewsFromCollection(collection), 
                    "collectionId": collection.number,
                     "type": "new_collection", 
                     "collection": this.getViewsFromCollection(collection), 
                     "collectionType": collection.type,
                     "collectionFilterList": collection.filterList,
                     "collectionAttributeList": collection.attributeList,
                     "collectionSecondaryAttributeList": collection.secondaryAttributeList,
                     "collectionAggrFilterList": collection.aggregatedFilter,
                     "replaceCollection": false,
                    "gridSpec": collection.gridSpec,
                    "category": collection.type,

                },
            ]
        };
    },


    createComplex: function( filterList, attributeList, viewTypeParams, theInfo){


    	var collection = this.initCollection("complex", filterList, attributeList);
        if( theInfo )
		  theInfo["complexCount"]++; 
        this.setCollectionConserved(collection, filter, null, null); 
        collection.conservedFilter = true;
        collection.conservedAttr1 = false;
        collection.conservedAttr2 = false; 

        // collection["type"] = "complex"; 
		// collection["filterList"] = filterList;
  //       collection["attributeList"] = attributeList;
  //       collection["isStandalone"] = true; 
  //       collection["isAParent"] = false; 
  //       collection["isAChild"] = false; 
  //       collection["parentId"] = null; 
  //       collection["childCollectionList"] = [];


		//singles
		for( var i = 0; i < filterList.length; i++){
		  for( var j = 0; j < attributeList.length; j++){
        	this.addViewToCollection(collection, this.makeChart(filterList[i], this.getAttributeFromName(attributeList[j]), null ) );
       	  }
       	}

       	//combos
       	for( var i = 0; i < filterList.length; i++){
		  for( var j = 0; j < attributeList.length; j++){
		  	for( var k = j+1; k < attributeList.length; k++){

                attrReordered = this.whichAttrIsPrimary(attributeList[j], attributeList[k] );
                if( attrReordered[0] != null ) 
                    this.addViewToCollection(collection, this.makeChart(filterList[i], this.getAttributeFromName(attrReordered[0]), this.getAttributeFromName(attrReordered[1]) ) );

        		//this.addViewToCollection(collection, this.makeChart(filterList[i], this.getAttributeFromName(attributeList[j]), this.getAttributeFromName(attributeList[k]) ) );
        		//this.addViewToCollection(collection, this.makeChart(filterList[i], this.getAttributeFromName(attributeList[k]), this.getAttributeFromName(attributeList[j]) ) );

       		}
       	  }
       	}

        this.fixAggFilter( filterList, attributeList, collection ); 

        this.layoutViews(collection);

        if( !theInfo )
            theInfo = {}; 

        // return this.getViewsFromCollection(collection);
        // return {"vis_collection": this.getViewsFromCollection(collection), "collectionNumber": collection.number};
        return {
            "count": 1, 
            "theInfo": theInfo,
            "list": [ 
                { 
                    "vis_collection": this.getViewsFromCollection(collection), 
                    "collectionId": collection.number,
                     "type": "new_collection", 
                     "collection": this.getViewsFromCollection(collection), 
                     "collectionType": collection.type,
                     "collectionFilterList": collection.filterList,
                     "collectionAttributeList": collection.attributeList,
                     "collectionSecondaryAttributeList": collection.secondaryAttributeList,
                     "collectionAggrFilterList": collection.aggregatedFilter,
                     "replaceCollection": false,
                     "gridSpec": collection.gridSpec,
                    "category": collection.type,

                },
            ]
        };
    },

    whichAttrIsPrimary: function( attr1, attr2 ){

        if( attr1.type == attr2.type ){
            return [attr1, attr2];
        }
        // if( attr1.type == "county-map" && ( attr2.type == "date_sum" || attr2.type == "date")) {
        //     return [null, null];
        // }
        // if( attr2.type == "county-map" && ( attr1.type == "date_sum" || attr1.type == "date")){
        //     return [null, null];
        // }
        if( attr1.type == "county-map"){
            return [attr1, attr2];
        }
        if( attr2.type == "county-map"){
            return [attr2, attr1];
        }
        if( attr1.type == "date_sum" || attr1.type == "date"){
            return [attr1, attr2];
        }
        if( attr2.type == "date_sum" || attr2.type == "date"){
            return [attr2, attr1];
        }
        return [attr2, attr1];

    },


    createBrowseAndCompare : function( filterList, attributeList, viewTypeParams, secondaryAttributeList, theInfo ){


        var collection;
        if( secondaryAttributeList.length == 0){
            collection = this.initCollection("b+c", filterList, attributeList); 
            this.determineConservedAndVaried("b+c", collection, filterList, attributeList); 
            if( theInfo )
                theInfo["b_cCount"]++; 
        } else if(secondaryAttributeList.length == 1){
            collection = this.initCollection("multifacetedB+C", filterList, attributeList, secondaryAttributeList); 
            if( theInfo )
                theInfo["multifacetedB_C_Count"]++; 
        }   
        //var collection = this.initCollection("grid", filterList, attributeList); 
        // collection.subtype = "b+c";

        // this.setCollectionConserved(collection, null, null, null); 
        // this.setCollectionVaried(collection, filterList, attributeList, null); 
        // collection.conservedFilter = false;
        // collection.conservedAttr1 = false;
        // collection.conservedAttr2 = false; 


		// collection["type"] = "b+c"; 
		// collection["filterList"] = filterList;
  //       collection["attributeList"] = attributeList;
        collection["isStandalone"] = false;  //change these defaults 
        collection["isAParent"] = true;  //it will be a parent of several sub-collections 
        // collection["isAChild"] = false;  
        // collection["parentId"] = null; 
        // collection["childCollectionList"] = []; //will fill.... 



		for( var i = 0; i < filterList.length; i++){
			// arr_fa = [ filterAttributes[i] ];
   //      	arr_fv = [ filterValues[i] ];
        	if( attributeList.length == 0 ){ //complete
        		obj = this.createSubsetComplete( filterList[i], viewTypeParams, secondaryAttributeList );
                views = obj.list[0].collection;
                collection.attributeList = this.getMostRecentCollection().attributeList; 
                // theInfo["browseCount"]--; 
        	} else { //partial
        	   obj = this.createSubsetPartial( filterList[i], attributeList,viewTypeParams, secondaryAttributeList );
        	   views = obj.list[0].collection;

               // theInfo["browseCount"]--; 
            }

            this.listOfCollections.pop();//remove it
            this.collectionCount--;
        	
        	for(var j = 0; j < views.length; j++){
        		this.addViewToCollection(collection, views[j] );
        	}

        	// this.listOfCollections[this.collectionCount-1]["isStandalone"] = false; 
        	// this.listOfCollections[this.collectionCount-1]["isAChild"] = true; 
        	// this.listOfCollections[this.collectionCount-1]["parentId"] = collection["id"]; 

        	// collection["childCollectionList"].push( this.collectionCount-1 ); 

		}
        console.log("VIEWS IN THE COLLECTION: ")
        console.log(this.getViewsFromCollection(collection) );

        this.fixAggFilter( filterList, attributeList, collection ); 

        this.layoutViews(collection);

        if( !theInfo )
            theInfo = {};

        // return this.getViewsFromCollection(collection); 
        // return {"vis_collection": this.getViewsFromCollection(collection), "collectionNumber": collection.number};
        return {
            "count": 1, 
            "theInfo": theInfo,
            "list": [ 
                { 
                    "vis_collection": this.getViewsFromCollection(collection), 
                    "collectionId": collection.number,
                     "type": "new_collection", 
                     "collection": this.getViewsFromCollection(collection), 
                     "collectionType": collection.type,
                     "collectionFilterList": collection.filterList,
                     "collectionAttributeList": collection.attributeList,
                     "collectionSecondaryAttributeList": collection.secondaryAttributeList,
                     "collectionAggrFilterList": collection.aggregatedFilter,
                     "replaceCollection": false,
                    "gridSpec": collection.gridSpec,
                     "category": collection.type,

                },
            ]
        };
    },


    createCompareByCompare: function(filterList, attributeList, aggregatedFilter, viewTypeParams, secondaryAttributeList, theInfo ){
        var collection;

        if(secondaryAttributeList.length == 0){
            collection = this.initCollection("compare_compare_grid", filterList, attributeList);
            if( theInfo )
                theInfo["compare_compare_gridCount"]++; 
        }
        else {
            collection = this.initCollection("multifaceted_compare_compare_grid", filterList, attributeList, secondaryAttributeList);
            if( theInfo )
                theInfo["multifacetedCompareCompareCount"]++; 
        }

        collection.aggregatedFilter = aggregatedFilter; //only storing this for cxc grids

        //this is identical to create comparison... 
        this.determineConservedAndVaried("comparison", collection, filterList, attributeList); 


        // this.setCollectionConserved(collection, null, attributeList[0], null); 
        // this.setCollectionVaried(collection, filterList, null, null); 
        // collection.conservedFilter = false;
        // collection.conservedAttr1 = true;
        // collection.conservedAttr2 = false; 

        // collection["type"] = "comparison"; 
        // collection["filterList"] = filterList;
        // collection["attributeList"] = attributeList;
        // collection["isStandalone"] = true; 
        // collection["isAParent"] = false; 
        // collection["isAChild"] = false; 
        // collection["parentId"] = null; 
        // collection["childCollectionList"] = [];


        console.log("CREATE COMPARISON");
        console.log("-- producing views...... ");  


        for(var i = 0; i < filterList.length; i++ ){
            console_debug("----- make a chart for filter: ")
            console_debug(filterList[i]);

            attr1 = this.getAttributeFromName (attributeList[0]);
            attr2 = null;
            if(secondaryAttributeList.length == 1)
                attr2 = this.getAttributeFromName( secondaryAttributeList[0]);
            view = this.makeChart( filterList[i], attr1, attr2 ); 
            console_debug(view);
            this.addViewToCollection(collection, view);    

        }

       this.fixAggFilter( filterList, attributeList, collection ); 
        this.layoutViews(collection);

        if( !theInfo )
            theInfo = {};

        // return this.getViewsFromCollection(collection);
        //return {"vis_collection": this.getViewsFromCollection(collection), "collectionNumber": collection.number};
        return {
            "count": 1, 
            "theInfo": theInfo,
            "list": [ 
                { 
                    "vis_collection": this.getViewsFromCollection(collection), 
                    "collectionId": collection.number,
                     "type": "new_collection", 
                     "collection": this.getViewsFromCollection(collection), 
                     "collectionType": collection.type,
                     "collectionFilterList": collection.filterList,
                     "collectionAttributeList": collection.attributeList,
                     "collectionSecondaryAttributeList": collection.secondaryAttributeList,
                     "collectionAggrFilterList": collection.aggregatedFilter,
                     "replaceCollection": false,
                    "gridSpec": collection.gridSpec,
                    "category": collection.type,

                },
            ]
        };

    },

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    pivotTheSubset:  function( filterList, attributeList, targetCollection, pivotCount ){
        console.log("PIVOT THE SUBSET");
        console.log("-- from: ");
        console.log(targetCollection.type);
        console.log(targetCollection.filterList);
        console.log(targetCollection.attributeList);
        console.log("-- to:")
        console.log(filterList); 

        var collection;
        var returnType; 

        var oldType = targetCollection.type;
        var oldFilterList = loadash.cloneDeep( targetCollection.filterList);
        var oldAttributeList = targetCollection.attributeList;
        var oldSecondaryAttributeList = targetCollection.secondaryAttributeList;
        var oldViews = loadash.cloneDeep(targetCollection.views);

        //types that modify the original collection.  Non-grids 
        if( targetCollection.type == "targeted" || targetCollection.type == "multifaceted" 
            || targetCollection.type == "browse" || targetCollection.type == "complex" 
            || targetCollection.type == "comparison" || targetCollection.type == "sub+attr" 
            || targetCollection.type == "multifacetedBrowse"  ||  targetCollection.type == "multifacetedCompare" )
        {
            returnType = "modify_collection"; 
            collection = targetCollection; // you are adding to the target collection

            //update filter list
            for(var i =0; i < filterList.length; i++){
                collection.filterList.push( filterList[i] );
            }

            //update types 
            if( oldType == "targeted" ){
                collection.type = "comparison"; //update type to comparison
            }
            else if( oldType == "multifaceted" ){
                collection.type = "multifacetedCompare"; //update type to comparison
            }
            else if( oldType == "browse"){
                collection.type = "b+c"; //update type to b+c
            } 
            else if( oldType == "complex" ){
                collection.type = "complexAndCompareGrid"; //update type to b+c
            }
            else if( oldType== "comparison"){
                collection.type = "compare_compare_grid"; //update type
            } 
            else if( oldType == "sub+attr"){ 
                collection.type = "subAttrByMultifacetedCompare"//"multifacetedCompareBySubAttr"; //update type //CHANGE
            }
            else if( oldType == "multifacetedBrowse" ){
                collection.type = "multifacetedB+C"; //preserve the collection type
            } 
            else if( oldType == "multifacetedCompare" ){
                collection.type = "multifaceted_compare_compare_grid"; //preserve the collection type
            } 
        } 
        else { //need to update this 
            returnType = "new_collection"; //maybe later make this new collection set or sth

            if( pivotCount == 1 ){ //one pivot, make new grid same type
                collection = this.initCollection(targetCollection.type, filterList, targetCollection.attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
            }
            else{ //multiple pivots 

                if( targetCollection.type == "b+c"){ 
                    collection = this.initCollection("b+cMultiple", filterList, targetCollection.attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
                }
                else if( targetCollection.type == "compare_compare_grid"){ 
                    collection = this.initCollection("compare_compare_gridMultiple", filterList, targetCollection.attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
                }
                else if( targetCollection.type == "complexAndCompareGrid" ){
                    collection = this.initCollection("complexAndCompareGridMultiple", filterList, targetCollection.attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
                } 
                else if( targetCollection.type == "multifacetedCompareBySubAttr"){ 
                    collection = this.initCollection("multifacetedCompareBySubAttrMultiple", filterList, targetCollection.attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
                }
                else if( targetCollection.type == "multifacetedBrowseBySubAttr"){ 
                    collection = this.initCollection("multifacetedBrowseBySubAttrMultiple", filterList, targetCollection.attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
                }  
                else if( targetCollection.type == "subAttrByMultifacetedCompare"){
                    collection = this.initCollection("subAttrByMultifacetedCompareMultiple", filterList, targetCollection.attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
                }

            }

        }

    //NOW MAKE THE NEW VIEWS

    if( oldFilterList.length == 1 ){ //one filter: target, browse... 
            for(var i = 0; i < filterList.length; i++){

                for( var j = 0; j < oldViews.length; j++ ){
                    this.addViewToCollection( collection, this.copyAndPivotChart( filterList[i], this.getAttributeFromName( oldViews[j].attribute1 ), this.getAttributeFromName( oldViews[j].attribute2 ), oldViews[j]) );
                }
            }
    } else {
              // for( var j = 0; j < targetCollection.views.length; j++ ){
              //       // console.log("TARGET  VIEWS ");
              //       // console.log(targetCollection.views[j]);
              //       this.addViewToCollection( collection, this.copyAndPivotChart( filterList[j], this.getAttributeFromName( targetCollection.views[j].attribute1 ), this.getAttributeFromName( targetCollection.views[j].attribute2 ), targetCollection.views[j]) );
              //   }
            if( oldType == "complexAndCompareGrid"){ //other type
                console.log(filterList);
                for(var i =0; i < targetCollection.views.length; i++){
                    filterIdx = Math.floor( i/(targetCollection.views.length/filterList.length) );
                    this.addViewToCollection( collection, this.copyAndPivotChart( filterList[filterIdx], this.getAttributeFromName( targetCollection.views[i].attribute1 ), this.getAttributeFromName( targetCollection.views[i].attribute2 ), targetCollection.views[i]) );
                }
            }
            else if(oldType == "multifaceted_compare_compare_grid" || targetCollection.type == "compare_compare_grid" ){

                for(var i =0 ; i < filterList.length; i++){
                    for(var j = 0; j < targetCollection.attributeList.length; j++){
                        this.addViewToCollection( collection, this.copyAndPivotChart( filterList[i], this.getAttributeFromName( targetCollection.views[i].attribute1 ), this.getAttributeFromName( targetCollection.views[i].attribute2 ), targetCollection.views[i]) );
                        //count++; 
                        console.log(filterList[i]);
                    }
                }
                // console.log("HOW MANY VIEWS")
            }
            else if( oldType == "subAttrByMultifacetedCompare"){
                count = 0;
                for(var i =0 ; i < filterList.length; i++){
                    for(var j = 0; j < targetCollection.secondaryAttributeList.length; j++){
                        this.addViewToCollection( collection, this.copyAndPivotChart( filterList[i], this.getAttributeFromName( targetCollection.attributeList[0] ), this.getAttributeFromName( targetCollection.secondaryAttributeList[j]), targetCollection.views[count]) );
                        count++; 
                        console.log(filterList[i]);
                    }
                }
            }  
            else if( oldType == "multifacetedCompareBySubAttr" ){
                count = 0;
                for( var j = 0; j < targetCollection.secondaryAttributeList.length; j++ ){
                    for(var i =0 ; i < filterList.length; i++){
                        this.addViewToCollection( collection, this.copyAndPivotChart( filterList[i], this.getAttributeFromName( targetCollection.attributeList[0] ), this.getAttributeFromName( targetCollection.secondaryAttributeList[j]), targetCollection.views[count]) );
                        count++; 
                        console.log(filterList[i]);
                    }
                } 
            }
            else {
                 count = 0;
              
                for(i =0 ; i < filterList.length; i++){
                    for(j = 0; j < targetCollection.attributeList.length; j++){
                        this.addViewToCollection( collection, this.copyAndPivotChart( filterList[i], this.getAttributeFromName( targetCollection.views[j].attribute1 ), this.getAttributeFromName( targetCollection.views[j].attribute2 ), targetCollection.views[j]) );
                        count++; 
                    }
                }
            }
         
        
            if( collection.type == "compare_compare_grid" || collection.type == "multifaceted_compare_compare_grid"){

                buildingAggregatedFilter = [];

                for(i =0; i < collection.filterList.length; i++){
                    for(j =0; j < collection.filterList[i].length; j++){
                        for(x = 0; x < collection.filterList[i][j]["vals"].length; x++){
                            val = collection.filterList[i][j]["vals"][x];
                            encountered = false; 
                            for(k = 0; k < buildingAggregatedFilter.length; k++){
                                if( buildingAggregatedFilter[k]["attr"] == collection.filterList[i][j]["attr"] ){
                                    countMatch = 0;
                                    encountered = true;
                                    for(l =0; l < buildingAggregatedFilter[k]["vals"].length; l++){
                                        if( buildingAggregatedFilter[k]["vals"][l] == val )
                                            countMatch++;
                                    }
                                    if( countMatch == 0 )
                                        buildingAggregatedFilter[k]["vals"].push(val);
                                }
                            }
                            if( ! encountered ){
                                 buildingAggregatedFilter.push( loadash.cloneDeep( collection.filterList[i][j] ) );

                            }
                        }

                    }
                }
                collection.aggregatedFilter = buildingAggregatedFilter;
                console.log("built the agg filter");
                console.log(collection.aggregatedFilter);

            }

        }

        //CHECKING
        console.log(collection.views.length);
        for(i = 0; i < collection.views.length; i++){
            console.log("CHECKING VIEW FILTERS");
            console.log(collection.views[i].filter);
        }

        this.fixAggFilter( collection.filterList, collection.attributeList, collection ); 

        this.layoutViews(collection);

        //TO DO 
        //this.integrateNewViewsTogether( collection, targetCollection ); 

        // return this.getViewsFromCollection(collection);         

        // return {"vis_collection": this.getViewsFromCollection(collection), "collectionNumber": collection.number, "returnType": returnType};
        return {
            "count": 1, 
            "list": [ 
                { 
                    "vis_collection": this.getViewsFromCollection(collection), 
                    "collectionId": collection.number,
                     "type": returnType, 
                     "collection": this.getViewsFromCollection(collection), 
                     "collectionType": collection.type,
                     "collectionFilterList": collection.filterList,
                     "collectionAttributeList": collection.attributeList,
                     "collectionSecondaryAttributeList": collection.secondaryAttributeList,
                     "collectionAggrFilterList": collection.aggregatedFilter,
                     "replaceCollection": false,
                     "gridSpec": collection.gridSpec,
                },
            ]
        };

    },

    //old version.... 
    pivotTheSubset2: function( filterList, attributeList, targetCollection, pivotCount ){
        
        console.log("PIVOT THE SUBSET");
        console.log("-- from: ");
        console.log(targetCollection.type);
        console.log(targetCollection.filterList);
        console.log(targetCollection.attributeList);
        console.log("-- to:")
        console.log(filterList); 


        var collection;
        if( pivotCount  == 1){
            collection = this.initCollection(targetCollection.type, filterList, targetCollection.attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
            console.log(collection);  
        } 
        else { //multiple pivots
            if( targetCollection.type == "targeted" ){
                collection = this.initCollection("comparison", filterList, targetCollection.attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
            }
            else if( targetCollection.type ==    "browse" ){
                collection = this.initCollection("b+c", filterList, targetCollection.attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
            } 
            else if( targetCollection.type == "complex" ){
                collection = this.initCollection("complexAndCompareGrid", filterList, targetCollection.attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
            } 
            else if( targetCollection.type == "comparison"){
                collection = this.initCollection("compare_compare_grid", filterList, targetCollection.attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
            } 
            else if( targetCollection.type == "b+c"){ 
                collection = this.initCollection("b+cMultiple", filterList, targetCollection.attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
            }
            else if( targetCollection.type == "sub+attr"){ 
                collection = this.initCollection("multifacetedCompareBySubAttr", filterList, targetCollection.attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
            }
            else if( targetCollection.type == "compare_compare_grid"){ 
                collection = this.initCollection("compare_compare_gridMultiple", filterList, targetCollection.attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
            }
            else if( targetCollection.type == "multifacetedBrowse" ){
                collection = this.initCollection("multifacetedB+C", filterList, targetCollection.attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
            } 
            else if( targetCollection.type == "multifacetedCompare" ){
                collection = this.initCollection("multifaceted_compare_compare_grid", filterList, targetCollection.attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
            } 
            else if( targetCollection.type == "complexAndCompareGrid" ){
                collection = this.initCollection("complexAndCompareGridMultiple", filterList, targetCollection.attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
            } 
            else if( targetCollection.type == "multifacetedCompareBySubAttr"){ 
                collection = this.initCollection("multifacetedCompareBySubAttrMultiple", filterList, targetCollection.attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
            }
            else if( targetCollection.type == "multifacetedBrowseBySubAttr"){ 
                collection = this.initCollection("multifacetedBrowseBySubAttrMultiple", filterList, targetCollection.attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
            }
        }

        //do we need this? 
            // this.setCollectionConserved(collection, filter, null, null); 
            // this.setCollectionVaried(collection, null, attributeList, null); 

        for(i = 0; i < filterList.length; i++){
            console.log(filterList[i]);
        }

        
        if( targetCollection.filterList.length == 1 ){ //one filter: target, browse... 
            for(var i = 0; i < filterList.length; i++){

                for( var j = 0; j < targetCollection.views.length; j++ ){
                    this.addViewToCollection( collection, this.copyAndPivotChart( filterList[i], this.getAttributeFromName( targetCollection.views[j].attribute1 ), this.getAttributeFromName( targetCollection.views[j].attribute2 ), targetCollection.views[j]) );
                }
            }
        } else {
              // for( var j = 0; j < targetCollection.views.length; j++ ){
              //       // console.log("TARGET  VIEWS ");
              //       // console.log(targetCollection.views[j]);
              //       this.addViewToCollection( collection, this.copyAndPivotChart( filterList[j], this.getAttributeFromName( targetCollection.views[j].attribute1 ), this.getAttributeFromName( targetCollection.views[j].attribute2 ), targetCollection.views[j]) );
              //   }
            if( targetCollection.type == "complexAndCompareGrid"){ //other type
                console.log(filterList);
                for(var i =0; i < targetCollection.views.length; i++){
                    filterIdx = Math.floor( i/(targetCollection.views.length/filterList.length) );
                    this.addViewToCollection( collection, this.copyAndPivotChart( filterList[filterIdx], this.getAttributeFromName( targetCollection.views[i].attribute1 ), this.getAttributeFromName( targetCollection.views[i].attribute2 ), targetCollection.views[i]) );
                }
            }
            else if(targetCollection.type == "multifaceted_compare_compare_grid" || targetCollection.type == "compare_compare_grid" ){

                for(var i =0 ; i < filterList.length; i++){
                    for(var j = 0; j < targetCollection.attributeList.length; j++){
                        this.addViewToCollection( collection, this.copyAndPivotChart( filterList[i], this.getAttributeFromName( targetCollection.views[i].attribute1 ), this.getAttributeFromName( targetCollection.views[i].attribute2 ), targetCollection.views[i]) );
                        //count++; 
                        console.log(filterList[i]);
                    }
                }
                // console.log("HOW MANY VIEWS")
            }
            else if( targetCollection.type == "multifacetedCompareBySubAttr" ){
                count = 0;
                for(var i =0 ; i < filterList.length; i++){
                    for(var j = 0; j < targetCollection.secondaryAttributeList.length; j++){
                        this.addViewToCollection( collection, this.copyAndPivotChart( filterList[i], this.getAttributeFromName( targetCollection.attributeList[0] ), this.getAttributeFromName( targetCollection.secondaryAttributeList[j]), targetCollection.views[count]) );
                        count++; 
                        console.log(filterList[i]);
                    }
                }
            }
            else {
                 count = 0;
              
                for(i =0 ; i < filterList.length; i++){
                    for(j = 0; j < targetCollection.attributeList.length; j++){
                        this.addViewToCollection( collection, this.copyAndPivotChart( filterList[i], this.getAttributeFromName( targetCollection.views[j].attribute1 ), this.getAttributeFromName( targetCollection.views[j].attribute2 ), targetCollection.views[j]) );
                        count++; 
                    }
                }
            }
         
            // if( targetCollection.type ==)
            // else{
            //     for(i =0 ; i < filterList.length; i++){
            //         for(j = 0; j < targetCollection.views.length; j++){
            //             this.addViewToCollection( collection, this.copyAndPivotChart( filterList[i], this.getAttributeFromName( targetCollection.views[j].attribute1 ), this.getAttributeFromName( targetCollection.views[j].attribute2 ), targetCollection.views[j]) );
            //             count++; 
            //         }
            //     }
            // }

            if( collection.type == "compare_compare_grid" || collection.type == "multifaceted_compare_compare_grid"){






                // newAggregatedFilterList = [] ; 
                // for( i = 0; i < filterAttributes.length; i++ ){ //for all filter attributes
                //     encountered = false; 
                //     for( j = 0; j < newAggregatedFilterList.length; j++){
                //         if( newAggregatedFilterList[j]["attr"] == filterAttributes[i] ){
                //             encountered = true;
                //             newAggregatedFilterList[j]["vals"].push( filterValues[i] ); 
                //         }
                //     }
                //     if( !encountered ){
                //         newAggregatedFilterList.push( { "attr": filterAttributes[i], "vals": [filterValues[i] ] }); 
                //     }
                // }

                // console_debug("aggregated filter");
                // console_debug(newAggregatedFilterList); 

                //recompute the aggregate filter
                // collection.aggregatedFilter = targetCollection.aggregatedFilter;

                //for all the values in the new filter


                // for(i =0; i < filterList.length; i++){
                //     for(j= 0;j<filterList[i].length;j++){ 
                //         for(l = 0; l< filterList[i][j]["vals"].length; l ++){
                //             val = filterList[i][j]["vals"][l];

                //             for(k = 0; k < collection.aggregatedFilter.length;k++){
                //                 if( filterList[i][j]["attr"] == collection.aggregatedFilter[k]["attr"] ){
                //                     countMatch = 0;
                //                     for( x = 0; x < collection.aggregatedFilter[k]["vals"].length; x++){
                //                         if( collection.aggregatedFilter[k]["vals"][x] == val )
                //                             countMatch++;
                //                     }

                //                     if( countMatch == 0 ){
                //                         collection.aggregatedFilter[k]["vals"].push(val );

                //                     }

                //                 }

                //             }


                //         }

                //     }
                // }

                buildingAggregatedFilter = [];
                // if( filterList.length > 0 && filterList[0].length > 0 )
                //     buildingAggregatedFilter.push( filterList[0][0] );
                for(i =0; i < filterList.length; i++){
                    for(j =0; j < filterList[i].length; j++){
                        for(x = 0; x < filterList[i][j]["vals"].length; x++){
                            val = filterList[i][j]["vals"][x];
                            encountered = false; 
                            for(k = 0; k < buildingAggregatedFilter.length; k++){
                                if( buildingAggregatedFilter[k]["attr"] == filterList[i][j]["attr"] ){
                                    countMatch = 0;
                                    encountered = true;
                                    for(l =0; l < buildingAggregatedFilter[k]["vals"].length; l++){
                                        if( buildingAggregatedFilter[k]["vals"][l] == val )
                                            countMatch++;
                                    }
                                    if( countMatch == 0 )
                                        buildingAggregatedFilter[k]["vals"].push(val);
                                }
                            }
                            if( ! encountered ){
                                 buildingAggregatedFilter.push( loadash.cloneDeep( filterList[i][j] ) );

                            }
                        }

                    }
                }
                collection.aggregatedFilter = buildingAggregatedFilter;
                console.log("built the agg filter");
                console.log(collection.aggregatedFilter);

            }

        }

        //CHECKING
        console.log(collection.views.length);
        for(i = 0; i < collection.views.length; i++){
            console.log("CHECKING VIEW FILTERS");
            console.log(collection.views[i].filter);
        }

        this.fixAggFilter( filterList, attributeList, collection ); 


        //TO DO 
        //this.integrateNewViewsTogether( collection, targetCollection ); 

        // return this.getViewsFromCollection(collection);         

        return {"vis_collection": this.getViewsFromCollection(collection), "collectionNumber": collection.number};


    },


    pivotTheDataAttribute: function(filterList, attributeList, targetCollection, pivotCount ){
        var collection;

        console.log("ATTRIBUTE PIVOT");
        console.log("-- from: ");
        console.log(targetCollection.type);
        console.log(targetCollection.filterList);
        console.log(targetCollection.attributeList); 
        console.log("-- to:")
        console.log(attributeList); 


         //check to make sure not adding a duplicate
            for(i =0 ; i < attributeList.length ; i++ ){
                for(j = 0 ; j < targetCollection.attributeList.length; j++ ){
                    if( attributeList[i] == targetCollection.attributeList[j] ){
                        console.log("removing duplicate " + attributeList[i] );

                        attributeList.splice(i, 1); //remove it
                        i--; 
                    }
                }
            }

            if( attributeList.length == 0 ){
                console.log("Found the error!");
                returnObj = {
                        "type": "error",
                        "error":"Looks like you already have this view in the most recent set of views you expanding.  Try a new query."
                    };
                return returnObj;
            }

            //done checking for errors 


        var returnType; 

        var oldType = targetCollection.type;
        var oldFilterList = loadash.cloneDeep( targetCollection.filterList);
        var oldAttributeList = targetCollection.attributeList;
        var oldSecondaryAttributeList = targetCollection.secondaryAttributeList;
        var oldViews = loadash.cloneDeep(targetCollection.views);

           
            
           

        //types that modify the original collection.  Non-grids 
        if( targetCollection.type == "targeted"  ||  targetCollection.type == "multifaceted"  
            || targetCollection.type == "comparison" || targetCollection.type == "sub+attr" 
            ||  targetCollection.type == "multifacetedCompare" )
        {
            returnType = "modify_collection"; 
            collection = targetCollection; // you are adding to the target collection

            //update attr list
            for(i =0; i < attributeList.length; i++){
                collection.attributeList.push( attributeList[i] );
            }


            //update types 
            if( oldType== "targeted"){
                collection.type = "browse"; 
                //filterList, attributeList); 
            } 
            else if( oldType== "multifaceted"){
                collection.type = "multifacetedBrowse"; 
                //filterList, attributeList); 
            } 
            else if( oldType== "comparison"){
                collection.type = "b+c";  
                 //filterList, attributeList);
            }
            else if( oldType == "sub+attr"){ //???
                collection.type = "subAttrByMultifacetedBrowse";//"multifacetedBrowseBySubAttr"; 
                //filterList, attributeList, targetCollection.secondaryAttributeList); 
            }
            else if( oldType== "multifacetedCompare" ){
                collection.type = "multifacetedB+C";
                //filterList, targetCollection.attributeList, targetCollection.secondaryAttributeList);
            }


        }
        else {
            returnType = "new_collection"; 
            if( pivotCount == 1 ){
                if( targetCollection.type == "compare_compare_grid"){ 
                    collection = this.initCollection("compare_compare_grid", filterList, attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
                }             
                else if( targetCollection.type == "multifaceted_compare_compare_grid" ){
                    collection = this.initCollection("multifaceted_compare_compare_grid", filterList, attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
                }
                else if( targetCollection.type == "multifacetedBrowseBySubAttr" ){
                    collection = this.initCollection("multifacetedBrowseBySubAttr", filterList, attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
                }
                else if( targetCollection.type == "multifacetedCompareBySubAttr" ){
                    collection = this.initCollection("multifacetedCompareBySubAttr", filterList, attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
                }
                else if( targetCollection.type == "subAttrByMultifacetedBrowse" ){
                    collection = this.initCollection("subAttrByMultifacetedBrowse", filterList, attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
                }
                else if( targetCollection.type == "subAttrByMultifacetedCompare" ){
                    collection = this.initCollection("subAttrByMultifacetedCompare", filterList, attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
                }
                else if( targetCollection.type == "multifaceted_compare_compare_grid"){ 
                    collection = this.initCollection("multifaceted_compare_compare_grid", filterList, attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
                }             
            } 
            else{//multiple pivots 
                if( targetCollection.type == "compare_compare_grid"){ 
                    collection = this.initCollection("compare_compare_gridMultiple", filterList, attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
                } 
                else if( targetCollection.type == "multifacetedBrowseBySubAttr" ){
                    collection = this.initCollection("multifacetedBrowseBySubAttrMultiple", filterList, attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
                }
                else if( targetCollection.type == "multifacetedCompareBySubAttr" ){
                    collection = this.initCollection("multifacetedCompareBySubAttrMultiple", filterList, attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
                }
                else if( targetCollection.type == "subAttrByMultifacetedBrowse" ){
                    collection = this.initCollection("subAttrByMultifacetedBrowseMultiple", filterList, attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
                }
                else if( targetCollection.type == "subAttrByMultifacetedCompare" ){
                    collection = this.initCollection("subAttrByMultifacetedCompareMultiple", filterList, attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
                } 
                else if( targetCollection.type == "multifaceted_compare_compare_grid" ){
                    
                    collection.type = "multifacetedB+CMultiple";
                    //filterList, attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
                }
            }

        }

        console.log("now creating " + collection.type );

        if( collection.type != "b+c" ){ 
             for(var j = 0; j < attributeList.length; j++){
                for( var i = 0; i < oldViews.length; i++ ){
                    // console.log("TARGET  VIEWS ");
                    // console.log(i);
                    console.log(attributeList[j]);
                    this.addViewToCollection( collection, this.copyAndPivotChart( oldViews[i].filter, this.getAttributeFromName( attributeList[j] ), this.getAttributeFromName(oldViews[i].attribute2), oldViews[i]) );
                }
            }
        } else { //need to enforce an order and orientation for b+c, or it all falls apart
            count = 0; 
             for( var i = 0; i < oldViews.length; i++ ){
                for(var j = 0; j < attributeList.length; j++){

                    console.log(attributeList[j]);
                    view = this.copyAndPivotChart( oldViews[i].filter, this.getAttributeFromName( attributeList[j] ), this.getAttributeFromName(oldViews[i].attribute2), oldViews[i]);
                    this.insertViewInCollection(collection, view, i+1+count);
                    count++;
                }
            }
            
        }

        if( collection.type == "compare_compare_grid" || collection.type == "multifaceted_compare_compare_grid"){
            collection.aggregatedFilter = targetCollection.aggregatedFilter; //for now..
        }

       this.fixAggFilter( collection.filterList, collection.attributeList, collection ); 
       this.layoutViews(collection);

        // return this.getViewsFromCollection(collection);
        // return {"vis_collection": this.getViewsFromCollection(collection), "collectionNumber": collection.number, "returnType": returnType};
        return {
            "count": 1, 
            "list": [ 
                { 
                    "vis_collection": this.getViewsFromCollection(collection), 
                    "collectionId": collection.number,
                     "type": returnType, 
                     "collection": this.getViewsFromCollection(collection), 
                     "collectionType": collection.type,
                     "collectionFilterList": collection.filterList,
                     "collectionAttributeList": collection.attributeList,
                     "collectionSecondaryAttributeList": collection.secondaryAttributeList,
                     "collectionAggrFilterList": collection.aggregatedFilter,
                     "replaceCollection": false,
                    "gridSpec": collection.gridSpec

                },
            ]
        };
            
         

    },

    //old
    pivotTheDataAttribute2: function(filterList, attributeList, targetCollection, pivotCount ){
        var collection;
        console.log("TEST");

        if( pivotCount == 1 ){//attributeList.length == 1 ){
            if( targetCollection.type == "targeted")
                collection = this.initCollection("targeted", filterList, attributeList); 
            else if( targetCollection.type == "comparison")
                collection = this.initCollection("comparison", filterList, attributeList); 
            else if( targetCollection.type == "sub+attr")
                collection = this.initCollection("sub+attr", filterList, attributeList); 
            else if( targetCollection.type == "compare_compare_grid"){ 
                collection = this.initCollection("compare_compare_grid", filterList, attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
            } 
            else if( targetCollection.type == "multifacetedCompare" ){
                collection = this.initCollection("multifacetedCompare", filterList, attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
            } 
            else if( targetCollection.type == "multifaceted_compare_compare_grid" ){
                collection = this.initCollection("multifaceted_compare_compare_grid", filterList, attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
            }
            else if( targetCollection.type == "multifacetedBrowseBySubAttr" ){
                collection = this.initCollection("multifacetedBrowseBySubAttr", filterList, attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
            }
            else if( targetCollection.type == "multifacetedCompareBySubAttr" ){
                collection = this.initCollection("multifacetedCompareBySubAttr", filterList, attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
            }
        }
        else {
            if( targetCollection.type == "targeted")
                collection = this.initCollection("browse", filterList, attributeList); 
            else if( targetCollection.type == "comparison")
                collection = this.initCollection("b+c", filterList, attributeList);  
            else if( targetCollection.type == "sub+attr")
                collection = this.initCollection("multifacetedBrowseBySubAttr", filterList, attributeList, targetCollection.secondaryAttributeList); 
            else if( targetCollection.type == "compare_compare_grid"){ 
                collection = this.initCollection("compare_compare_gridMultiple", filterList, attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
            } 
            else if( targetCollection.type == "multifacetedCompare" ){
                collection = this.initCollection("multifacetedB+C", filterList, targetCollection.attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
            } else if( targetCollection.type == "multifaceted_compare_compare_grid" ){
                collection = this.initCollection("multifacetedB+CMultiple", filterList, attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
            }
            else if( targetCollection.type == "multifacetedBrowseBySubAttr" ){
                collection = this.initCollection("multifacetedBrowseBySubAttrMultiple", filterList, attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
            }
            else if( targetCollection.type == "multifacetedCompareBySubAttr" ){
                collection = this.initCollection("multifacetedCompareBySubAttrMultiple", filterList, attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
            } 
        }

        console.log("ATTRIBUTE PIVOT");
        console.log("-- from: ");
        console.log(targetCollection.type);
        console.log(targetCollection.filterList);
        console.log(targetCollection.attributeList); 
        console.log("-- to:")
        console.log(attributeList); 

        for(j = 0; j < attributeList.length; j++){
            for( var i = 0; i < targetCollection.views.length; i++ ){
                // console.log("TARGET  VIEWS ");
                // console.log(i);
                // console.log(targetCollection.views[i]);
                this.addViewToCollection( collection, this.copyAndPivotChart( targetCollection.views[i].filter, this.getAttributeFromName( attributeList[j] ), this.getAttributeFromName(targetCollection.views[i].attribute2), targetCollection.views[i]) );
            }
        }

        if( collection.type == "compare_compare_grid" || collection.type == "multifaceted_compare_compare_grid"){
            collection.aggregatedFilter = targetCollection.aggregatedFilter; //for now..
        }

       this.fixAggFilter( filterList, attributeList, collection ); 

        // return this.getViewsFromCollection(collection);
        return {"vis_collection": this.getViewsFromCollection(collection), "collectionNumber": collection.number};

    },



   filterTheCollection: function( filterList, attributeList, targetCollection, pivotCount ){
        console.log("FILTER THE COLLECTION");
        console.log("-- from: ");
        console.log(targetCollection.type);
        console.log(targetCollection.filterList);
        console.log(targetCollection.attributeList);
        console.log("-- to:")
        console.log(filterList); 

        //1. MAKE A NEW COLLECTION 
        var collection;

        if( pivotCount == 1){//filterList.length == 1){
          collection = this.initCollection(targetCollection.type, filterList, targetCollection.attributeList, targetCollection.secondaryAttributeList);   
        } 
        else
        {
            if( targetCollection.type == "targeted" ){
                collection = this.initCollection("comparison", filterList, targetCollection.attributeList, targetCollection.secondaryAttributeList ); //preserve the collection type
            }
            if( targetCollection.type == "multifaceted" ){
                collection = this.initCollection("multifacetedCompare", filterList, targetCollection.attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
            }
            else if( targetCollection.type == "browse" ){
                collection = this.initCollection("b+c", filterList, targetCollection.attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
            } 
            else if( targetCollection.type == "complex" ){
                collection = this.initCollection("complexAndCompareGrid", filterList, targetCollection.attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
            } 
            else if( targetCollection.type == "comparison"){
                collection = this.initCollection("compare_compare_grid", filterList, targetCollection.attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
            } 
            else if( targetCollection.type == "b+c"){ //FIX
                collection = this.initCollection("b+cMultiple", filterList, targetCollection.attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
            }
            else if( targetCollection.type == "sub+attr"){ //FIX
                collection = this.initCollection("multifacetedCompareBySubAttr", filterList, targetCollection.attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
            }
            else if( targetCollection.type == "compare_compare_grid"){  
                collection = this.initCollection("compare_compare_gridMultiple", filterList, targetCollection.attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
            }
            else if( targetCollection.type == "multifacetedBrowse" ){
                collection = this.initCollection("multifacetedB+C", filterList, targetCollection.attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
            }
            else if( targetCollection.type == "multifacetedCompare" ){
                collection = this.initCollection("multifaceted_compare_compare_grid", filterList, targetCollection.attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
            }  
            else if( targetCollection.type == "complexAndCompareGrid" ){
                collection = this.initCollection("complexAndCompareGridMultiple", filterList, targetCollection.attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
            } 
            else if( targetCollection.type == "multifacetedCompareBySubAttr"){ 
                collection = this.initCollection("multifacetedCompareBySubAttrMultiple", filterList, targetCollection.attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
            }
            else if( targetCollection.type == "multifacetedBrowseBySubAttr"){ 
                collection = this.initCollection("multifacetedBrowseBySubAttrMultiple", filterList, targetCollection.attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
            }  
            else if( targetCollection.type == "subAttrByMultifacetedCompare"){ 
                collection = this.initCollection("subAttrByMultifacetedCompareMultiple", filterList, targetCollection.attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
            }
            else if( targetCollection.type == "subAttrByMultifacetedBrowse"){ 
                collection = this.initCollection("subAttrByMultifacetedBrowseMultiple", filterList, targetCollection.attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
            }             
        }


        updatedFilterList = []; 

        // 2. FOR ALL VIEWS IN THE TARGET COLLECTION, PIVOT.  SKIP IF NECESSARY
       //   newAttrList = []; 
        for(var x = 0; x < filterList.length; x++){
            for( var i = 0; i < targetCollection.views.length; i++ ){
                    console.log("TARGET  VIEWS ");
                    console.log(i);
                    console.log(targetCollection.views[i]);
                    skipThis = false; 
                    for( var j = 0; j < filterList[x].length; j++){ 
                            console.log( filterList[x][j]["attr"] );
                            console.log( targetCollection.views[i].attribute1 );
                        if (filterList[x][j]["attr"] == targetCollection.views[i].attribute1 || filterList[x][j]["attr"] == targetCollection.views[i].attribute2 ){
                            skipThis = true; 
                            // console.log("-----------------skip!");


                        } 
                    }
                    if(!skipThis){

                        console.log("filtering: filterList[" + x + "], targetCollection.views[" + i + "]");
                        console.log("old filter: ");
                        console.log(targetCollection.views[i].filter);
                        console.log("adding: ");
                        console.log(filterList[x]);

                        newFilter = targetCollection.views[i].filter.concat(filterList[x]);
                        if( targetCollection.views[i].filter[0]["attr"] == "all")
                            newFilter = filterList[x]; 
                        updatedFilterList.push(newFilter); 
                        console.log(newFilter)
                        this.addViewToCollection( collection, this.copyAndPivotChart( newFilter, this.getAttributeFromName( targetCollection.views[i].attribute1 ), this.getAttributeFromName( targetCollection.views[i].attribute2 ), targetCollection.views[i]) );
                    }
            }
        }

        //update attribute list
        for(i =0 ; i < targetCollection.attributeList.length; i++){
            remove = false; 
            for(j =0 ; j < filterList.length; j++){
                for(k = 0; k < filterList[j].length; k++){
                    if( filterList[j][k]["attr"] == targetCollection.attributeList[i] )
                    {
                        remove = true;
                    }
                }            
            }
            if( remove ){
                console.log("remove : " + targetCollection.attributeList[i] );
                 targetCollection.attributeList.splice(i,1);
                 i--;
            }
        }

        //update secondary attribute list
        for(i =0 ; i < targetCollection.secondaryAttributeList.length; i++){
            remove = false; 
            for(j =0 ; j < filterList.length; j++){
                for(k = 0; k < filterList[j].length; k++){
                    if( filterList[j][k]["attr"] == targetCollection.secondaryAttributeList[i] )
                    {
                        remove = true;
                    }
                }            
            }
            if( remove ){
                console.log("remove : " + targetCollection.secondaryAttributeList[i] );
                 targetCollection.secondaryAttributeList.splice(i,1);
                 i--;
            }
        }


        //alternate update filter
        updatedFilterList = []; 
        for(i =0 ; i < filterList.length; i++){
            for(j = 0; j < targetCollection.filterList.length; j++){
                if( targetCollection.filterList[j][0]["attr"] != "all")
                    updatedFilterList.push( targetCollection.filterList[j].concat( filterList[i] ) );
                else
                    updatedFilterList.push( filterList[i] );
            }
        }


        // if( collection.type == "browse")
        //     collection.filterList = [ updatedFilterList[0] ];
        // else
            collection.filterList = updatedFilterList;

            if( collection.type == "compare_compare_grid" || collection.type == "multifaceted_compare_compare_grid"){

                //recompute the aggregate filter
                collection.aggregatedFilter = targetCollection.aggregatedFilter;
                console.log(collection.aggregatedFilter);

                for(i =0; i < filterList.length; i++){
                    for(j= 0;j<filterList[i].length;j++){ 
                        for(l = 0; l< filterList[i][j]["vals"].length; l ++){
                            val = filterList[i][j]["vals"][l];

                            for(k = 0; k < collection.aggregatedFilter.length;k++){
                                if( filterList[i][j]["attr"] == collection.aggregatedFilter[k]["attr"] ){
                                    countMatch = 0;
                                    for( x = 0; x < collection.aggregatedFilter[k]["vals"].length; x++){
                                        if( collection.aggregatedFilter[k]["vals"][x] == val )
                                            countMatch++;
                                    }

                                    if( countMatch == 0 ){
                                        collection.aggregatedFilter[k]["vals"].push(val );

                                    }

                                }

                            }


                        }

                    }
                }
            }
        this.fixAggFilter( filterList, attributeList, collection ); 
        this.layoutViews(collection);


        // return this.getViewsFromCollection(collection); 
        // return {"vis_collection": this.getViewsFromCollection(collection), "collectionNumber": collection.number, "returnType": "new_collection"};

        return {
            "count": 1, 
            "list": [ 
                { 
                    "vis_collection": this.getViewsFromCollection(collection), 
                    "collectionId": collection.number,
                     "type": "new_collection", 
                     "collection": this.getViewsFromCollection(collection), 
                     "collectionType": collection.type,
                     "collectionFilterList": collection.filterList,
                     "collectionAttributeList": collection.attributeList,
                     "collectionSecondaryAttributeList": collection.secondaryAttributeList,
                     "collectionAggrFilterList": collection.aggregatedFilter,
                     "replaceCollection": false,
                    "gridSpec": collection.gridSpec

                },
            ]
        };
    },
   

    splitTheCollection: function(filterList, attributeList, targetCollection, pivotCount ){

        console.log("SPLIT THE COLLECTION");
        console.log("-- from: ");
        console.log(targetCollection.type);
        console.log(targetCollection.filterList);
        console.log(targetCollection.attributeList);
        console.log("-- to:")
        console.log(attributeList); 

        var collection;

        if(pivotCount == 1){//attributeList.length == 1){
            if( targetCollection.type == "targeted" ){
                collection = this.initCollection("multifaceted", targetCollection.filterList,  targetCollection.attributeList, attributeList ); //preserve the collection type
            }
            else if( targetCollection.type == "browse" ){
                collection = this.initCollection("multifacetedBrowse", targetCollection.filterList, targetCollection.attributeList, attributeList ); //preserve the collection type
            } 
            // else if( targetCollection.type == "complex" ){
            //     collection = this.initCollection("complexAndCompareGrid", filterList, attributeList); //preserve the collection type
            // } 
            else if( targetCollection.type == "comparison"){
                collection = this.initCollection("multifacetedCompare", targetCollection.filterList,  targetCollection.attributeList, attributeList ); //preserve the collection type
            } 
            else if( targetCollection.type == "b+c"){ //FIX
                collection = this.initCollection("multifacetedB+C", targetCollection.filterList,  targetCollection.attributeList, attributeList ); //preserve the collection type
            }  
            else if( targetCollection.type == "compare_compare_grid"){ 
                collection = this.initCollection("multifaceted_compare_compare_grid", targetCollection.filterList, targetCollection.attributeList, attributeList );//targetCollection.secondaryAttributeList); //preserve the collection type
            }      
        } 
        else
        {
            if( targetCollection.type == "targeted" ){
                collection = this.initCollection("sub+attr", targetCollection.filterList, targetCollection.attributeList, attributeList); //preserve the collection type
            }
            else if( targetCollection.type == "browse" ){
                collection = this.initCollection("multifacetedBrowseBySubAttr", targetCollection.filterList, targetCollection.attributeList, attributeList); //preserve the collection type
            } 
            // else if( targetCollection.type == "complex" ){
            //     collection = this.initCollection("complexAndCompareGrid", filterList, attributeList); //preserve the collection type
            // } 
            else if( targetCollection.type == "comparison"){
                collection = this.initCollection("multifacetedCompareBySubAttr", targetCollection.filterList, targetCollection.attributeList, attributeList); //preserve the collection type
            } 
            else if( targetCollection.type == "b+c"){ //FIX
                collection = this.initCollection("multifacetedB+CMultiple", targetCollection.filterList, targetCollection.attributeList, attributeList); //preserve the collection type
            }
            else if( targetCollection.type == "compare_compare_grid"){ 
                collection = this.initCollection("multifaceted_compare_compare_gridMultiple", targetCollection.filterList, targetCollection.attributeList, attributeList);//targetCollection.secondaryAttributeList); //preserve the collection type
            }    
        }
         //= this.initCollection("subsetAndAttributeGrid", filterList, targetCollection.attributeList, attributeList[0] ); //**** this is actually a b+c alternative 


        // for(f = 0; f < targetCollection.views.length; f++){
            for(var j = 0; j < attributeList.length; j++){
                for(var i = 0; i < targetCollection.views.length; i++){
                    console.log("attr: " + attributeList[j] )
                    console.log("view: " + i );

                    //if(targetCollection.views[i].attribute1 !=  attributeList[j] ){
                        this.addViewToCollection(collection, this.makeChart(targetCollection.views[i].filter, this.getAttributeFromName(targetCollection.views[i].attribute1), this.getAttributeFromName(attributeList[j] ) ) );
                    //}
                }
            }

            //fix the attr list
            newAttrList = []; 
            for(i = 0; i < collection.views.length; i++){
                countMatch = 0;
                for(j = 0; j < newAttrList.length; j++){
                    if( newAttrList[j] == collection.views[i].attribute1 )
                        countMatch++;
                }
                if( countMatch == 0)
                    newAttrList.push(collection.views[i].attribute1);
            }
            collection.attributeList = newAttrList;
        // }
      
            if( collection.type == "compare_compare_grid" || collection.type == "multifaceted_compare_compare_grid"){

                //recompute the aggregate filter
                collection.aggregatedFilter = targetCollection.aggregatedFilter;
                console.log(collection.aggregatedFilter);


                for(i =0; i < filterList.length; i++){
                    for(j= 0;j<filterList[i].length;j++){ 
                        for(l = 0; l< filterList[i][j]["vals"].length; l ++){
                            val = filterList[i][j]["vals"][l];

                            for(k = 0; k < collection.aggregatedFilter.length;k++){
                                if( filterList[i][j]["attr"] == collection.aggregatedFilter[k]["attr"] ){
                                    countMatch = 0;
                                    for( x = 0; x < collection.aggregatedFilter[k]["vals"].length; x++){
                                        if( collection.aggregatedFilter[k]["vals"][x] == val )
                                            countMatch++;
                                    }

                                    if( countMatch == 0 ){
                                        collection.aggregatedFilter[k]["vals"].push(val );

                                    }

                                }

                            }


                        }

                    }
                }
            }
        
        this.fixAggFilter( filterList, attributeList, collection ); 
        this.layoutViews(collection);


        // return {"vis_collection": this.getViewsFromCollection(collection), "collectionNumber": collection.number, "returnType": "new_collection"};
        return {
            "count": 1, 
            "list": [ 
                { 
                    "vis_collection": this.getViewsFromCollection(collection), 
                    "collectionId": collection.number,
                     "type": "new_collection", 
                     "collection": this.getViewsFromCollection(collection), 
                     "collectionType": collection.type,
                     "collectionFilterList": collection.filterList,
                     "collectionAttributeList": collection.attributeList,
                     "collectionSecondaryAttributeList": collection.secondaryAttributeList,
                     "collectionAggrFilterList": collection.aggregatedFilter,
                     "replaceCollection": false,
                    "gridSpec": collection.gridSpec

                },
            ]
        };
        // return this.getViewsFromCollection(collection); 
    },

    getFilterString: function( filter ){
        str = ""; 
        for(var x= 0; x < filter.length; x++){
            str += filter[x]["attr"] + " ";
            for(var y = 0; y < filter[x]["vals"].length; y++){
                str += filter[x]["vals"][y] + " ";
            }
        }
        return str;
    },

 
    extendTheCollection: function(filterList, attributeList, targetCollection ){
        console.log( "EXTEND THE COLLECTION");

        console.log("-- from: ");
        console.log(targetCollection.type);
        console.log(targetCollection.number);
        console.log(targetCollection.filterList);
        console.log(targetCollection.attributeList); 
        console.log("-- to: ")
        console.log(filterList); 
        console.log(attributeList); 

        if(targetCollection.type == "browse" || (targetCollection.type == "targeted" && attributeList.length != 0 ) ||  targetCollection.type == "multifacetedBrowse" ){
            
            //check to make sure not adding a duplicate
            for(i =0 ; i < attributeList.length ; i++ ){
                for(j = 0 ; j < targetCollection.attributeList.length; j++ ){
                    if( attributeList[i] == targetCollection.attributeList[j] ){
                        console.log("removing duplicate " + attributeList[i] );

                        attributeList.splice(i, 1); //remove it
                        i--; 
                    }
                }
            }

            if( attributeList.length == 0 ){
                returnObj = {
                        "type": "error",
                        "error":"Looks like you already have this view in the most recent set of views you expanding.  Try a new query."
                    };
                return returnObj;
            }

            //done checking for errors 

            if(targetCollection.type == "targeted") 
                targetCollection.type = "browse"; //it becomes a browse
            if( targetCollection.type == "multifaceted" )
                targetCollection.type = "multifacetedBrowse"; //it becomes a browse
            
            for(var i = 0; i < attributeList.length; i++ ){
                console_debug("----- make a chart for attribute: ")
                console_debug(attributeList[i]);
                attr1 = this.getAttributeFromName (attributeList[i]);
                attr2 = null;
                if( targetCollection.type == "multifacetedBrowse" )
                    attr2 = this.getAttributeFromName(targetCollection.secondaryAttributeList[0]);

                view = this.makeChart( targetCollection.filterList[0], attr1, attr2 ); 
                console_debug(view);
                this.addViewToCollection(targetCollection, view);         
            }

            targetCollection.attributeList = targetCollection.attributeList.concat( attributeList );

        }
        else if( targetCollection.type == "complex" ){

            //insert 
            for(var i = 0; i < attributeList.length; i++ ){
                view = this.makeChart( targetCollection.filterList[0], this.getAttributeFromName( attributeList[i] ), null ); 
                this.insertViewInCollection( targetCollection, view, targetCollection.attributeList.length+i );  
            }

            for(var i = 0; i < attributeList.length; i++ ){
                for(var j =0 ; j < targetCollection.attributeList.length; j++){


                    attrReordered = this.whichAttrIsPrimary(attributeList[i], targetCollection.attributeList[j] ); 
                    console.log("Attribute list reordered " + attrReordered); 
                    if(attrReordered[0] != null ){
                        view = this.makeChart(filterList[i], this.getAttributeFromName(attrReordered[0]), this.getAttributeFromName(attrReordered[1]) ) ;
                        this.addViewToCollection(targetCollection, view);
                    } 



                    // view1 = this.makeChart( targetCollection.filterList[0], this.getAttributeFromName( attributeList[i] ), this.getAttributeFromName(targetCollection.attributeList[j] ) ); 
                    // this.addViewToCollection( targetCollection, view1);  

                    // view2 = this.makeChart( targetCollection.filterList[0], this.getAttributeFromName(targetCollection.attributeList[j] ), this.getAttributeFromName( attributeList[i] ) ); 
                    // this.addViewToCollection( targetCollection, view2);  

                }
            }


            for(var i = 0; i < attributeList.length; i++ ){
                targetCollection.attributeList.push( attributeList[i] );
            }

        }
        else if( targetCollection.type == "comparison" || (targetCollection.type == "targeted" && attributeList.length != 0) || targetCollection.type == "multifacetedCompare"  ){
            if(targetCollection.type == "targeted") 
                targetCollection.type = "comparison"; //mostly this is for targeted
            if(targetCollection.type == "multifaceted") 
                targetCollection.type = "multifacetedCompare"; //mostly this is for targeted

            //check for duplicates 
            for(i =0 ; i < filterList.length; i++){
                filterListIString = this.getFilterString( filterList[i] ); 

                for( j = 0 ; j < targetCollection.filterList.length; j++ ){
                    filterListJString = this.getFilterString( targetCollection.filterList[j] ); 

                    if( filterListIString == filterListJString )
                    {
                        console.log("DUPLICATES " + filterListIString + " " + filterListJString );

                        filterList.splice( i, 1);
                        i--;
                    }
                }
            }

            if( filterList.length == 0 ){
                returnObj = {
                        "type": "error",
                        "error":"Looks like you already have this view in the most recent set of views you expanding.  Try a new query."
                    };
                return returnObj;
            }

            targetCollection.filterList = targetCollection.filterList.concat( filterList ); //maybe? 

            for(var i = 0; i < filterList.length; i++){
                this.addViewToCollection( targetCollection, this.copyAndPivotChart( filterList[i], this.getAttributeFromName( targetCollection.views[0].attribute1 ), this.getAttributeFromName( targetCollection.views[0].attribute2 ), targetCollection.views[0]) );
            }
            console_debug3("after:");
            console_debug3(targetCollection);

            // return this.getViewsFromCollection(targetCollection);  //why?  
        } else if( targetCollection.type == "b+c" || targetCollection.type == "multifacetedB+C" ){

            if( filterList.length != 0 && filterList[0][0]["attr"] != "all"){ //extend in the browse direction
           
           //check for duplicates 
            for(i =0 ; i < filterList.length; i++){
                filterListIString = this.getFilterString( filterList[i] ); 

                for( j = 0 ; j < targetCollection.filterList.length; j++ ){
                    filterListJString = this.getFilterString( targetCollection.filterList[j] ); 

                    if( filterListIString == filterListJString )
                    {
                        console.log("DUPLICATES " + filterListIString + " " + filterListJString );

                        filterList.splice( i, 1);
                        i--;
                    }
                }
            }

            if( filterList.length == 0 ){
                returnObj = {
                        "type": "error",
                        "error":"Looks like you already have this view in the most recent set of views you expanding.  Try a new query."
                    };
                return returnObj;
            }




                for(i =0 ; i < filterList.length; i++){
                    for(j = 0; j < targetCollection.attributeList.length; j++){
                        this.addViewToCollection( targetCollection, this.copyAndPivotChart( filterList[i], this.getAttributeFromName( targetCollection.views[j].attribute1 ), this.getAttributeFromName( targetCollection.views[j].attribute2 ), targetCollection.views[j]) ); 
                    }
                    targetCollection.filterList.push(filterList[i]);

                }
            }
            if( attributeList.length != 0 ){ //extend in the compare direction

                 //check to make sure not adding a duplicate
                for(i =0 ; i < attributeList.length ; i++ ){
                    for(j = 0 ; j < targetCollection.attributeList.length; j++ ){
                        if( attributeList[i] == targetCollection.attributeList[j] ){
                            console.log("removing duplicate " + attributeList[i] );

                            attributeList.splice(i, 1); //remove it
                            i--; 
                        }
                    }
                }

                if( attributeList.length == 0 ){
                    returnObj = {
                            "type": "error",
                            "error":"Looks like you already have this view in the most recent set of views you expanding.  Try a new query."
                        };
                    return returnObj;
                }

                //done checking for errors 

                countInsert = 0; 
                for(i =0 ; i < targetCollection.filterList.length; i++){
                    for(j = 0; j < attributeList.length; j++){
                        view = this.makeChart(targetCollection.filterList[i], this.getAttributeFromName(attributeList[j]), this.getAttributeFromName( targetCollection.secondaryAttributeList[0] ) );
                        // this.insertViewInCollection( targetCollection, view, (i+1)*targetCollection.attributeList.length+j+i); 
                        this.insertViewInCollection( targetCollection, view, (i+1)*targetCollection.attributeList.length+countInsert); 

                        countInsert++;
                    }
                }

                for(j = 0; j < attributeList.length; j++){
                    targetCollection.attributeList.push(attributeList[j]);
                }

            }


        }
        else if( targetCollection.type == "compare_compare_grid" ||  targetCollection.type == "multifaceted_compare_compare_grid" ){
            

            // //first insert new filters into the appropriate place in the aggregated filter
            // console.log("compare by compare grid");
            // countMatch = 0;
            // for(i =0 ; i < filterList.length; i++){ //eg uic theft, uic burglary, rn theft, rn burglary, loop theft, loop burglary
            //     for(j=0; j < filterList[i].length; j++){//eg {attr: neigh, vals: [uic]}, {attr:ct, vals:[theft]
                   
            //         console.log(targetCollection.views );

            //         if( i >= targetCollection.views.length){
            //             //insert now
            //             view = this.makeChart(filterList[i], this.getAttributeFromName(targetCollection.attributeList[0]), this.getAttributeFromName( targetCollection.secondaryAttributeList[0] ) );
            //             // this.insertViewInCollection( targetCollection, view, (i+1)*targetCollection.attributeList.length+j+i); 
            //             this.insertViewInCollection( targetCollection, view, i); 

            //         }
                    
            //         console.log(targetCollection.views[i].filter[j]["vals"][0]);
            //         console.log(filterList[i][j]); 
            //         if( targetCollection.views[i].filter[j]["vals"][0] == filterList[i][j]["vals"][0] )
            //             countMatch++; 
            //     }
            //     if(countMatch == filterList[i].length )
            //     {
            //         console.log("match! " + filterList[i] )
            //     }
            //     else {
            //         console.log("insert here! " + countMatch)
            //         console.log( filterList[i] );
            //       //insert now
            //         view = this.makeChart(filterList[i], this.getAttributeFromName(targetCollection.attributeList[0]), this.getAttributeFromName( targetCollection.secondaryAttributeList[0] ) );
            //         // this.insertViewInCollection( targetCollection, view, (i+1)*targetCollection.attributeList.length+j+i); 
            //         this.insertViewInCollection( targetCollection, view, i); 

            //     }
            //     countMatch = 0;
            // }

            targetCollection.views = []; 
            for(i =0 ; i < filterList.length; i++){ //eg uic theft, uic burglary, rn theft, rn burglary, loop theft, loop burglary
                view = this.makeChart(filterList[i], this.getAttributeFromName(targetCollection.attributeList[0]), this.getAttributeFromName( targetCollection.secondaryAttributeList[0] ) );
                this.addViewToCollection( targetCollection, view, i); 
            }

            targetCollection.filterList = filterList;


            //update the aggregated filter
            if( targetCollection.type == "compare_compare_grid" || targetCollection.type == "multifaceted_compare_compare_grid"){

                //recompute the aggregate filter
                //targetCollection.aggregatedFilter = targetCollection.aggregatedFilter;
                console.log(targetCollection.aggregatedFilter);


                for(i =0; i < filterList.length; i++){
                    for(j= 0;j<filterList[i].length;j++){ 
                        for(l = 0; l< filterList[i][j]["vals"].length; l ++){
                            val = filterList[i][j]["vals"][l];

                            for(k = 0; k < targetCollection.aggregatedFilter.length;k++){
                                if( filterList[i][j]["attr"] == targetCollection.aggregatedFilter[k]["attr"] ){
                                    countMatch = 0;
                                    for( x = 0; x < targetCollection.aggregatedFilter[k]["vals"].length; x++){
                                        if( targetCollection.aggregatedFilter[k]["vals"][x] == val )
                                            countMatch++;
                                    }

                                    if( countMatch == 0 ){
                                        targetCollection.aggregatedFilter[k]["vals"].push(val );

                                    }

                                }

                            }


                        }

                    }
                }
            }
        
        
        console.log("EXTEND CXC GRID FIXED AGG FILTER");
        console.log(targetCollection.aggregatedFilter);
        console.log(targetCollection.filterList);
        }
        else if(targetCollection.type == "complexAndCompareGrid" ){
            console.log("EXTEND COMPLEX AND COMPARE");
            console.log(filterList);
            console.log(attributeList);

            if( filterList.length > 0 && filterList[0][0]["attr"] != "all"){
                    //singles
                for( var i = 0; i < filterList.length; i++){
                  for( var j = 0; j < targetCollection.attributeList.length; j++){
                    this.addViewToCollection(targetCollection, this.makeChart(filterList[i], this.getAttributeFromName(targetCollection.attributeList[j]), null ) );
                    //targetCollection.filterList.push( filterList[i] );

                  }
                }

                targetCollection.filterList = targetCollection.filterList.concat( filterList ); //maybe? 


                //combos
                for( var i = 0; i < filterList.length; i++){
                  for( var j = 0; j < targetCollection.attributeList.length; j++){
                    for( var k = j+1; k < targetCollection.attributeList.length; k++){

                    attrReordered = this.whichAttrIsPrimary(targetCollection.attributeList[j], targetCollection.attributeList[k]);
                    if( attrReordered[0] != null ) {
                        this.addViewToCollection(targetCollection, this.makeChart(filterList[i], this.getAttributeFromName(attrReordered[0]), this.getAttributeFromName(attrReordered[1]) ) );

                    }
                        // this.addViewToCollection(targetCollection, this.makeChart(filterList[i], this.getAttributeFromName(targetCollection.attributeList[j]), this.getAttributeFromName(targetCollection.attributeList[k]) ) );
                        // this.addViewToCollection(targetCollection, this.makeChart(filterList[i], this.getAttributeFromName(targetCollection.attributeList[k]), this.getAttributeFromName(targetCollection.attributeList[j]) ) );

                    }
                  }
                }
            } 
            if ( attributeList.length > 0 ){
                            console.log("attr");

                lengthOfGrid = targetCollection.views.length / targetCollection.filterList.length; 
                diffLength = lengthOfGrid - targetCollection.attributeList.length ; 

                //insert 
                countInsert = 0;
                for( var j = 0 ; j < targetCollection.filterList.length; j++){
                    for(var i = 0; i < attributeList.length; i++ ){
                        view = this.makeChart( targetCollection.filterList[j], this.getAttributeFromName( attributeList[i] ), null ); 
                        //this.insertViewInCollection( targetCollection, view, (targetCollection.attributeList.length*2)*j+targetCollection.attributeList.length+countInsert );//targetCollection.attributeList.length+i );  
                        this.insertViewInCollection( targetCollection, view, ( targetCollection.attributeList.length*(j+1)+countInsert + diffLength*j) );//targetCollection.attributeList.length+i );  

                        countInsert++;
                    }
                }

                countInsert = 0;
                for( var k = 0 ; k < targetCollection.filterList.length; k++){

                    for(var i = 0; i < attributeList.length; i++ ){
                        for(var j =0 ; j < targetCollection.attributeList.length; j++){


                            attrReordered = this.whichAttrIsPrimary(attributeList[i], targetCollection.attributeList[j] ); 
                            console.log("Attribute list reordered " + attrReordered); 
                            if(attrReordered[0] != null ){
                                view = this.makeChart(targetCollection.filterList[k], this.getAttributeFromName(attrReordered[0]), this.getAttributeFromName(attrReordered[1]) ) ;
                                //this.addViewToCollection(targetCollection, view);
                                this.insertViewInCollection( targetCollection, view, (lengthOfGrid)*(k+1)+(attributeList.length*(k+1))+countInsert);   //targetCollection.attributeList.length*2*k+i ); //targetCollection.filterList.length+k );  
                                    countInsert++;
                            } 



                            // view1 = this.makeChart( targetCollection.filterList[0], this.getAttributeFromName( attributeList[i] ), this.getAttributeFromName(targetCollection.attributeList[j] ) ); 
                            // this.addViewToCollection( targetCollection, view1);  

                            // view2 = this.makeChart( targetCollection.filterList[0], this.getAttributeFromName(targetCollection.attributeList[j] ), this.getAttributeFromName( attributeList[i] ) ); 
                            // this.addViewToCollection( targetCollection, view2);  

                        }
                    }
                }


                for(var i = 0; i < attributeList.length; i++ ){
                    targetCollection.attributeList.push( attributeList[i] );
                }
            }
            

        }
        else if( targetCollection.type == "subAttrByMultifacetedCompare" ){

           //check for duplicates 
            for(i =0 ; i < filterList.length; i++){
                filterListIString = this.getFilterString( filterList[i] ); 

                for( j = 0 ; j < targetCollection.filterList.length; j++ ){
                    filterListJString = this.getFilterString( targetCollection.filterList[j] ); 

                    if( filterListIString == filterListJString )
                    {
                        console.log("DUPLICATES " + filterListIString + " " + filterListJString );

                        filterList.splice( i, 1);
                        i--;
                    }
                }
            }

            if( filterList.length == 0 ){
                returnObj = {
                        "type": "error",
                        "error":"Looks like you already have this view in the most recent set of views you expanding.  Try a new query."
                    };
                return returnObj;
            }




            targetCollection.filterList = targetCollection.filterList.concat( filterList ); //maybe? 

            for(var i = 0; i < filterList.length; i++){
                for( j = 0; j < targetCollection.secondaryAttributeList.length; j++ ){
                    this.addViewToCollection( targetCollection, this.copyAndPivotChart( filterList[i], this.getAttributeFromName( targetCollection.views[0].attribute1 ), this.getAttributeFromName( targetCollection.secondaryAttributeList[j] ), targetCollection.views[j]) );
                }
            }
            console_debug3("after:");
            console_debug3(targetCollection);

        }
        else if( targetCollection.type == "subAttrByMultifacetedBrowse" ){
            // targetCollection.filterList = targetCollection.filterList.concat( filterList ); //maybe? 

              //check to make sure not adding a duplicate
            for(i =0 ; i < attributeList.length ; i++ ){
                for(j = 0 ; j < targetCollection.attributeList.length; j++ ){
                    if( attributeList[i] == targetCollection.attributeList[j] ){
                        console.log("removing duplicate " + attributeList[i] );

                        attributeList.splice(i, 1); //remove it
                        i--; 
                    }
                }
            }

            if( attributeList.length == 0 ){
                returnObj = {
                        "type": "error",
                        "error":"Looks like you already have this view in the most recent set of views you expanding.  Try a new query."
                    };
                return returnObj;
            }

            targetCollection.attributeList = targetCollection.attributeList.concat( attributeList ); //maybe? 


            for(var i = 0; i < attributeList.length; i++){
                for( j = 0; j < targetCollection.secondaryAttributeList.length; j++ ){
                    this.addViewToCollection( targetCollection, this.copyAndPivotChart( targetCollection.filterList[0], this.getAttributeFromName( attributeList[i] ), this.getAttributeFromName( targetCollection.secondaryAttributeList[j] ), targetCollection.views[j]) );
                }
            }
            console_debug3("after:");
            console_debug3(targetCollection);

        }
        else if( targetCollection.type == "multifacetedCompareBySubAttr" ){

            //check for duplicates 
            for(i =0 ; i < filterList.length; i++){
                filterListIString = this.getFilterString( filterList[i] ); 

                for( j = 0 ; j < targetCollection.filterList.length; j++ ){
                    filterListJString = this.getFilterString( targetCollection.filterList[j] ); 

                    if( filterListIString == filterListJString )
                    {
                        console.log("DUPLICATES " + filterListIString + " " + filterListJString );

                        filterList.splice( i, 1);
                        i--;
                    }
                }
            }

            if( filterList.length == 0 ){
                returnObj = {
                        "type": "error",
                        "error":"Looks like you already have this view in the most recent set of views you expanding.  Try a new query."
                    };
                return returnObj;
            }




            countInsert = 0;
            dim = targetCollection.filterList.length; 
            for(var j = 0; j < targetCollection.secondaryAttributeList.length; j++){
                for( var i = 0; i < filterList.length; i++){
                    view = this.copyAndPivotChart( filterList[i], this.getAttributeFromName( targetCollection.views[0].attribute1 ), this.getAttributeFromName( targetCollection.secondaryAttributeList[j] ), targetCollection.views[j]);
                    this.insertViewInCollection( targetCollection, view, dim*j+1+countInsert); //(i+1)*targetCollection.attributeList.length+countInsert); 
                    countInsert++;

                    //this.addViewToCollection( targetCollection, this.copyAndPivotChart( filterList[i], this.getAttributeFromName( targetCollection.views[0].attribute1 ), this.getAttributeFromName( targetCollection.secondaryAttributeList[j] ), targetCollection.views[j]) );
                }
            }

            targetCollection.filterList = targetCollection.filterList.concat( filterList ); //maybe? 

        }
        else if( targetCollection.type == "multifacetedBrowseBySubAttr" ){
            // targetCollection.filterList = targetCollection.filterList.concat( filterList ); //maybe? 

              //check to make sure not adding a duplicate
            for(i =0 ; i < attributeList.length ; i++ ){
                for(j = 0 ; j < targetCollection.attributeList.length; j++ ){
                    if( attributeList[i] == targetCollection.attributeList[j] ){
                        console.log("removing duplicate " + attributeList[i] );

                        attributeList.splice(i, 1); //remove it
                        i--; 
                    }
                }
            }

            if( attributeList.length == 0 ){
                returnObj = {
                        "type": "error",
                        "error":"Looks like you already have this view in the most recent set of views you expanding.  Try a new query."
                    };
                return returnObj;
            }


            for(var i = 0; i < attributeList.length; i++){
                for( j = 0; j < targetCollection.secondaryAttributeList.length; j++ ){
                    this.addViewToCollection( targetCollection, this.copyAndPivotChart( targetCollection.filterList[0], this.getAttributeFromName( attributeList[i] ), this.getAttributeFromName( targetCollection.secondaryAttributeList[j] ), targetCollection.views[j]) );
                }
            }
            console_debug3("after:");
            console_debug3(targetCollection);

        }

        //loop through existing views
        // for(i = 0; i < targetCollection.views.length; i++){
        //     existingFilter = targetCollection.views[i].filter; 

        //     //does this filter match the first one in the list?
        //     countMatch = 0;
        //     for(j = 0; j < existingFilter.length; j++){
        //         console_debug(existingFilter[j]);
        //         console_debug(filterList[j])
        //     }

        // }

       this.fixAggFilter( filterList, attributeList, targetCollection ); 

       collection = targetCollection;
       this.layoutViews(collection);


        // return this.getViewsFromCollection( targetCollection );
        // return {"vis_collection": this.getViewsFromCollection(targetCollection), "collectionNumber": targetCollection.number, "returnType": "modify_collection"};
        return {
            "count": 1, 
            "list": [ 
                { 
                    "vis_collection": this.getViewsFromCollection(collection), 
                    "collectionId": collection.number,
                     "type": "modify_collection", 
                     "collection": this.getViewsFromCollection(collection), 
                     "collectionType": collection.type,
                     "collectionFilterList": collection.filterList,
                     "collectionAttributeList": collection.attributeList,
                     "collectionSecondaryAttributeList": collection.secondaryAttributeList,
                     "collectionAggrFilterList": collection.aggregatedFilter,
                     "replaceCollection": false,
                    "gridSpec": collection.gridSpec

                },
            ]
        };
    },

    splitPivot: function(filterList, attributeList, targetCollection, pivotCount ){
        // if( pivotCount == 1){//filterList.length == 1){
        //   collection = this.initCollection(targetCollection.type, targetCollection.filterList, targetCollection.attributeList, targetCollection.secondaryAttributeList);   
        
        console.log("SPLIT PIVOT--------");
        console.log(attributeList);

        //check to make sure not adding a duplicate
            for(i =0 ; i < attributeList.length ; i++ ){
                for(j = 0 ; j < targetCollection.secondaryAttributeList.length; j++ ){
                    if( attributeList[i] == targetCollection.secondaryAttributeList[j] ){
                        console.log("removing duplicate " + attributeList[i] );

                        attributeList.splice(i, 1); //remove it
                        i--; 
                    }
                }
            }

            if( attributeList.length == 0 ){
                console.log("Found the error!");
                returnObj = {
                        "type": "error",
                        "error":"Looks like you already have this view in the most recent set of views you expanding.  Try a new query."
                    };
                return returnObj;
            }

            //done checking for errors 

        viewList = loadash.cloneDeep( targetCollection.views );


        // } 
        // else
        // {
        //     // if( targetCollection.type == "targeted" ){
        //     //     collection = this.initCollection("comparison", filterList, targetCollection.attributeList); //preserve the collection type
        //     // }
        //     // else if( targetCollection.type == "browse" ){
        //     //     collection = this.initCollection("b+c", filterList, targetCollection.attributeList); //preserve the collection type
        //     // } 
        //     if( targetCollection.type == "complex" ){
        //         collection = this.initCollection("complexAndCompareGrid", targetCollection.filterList, targetCollection.attributeList); //preserve the collection type
        //     } 
        //     // else if( targetCollection.type == "comparison"){
        //     //     collection = this.initCollection("compareByCompareGrid", filterList, targetCollection.attributeList); //preserve the collection type
        //     // } 
        //     // else if( targetCollection.type == "b+c"){ //FIX
        //     //     collection = this.initCollection("b+cMultiple", filterList, targetCollection.attributeList); //preserve the collection type
        //     // }
        //     else if( targetCollection.type == "sub+attr"){ //FIX
        //         collection = this.initCollection("multifacetedCompareBySubAttr", targetCollection.filterList, targetCollection.attributeList); //preserve the collection type
        //     }
        //     // else if( targetCollection.type == "compare_compare_grid"){  
        //     //     collection = this.initCollection("compare_compare_gridMultiple", filterList, targetCollection.attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
        //     // }
        //     else if( targetCollection.type == "multifacetedBrowse" ){
        //         collection = this.initCollection("multifacetedB+C", targetCollection.filterList, targetCollection.attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
        //     }
        //     else if( targetCollection.type == "multifacetedCompare" ){
        //         collection = this.initCollection("multifaceted_compare_compare_grid", targetCollection.filterList, targetCollection.attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
        //     }  
        //     else if( targetCollection.type == "complexAndCompareGrid" ){
        //         collection = this.initCollection("complexAndCompareGridMultiple", targetCollection.filterList, targetCollection.attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
        //     } 
        //     else if( targetCollection.type == "multifacetedCompareBySubAttr"){ 
        //         collection = this.initCollection("multifacetedCompareBySubAttrMultiple", targetCollection.filterList, targetCollection.attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
        //     }
        //     else if( targetCollection.type == "multifacetedBrowseBySubAttr"){ 
        //         collection = this.initCollection("multifacetedBrowseBySubAttrMultiple", targetCollection.filterList, targetCollection.attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
        //     } 
        //     else if( targetCollection.type == "multifaceted_compare_compare_grid" ){
        //         collection = this.initCollection("multifacetedB+C", targetCollection.filterList, targetCollection.attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
        //     }           
        // 
            returnType = "modify_collection"; 
            collection = targetCollection; // you are adding to the target collection

            if( targetCollection.type == "multifaceted" ){
                collection.type = "sub+attr"; 
            } 
            // if( targetCollection.type == "complex" ){ //TO DO
            //     collection = this.initCollection("complexAndCompareGrid", targetCollection.filterList, targetCollection.attributeList); //preserve the collection type
            // } 
            //else if( targetCollection.type == "sub+attr"){ //FIX
                //collection = this.initCollection("multifacetedCompareBySubAttr", targetCollection.filterList, targetCollection.attributeList); //preserve the collection type
                //type stays the same
            //}
            // else if( targetCollection.type == "compare_compare_grid"){  
            //     collection = this.initCollection("compare_compare_gridMultiple", filterList, targetCollection.attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
            // }
            else if( targetCollection.type == "multifacetedBrowse" ){
                collection.type = "multifacetedBrowseBySubAttr"; //preserve the collection type
            }
            else if( targetCollection.type == "multifacetedCompare" ){
                collection.type = "multifacetedCompareBySubAttr"; //collection = this.initCollection("multifaceted_compare_compare_grid", targetCollection.filterList, targetCollection.attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
            }  
            // else if( targetCollection.type == "complexAndCompareGrid" ){
            //     collection = this.initCollection("complexAndCompareGridMultiple", targetCollection.filterList, targetCollection.attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
            // } 
            // else if( targetCollection.type == "multifacetedCompareBySubAttr"){ 
            //     collection = this.initCollection("multifacetedCompareBySubAttrMultiple", targetCollection.filterList, targetCollection.attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
            // }
            // else if( targetCollection.type == "multifacetedBrowseBySubAttr"){ 
            //     collection = this.initCollection("multifacetedBrowseBySubAttrMultiple", targetCollection.filterList, targetCollection.attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
            // } 
            else if( targetCollection.type == "multifaceted_compare_compare_grid" ){
                collection = this.initCollection("multifaceted_compare_compare_grid", targetCollection.filterList, targetCollection.attributeList, targetCollection.secondaryAttributeList); //preserve the collection type
                returnType = "new_collection"; 
            }           
        


        for(var j = 0; j < attributeList.length; j++){
            collection.secondaryAttributeList.push(attributeList[j]);

            for( var i = 0; i < viewList.length; i++){//targetCollection.views.length; i++ ){
                // console.log("TARGET  VIEWS ");
                // console.log(i);
                // console.log(j);

                // console.log(targetCollection.views[i].filter);
                // console.log( attributeList[j] );
                // console.log( viewList.length );


                this.addViewToCollection( collection, this.copyAndPivotChart( targetCollection.views[i].filter, this.getAttributeFromName( targetCollection.views[i].attribute1 ), this.getAttributeFromName(attributeList[j]), viewList[i]) );
            }
        }

       this.fixAggFilter( filterList, attributeList, collection ); 
       this.layoutViews(collection);


        // return this.getViewsFromCollection( collection );
        // return {"vis_collection": this.getViewsFromCollection(collection), "collectionNumber": collection.number, "returnType": "new_collection"};
        return {
            "count": 1, 
            "list": [ 
                { 
                    "vis_collection": this.getViewsFromCollection(collection), 
                    "collectionId": collection.number,
                     "type": returnType,//"new_collection", 
                     "collection": this.getViewsFromCollection(collection), 
                     "collectionType": collection.type,
                     "collectionFilterList": collection.filterList,
                     "collectionAttributeList": collection.attributeList,
                     "collectionSecondaryAttributeList": collection.secondaryAttributeList,
                     "collectionAggrFilterList": collection.aggregatedFilter,
                     "replaceCollection": false,
                    "gridSpec": collection.gridSpec

                },
            ]
        };



    },


    splitPivotExtend: function(filterList, attributeList, targetCollection ){

        console.log("split pivot extend");
        console.log(attributeList);

        //check to make sure not adding a duplicate
            for(i =0 ; i < attributeList.length ; i++ ){
                for(j = 0 ; j < targetCollection.secondaryAttributeList.length; j++ ){
                    if( attributeList[i] == targetCollection.secondaryAttributeList[j] ){
                        console.log("removing duplicate " + attributeList[i] );
                        attributeList.splice(i, 1); //remove it
                        i--; 
                    }
                }
            }

            if( attributeList.length == 0 ){
                console.log("Found the error!");
                returnObj = {
                        "type": "error",
                        "error":"Looks like you already have this view in the most recent set of views you expanding.  Try a new query."
                    };
                return returnObj;
            }

            //done checking for errors 

        if( targetCollection.type == "multifaceted"){
            targetCollection.type = "sub+attr"; 
            for(var i = 0; i < attributeList.length; i++){
                this.addViewToCollection( targetCollection, this.copyAndPivotChart( targetCollection.filterList[0], this.getAttributeFromName( targetCollection.attributeList[0] ), this.getAttributeFromName( attributeList[i] ), targetCollection.views[0] ) );
                targetCollection.secondaryAttributeList.push( attributeList[i] );
            }
            this.fixAggFilter( filterList, attributeList, targetCollection ); 
            this.layoutViews(targetCollection);
        }
        if( targetCollection.type == "sub+attr"){

            for(var i = 0; i < attributeList.length; i++){
                this.addViewToCollection( targetCollection, this.copyAndPivotChart( targetCollection.filterList[0], this.getAttributeFromName( targetCollection.attributeList[0] ), this.getAttributeFromName( attributeList[i] ), targetCollection.views[0] ) );
                targetCollection.secondaryAttributeList.push( attributeList[i] );
            }
            this.fixAggFilter( filterList, attributeList, targetCollection ); 
            this.layoutViews(targetCollection);
        }
        if( targetCollection.type == "subAttrByMultifacetedBrowse"){
            countInsert = 0; 
            for(i =0 ; i < targetCollection.attributeList.length; i++){
                for(j = 0; j < attributeList.length; j++){
                    view = this.makeChart(targetCollection.filterList[0], this.getAttributeFromName(targetCollection.attributeList[i]), this.getAttributeFromName( attributeList[j] ) );
                    // this.insertViewInCollection( targetCollection, view, (i+1)*targetCollection.attributeList.length+j+i); 
                    this.insertViewInCollection( targetCollection, view, (i+1)*targetCollection.attributeList.length+countInsert); 

                    countInsert++;
                }
            }

            for(j = 0; j < attributeList.length; j++){
                targetCollection.secondaryAttributeList.push(attributeList[j]);
            }


            this.fixAggFilter( filterList, attributeList, targetCollection ); 
            this.layoutViews(targetCollection);
        }
        if( targetCollection.type == "multifacetedBrowseBySubAttr"){
            for(var i = 0; i < attributeList.length; i++){
                for(var j =0; j < targetCollection.filterList.length; j++ ){
                    this.addViewToCollection( targetCollection, this.copyAndPivotChart( targetCollection.filterList[j], this.getAttributeFromName( targetCollection.attributeList[0] ), this.getAttributeFromName( attributeList[i] ), targetCollection.views[j] ) );

                }
                targetCollection.secondaryAttributeList.push( attributeList[i] );

            }

            this.fixAggFilter( filterList, attributeList, targetCollection ); 
            columns = targetCollection.filterList.length;
            targetCollection.gridSpec = "repeat( " + columns + ", 1fr)";

          
        } if( targetCollection.type == "multifacetedCompareBySubAttr"){
            
            for(var i = 0; i < attributeList.length; i++){
                for(var j =0; j <targetCollection.filterList.length; j++ ){
                    this.addViewToCollection( targetCollection, this.copyAndPivotChart( targetCollection.filterList[j], this.getAttributeFromName( targetCollection.attributeList[0] ), this.getAttributeFromName( attributeList[i] ), targetCollection.views[j] ) );

                }
                targetCollection.secondaryAttributeList.push( attributeList[i] );

            }

            this.fixAggFilter( filterList, attributeList, targetCollection ); 
            columns = targetCollection.filterList.length;
            targetCollection.gridSpec = "repeat( " + columns + ", 1fr)";
        } if( targetCollection.type == "subAttrByMultifacetedCompare"){
            // countInsert = 0; 
            // for(i =0 ; i < targetCollection.attributeList.length; i++){
            //     for(j = 0; j < attributeList.length; j++){
            //         view = this.makeChart(targetCollection.filterList[0], this.getAttributeFromName(targetCollection.attributeList[i]), this.getAttributeFromName( attributeList[j] ) );
            //         // this.insertViewInCollection( targetCollection, view, (i+1)*targetCollection.attributeList.length+j+i); 
            //         this.insertViewInCollection( targetCollection, view, (i+1)*targetCollection.attributeList.length+countInsert); 

            //         countInsert++;
            //     }
            // }

            // for(j = 0; j < attributeList.length; j++){
            //     targetCollection.secondaryAttributeList.push(attributeList[j]);
            // }

            // for(var i = 0; i < attributeList.length; i++){
            //     count = 0;
            //     for( var j =0; j < targetCollection.attributeList.length)
            //     this.addViewToCollection( targetCollection, this.copyAndPivotChart( targetCollection.filterList[0], this.getAttributeFromName( targetCollection.attributeList[0] ), this.getAttributeFromName( attributeList[i] ), targetCollection.views[0] ) );
            //     targetCollection.secondaryAttributeList.push( attributeList[i] );
            // }


             countInsert = 0; 
                for(var i =0 ; i < targetCollection.filterList.length; i++){
                    for(var j = 0; j < attributeList.length; j++){
                        view = this.makeChart(targetCollection.filterList[i], this.getAttributeFromName(targetCollection.attributeList[0]), this.getAttributeFromName( attributeList[j] ) );
                        // this.insertViewInCollection( targetCollection, view, (i+1)*targetCollection.attributeList.length+j+i); 
                        this.insertViewInCollection( targetCollection, view, (i+1)*targetCollection.secondaryAttributeList.length+countInsert); 

                        countInsert++;
                    }
                }

                for(var j = 0; j < attributeList.length; j++){
                    targetCollection.secondaryAttributeList.push(attributeList[j]);
                }
            this.fixAggFilter( filterList, attributeList, targetCollection ); 
            columns = targetCollection.secondaryAttributeList.length;
            targetCollection.gridSpec = "repeat( " + columns + ", 1fr)";
        }




        // return this.getViewsFromCollection( targetCollection );
        // return {"vis_collection": this.getViewsFromCollection(targetCollection), "collectionNumber": targetCollection.number, "returnType": "modify_collection"};
        return {
            "count": 1, 
            "list": [ 
                { 
                    "vis_collection": this.getViewsFromCollection(targetCollection), 
                    "collectionId": targetCollection.number,
                     "type": "modify_collection", 
                     "collection": this.getViewsFromCollection(targetCollection), 
                     "collectionType": targetCollection.type,
                     "collectionFilterList": targetCollection.filterList,
                     "collectionAttributeList": targetCollection.attributeList,
                     "collectionSecondaryAttributeList": targetCollection.secondaryAttributeList,
                     "collectionAggrFilterList": targetCollection.aggregatedFilter,
                     "replaceCollection": false,
                    "gridSpec": targetCollection.gridSpec

                },
            ]
        };


    },

    deleteFromCollection: function(filterList, attributeList, targetCollection ){

        //Filter
        toDelete = [];

        for(x = 0; x < targetCollection.views.length; x++){
            filter = targetCollection.views[x].filter;
            for(y = 0; y < filter.length; y++){
                for(i =0; i < filterList.length; i++){
                    for(j= 0;j<filterList[i].length;j++){ 
                        for(l = 0; l< filterList[i][j]["vals"].length; l ++){
                            val1 = filterList[i][j]["vals"][l];


                    
                            if( filter[y]["attr"] == filterList[i][j]["attr"] ){
                                for(z = 0; z < filter[y]["vals"].length; z++){
                                    val2 = filter[y]["vals"][z];

                                    if( val2 == val1 ){//IF MATCH
                                        toDelete.push( x ); 
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }


        //Attr
        for(x = 0; x < targetCollection.views.length; x++){

            for(i = 0; i < attributeList.length; i++){
                if( targetCollection.views[x].attribute1 == attributeList[i] )
                    toDelete.push(x);
                else if( targetCollection.views[x].attribute2 == attributeList[i] )
                    toDelete.push(x);
            }
        }

        //remove duplicates
        for(x = 0; x < toDelete.length; x++){

            for(i = x+1 ; i < toDelete.length; i++){
                if( toDelete[x] == toDelete[i] ){
                    toDelete.splice(i,1); 
                    i--;
                }
            }
        }

        //sort
        toDelete.sort( function(a, b){ return a-b; } );


        //remove views
        count = 0;
        for(i =0; i < toDelete.length; i++){
            console.log("removing : " + (toDelete[i]-count));
            console.log( targetCollection.views[ toDelete[i]-count ].filter );
            console.log( targetCollection.views[ toDelete[i]-count ].attribute1 );
            console.log( targetCollection.views[ toDelete[i]-count ].attribute2 );


            targetCollection.views.splice( toDelete[i]-count, 1);
            count++; 
        }


        //CLEANUP


        //remove from attribute list
        for(x = 0; x < targetCollection.attributeList.length; x++){
            for(i = 0; i < attributeList.length; i++){
                if( targetCollection.attributeList[x] == attributeList[i] ){
                    targetCollection.attributeList.splice( x, 1);
                    x--;
                }
            }
        }

        for(x = 0; x < targetCollection.secondaryAttributeList.length; x++){
            for(i = 0; i < attributeList.length; i++){
                if( targetCollection.secondaryAttributeList[x] == attributeList[i] ){
                    targetCollection.secondaryAttributeList.splice( x, 1);
                    x--;
                }
            }
        }

        toDelete = [];

        for(x = 0; x < targetCollection.filterList.length; x++){
            // filter = targetCollection.filterList[x];
            for(y = 0; y < targetCollection.filterList[x].length; y++){ 
                for(i =0; i < filterList.length; i++){
                    for(j= 0;j<filterList[i].length;j++){ 
                        for(l = 0; l< filterList[i][j]["vals"].length; l ++){
                            val1 = filterList[i][j]["vals"][l];

                            if( targetCollection.filterList[x][y]["attr"] == filterList[i][j]["attr"] ){
                                for(z = 0; z < targetCollection.filterList[x][y]["vals"].length; z++){
                                    val2 = targetCollection.filterList[x][y]["vals"][z];

                                    if( val2 == val1 ){//IF MATCH
                                        //targetCollection.filterList[x][y]["vals"].splice(z,1);
                                        toDelete.push(x); 
                                    }
                                }
                            }

                            // if( targetCollection.filterList[x][y]["vals"].length == 0 ){
                            //     targetCollection.filterList[x].splice(y,1);
                            //     y--;
                            // }

                        }
                    }
                }
            }
        }

        count = 0;
        for(x = 0; x < toDelete.length; x++){
            targetCollection.filterList.splice(toDelete[x]-count,1);
            count++;
        }

       this.fixAggFilter( filterList, attributeList, targetCollection ); 

       //which type is it now? 
       //all - if filter list is 1, attribute list is 1, and secondary attribute list is 0
            //target
        //all- if filter list is 1, attribute list is 1, secondary attribute list is 1
            //multifaceted single view

       //browse: if attribute list is 1 -> target
       //compare: if filterList is 1 -> target
       //complex: if atribute list is 1 -> target
       if( targetCollection.filterList.length == 1 
            &&  targetCollection.attributeList.length == 1 
                && targetCollection.secondaryAttributeList.length == 0){
            targetCollection.type = "targeted";
       }
       else if( targetCollection.filterList.length == 1 
            &&  targetCollection.attributeList.length == 1 
                && targetCollection.secondaryAttributeList.length == 1){
            targetCollection.type = "multifaceted";
       }

       //b+c: if attribute list is 1 -> compare
       //b+c: if filter list is 1 -> browse
       if( targetCollection.type == "b+c" ){
            if( targetCollection.filterList.length == 1 )
                targetCollection.type = "browse";
            if( targetCollection.attributeList.length == 1 )
                targetCollection.type = "compare";           
       }

       //multifaceted b+c: if attr list is 1, multifaceted compare
       //multifaceted b+C: if filte rlist is 1, multifaceted browse
        if( targetCollection.type == "multifacetedB+C" ){
            if( targetCollection.filterList.length == 1 )
                targetCollection.type = "multifacetedBrowse";
            if( targetCollection.attributeList.length == 1 )
                targetCollection.type = "multifacetedCompare";           
       }

       //complex by compare: if filter list is 1 -> complex
       //complex by compare: if attribute list is 1 -> compare
        if( targetCollection.type == "complexAndCompareGrid" ){
            if( targetCollection.filterList.length == 1 )
                targetCollection.type = "complex";
            if( targetCollection.attributeList.length == 1 )
                targetCollection.type = "compare";           
       }

       //sub+attr if secondary attr list is 1-> multifaceted single view
        if( targetCollection.type == "sub+attr" ){
            if( targetCollection.secondaryAttributeList.length == 1 )
                targetCollection.type = "multifaceted";        
       }


       //multifacetedCompare if filter is 1, multifaceted single view
       if( targetCollection.type == "multifacetedCompare" ){
            if( targetCollection.secondaryAttributeList.length == 1 )
                targetCollection.type = "multifaceted";        
       }

       //multifacetedBrowse, if attr is 1, multifaceted single view
       if( targetCollection.type == "multifacetedBrowse" ){
            if( targetCollection.attributeList.length == 1 )
                targetCollection.type = "multifaceted";        
       }

       //multifaceted compare by sub+attr- if filter list list is 1, sub plus attr
       //multifaceted compare by sub+attr- if secondary attr list is 1, multifaceted compare
        if( targetCollection.type == "multifacetedCompareBySubAttr" ){
            if( targetCollection.filterList.length == 1 )
                targetCollection.type = "sub+attr";
            if( targetCollection.secondaryAttributeList.length == 1 )
                targetCollection.type = "multifacetedCompare";           
       }

       //multifaceted browse by sub+attr- if secondary attr list is 1, multifaceted browse
       //multifaceted browse by sub+attr- if attr list is 1, sub+attr
        if( targetCollection.type == "multifacetedBrowseBySubAttr" ){
            if( targetCollection.filterList.length == 1 )
                targetCollection.type = "sub+attr";
            if( targetCollection.secondaryAttributeList.length == 1 )
                targetCollection.type = "multifacetedBrowse";           
       }
        

        //cxc: ... complicated, check agg filter? 
        if( targetCollection.type == "compare_compare_grid" ){

            countGreaterThanTwo = 0;
            for(i =0; i < targetCollection.aggregatedFilter.length ; i++){
                if( targetCollection.aggregatedFilter[i]["vals"].length >= 2 ){
                    countGreaterThanTwo++;
                }
            }

            if( countGreaterThanTwo == 1 )
                targetCollection.type = "compare";
            if( countGreaterThanTwo == 0 )
                targetCollection.type = "targeted"; //should never happen?        
       } 
       //multifaceted compare x multifaceted compare- not sure


       console.log(targetCollection.type);

       console.log(targetCollection.aggregatedFilter);
       collection = targetCollection;
       this.layoutViews(collection);


        // return { "vis_collection": this.getViewsFromCollection( targetCollection ), "collectionNumber": targetCollection.number, "returnType": "modify_collection" };
        return {
            "count": 1, 
            "list": [ 
                { 
                    "vis_collection": this.getViewsFromCollection(collection), 
                    "collectionId": collection.number,
                     "type": "modify_collection", 
                     "collection": this.getViewsFromCollection(collection), 
                     "collectionType": collection.type,
                     "collectionFilterList": collection.filterList,
                     "collectionAttributeList": collection.attributeList,
                     "collectionSecondaryAttributeList": collection.secondaryAttributeList,
                     "collectionAggrFilterList": collection.aggregatedFilter,
                     "replaceCollection": false,
                     "gridSpec": collection.gridSpec
                },
            ]
        };
    },


    selectFromCollection: function(filterList, attributeList, targetCollection ){

        var collection; 

        console.log("original list:")
        console.log(targetCollection.filterList);
        console.log("filter list:")
        console.log(filterList);


        //update filter
        var newFilterList = []; 
        for(i =0; i < filterList.length; i++){
            // for(j =0 j < filterList[i].length; j++){ //for all filters expressed 
            
                for(x = 0; x < targetCollection.filterList.length; x++){
                    newFilter = loadash.cloneDeep(targetCollection.filterList[x]);
                    newFilter = newFilter.concat(filterList[i]);
                    if( targetCollection.filterList[x][0]["attr"] == "all")
                        newFilter = loadash.cloneDeep( filterList[i] ); 
                    console.log(newFilter);
                    newFilterList.push(newFilter);            

                } 
        }

        console.log("Filter updated: ");
        console.log(newFilterList);


        if( targetCollection.type == "targeted"){
            if( filterList.length == 1 ){ //single selection
                
                if( attributeList.length == 1 ){//single pivot 
                    return this.createTargeted( newFilterList[0], attributeList );
                } else { //multi-pivot
                    return this.createSubsetPartial( newFilterList[0], attributeList );
                }
            }
            else { //multi-select 
                if( attributeList.length == 1 ){//single pivot 
                    return this.createComparisonSet( newFilterList, attributeList );

                } else { //multi-pivot
                    return this.createBrowseAndCompare( newFilterList, attributeList );
                }
            }

        }
        else if( targetCollection.type == "comparison"){
            if( filterList.length == 1 ){ //single selection  
                if( attributeList.length == 1 ){//single pivot 
                    return this.createComparisonSet( newFilterList, attributeList );
                } else { //multi-pivot
                    return this.createBrowseAndCompare( newFilterList, attributeList );
                }
            }
            else { //multi-select 
                if( attributeList.length == 1 ){//single pivot 
                    //aggFilter = createAggFilter( newFilterList, )
                    console.log("compare by compare!!");
                    console.log( newFilterList );
                    aggFilter = this.createAggFilter(newFilterList);
                    return this.createCompareByCompare( newFilterList, attributeList, aggFilter );

                } else { //multi-pivot
                    //multiple b+c
                    //return this.createBrowseAndCompare( newFilterList, attributeList );
                }
            }
        }
        else if( targetCollection.type == "multifacetedCompare"){

        }

          //Compare then select 
  // //----- single selection, single pivot: produces comparison set with overview set
  // { query: "I want to compare UIC and Loop thefts by the day of the week that the crime occurred", expected: "Independent, Create Many, Compare" },
  // { query: "This but select Monday and show by month", expected: "Select" }, //creates a comparison set

  // //----- single selections, multiple pivot: 
  // { query: "I want to compare UIC and Loop thefts by the day of the week that the crime occurred", expected: "Independent, Create Many, Compare" },
  // { query: "This but select Monday and show by month and year", expected: "Select" }, //creates b+c with overview collection

  // //----- multiple selection, single pivot: produces comparison set with overview set
  // { query: "I want to compare UIC and Loop thefts by the day of the week that the crime occurred", expected: "Independent, Create Many, Compare" },
  // { query: "This but select Monday and Tuesday and show by month", expected: "Select" }, //creates a cxc set

  // //----- multiple selections, multiple pivot: 
  // { query: "I want to compare UIC and Loop thefts by the day of the week that the crime occurred", expected: "Independent, Create Many, Compare" },
  // { query: "This but select Monday and Tuesday and show by month and year", expected: "Select" }, //creates multiple b+c or multiple cxc (sister b+c becomes cxc?)

    },

    fixAggFilter: function( filterList, attributeList, targetCollection ){
                //new aggr filter
        agFilter = []; 
        for(x = 0; x < targetCollection.filterList.length; x++){
            // filter = targetCollection.filterList[x];
            for(y = 0; y < targetCollection.filterList[x].length; y++){ 
                countMatch = 0; 
                for( i = 0; i < agFilter.length; i++){
                    if( agFilter[i]["attr"] == targetCollection.filterList[x][y]["attr"] ){
                        countMatch++; 

                        for( z = 0; z < targetCollection.filterList[x][y]["vals"].length; z++){
                            findVal = false; 
                            for(j =0; j < agFilter[i]["vals"].length; j++){
                                if(targetCollection.filterList[x][y]["vals"][z] ==  agFilter[i]["vals"][j] )
                                    findVal = true; 
                            }
                            if( !findVal )
                                agFilter[i]["vals"].push( loadash.cloneDeep(targetCollection.filterList[x][y]["vals"][z]) );
                        }
                    }
                }

                if( countMatch == 0 ){
                    agFilter.push( loadash.cloneDeep( targetCollection.filterList[x][y]) );
                }

            }
        }

        console.log( agFilter);

        targetCollection.aggregatedFilter = agFilter;
    },

    createAggFilter: function(filterList){

        aggFilter = [];
        for(i =0; i < filterList.length; i++){// [i]: [ {attr: ..., val}, {attr:  val:  } ]
            for(j =0; j < filterList[i].length; j++){ //{attr ... val }
                found = false;
                for(k =0; k < aggFilter.length; k++){
                    if( aggFilter[k]["attr"] == filterList[i][j]["attr"] ){
                        aggFilter[k]["vals"].concat(filterList[i][j]["vals"]);  
                        found = true;
                    }                 
                }
                if(!found)
                    aggFilter.push( filterList[i][j] ); 
            }
        }
        return aggFilter;
    },


    integrateNewViewsTogether: function( pivotType, collection, targetCollection, pivotCount ){

        if(  targetCollection.type == "targeted" ){
            if( pivotType == "pivotTheSubset "){

            }

        }


    },

    layoutViews: function( collection ){

        // if( collection.type == "browse"){

        //     centerX = 700;
        //     centerY = 700; 
        //     radius = 1.5*400; 

        //     //runningX = centerX;
        //     //runningY = centerY-radius; 
        //     //angle = (2*Math.pi ) / collection.views.length;  
        //     for(i =0; i < collection.views.length; i++){
        //         // posx = radius * Math.sin( (2*Math.PI) / (collection.views.length) * i ) + centerX;
        //         // posy = radius * Math.cos( (2*Math.PI) / (collection.views.length) * i ) + centerY
        //         posx = radius * Math.sin( (2*Math.PI) / (300) * i ) + centerX;
        //         posy = radius * Math.cos( (2*Math.PI) / (300) * i ) + centerY;
                
        //         collection.views[i].xPos = posx;
        //         collection.views[i].yPos = posy;
        //         //runningX += collection.views[i].width + padding;
        //     }
        // }
        // else{ //line for now.... 
        //     padding = 10; 
        //     startX = 10; 
        //     startY = 10;
        //     width = 200;
        // // if( collection.type == "comparison" || collection.type == "multifacetedCompare" ){

        //     //place views 200 apart, as a test
        //     runningX = startX; 
        //     for(i =0; i < collection.views.length; i++){
        //         collection.views[i].xPos = runningX;
        //         collection.views[i].yPos = startY;
        //         runningX = runningX + (width + padding);//collection.views[i].width + padding;
        //     }
        // }

        // }
        type = collection.type;
        columns = -1;
        if( !this.isAGrid(type) ){
            columns = (this.getViewsFromCollection(collection).length);
            collection.gridSpec = "repeat( " + columns + ", 1fr)";
        } else{
            if( type == "b+c" || type == "multifacetedB+C" ){
                columns = collection.attributeList.length;
                collection.gridSpec = "repeat( " + columns + ", 1fr)";
            }
            else if( type == "subAttrByMultifacetedCompare" ) { //"multifacetedCompareBySubAttr" ){
                columns = collection.secondaryAttributeList.length;
                collection.gridSpec = "repeat( " + columns + ", 1fr)";
            }
            else if( type == "subAttrByMultifacetedBrowse"){
                columns = collection.secondaryAttributeList.length;
                collection.gridSpec = "repeat( " + columns + ", 1fr)";
            }
            else if( type == "multifacetedBrowseBySubAttr"){
                columns = collection.attributeList.length;
                collection.gridSpec = "repeat( " + columns + ", 1fr)";
            }
            else if( type == "multifacetedCompareBySubAttr" ){
                columns = collection.filterList.length;
                collection.gridSpec = "repeat( " + columns + ", 1fr)";
            }
            else if ( type == "compare_compare_grid" || type == "multifaceted_compare_compare_grid" ){

                found = false;
                foundDimension = 0;
                prev = 1; 
                for(i =0; i < collection.aggregatedFilter.length; i++){
                    if( collection.aggregatedFilter[i]["vals"].length > 1){
                        found = true;
                        foundDimension = collection.aggregatedFilter[i]["vals"].length;
                        //break;
                    }
                }

                if( found ){
                    columns = foundDimension
                    collection.gridSpec = "repeat( " + columns + ", 1fr)";
                }
                else{
                    columns =  Math.floor(this.getViewsFromCollection(collection).length/2) ;
                    collection.gridSpec = "repeat( " +columns+ ", 1fr)";
                }

            } else if( type == "complexAndCompareGrid"){
                 columns =  Math.floor(this.getViewsFromCollection(collection).length/collection.filterList.length);
                collection.gridSpec = "repeat( " +columns + ", 1fr)";

            }
            // else if( type == "multifacetedCompareBySubAttr"){
            //     collection.gridSpec = "repeat( " + collection.filterList.length + ", 1fr)";

            // }
            // else if( type == "multifacetedBrowseBySubAttr"){
            //     collection.gridSpec = "repeat( " + collection.secondaryAttributeList.length + ", 1fr)";
            // }
            else{ 
                columns = Math.floor(this.getViewsFromCollection(collection).length/2);
                collection.gridSpec = "repeat( " + columns + ", 1fr)";
            }
        }


        //columns....  here is where I go from columns to positions, relative to the start of the collection
        


    },

    isAGrid(type){

        if( type == "targeted" || type == "browse" || type == "comparison" 
            || type == "sub+attr" || type == "multifaceted" || type == "complex"
             || type == "multifacetedCompare" || type == "multifacetedBrowse" ){
            return false;
        }
        return true;
    },

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////////////////////

    testMultiline: function(filterAttr, filterVals, attribute1, attribute2){
        collection = [];

        collection.push( this.makeChart(filterAttr, filterVals, this.getAttributeFromName(attribute1), this.getAttributeFromName(attribute2) )); 

        return collection;[i]
    },

    testGroupedBar: function(filterAttr, filterVals, attribute1, attribute2){
        collection = [];

        collection.push( this.makeChart(filterAttr, filterVals, this.getAttributeFromName(attribute1), this.getAttributeFromName(attribute2) )); 

        collection.push( this.makeChart(filterAttr, filterVals, this.getAttributeFromName(attribute2), this.getAttributeFromName(attribute1) )); 

        return collection;
    },


    ///////////////////////////////////

    makeChart: function( filter, attribute1, attribute2, viewTypeParam ){
    	console.log("--------------- MAKE CHART V2 with new filter obj!!");
    	console.log(filter); 
    	console.log(attribute1); 
    	console.log(attribute2); 

    	attr1 = this.getAttribute(attribute1); // is this function call necessary 
        attr2 = null;
        if( attribute2 != null ){
            attr2 = this.getAttribute(attribute2); //is this function call necessary? 
        }

        plotType = attr1["plotType"];
        if( viewTypeParam )
            plotType = viewTypeParam;

        // console_debug(attr1);
        // console_debug(attr2);

        //ASSUME BAR CHART
        var view = {}; 

        if(plotType == "bar"){
            // console.log(attr.list);
	        if( attr2 == null){
	                view = this.processBar(filter, attr1, null); 
	        } else{
	                view = this.processHeatTable(filter, attr1, attr2, null);//this.processGroupedBar(filter, attr1, attr2, null); 
	        }
	    }
        else if( plotType == "barWithSums"){
           // console.log(attr.list);
            if( attr2 == null){
                    view = this.processBarWithSums(filter, attr1, null); 
            } else { 
                    view = this.processBarWith(filter, attr1, attr2, null);//this.processGroupedBar(filter, attr1, attr2, null); 
            }
        }
        else if( plotType == "line"){
            if( attr2 == null){
                view = this.processLine(filter, attr1, null); 
            }
            else{
                view = this.processMultiline(filter, attr1, attr2, null); 
            }

        }
        else if( plotType == "map"){
        	view = this.processMap(filter); 
        }
        else if( plotType == "county-map"){
            console.log("COUNTY MAP!");
            view = this.processCountyMap(filter, attr2, null, null);
        }
        else if(plotType == "date_sum"){

            console.log("DATE");
            console.log(attr1);
            console.log(attr2);


            if( attr2 == null )
                view = this.processDateSingleLine(filter, attr1, null); 
            else
                view = this.processDateMultiLine(filter, attr1, attr2);
        }

        view.id = "vis"+this.viewCount;
        this.viewCount = this.viewCount+1;
        this.listOfViews.push(view); 

        return view; 

    },

    copyAndPivotChart: function( filter, attribute1, attribute2, vegaSpecToCopy, viewTypeParam){
		console_debug("--------------- COPY AND PIVOT CHART !!");
    	console_debug(filter); 
    	console_debug(attribute1); 
    	console_debug(attribute2); 

    	attr1 = this.getAttribute(attribute1); // is this function call necessary 
        attr2 = null;
        if( attribute2 != null ){
            attr2 = this.getAttribute(attribute2); //is this function call necessary? 
        }

        console_debug(attr1);
        console_debug(attr2);

        var view = {}; 

        plotType = attr1["plotType"];
        if( viewTypeParam )
            plotType = viewTypeParam;

        if(plotType == "bar" || plotType == "heatTable" ){
            // console.log(attr.list);
	        if( attr2 == null){
	                view = this.processBar(filter, attr1, vegaSpecToCopy); 
	        } else {
	                view = this.processHeatTable(filter, attr1, attr2, vegaSpecToCopy); //this.processGroupedBar(filter, attr1, attr2, vegaSpecToCopy); 
	        }
	    }
        else if( plotType == "line"){
            if( attr2 == null){
                view = this.processLine(filter, attr1, vegaSpecToCopy); 
            }
            else{
                view = this.processMultiline(filter, attr1, attr2, vegaSpecToCopy); 
            }

        }
        else if( plotType == "map"){
        	view = this.processMap(filter); 
        }
        else if( plotType == "county-map"){
            console.log("COUNTY MAP!");
            view = this.processCountyMap(filter, attr2, null, vegaSpecToCopy);//null);
        }
        else if(plotType == "date_sum"){

            console.log("DATE");
            console.log(attr1);
            console.log(attr2);

            if( attr2 == null )
                view = this.processDateSingleLine(filter, attr1, null, vegaSpecToCopy); 
            else
                view = this.processDateMultiLine(filter, attr1, attr2, vegaSpecToCopy);
        }

        view.id = "vis"+this.viewCount;
        this.viewCount = this.viewCount+1;
        this.listOfViews.push(view); 



        return view; 

    },

    runTest: function( filter, attr, attr2 ){
    	collection = this.initCollection(); 
        collection["type"] = "targeted";

        console_debug("running test"); 
        console_debug(filter);
        console_debug( attr ); 
        console_debug( attr2 ); 

        //collection.push ( this.makeChart(filterAttributes, filterValues, this.getAttributeFromName(attributeList[0]), this.getAttributeFromName(attributeList[1]) ) ); 

        this.addViewToCollection(collection, this.makeChart(filter, this.getAttributeFromName(attr), this.getAttributeFromName(attr2) ) );

        return this.getViewsFromCollection(collection);
    },



    getRandomNumber: function(){
        return Math.floor( Math.random()*1000 );

    }, 


    createTitle: function(filter, attribute1, attribute2){

        // var values = attribute1["list"].split(",");

        var plotTitle = []; 
        first = true; 
        //this enforces an order
        for(i =0 ; i < this.attributeAndValues.length; i++){

            var vals = this.attributeAndValues[i]["list"].split(",");

            for(j = 0; j < vals.length; j++){
                // console.log(vals[j]);

                for(k = 0; k < filter.length; k++){
                    for(l = 0; l < filter[k]["vals"].length; l++){
                        if( vals[j] == filter[k]["vals"][l] ){
                            console.log(filter[k]["vals"][l]);
                            if( !first ){
                                // plotTitle = plotTitle + " and ";
                                // plotTitle = plotTitle + filter[k]["vals"][l];
                                plotTitle.push(" and " + filter[k]["vals"][l] ) ;
                            } else{
                                // plotTitle = filter[k]["vals"][l]; 
                                plotTitle.push(filter[k]["vals"][l] ) ;

                                first = false;
                            }

                        }

                    }

                }
            }

        }

        return plotTitle;
    },

    createTitle2: function(filter, attribute1, attribute2){

        // var values = attribute1["list"].split(",");

        var plotTitle = ""; 
        first = true; 
        //this enforces an order
        for(i =0 ; i < this.attributeAndValues.length; i++){

            var vals = this.attributeAndValues[i]["list"].split(",");

            for(j = 0; j < vals.length; j++){
                // console.log(vals[j]);

                for(k = 0; k < filter.length; k++){
                    for(l = 0; l < filter[k]["vals"].length; l++){
                        if( vals[j] == filter[k]["vals"][l] ){
                            console.log(filter[k]["vals"][l]);
                            if( !first ){
                                plotTitle = plotTitle + " and ";
                                plotTitle = plotTitle + filter[k]["vals"][l]; 
                            } else{
                                plotTitle = filter[k]["vals"][l]; 
                                first = false;
                            }

                        }

                    }

                }
            }

        }

        return plotTitle;
    },


    processBar: function( filter, attribute, vegaSpecToCopy ){

    	console.log("process bar");

        plotTitle = this.createTitle(filter, attribute, null);
        // plotTitle = plotTitle + " by " + attribute["name"];
        plotTitle.push(" by " + attribute["name"]);


		var values = attribute["list"].split(",");
        var valueLabels = attribute["listOfLabels"].split(",");

        console.log(attr1["type"] );
        sortParam = "-y";
        if( attr1["type"] == "ordinal")
            sortParam = "none";

        console.log(sortParam );

        var labelForNumber = "number of " + this.whatDataContains[0];

		var dataToVisualize = [];
        for( var i = 0; i < values.length; i++){
            obj = {};
            obj[labelForNumber] =  this.retrieveValue(filter, [attribute["name"]], [values[i].replace(" ", "")] );
            obj[""+attribute["name"]] = valueLabels[i];//values[i].replace(" ", "");
            obj["c"] = "null";
            dataToVisualize.push(obj); 
        }

        console.log(dataToVisualize.length);
        console.log( attribute["color"]);

         var scale = { "scheme": "tableau20"};
            if( attribute["type"] == "nominal" && attribute["color"] != "Tableau20"){
                //console.log(colorbrewer.schemeGroups.qualitative);
                //if(attribute["color"] != "Tableau20" )
                console.log("hi");

                domainArr = [];
                // domainArr.push("null");
                // arr= this.getAttributeList(attribute);

                arr = attribute["listOfLabels"].split(",");
                console.log(arr);
                //domainArr.push("null");
                for(i = 0; i < arr.length; i++){
                    domainArr.push(""+arr[i]);
                }

                // colorArr = this.getColorArrayByColorBrewerName(attribute["color"], this.getAttributeList(attribute).length, false ); //"Blues"); //this.getColorArray("pink5");
                colorArr = this.colorLookupForList( attribute["name"], domainArr );  
                console.log(colorArr);
                console.log(domainArr);


                scale =   {
                            "domain": domainArr,//[ 'null','very-low-','low-','moderate-','high-','very-high-' ],
                            "range": colorArr,//["orange", "gray", "blue", "green", "red", "pink", "purple"]//colorArr,
                        }
            }
            else if( attribute["type"] == "ordinal" && attribute["color"] != "Tableau20"){
                //colorArr = this.getColorArrayByColorBrewerName(attribute["color"], 5, false); //"Blues"); //this.getColorArray("pink5");
                domainArr = [];
                // domainArr.push("null");
                // arr= this.getAttributeList(attribute);
                                        arr = attribute["listOfLabels"].split(",");

                //domainArr.push("null");
                for(i = 0; i < arr.length; i++){
                    domainArr.push(""+arr[i]);
                }
                console.log(domainArr);
                
                colorArr = this.colorLookupForList( attribute["name"], domainArr );  
                console.log(domainArr);
                console.log(colorArr);

                scale =   {
                            "domain": domainArr,//legendValues,//[ 'null','very-low-','low-','moderate-','high-','very-high-' ],
                            "range": colorArr,//["orange", "gray", "blue", "green", "red", "pink", "purple"]//colorArr,
                        }
            }





        copyTheSpec = true; 
        if( vegaSpecToCopy != null && vegaSpecToCopy["mark"] != "bar" ){ //don't copy if mismatch type
            copyTheSpec = false; 
        }

        var vegaLiteSpec = null;
        if( vegaSpecToCopy != null && copyTheSpec ){ // copy pivot
        	vegaLiteSpec = loadash.cloneDeep(vegaSpecToCopy); 

        	vegaLiteSpec.description = plotTitle;
        	vegaLiteSpec.title.text = plotTitle;
        	vegaLiteSpec.data.values = dataToVisualize;
        	vegaLiteSpec.encoding.x.field = attribute["name"]; 
            vegaLiteSpec.attribute1 = attribute["name"];//added
            vegaLiteSpec.filter = filter; //added
            vegaLiteSpec.encoding.x.sort = sortParam;

            vegaLiteSpec.encoding.color.field = attribute["name"]; 
            vegaLiteSpec.encoding.color.scale = scale;
        }
        else {
			vegaLiteSpec = {
                          "$schema": "https://vega.github.io/schema/vega-lite/v4.json",
                          "description": plotTitle,
                          "viewPosAndSize": {"width": 500, "height": 300, "x": 0, "y": 0 },
                          //"width": 400,//"container",
                          // "width": 400,
                          // "height": 250,
                           "autosize": {
                            "type": "fit",
                            "resize": false
                          },
                          "title": {
                            "text": plotTitle,
                             "anchor": "start"
                          },
                         "data": {
                            "values" : dataToVisualize
                         },
                          "mark": "bar",
                          "encoding": {
                            // "x": {"field": attribute["name"], "type": "ordinal",  "sort": {"op": "count", "field": "number of crimes", "order": "descending"}},
                            "x": {"field": attribute["name"], "type": "ordinal",  "sort": sortParam},
                            "y": {"field": labelForNumber, "type": "quantitative"},
                            // "tooltip": {"field": "number", "type": "quantitative"}
                                                        "color": {"field": attribute["name"], "type": "nominal", "scale": scale }, //{"field": attribute2["name"], "type": "nominal", "legend": {"values": legendValues}, "scale": {"scheme": "tableau20"}}, 

                          },
                         "config": {
                                "legend": {
                                  "disable": true,
                                }
                            },
                          "filter": filter,
                          "attribute1": attribute["name"],
                          "attribute2": null,
                          "type": "bar"                        };
        }

        console_debug2("---------------------------------");
        console_debug2("---------------------------------");
        console_debug2(JSON.stringify(vegaLiteSpec));
        console_debug2("---------------------------------");
        console_debug2("---------------------------------");

        return vegaLiteSpec;


    },

    processBarWithSums: function( filter, attribute1, attribute2, vegaSpecToCopy){


        console_debug("process bar with sums ");

        var plotTitle = "";

        if( !this.hashFromIdToIdxInitialized ){
            this.initHashmap("county-map");
            this.hashFromIdToIdxInitialized = true; //only do this once
        }


        plotTitle = this.createTitle(filter, attribute1, attribute2);
        // plotTitle = plotTitle + " by " + attribute1["name"] + " and " + attribute2["name"];
        plotTitle.push(" by " + attribute1["name"] + " and " + attribute2["name"]);

        var labelForNumber = "number of " + this.whatDataContains[1];


        //timeUnit = "yearmonth";

        // attrReordered = this.whichAttrIsPrimary(attribute1, attribute2); 
        // console.log("Attribute list reordered " + attrReordered);  
               
        var attr1Values = this.getAttributeList(attribute1); 
        var attr2Values = this.getAttributeList(attribute2); 

        var dataToVisualize = [];
        var countAttr2 = []; 

        for(var j = 0; j < attr2Values.length; j++){
            var thisSum = {"value": attr2Values[j], "sum": 0};

            for( var i = 0; i < attr1Values.length; i++){

                //console.log(i);
                obj = {};

                // filterAttr.push( attribute2["name"] );
                // filterVals.push( attr2Values[j] ); 

                filter.push( { "attr": attribute2["name"], "vals": [ attr2Values[j] ] } );
 
                obj["time"] = attr1Values[i].replace(" ", "");//this.parseDateTime(attr1Values[i].replace(" ", ""),timeUnit);
                obj[labelForNumber] =  this.retrieveSum(filter, [ attribute1["name"] ], [ attr1Values[i].replace(" ", "") ], "new-cases" );//this.retrieveValue(filter, [attribute1["name"]], [attr1Values[i].replace(" ", "")] );
                // obj["number of crimes"] =  theCount; // this.retrieveValue(filterAttr, filterVals, [attribute1["name"]], [attr1Values[i].replace(" ", "")] ); //getRandomNumber();
                
                //HACK!!!
                strToDisplay = attr2Values[j].replace(attribute2["name"], "" );
                obj[attribute2["name"]] = strToDisplay;//attr2Values[j];

                filter.pop();

                dataToVisualize.push(obj); 
                thisSum["sum"]+= obj["sum"];//theCount; 

            }
            countAttr2.push( thisSum);
        }


        //sort- display legend in sorted order  TURN OFF
        // if( attribute2["type"] != "ordinal")
        //     countAttr2.sort( function(a, b){ return b["sum"] - a["sum"]; } );
        legendValues = [];
        for(i =0; i < countAttr2.length; i++){
            strToDisplay = countAttr2[i]["value"].replace(attribute2["name"], "" );
            legendValues.push(strToDisplay);//countAttr2[i]["value"]);
        }

        var capAt = 12;
        var wasCapped = false;
        if( legendValues.length > capAt ){
            wasCapped = true;
            // plotTitle = plotTitle + " (top " + capAt + " " + attribute2["name"] + ")";
            plotTitle.push(" (top " + capAt + " " + attribute2["name"] + ")");
            for(i = capAt; i < legendValues.length; i++ ){
                for(j =0; j < dataToVisualize.length; j++ ){
                    if( dataToVisualize[j][""+attribute2["name"]] == legendValues[i] ){
                        dataToVisualize.splice(j,1);
                        j--;
                    }

                }
                legendValues.splice(i,1);
                i--;
            }
        }

        console.log(dataToVisualize);

    }, 


    processGroupedBar : function(filter, attribute1, attribute2, vegaSpecToCopy ){
        console.log("process grouped bar ");

        var plotTitle = "";

        //Create title
        // for( var i = 0; i < filter.length; i++){
        //     plotTitle = plotTitle + filter[i]["vals"][0] ; //NOTE- not going to work totally.... 
        //     if( filter[i]["vals"].length > 1 ){
        //         for( var j = 1; j < filter[i]["vals"].length; j++ ){
        //             plotTitle = plotTitle + " and ";
        //             plotTitle = plotTitle + filter[i]["vals"][j]; 
        //         }
        //     }
        //     plotTitle = plotTitle + ", ";
        // }

        // plotTitle = plotTitle.substring(0, plotTitle.length - 2);

        plotTitle = this.createTitle(filter, attribute1, attribute2);
        // plotTitle = plotTitle + " by " + attribute1["name"] + " and " + attribute2["name"];
        plotTitle.push(" by " + attribute1["name"] + " and " + attribute2["name"]);

        //don't sort if ordinal
        console.log(attr1["type"] );
        sortParam = "-y";
        if( attr1["type"] == "ordinal")
            sortParam = "none";


        var labelForNumber = "number of " + this.whatDataContains[0];

        //Get list of attr1 values (eg, 2010,2011....)
        var attr1Values = this.getAttributeList(attribute1);  //LOCATION TYPE
        var attr2Values = this.getAttributeList(attribute2);  //YEAR


        var dataToVisualize = [];

        var countAttr2 = []; 
        var countAttr1 = {};

        var valuesAttr1 = attribute1["list"].split(",");
        var valueLabelsAttr1 = attribute1["listOfLabels"].split(",");

        var valuesAttr2 = attribute2["list"].split(",");
        var valueLabelsAttr2 = attribute2["listOfLabels"].split(",");



        for( var i = 0; i < valuesAttr2.length; i++){ //years
            var thisCount = {"value": valueLabelsAttr2[i], "count": 0};//init
            for(var j = 0; j < valuesAttr1.length; j++){ //loc types
                obj = {};

                // filterAttr.push( attribute1["name"] );
                // filterVals.push( attr1Values[j] ); 

                filter.push( { "attr": attribute1["name"], "vals": [ valuesAttr1[j] ] } );


                obj[""+attribute2["name"]] = valueLabelsAttr2[i]; //attr2Values[i]; //this.parseDateTime(attr1Values[i].replace(" ", ""),timeUnit);
                theCount =  this.retrieveValue(filter, [attribute2["name"]], [valuesAttr2[i].replace(" ", "")] );
                obj[labelForNumber] = theCount;   //this.retrieveValue(filter, [attribute2["name"]], [attr2Values[i].replace(" ", "")] );  //this.retrieveValue(filterAttr, filterVals, [attribute2["name"]], [attr2Values[i].replace(" ", "")] ); //getRandomNumber();
                obj[""+attribute1["name"]] = valueLabelsAttr1[j]; //attr1Values[j];

                filter.pop();

                dataToVisualize.push(obj); 
                thisCount["count"] += theCount;  

                if( countAttr1.hasOwnProperty(attr1Values[j] )){
                    countAttr1[ attr1Values[j] ] += theCount;
                } else{
                    countAttr1[ attr1Values[j] ] = 0;

                }

            }
            countAttr2.push( thisCount );

        }
        console.log("count attr 1");
        console.log(countAttr1);
        console.log("count attr 2");
        console.log(countAttr2);

        // for( var i = 0; i < attr2Values.length; i++){ //years
        //     var thisCount = {"value": attr2Values[i], "count": 0};//init
        //     for(var j = 0; j < attr1Values.length; j++){ //loc types
        //         obj = {};

        //         // filterAttr.push( attribute1["name"] );
        //         // filterVals.push( attr1Values[j] ); 

        //         filter.push( { "attr": attribute1["name"], "vals": [ attr1Values[j] ] } );


        //         obj[""+attribute2["name"]] = attribute2["listOfLabels"][i]; //attr2Values[i]; //this.parseDateTime(attr1Values[i].replace(" ", ""),timeUnit);
        //         theCount =  this.retrieveValue(filter, [attribute2["name"]], [attr2Values[i].replace(" ", "")] );
        //         obj["number"] = theCount;   //this.retrieveValue(filter, [attribute2["name"]], [attr2Values[i].replace(" ", "")] );  //this.retrieveValue(filterAttr, filterVals, [attribute2["name"]], [attr2Values[i].replace(" ", "")] ); //getRandomNumber();
        //         obj[""+attribute1["name"]] = attribute1["listOfLabels"][j]; //attr1Values[j];

        //         filter.pop();

        //         dataToVisualize.push(obj); 
        //         thisCount["count"] += theCount;  

        //         if( countAttr1.hasOwnProperty(attr1Values[i] )){
        //             countAttr1[ attr1Values[i] ] += theCount;
        //         } else{
        //             countAttr1[ attr1Values[i] ] = 0;

        //         }

        //     }
        //     countAttr2.push( thisCount );

        // }
        // console.log(countAttr1);


        legendValues = [];
        if( attribute2["type"] != "ordinal" ){
                 // //sort- display legend in sorted order
            countAttr2.sort( function(a, b){ return b["count"] - a["count"]; } );
            for(i =0; i < countAttr2.length; i++){
                legendValues.push(countAttr2[i]["value"]);
            }

            countAttr2.sort( function(a, b){ return b["count"] - a["count"]; } );
            topVals = [];
            for(i =0; i < countAttr2.length; i++){
                topVals.push(countAttr2[i]["value"]);
            }

            // // //limit to 20
            var capAt = 12;
            var wasCapped = false;
            if( topVals.length > capAt ){
                wasCapped = true;
                // plotTitle = plotTitle + " (top " + capAt + ")";
                plotTitle.push( " (top " + capAt + ")" );
                for(i = capAt; i < topVals.length; i++ ){
                    for(j =0; j < dataToVisualize.length; j++ ){
                        if( dataToVisualize[j][""+attribute2["name"]] == topVals[i] ){
                            dataToVisualize.splice(j,1);
                            j--;
                        }

                    }
                    topVals.splice(i,1);
                    i--;
                }
            }

            // var capAt = 

            // //remove data if it is not top 20
            attr1Order = []; 
            if( ""+attribute1["type"] != "ordinal"){

                //put in an array
                arr = [];
                for(i = 0; i < attr1Values.length; i++){
                    arr.push( {"name": attr1Values[i], "count": countAttr1[attr1Values[i]] } );
                }//end for


                //sort
                // console.log("sort!");
                // console.log(arr);
                // arr.sort( function(a, b){ return b["count"] - a["count"]; } );
                // console.log(arr);

                for(i = 0; i < capAt; i++){
                    if( i < arr.length )
                        attr1Order.push(arr[i]["name"]);
                }

                if(arr.length > capAt){
                    for(i = capAt; i < arr.length; i++){
                        for(j =0; j < dataToVisualize.length; j++ ){
                            if( dataToVisualize[j][""+attribute1["name"]] == arr[i]["name"] ){
                                dataToVisualize.splice(j,1);
                                j--;
                            }

                        }
                    }

                }
                
            }//end if
            // //now count with respect to other attributr
        } else {
                 // //sort- display legend in sorted order

            for(i =0; i < countAttr2.length; i++){
                legendValues.push(countAttr2[i]["value"]);
            }
        }
       


        // copyTheSpec = true; 
        // if( vegaSpecToCopy != null && vegaSpecToCopy["mark"] != "bar" && vegaLiteSpec.encoding.column != null ){ //don't copy if mismatch type
        //     copyTheSpec = false; 
        // }

        var scale = { "scheme": "tableau20"};
        if( attribute2["type"] == "ordinal" && attribute2["color"] != "Tableau20"){
            //colorArr = this.getColorArrayByColorBrewerName(attribute2["color"], this.getAttributeList(attribute2).length, false);//this.getColorArray("pink5");
            domainArr = [];
            domainArr.push("null");
            arr= this.getAttributeList(attribute2);
            for(i = 0; i < arr.length; i++){
                domainArr.push(""+arr[i]);
            }
            console.log(domainArr);

            colorArr = this.colorLookupForList( attribute["name"], domainArr );  
            console.log(colorArr);

            scale =   {
                        "domain": legendValues,//[ 'null','very-low-','low-','moderate-','high-','very-high-' ],
                        "range": colorArr,//["orange", "gray", "blue", "green", "red", "pink", "purple"]//colorArr,
                    }
        } if( attribute2["type"] == "nominal" && attribute2["color"] != "Tableau20"){
            len = this.getAttributeList(attribute2).length 
            if(len > capAt )
                len = capAt; 
             console.log(colorbrewer.schemeGroups.qualitative);
             //colorArr = this.getColorArrayByColorBrewerName(attribute2["color"], len, false ); //"Blues"); //this.getColorArray("pink5");
                domainArr = [];
                // domainArr.push("null");
                arr= this.getAttributeList(attribute2);
                //domainArr.push("null");
                for(i = 0; i < arr.length; i++){
                    domainArr.push(""+arr[i]);
                }
                console.log(domainArr);

            colorArr = this.colorLookupForList( attribute["name"], domainArr );  
            console.log(colorArr);

                scale =   {
                            "domain": legendValues,//[ 'null','very-low-','low-','moderate-','high-','very-high-' ],
                            "range": colorArr,//["orange", "gray", "blue", "green", "red", "pink", "purple"]//colorArr,
                        }
        }

        // sortOrder = attribute2["listOfLabels"];

        copyTheSpec = false; 
        if( vegaSpecToCopy != null && ( vegaSpecToCopy["mark"] == "bar" || vegaSpecToCopy.encoding.column != null ) ){//vegaLiteSpec.encoding.column != null ) ){ //don't copy if mismatch type
            copyTheSpec = false; 
        }

        var vegaLiteSpec = null;
        if( vegaSpecToCopy != null && copyTheSpec ){ // copy pivot
            vegaLiteSpec = loadash.cloneDeep(vegaSpecToCopy); 
            vegaLiteSpec.description = plotTitle;
            vegaLiteSpec.title.text = plotTitle;
            vegaLiteSpec.data.values = dataToVisualize;
            vegaLiteSpec.encoding.column.field = attribute1["name"]; 
            vegaLiteSpec.encoding.x.field = attribute2["name"]; 

            vegaLiteSpec.encoding.color.field = attribute2["name"]; 
            vegaLiteSpec.encoding.color.scale = scale;

            vegaLiteSpec.attribute2 = attribute2["name"];//added
            vegaLiteSpec.attribute1 = attribute1["name"];//added
            vegaLiteSpec.filter = filter; //added
        }
        else {
            vegaLiteSpec = {
                          "$schema": "https://vega.github.io/schema/vega-lite/v4.json",
                          "description": plotTitle,
                          // "width": 400,
                          // "height": 250,
                        // "viewPosAndSize": {"width": 500, "height": 300, "x": 0, "y": 0 },
                            "width": {"step": 12},
                          "spacing": 1,
                           "autosize": {
                            "type": "fit",
                            "resize": true
                          },
                          "title": {
                            "text": plotTitle,
                             "anchor": "start",
                          },
                         "data": {
                            "values" : dataToVisualize
                         },
                          "mark": "bar",
                          "encoding": {
                            "facet": {
                                  "field": attribute2["name"],
                                  "type": "ordinal",
                                  "columns": 3,
                                  "sort": valueLabelsAttr2,
                                  "spacing": 30
                                },
                            // "column": { "field": attribute1["name"] },//, "type": "ordinal", "sort": attr1Order},
                            "x": {"field": attribute1["name"], "type": "ordinal", "sort": sortParam}, //{"op": "count", "field": "number of crimes", "order": "descending"}}, // "sort": legendValues },
                            //"x": {"field": attribute2["name"], "type": "ordinal",  "sort": {"op": "count", "field": "number of crimes", "order": "descending"}},

                            "y": {"field": labelForNumber, "type": "quantitative"},//{"field": "number of crimes", "type": "quantitative"},
                            "color": {"field": attribute2["name"], "type": "nominal", "scale": scale }, //{"field": attribute2["name"], "type": "nominal", "legend": {"values": legendValues}, "scale": {"scheme": "tableau20"}}, 
                            "tooltip": {"field": labelForNumber, "type": "quantitative"}
                          },
                          "config": {
                            "column": {"spacing": 1},
                            "color": {"titleLimit": 10}//does not work...
                          },
                          "filter": filter,
                          "attribute1": attribute1["name"],
                          "attribute2": attribute2["name"],
                          "type": "groupedBar"
                        };
        }

        console_debug2("---------------------------------");
        console_debug2("---------------------------------");
        console_debug2(JSON.stringify(vegaLiteSpec));
        console_debug2("---------------------------------");
        console_debug2("---------------------------------");

        return vegaLiteSpec;

    }, 

    //one with colored axes
    processHeatTable: function( filter, attribute1, attribute2, vegaSpecToCopy ){

        //return this.processGroupedBar(filter, attribute1, attribute2, vegaSpecToCopy);


        console.log("process heat table 2 ");

        var plotTitle = "";

        //Create title
        // for( var i = 0; i < filter.length; i++){
        //     plotTitle = plotTitle + filter[i]["vals"][0] ; //NOTE- not going to work totally.... 
        //     if( filter[i]["vals"].length > 1 ){
        //         for( var j = 1; j < filter[i]["vals"].length; j++ ){
        //             plotTitle = plotTitle + " and ";
        //             plotTitle = plotTitle + filter[i]["vals"][j]; 
        //         }
        //     }
        //     plotTitle = plotTitle + ", ";
        // }

        // plotTitle = plotTitle.substring(0, plotTitle.length - 2);

        plotTitle = this.createTitle(filter, attribute1, attribute2);
        // plotTitle = plotTitle + " by " + attribute1["name"] + " and " + attribute2["name"];
        plotTitle.push(" by " + attribute1["name"] + " and " + attribute2["name"]);

        console.log(plotTitle);

        //don't sort if ordinal
        console.log(attr1["type"] );
        sortParam = "-y";
        if( attr1["type"] == "ordinal")
            sortParam = "none";



        //Get list of attr1 values (eg, 2010,2011....)
        var attr1Values = this.getAttributeList(attribute1);  //LOCATION TYPE
        var attr2Values = this.getAttributeList(attribute2);  //YEAR


        var dataToVisualize = [];

        var countAttr2 = []; 
        var countAttr1 = {};

        var labelForNumber = "number of " + this.whatDataContains[0];

        console.log("1");
        var valuesAttr1 = attribute1["list"].split(",");
                console.log("2");

        var valueLabelsAttr1 = attribute1["listOfLabels"].split(",");
                console.log("3");


        var valuesAttr2 = attribute2["list"].split(",");
        var valueLabelsAttr2 = attribute2["listOfLabels"].split(",");

                console.log("4");


        for( var i = 0; i < valuesAttr2.length; i++){ //years
            var thisCount = {"value": valueLabelsAttr2[i], "count": 0};//init
            for(var j = 0; j < valuesAttr1.length; j++){ //loc types
                obj = {};

                // filterAttr.push( attribute1["name"] );
                // filterVals.push( attr1Values[j] ); 

                filter.push( { "attr": attribute1["name"], "vals": [ valuesAttr1[j] ] } );


                obj[""+attribute2["name"]] = valueLabelsAttr2[i]; //attr2Values[i]; //this.parseDateTime(attr1Values[i].replace(" ", ""),timeUnit);
                theCount =  this.retrieveValue(filter, [attribute2["name"]], [valuesAttr2[i].replace(" ", "")] );
                obj[labelForNumber] = theCount;   //this.retrieveValue(filter, [attribute2["name"]], [attr2Values[i].replace(" ", "")] );  //this.retrieveValue(filterAttr, filterVals, [attribute2["name"]], [attr2Values[i].replace(" ", "")] ); //getRandomNumber();
                obj[""+attribute1["name"]] = valueLabelsAttr1[j]; //attr1Values[j];

                filter.pop();

                dataToVisualize.push(obj); 
                thisCount["count"] += theCount;  

                if( countAttr1.hasOwnProperty(attr1Values[j] )){
                    countAttr1[ attr1Values[j] ] += theCount;
                } else{
                    countAttr1[ attr1Values[j] ] = 0;

                }

            }
            countAttr2.push( thisCount );

        }
        console.log("count attr 1");
        console.log(countAttr1);
        console.log("count attr 2");
        console.log(countAttr2);



        // legendValues = [];
        // if( attribute2["type"] != "ordinal" ){
        //          // //sort- display legend in sorted order
        //     countAttr2.sort( function(a, b){ return b["count"] - a["count"]; } );
        //     for(i =0; i < countAttr2.length; i++){
        //         legendValues.push(countAttr2[i]["value"]);
        //     }

        //     countAttr2.sort( function(a, b){ return b["count"] - a["count"]; } );
        //     topVals = [];
        //     for(i =0; i < countAttr2.length; i++){
        //         topVals.push(countAttr2[i]["value"]);
        //     }

        //     // // //limit to 20
        //     var capAt = 12;
        //     var wasCapped = false;
        //     if( topVals.length > capAt ){
        //         wasCapped = true;
        //         // plotTitle = plotTitle + " (top " + capAt + ")";
        //         plotTitle.push( " (top " + capAt + ")" );
        //         for(i = capAt; i < topVals.length; i++ ){
        //             for(j =0; j < dataToVisualize.length; j++ ){
        //                 if( dataToVisualize[j][""+attribute2["name"]] == topVals[i] ){
        //                     dataToVisualize.splice(j,1);
        //                     j--;
        //                 }

        //             }
        //             topVals.splice(i,1);
        //             i--;
        //         }
        //     }

        //     // var capAt = 

        //     // //remove data if it is not top 20
        //     attr1Order = []; 
        //     if( ""+attribute1["type"] != "ordinal"){

        //         //put in an array
        //         arr = [];
        //         for(i = 0; i < attr1Values.length; i++){
        //             arr.push( {"name": attr1Values[i], "count": countAttr1[attr1Values[i]] } );
        //         }//end for


        //         //sort
        //         // console.log("sort!");
        //         // console.log(arr);
        //         // arr.sort( function(a, b){ return b["count"] - a["count"]; } );
        //         // console.log(arr);

        //         for(i = 0; i < capAt; i++){
        //             if( i < arr.length )
        //                 attr1Order.push(arr[i]["name"]);
        //         }

        //         if(arr.length > capAt){
        //             for(i = capAt; i < arr.length; i++){
        //                 for(j =0; j < dataToVisualize.length; j++ ){
        //                     if( dataToVisualize[j][""+attribute1["name"]] == arr[i]["name"] ){
        //                         dataToVisualize.splice(j,1);
        //                         j--;
        //                     }

        //                 }
        //             }

        //         }
                
        //     }//end if
        //     // //now count with respect to other attributr
        // } else {
        //          // //sort- display legend in sorted order

        //     for(i =0; i < countAttr2.length; i++){
        //         legendValues.push(countAttr2[i]["value"]);
        //     }
        // }
       


        // copyTheSpec = true; 
        // if( vegaSpecToCopy != null && vegaSpecToCopy["mark"] != "bar" && vegaLiteSpec.encoding.column != null ){ //don't copy if mismatch type
        //     copyTheSpec = false; 
        // }

        //NO COLOR SCALE HERE
        // var scale = { "scheme": "tableau20"};
        // if( attribute2["type"] == "ordinal" && attribute2["color"] != "Tableau20"){
        //     colorArr = this.getColorArrayByColorBrewerName(attribute2["color"], this.getAttributeList(attribute2).length, false);//this.getColorArray("pink5");
        //     domainArr = [];
        //     domainArr.push("null");
        //     arr= this.getAttributeList(attribute2);
        //     for(i = 0; i < arr.length; i++){
        //         domainArr.push(""+arr[i]);
        //     }
        //     console.log(domainArr);


        //     scale =   {
        //                 "domain": legendValues,//[ 'null','very-low-','low-','moderate-','high-','very-high-' ],
        //                 "range": colorArr,//["orange", "gray", "blue", "green", "red", "pink", "purple"]//colorArr,
        //             }
        // } if( attribute2["type"] == "nominal" && attribute2["color"] != "Tableau20"){
        //     len = this.getAttributeList(attribute2).length 
        //     if(len > capAt )
        //         len = capAt; 
        //      console.log(colorbrewer.schemeGroups.qualitative);
        //      colorArr = this.getColorArrayByColorBrewerName(attribute2["color"], len, false ); //"Blues"); //this.getColorArray("pink5");
        //         domainArr = [];
        //         // domainArr.push("null");
        //         arr= this.getAttributeList(attribute2);
        //         //domainArr.push("null");
        //         for(i = 0; i < arr.length; i++){
        //             domainArr.push(""+arr[i]);
        //         }
        //         console.log(domainArr);


        //         scale =   {
        //                     "domain": legendValues,//[ 'null','very-low-','low-','moderate-','high-','very-high-' ],
        //                     "range": colorArr,//["orange", "gray", "blue", "green", "red", "pink", "purple"]//colorArr,
        //                 }
        // }

        // sortOrder = attribute2["listOfLabels"];
       //  if( attribute1["color"] != "Tableau20" )
       //      colorArrAttr1 = this.getColorArrayByColorBrewerName(attribute1["color"], this.getAttributeList(attribute1).length, false);//this.getColorArray("pink5");
       // else
       //      colorArrAttr1 = "Tableau20";


       //  domainArr1 = [];
       //  // domainArr1.push("null");
       //  arr1= this.getAttributeList(attribute1);
       //  for(i = 0; i < arr1.length; i++){
       //      domainArr1.push(""+arr1[i]);
       //  }
       //  console.log(domainArr1);

       // scale1 =   {
       //      "domain": domainArr1,//[ 'very-low-','low-','moderate-','high-','very-high-' ],
       //      "range": [  "blue", "green", "red", "pink", "purple"]//colorArr,
       //  };

       // if( attribute2["color"] != "Tableau20" )
       //      colorArrAttr2 = this.getColorArrayByColorBrewerName(attribute2["color"], this.getAttributeList(attribute2).length, false);//this.getColorArray("pink5");
       //  else
       //      colorArrAttr2 = "Tableau20";

        //rev order attr2
        // revOrderColor = [];
        // for(i = colorArrAttr2.length; i >= 0; i--){
        //     revOrderColor.push( colorArrAttr2[i] );
        // }
        // colorArrAttr2 = revOrderColor;

        // revListOrder = [];
        listOfAttr2 = this.getAttributeList(attribute2);
        // for(i = listOfAttr2.length; i >= 0; i--){
        //     revListOrder.push( listOfAttr2[i] );
        // }
        // listOfAttr2 = revListOrder;

        colorArrAttr2 = this.colorLookupForList( attribute2["name"], attribute2["listOfLabels"].split(",") );//domainArr );  
        colorArrAttr1 = this.colorLookupForList( attribute1["name"], attribute1["listOfLabels"].split(",") );//domainArr );  


        colorScale1 =  {
                            "domain": attribute1["listOfLabels"].split(","),//[ 'null','very-low-','low-','moderate-','high-','very-high-' ],
                            "range": colorArrAttr1//["orange", "gray", "blue", "green", "red", "pink", "purple"]//colorArr,
                        };

        colorScale2 =  {
                            "domain": attribute2["listOfLabels"].split(","),//[ 'null','very-low-','low-','moderate-','high-','very-high-' ],
                            "range": colorArrAttr2//["orange", "gray", "blue", "green", "red", "pink", "purple"]//colorArr,
                        };
        //         scale =   {
        //                     "domain": legendValues,//[ 'null','very-low-','low-','moderate-','high-','very-high-' ],
        //                     "range": colorArr,//["orange", "gray", "blue", "green", "red", "pink", "purple"]//colorArr,
        //                 }
        //colorArrAttr2 = [ "#ff0000", "#00ff00", "#00ffff", "#ffff00", "#ff00ff", "#cdcdcd", "#dddddd"  ];

        console.log( attribute2["listOfLabels"].split(",") );
        console.log( colorArrAttr2);

        console.log( attribute1["listOfLabels"].split(",") );
        console.log( colorArrAttr1);

        copyTheSpec = false; 
        if( vegaSpecToCopy != null && vegaSpecToCopy["type"] == "heatTable" ) { //( vegaSpecToCopy["mark"] == "bar" || vegaSpecToCopy.encoding.column != null ) ){//vegaLiteSpec.encoding.column != null ) ){ //don't copy if mismatch type
            copyTheSpec = false; 
        }

        var vegaLiteSpec = null;
        if( vegaSpecToCopy != null && copyTheSpec ){ // copy pivot
            vegaLiteSpec = loadash.cloneDeep(vegaSpecToCopy); 
            vegaLiteSpec.description = plotTitle;
            vegaLiteSpec.title.text = plotTitle;
            vegaLiteSpec.data.values = dataToVisualize;

      
            vegaLiteSpec.hconcat[0].encoding.y.field = attribute2["name"];
            vegaLiteSpec.hconcat[0].encoding.y.sort = this.getAttributeList(attribute2);
            vegaLiteSpec.hconcat[0].encoding.y.type = this.getAttributeType(attribute2);

            vegaLiteSpec.hconcat[0].encoding.color.field = attribute2["name"];
            vegaLiteSpec.hconcat[0].encoding.color.scale = {"scheme": colorArrAttr2};
            vegaLiteSpec.hconcat[0].encoding.color.sort = this.getAttributeList(attribute2);
            vegaLiteSpec.hconcat[0].encoding.color.type = this.getAttributeType(attribute2);  

            vegaLiteSpec.hconcat[1].vconcat[0].encoding.y.field = attribute2["name"];
            vegaLiteSpec.hconcat[1].vconcat[0].encoding.y.type = this.getAttributeType(attribute2); 
            vegaLiteSpec.hconcat[1].vconcat[0].encoding.y.sort = this.getAttributeList(attribute2);

            vegaLiteSpec.hconcat[1].vconcat[0].encoding.x.field = attribute1["name"];
            vegaLiteSpec.hconcat[1].vconcat[0].encoding.x.type = this.getAttributeType(attribute1); 
            vegaLiteSpec.hconcat[1].vconcat[0].encoding.x.sort = this.getAttributeList(attribute1);


            vegaLiteSpec.hconcat[1].vconcat[1].encoding.x.field = attribute1["name"];
            vegaLiteSpec.hconcat[1].vconcat[1].encoding.x.type = this.getAttributeType(attribute1); 
            vegaLiteSpec.hconcat[1].vconcat[1].encoding.x.sort = this.getAttributeList(attribute1);

            vegaLiteSpec.hconcat[1].vconcat[1].encoding.color.field = attribute1["name"];
            vegaLiteSpec.hconcat[1].vconcat[1].encoding.color.scale = {"scheme": colorArrAttr1};
            vegaLiteSpec.hconcat[1].vconcat[1].encoding.color.sort = this.getAttributeList(attribute1);
            vegaLiteSpec.hconcat[1].vconcat[1].encoding.color.type = this.getAttributeType(attribute1);  


            // vegaLiteSpec.encoding.color.field = attribute2["name"]; 
            // vegaLiteSpec.encoding.color.scale = scale;

            vegaLiteSpec.attribute2 = attribute2["name"];//added
            vegaLiteSpec.attribute1 = attribute1["name"];//added
            vegaLiteSpec.filter = filter; //added
        }
        else {
            vegaLiteSpec = {

                "$schema": "https://vega.github.io/schema/vega-lite/v5.json",
                "data": {
                            "values" : dataToVisualize
                         },     
                "description": "heat table",
                "autosize": {
                            "type": "fit",
                            "resize": false
                          },

                    "title": {
                            "text": plotTitle,
                             "anchor": "start",
                          },

                "spacing": 5,
                "bounds": "flush",
                "hconcat": [
                  

                  {
                      "mark": "rect",
                      // "width": 10,
                      "encoding": {
                        "y": {"field": attribute2["name"], "type": "ordinal", "sort": this.getAttributeList(attribute2) },
                        "x": {},
                        "color": {"field": attribute2["name"], "scale": colorScale2, "legend": null , "sort": this.getAttributeList(attribute2)}
                      },
                      "config": {
                      "axis": {"grid": true, "tickBand": "extent"}
                      }
                  },
                    
                    {
                      "spacing": 5,
                      "bounds": "flush",
                      // "width": 100,
                      "vconcat":[

                        {
                            "mark": "rect",
                            "encoding": {
                                "y": {"field": attribute2["name"], "type": "nominal", "axis": null, "sort":  listOfAttr2 },//this.getAttributeList(attribute2) },//["very-low-", "low-", "moderate-", "high-", "very-high-"]}, //attribute2["type"]},
                                "x": {"field": attribute1["name"], "type": "nominal", "axis": null, "sort": this.getAttributeList(attribute1) },//attribute1["type"]},
                              "color": {
                                    "field": labelForNumber, 
                                    "type": "quantitative",
                                    "scale": { "scheme": "warmgreys" } //inferno  //magma
                                }
                            },
                            "config": {
                            "axis": {"grid": true, "tickBand": "extent", "labels": false}
                            }
                        },
                        {
                          "mark": "rect",
                          // "height": 10,
                          "encoding": {
                            "y": { },
                            "x": {"field": attribute1["name"], "type": "ordinal", "sort": this.getAttributeList(attribute1) },
                            // "color": {
                            //     "field": attribute1["name"], 
                            //     //"scale": {"scheme": "blues"}, 
                            //     "scale": scale1,
                            //     "legend": null 
                            // }
                            "color": {"field": attribute1["name"], "type": "nominal", "scale": colorScale1, "legend": null, "sort": this.getAttributeList(attribute1) }, //{"field": attribute2["name"], "type": "nominal", "legend": {"values": legendValues}, "scale": {"scheme": "tableau20"}}, 

                          },
                          "config": {
                          "axis": {"grid": true, "tickBand": "extent"}
                          }
                        }



                      ]
                    }

                  
                ],

              "filter": filter,
              "attribute1": attribute1["name"],
              "attribute2": attribute2["name"],
              "type": "heatTable"



                  };


        }

        console_debug2("---------------------------------");
        console_debug2("---------------------------------");
        console_debug2(JSON.stringify(vegaLiteSpec));
        console_debug2("---------------------------------");
        console_debug2("---------------------------------");

        return vegaLiteSpec;

    },



    processLine: function(filter, attribute, vegaSpecToCopy ){

    	console.log("process line ");

 		// var plotTitle = "";

 		// for( var i = 0; i < filter.length; i++){
            //          plotTitle = plotTitle + filter[i]["vals"][0] ; //NOTE- not going to work totally.... 
                //          if( filter[i]["vals"].length > 1 ){
           //          	for( var j = 1; j < filter[i]["vals"].length; j++ ){
           //          		plotTitle = plotTitle + " and ";
           //          		plotTitle = plotTitle + filter[i]["vals"][j]; 
           //          	}
           //          }
           //          plotTitle = plotTitle + ", ";
           //      }

           //      plotTitle = plotTitle.substring(0, plotTitle.length - 2);
        var labelForNumber = "number of " + this.whatDataContains[0];

        plotTitle = this.createTitle(filter, attribute, null);
        // plotTitle = plotTitle + " by " + attribute["name"];
        plotTitle.push( " by " + attribute["name"] );


         // console.log("process line");
        var timeUnit = "year";
        if( attribute["name"] == "time-of-the-day" )
            timeUnit = "hours";
        if( attribute["name"] == "month-of-the-year")
            timeUnit = "month";
        if( attribute["name"] == "day-of-the-week")
            timeUnit = "day";

		var values = attribute["list"].split(",");


		var dataToVisualize = [];
        for( var i = 0; i < values.length; i++){
            // console.log(i);
            obj = {};

            obj["time"] = this.parseDateTime(values[i].replace(" ", ""),timeUnit);
            obj[labelForNumber] = this.retrieveValue(filter, [attribute["name"]], [values[i].replace(" ", "")] );//this.retrieveValue(filterAttr, filterVals, [attribute["name"]], [values[i].replace(" ", "")] ); //getRandomNumber();
            obj["legend"] = "all";

            dataToVisualize.push(obj); 
        }


        copyTheSpec = true; 
        if( vegaSpecToCopy != null && vegaSpecToCopy["mark"] == "bar" ){ //don't copy if mismatch type
            copyTheSpec = false; 
        }

 		var vegaLiteSpec = null;
        if( vegaSpecToCopy != null && copyTheSpec){ // copy pivot
        	vegaLiteSpec = loadash.cloneDeep(vegaSpecToCopy); 
        	vegaLiteSpec.description = plotTitle;
        	vegaLiteSpec.title.text = plotTitle;
        	vegaLiteSpec.data.values = dataToVisualize;
        	vegaLiteSpec.transform[0].timeUnit = timeUnit;
        	vegaLiteSpec.transform[0].as = timeUnit; 
        	vegaLiteSpec.encoding.x.timeUnit = timeUnit; //attribute["name"]; 

            vegaLiteSpec.attribute1 = attribute["name"];//added
            vegaLiteSpec.filter = filter; //added
        }
        else {
			vegaLiteSpec =  {

                        "$schema": "https://vega.github.io/schema/vega-lite/v4.json",
                         "description": plotTitle,
                        //  "width": 400,
                        // "height": 250,
                        "title": {
                            "text": plotTitle,
                             "anchor": "start"
                         },
                        "data": {
                            "values": dataToVisualize, 
                         },
                        "autosize": {
                            "type": "fit",
                            "resize": false
                        },
                        "mark": {
                            "type": "line",
                            "interpolate": "monotone",
                            "point": true,
                            "tooltip": null
                        },
                        "transform": [{"timeUnit": timeUnit, "field": "x", "as": timeUnit}],
                        "encoding": {
                            "x": { "field": "time", "type": "ordinal", "timeUnit": timeUnit,  "axis": {"ticks": true, "tickCount": 5} },
                            "y": {"field": labelForNumber, "type": "quantitative"},
                            "color": {"field": "legend", "type": "nominal"},
                            // "tooltip": {"field": "number of crimes", "type": "quantitative"}

                        },
                         "config": {"point": {"size": 50}, "mark": {"tooltip": null}},
                         "filter": filter,
                         "attribute1": attribute["name"],
                         "attribute2": null,
                         "type": "line"
                    };
        }

        console_debug2("---------------------------------");
        console_debug2("---------------------------------");
        console_debug2(JSON.stringify(vegaLiteSpec));
        console_debug2("---------------------------------");
       	console_debug2("---------------------------------");


        return vegaLiteSpec;



	},
 

    processMultiline: function( filter, attribute1, attribute2, vegaSpecToCopy ){

        console.log("process multi line ");

       //  	var plotTitle = "";

     		// for( var i = 0; i < filter.length; i++){
       //          plotTitle = plotTitle + filter[i]["vals"][0] ; //NOTE- not going to work totally.... 
       //          if( filter[i]["vals"].length > 1 ){
       //          	for( var j = 1; j < filter[i]["vals"].length; j++ ){
       //          		plotTitle = plotTitle + " and ";
       //          		plotTitle = plotTitle + filter[i]["vals"][j]; 
       //          	}
       //          }
       //          plotTitle = plotTitle + ", ";
       //      }

       //      plotTitle = plotTitle.substring(0, plotTitle.length - 2);
                
        plotTitle = this.createTitle(filter, attribute1, attribute2);
        // plotTitle = plotTitle + " by " + attribute1["name"] + " and " + attribute2["name"];
        plotTitle.push( " by " + attribute1["name"] + " and " + attribute2["name"] );

        var labelForNumber = "number of " + this.whatDataContains[0];


        console.log("process line");
        var timeUnit = "year";
        if( attribute1["name"] == "time-of-the-day" )
            timeUnit = "hours";
        if( attribute1["name"] == "month-of-the-year")
            timeUnit = "month";
        if( attribute1["name"] == "day-of-the-week")
            timeUnit = "day";

        console_debug("time unit = " + timeUnit);

        var attr1Values = this.getAttributeList(attribute1); 
        var attr2Values = this.getAttributeList(attribute2); 

        var dataToVisualize = [];
        var countAttr2 = []; 

        for(var j = 0; j < attr2Values.length; j++){
            var thisCount = {"value": attr2Values[j], "count": 0};

            for( var i = 0; i < attr1Values.length; i++){

                //console.log(i);
                obj = {};

                // filterAttr.push( attribute2["name"] );
                // filterVals.push( attr2Values[j] ); 

                filter.push( { "attr": attribute2["name"], "vals": [ attr2Values[j] ] } );


                obj["time"] = this.parseDateTime(attr1Values[i].replace(" ", ""),timeUnit);
                theCount =  this.retrieveValue(filter, [attribute1["name"]], [attr1Values[i].replace(" ", "")] );
                obj[labelForNumber] =  theCount; // this.retrieveValue(filterAttr, filterVals, [attribute1["name"]], [attr1Values[i].replace(" ", "")] ); //getRandomNumber();
                obj[attribute2["name"]] = attr2Values[j];

                filter.pop();

                dataToVisualize.push(obj); 
                thisCount["count"]+= theCount; 

            }
            countAttr2.push( thisCount );
        }


        //sort- display legend in sorted order
        countAttr2.sort( function(a, b){ return b["count"] - a["count"]; } );
        legendValues = [];
        for(i =0; i < countAttr2.length; i++){
            legendValues.push(countAttr2[i]["value"]);
        }

        var capAt = 12;
        var wasCapped = false;
        if( legendValues.length > capAt ){
            wasCapped = true;
            // plotTitle = plotTitle + " (top " + capAt + " " + attribute2["name"] + ")";
            plotTitle.push( " (top " + capAt + " " + attribute2["name"] + ")" );

            for(i = capAt; i < legendValues.length; i++ ){
                for(j =0; j < dataToVisualize.length; j++ ){
                    if( dataToVisualize[j][""+attribute2["name"]] == legendValues[i] ){
                        dataToVisualize.splice(j,1);
                        j--;
                    }

                }
                legendValues.splice(i,1);
                i--;
            }
        }

        // colorArrAttr = this.colorLookupForList( attribute["name"], legendValues );//attribute["listOfLabels"].split(",") );//domainArr );  


        // colorScale =  {
        //                     "domain": legendValues,//[ 'null','very-low-','low-','moderate-','high-','very-high-' ],
        //                     "range": colorArrAttr1//["orange", "gray", "blue", "green", "red", "pink", "purple"]//colorArr,
        //                 };


                colorArrAttr = this.colorLookupForList( attribute2["name"], attribute2["listOfLabels"].split(",") );//domainArr );//attribute["listOfLabels"].split(",") );//domainArr );  

                colorScale =   {
                            "domain": legendValues,//this.getAttributeList(attribute2),//attribute2["listOfLabels"].split(","),//domainArr,//legendValues,//[ 'null','very-low-','low-','moderate-','high-','very-high-' ],
                            "range": colorArrAttr//colorArr,//["orange", "gray", "blue", "green", "red", "pink", "purple"]//colorArr,
                        }

        copyTheSpec = true; 
        if( vegaSpecToCopy != null && vegaSpecToCopy["mark"] == "bar" ){ //don't copy if mismatch type
            copyTheSpec = false; 
        }

 		var vegaLiteSpec = null;
        if( vegaSpecToCopy != null && copyTheSpec ){ // copy pivot
        	vegaLiteSpec = loadash.cloneDeep(vegaSpecToCopy); 
        	vegaLiteSpec.description = plotTitle;
        	vegaLiteSpec.title.text = plotTitle;
        	vegaLiteSpec.data.values = dataToVisualize;
        	vegaLiteSpec.transform[0].timeUnit = timeUnit;
        	vegaLiteSpec.transform[0].as = timeUnit;
        	vegaLiteSpec.encoding.x.timeUnit = timeUnit; 

            vegaLiteSpec.attribute2 = attribute2["name"];//added
            vegaLiteSpec.attribute1 = attribute1["name"];//added
            vegaLiteSpec.filter = filter; //added

            vegaLiteSpec.encoding.color = { "field": attribute2["name"], "type": "nominal",  "legend": {"values": legendValues}, "scale": {"scheme": "tableau20"} };

        }
        else {
			vegaLiteSpec =  {

                        "$schema": "https://vega.github.io/schema/vega-lite/v4.json",
                         "description": plotTitle,
                        //  "width": 400,
                        // "height": 250,
                        "title": {
                            "text": plotTitle,
                             "anchor": "start"
                         },
                        "data": {
                            "values": dataToVisualize, 
                         },
                        "mark": {
                            "type": "line",
                            "interpolate": "monotone",
                            "point": true,
                            "tooltip": null
                        },
                        "autosize": {
                            "type": "fit",
                            "resize": false
                        },
                        "transform": [{"timeUnit": timeUnit, "field": "x", "as": timeUnit}],
                        "encoding": {
                            "x": { "field": "time", "type": "ordinal", "timeUnit": timeUnit,  "axis": {"ticks": true, "tickCount": 5} },
                            "y": {"field": labelForNumber, "type": "quantitative"},
                            "color": {"field": attribute2["name"], "type": "nominal",  "legend": {"values": legendValues}, "scale": colorScale},
                            // "tooltip": {"field": "number of crimes", "type": "quantitative"}

                        },
                          "config": {"point": {"size": 50}, "mark": {"tooltip": null}},
                         "filter": filter,
                         "attribute1": attribute1["name"],
                         "attribute2": attribute2["name"],
                         "type": "line"
                    };
        }

        console_debug2("---------------------------------");
        console_debug2("---------------------------------");
        console_debug2(JSON.stringify(vegaLiteSpec));
        console_debug2("---------------------------------");
        console_debug2("---------------------------------");


        return vegaLiteSpec;

    },

    initHashmap: function( hashAttr ){

        filenames = Object.keys( this.datasets ); 
        // console.log(filenames);
        for(i = 0; i < filenames.length; i++){
            // console.log(filenames[i])
            dataset = this.datasets[filenames[i]];
                    
            for(j = 0; j < dataset.length; j++){
                identifier = dataset[j][hashAttr]; 
                // if( identifier == "39061" ){
                //     console.log("FILE " + j + " " + filenames[i]);
                // }
                // console.log(identifier);
                if( identifier in this.hashFromIdToIdx ){
                    if( filenames[i] in this.hashFromIdToIdx[identifier]){
                        // if( identifier == "39061" ){
                        //     console.log("ADD TO OLD " + j + " " + filenames[i]);
                        // }
                        this.hashFromIdToIdx[ identifier ][ filenames[i] ].push(j); 
                    }
                    else{
                        // if( identifier == "39061" ){
                        //     console.log("MAKE NEW " + j + " " + filenames[i]);
                        // }
                        this.hashFromIdToIdx[ identifier ][ filenames[i] ] = [];
                        this.hashFromIdToIdx[ identifier ][ filenames[i] ].push(j); 

                    }
                }
                else{
                    this.hashFromIdToIdx[ identifier ] = {}; 
                    this.hashFromIdToIdx[ identifier ][ filenames[i] ] = [];
                    this.hashFromIdToIdx[ identifier ][ filenames[i] ].push(j); 
                }
            }
        }
        // console.log("39061");
        // console.log( this.hashFromIdToIdx["39061"] );
    },


    processDateSingleLine : function(filter, attribute1, attribute2, vegaSpecToCopy){
        console_debug("process date single line ");

        if( !this.hashFromIdToIdxInitialized ){
            this.initHashmap("county-map");
            this.hashFromIdToIdxInitialized = true; //only do this once
        }

        // var plotTitle = "";

        // for( var i = 0; i < filter.length; i++){
        //     plotTitle = plotTitle + filter[i]["vals"][0] ; //NOTE- not going to work totally.... 
        //     if( filter[i]["vals"].length > 1 ){
        //         for( var j = 1; j < filter[i]["vals"].length; j++ ){
        //             plotTitle = plotTitle + " and ";
        //             plotTitle = plotTitle + filter[i]["vals"][j]; 
        //         }
        //     }
        //     plotTitle = plotTitle + ", ";
        // }

        // plotTitle = plotTitle.substring(0, plotTitle.length - 2);

        plotTitle = this.createTitle(filter, attribute1, attribute2);
        // plotTitle = plotTitle + " by " + attribute1["name"];
        plotTitle.push(" by " + attribute1["name"]);

        var labelForNumber = "number of " + this.whatDataContains[1];

        var values = attribute1["list"].split(",");


        var dataToVisualize = [];
        for( var i = 0; i < values.length; i++){//for dates
            // console.log(i);
            obj = {};

            obj["time"] = values[i].replace(" ", "");//this.parseDateTime(values[i].replace(" ", ""),timeUnit);
            //filter, targetAttrs, targetVals, sumAttr){
            obj[labelForNumber] = this.retrieveSum(filter, [ attribute1["name"] ], [ values[i].replace(" ", "") ], "new-cases" );//this.retrieveValue(filterAttr, filterVals, [attribute["name"]], [values[i].replace(" ", "")] ); //getRandomNumber();
            obj["legend"] = "all";

            dataToVisualize.push(obj); 
        }

        // console.log(dataToVisualize);


        timeUnit = "yearmonth";

        copyTheSpec = true; 
        if( vegaSpecToCopy != null && vegaSpecToCopy["mark"] != "date_sum" ){ //don't copy if mismatch type
            copyTheSpec = false; 
        }

        var vegaLiteSpec = null;
        if( vegaSpecToCopy != null && copyTheSpec){ // copy pivot
            vegaLiteSpec = loadash.cloneDeep(vegaSpecToCopy); 
            vegaLiteSpec.description = plotTitle;
            vegaLiteSpec.title.text = plotTitle;
            vegaLiteSpec.data.values = dataToVisualize;
            vegaLiteSpec.transform[0].timeUnit = timeUnit;
            vegaLiteSpec.transform[0].as = timeUnit; 
            vegaLiteSpec.encoding.x.timeUnit = timeUnit; //attribute["name"]; 

            vegaLiteSpec.attribute1 = attribute["name"];//added
            vegaLiteSpec.filter = filter; //added
        }
        else {
            vegaLiteSpec =  {

                        "$schema": "https://vega.github.io/schema/vega-lite/v4.json",
                         "description": plotTitle,
                        //  "width": 400,
                        // "height": 250,
                        "title": {
                            "text": plotTitle,
                             "anchor": "start"
                         },
                        "data": {
                            "values": dataToVisualize, 
                             // "values": [ 
                             //    {"time": "2020-04-01", "sum": 100 },
                             //    {"time": "2020-05-01", "sum": 300 },
                             //    {"time": "2020-06-01", "sum": 250 },
                             //    {"time": "2020-07-01", "sum": 350 }
                             // ]
                         },
                        "autosize": {
                            "type": "fit",
                            "resize": true
                        },
                        "mark": {
                            "type": "line",
                            "interpolate": "monotone",
                            "point": true,
                            "tooltip": null
                        },
                         "transform": [ {"timeUnit": timeUnit, "field": "x", "as": timeUnit} ],
                          "encoding": {
                              "x": { "field": "time", "type": "ordinal", "timeUnit": timeUnit,  "axis": {"ticks": true, "tickCount": 5} },
                              "y": {"field": labelForNumber, "type": "quantitative"}
                          },
                         "config": {"point": {"size": 50}, "mark": {"tooltip": null}},
                         "filter": filter,
                         "attribute1": attribute1["name"],
                         "attribute2": null,
                         "type": "line"
                    };
        }

        console_debug2("---------------------------------");
        console_debug2("---------------------------------");
        console_debug2(JSON.stringify(vegaLiteSpec));
        console_debug2("---------------------------------");
        console_debug2("---------------------------------");


        return vegaLiteSpec;

        
    },


    processDateMultiLine: function( filter, attribute1, attribute2, vegaSpecToCopy ){
        console_debug("process date multi line ");

        var plotTitle = "";

        if( !this.hashFromIdToIdxInitialized ){
            this.initHashmap("county-map");
            this.hashFromIdToIdxInitialized = true; //only do this once
        }

        // for( var i = 0; i < filter.length; i++){
        //     plotTitle = plotTitle + filter[i]["vals"][0] ; //NOTE- not going to work totally.... 
        //     if( filter[i]["vals"].length > 1 ){
        //         for( var j = 1; j < filter[i]["vals"].length; j++ ){
        //             plotTitle = plotTitle + " and ";
        //             plotTitle = plotTitle + filter[i]["vals"][j]; 
        //         }
        //     }
        //     plotTitle = plotTitle + ", ";
        // }

        // plotTitle = plotTitle.substring(0, plotTitle.length - 2);
        // //plotTitle = plotTitle + " by " + attribute1["name"];


        plotTitle = this.createTitle(filter, attribute1, attribute2);
        // plotTitle = plotTitle + " by " + attribute1["name"] + " and " + attribute2["name"];
        plotTitle.push(" by " + attribute1["name"] + " and " + attribute2["name"]);

        var labelForNumber = "number of " + this.whatDataContains[1];


        // console.log("process line");
        // var timeUnit = "year";
        // if( attribute1["name"] == "time-of-the-day" )
        //     timeUnit = "hours";
        // if( attribute1["name"] == "month-of-the-year")
        //     timeUnit = "month";
        // if( attribute1["name"] == "day-of-the-week")
        //     timeUnit = "day";

        // console_debug("time unit = " + timeUnit);
        timeUnit = "yearmonth";

        var attr1Values = this.getAttributeList(attribute1); 
        var attr2Values = this.getAttributeList(attribute2); 

        var dataToVisualize = [];
        var countAttr2 = []; 

        for(var j = 0; j < attr2Values.length; j++){
            var thisSum = {"value": attr2Values[j], "sum": 0};

            for( var i = 0; i < attr1Values.length; i++){

                //console.log(i);
                obj = {};

                // filterAttr.push( attribute2["name"] );
                // filterVals.push( attr2Values[j] ); 

                filter.push( { "attr": attribute2["name"], "vals": [ attr2Values[j] ] } );
 
                obj["time"] = attr1Values[i].replace(" ", "");//this.parseDateTime(attr1Values[i].replace(" ", ""),timeUnit);
                obj[labelForNumber] =  this.retrieveSum(filter, [ attribute1["name"] ], [ attr1Values[i].replace(" ", "") ], "new-cases" );//this.retrieveValue(filter, [attribute1["name"]], [attr1Values[i].replace(" ", "")] );
                // obj["number of crimes"] =  theCount; // this.retrieveValue(filterAttr, filterVals, [attribute1["name"]], [attr1Values[i].replace(" ", "")] ); //getRandomNumber();
                
                //HACK!!!
                strToDisplay = attr2Values[j].replace(attribute2["name"], "" );
                obj[attribute2["name"]] = strToDisplay;//attr2Values[j];

                filter.pop();

                dataToVisualize.push(obj); 
                thisSum["sum"]+= obj["sum"];//theCount; 

            }
            countAttr2.push( thisSum);
        }


        //sort- display legend in sorted order  TURN OFF
        // if( attribute2["type"] != "ordinal")
        //     countAttr2.sort( function(a, b){ return b["sum"] - a["sum"]; } );
        legendValues = [];
        for(i =0; i < countAttr2.length; i++){
            strToDisplay = countAttr2[i]["value"].replace(attribute2["name"], "" );
            legendValues.push(strToDisplay);//countAttr2[i]["value"]);
        }

        var capAt = 12;
        var wasCapped = false;
        if( legendValues.length > capAt ){
            wasCapped = true;
            // plotTitle = plotTitle + " (top " + capAt + " " + attribute2["name"] + ")";
            plotTitle.push(" (top " + capAt + " " + attribute2["name"] + ")");
            for(i = capAt; i < legendValues.length; i++ ){
                for(j =0; j < dataToVisualize.length; j++ ){
                    if( dataToVisualize[j][""+attribute2["name"]] == legendValues[i] ){
                        dataToVisualize.splice(j,1);
                        j--;
                    }

                }
                legendValues.splice(i,1);
                i--;
            }
        }

        console.log(dataToVisualize);

        // var scale = { "scheme": "tableau20"};
        // if( attribute2["type"] == "ordinal"){
        //     colorArr =  this.getColorArrayByColorBrewerName(attribute2["color"], 5, false);//this.getColorArray("pink5");
        //     domainArr = [];
        //     // domainArr.push("null");
        //     arr= this.getAttributeList(attribute2);
        //     for(i = 0; i < arr.length; i++){
        //         domainArr.push(""+arr[i]);
        //     }
        //     console.log(domainArr);


        //     scale =   {
        //                 "domain": legendValues,//[ 'null','very-low-','low-','moderate-','high-','very-high-' ],
        //                 "range": colorArr,//["orange", "gray", "blue", "green", "red", "pink", "purple"]//colorArr,
        //             }
        // }



        var scale = { "scheme": "tableau20"};
            if( attribute2["type"] == "nominal"){
                console.log(colorbrewer.schemeGroups.qualitative);
                //colorArr = this.getColorArrayByColorBrewerName(attribute2["color"], this.getAttributeList(attribute2).length, false ); //"Blues"); //this.getColorArray("pink5");
                domainArr = [];
                // domainArr.push("null");
                arr= this.getAttributeList(attribute2);
                //domainArr.push("null");
                for(i = 0; i < arr.length; i++){
                    domainArr.push(""+arr[i]);
                }
                console.log(domainArr);

                colorArrAttr = this.colorLookupForList( attribute2["name"], attribute2["listOfLabels"].split(",") );//domainArr );//attribute["listOfLabels"].split(",") );//domainArr );  

                scale =   {
                            "domain": legendValues,//this.getAttributeList(attribute2), //attribute2["listOfLabels"].split(","),//domainArr,//legendValues,//[ 'null','very-low-','low-','moderate-','high-','very-high-' ],
                            "range": colorArrAttr//colorArr,//["orange", "gray", "blue", "green", "red", "pink", "purple"]//colorArr,
                        }

            }
            if( attribute2["type"] == "ordinal"){
                //colorArr = this.getColorArrayByColorBrewerName(attribute2["color"], 5, false); //"Blues"); //this.getColorArray("pink5");
                domainArr = [];
                // domainArr.push("null");
                arr= this.getAttributeList(attribute2);
                //domainArr.push("null");
                for(i = 0; i < arr.length; i++){
                    domainArr.push(""+arr[i]);
                }
                console.log(domainArr);

                // colorArrAttr = this.colorLookupForList( attribute2["name"], domainArr );//attribute["listOfLabels"].split(",") );//domainArr );  

                // scale =   {
                //             "domain": domainArr,//legendValues,//[ 'null','very-low-','low-','moderate-','high-','very-high-' ],
                //             "range": colorArrAttr//colorArr,//["orange", "gray", "blue", "green", "red", "pink", "purple"]//colorArr,
                //         }

                colorArrAttr = this.colorLookupForList( attribute2["name"], attribute2["listOfLabels"].split(",") );//domainArr );//attribute["listOfLabels"].split(",") );//domainArr );  

                scale =   {
                            "domain": legendValues,//this.getAttributeList(attribute2),//attribute2["listOfLabels"].split(","),//domainArr,//legendValues,//[ 'null','very-low-','low-','moderate-','high-','very-high-' ],
                            "range": colorArrAttr//colorArr,//["orange", "gray", "blue", "green", "red", "pink", "purple"]//colorArr,
                        }
            }




        console.log(scale);

            // console.log(colorScale);
        colorByField = attribute2["name"];


        copyTheSpec = true; 
        if( vegaSpecToCopy != null && vegaSpecToCopy["mark"] != "date_sum" ){ //don't copy if mismatch type
            copyTheSpec = false; 
        }

        var vegaLiteSpec = null;
        if( vegaSpecToCopy != null && copyTheSpec ){ // copy pivot
            vegaLiteSpec = loadash.cloneDeep(vegaSpecToCopy); 
            vegaLiteSpec.description = plotTitle;
            vegaLiteSpec.title.text = plotTitle;
            vegaLiteSpec.data.values = dataToVisualize;
            vegaLiteSpec.transform[0].timeUnit = timeUnit;
            vegaLiteSpec.transform[0].as = timeUnit;
            vegaLiteSpec.encoding.x.timeUnit = timeUnit; 

            vegaLiteSpec.attribute2 = attribute2["name"];//added
            vegaLiteSpec.attribute1 = attribute1["name"];//added
            vegaLiteSpec.filter = filter; //added

            vegaLiteSpec.encoding.color = { "field": attribute2["name"], "type": "nominal",  "legend": {"values": legendValues}, "scale": scale };

        }
        else {
            vegaLiteSpec =  {

                        "$schema": "https://vega.github.io/schema/vega-lite/v4.json",
                         "description": "date multiline",
                         "width": 500,
                        "height": 300,
                        "title": {
                            "text": plotTitle,
                             "anchor": "start"
                         },
                        "data": {
                            "values": dataToVisualize, 
                         },
                        "mark": {
                            "type": "line",
                            "interpolate": "monotone",
                            "point": true,
                            "tooltip": null
                        },
                        "autosize": {
                            "type": "fit",
                            "resize": false
                        },
                        "transform": [{"timeUnit": timeUnit, "field": "x", "as": timeUnit}],
                        "encoding": {
                            "x": { "field": "time", "type": "ordinal", "timeUnit": timeUnit,  "axis": {"ticks": true, "tickCount": 5} },
                            "y": {"field": labelForNumber, "type": "quantitative"},
                          // "color": {"field": attribute2["name"], "type": "nominal",  "legend": {"values": legendValues}, "scale": { "scheme": "tableau20"}},
                            // "tooltip": {"field": "number of crimes", "type": "quantitative"}
                             "color": {
                                "field": attribute2["name"], 
                                "type": "nominal", 
                                "legend": {"values": legendValues},
                                "scale": scale,
                            },

                        },
                          "config": {"point": {"size": 50}, "mark": {"tooltip": null}},
                         "filter": filter,
                         "attribute1": attribute1["name"],
                         "attribute2": attribute2["name"],
                         "type": "line"
                    };
        }

        console_debug2("---------------------------------");
        console_debug2("---------------------------------");
        console_debug2(JSON.stringify(vegaLiteSpec));
        console_debug2("---------------------------------");
        console_debug2("---------------------------------");


        return vegaLiteSpec;

    },


    processMap: function(filter){
    	console_debug("MAP");

    	return {"title": "Map",  "filter": filter,  "attribute1": "map", "attribute2": null }; 
    },

    processCountyMap: function( filter, attribute1, vegaSpecToCopy ){

        //attribute1 = this.getAttributeFromName( "poverty-rate");
        fipsAttr = this.getAttributeFromName("county-map");

        console.log(attribute1);
        if( attribute1 != null){ //color by....

            var plotTitle = "";

            // for( var i = 0; i < filter.length; i++){
            //     plotTitle = plotTitle + filter[i]["vals"][0] ; //NOTE- not going to work totally.... 
            //     if( filter[i]["vals"].length > 1 ){
            //         for( var j = 1; j < filter[i]["vals"].length; j++ ){
            //             plotTitle = plotTitle + " and ";
            //             plotTitle = plotTitle + filter[i]["vals"][j]; 
            //         }
            //     }
            //     plotTitle = plotTitle + ", ";
            // }

            // plotTitle = plotTitle.substring(0, plotTitle.length - 2);
            // plotTitle = plotTitle + " colored by " + attribute1["name"];

            plotTitle = this.createTitle(filter, attribute1, null);
            // plotTitle = plotTitle + " map";
            plotTitle.push("map");


            var dataToVisualize = [];
            dataToVisualize = this.retrievaPairs(filter, fipsAttr["name"], attribute1["name"] );
            //console.log(dataToVisualize);

            // colorArr = this.getColorArray("pink");
            // domainArr = [];
            // domainArr.push("null");
            // arr= this.getAttributeList(attribute1);
            // for(i = 0; i < arr.length; i++){
            //     domainArr.push(""+arr[i]);
            // }
            // console.log(domainArr);

            colorByField = attribute1["name"];

            ////
             var scale = { "scheme": "tableau20"};
            if( attribute1["type"] == "nominal"){
                // console.log(colorbrewer.schemeGroups.qualitative);
                //colorArr = this.getColorArrayByColorBrewerName(attribute1["color"], this.getAttributeList(attribute1).length, true ); //"Blues"); //this.getColorArray("pink5");
                domainArr = [];
                // domainArr.push("null");
                arr= this.getAttributeList(attribute1);
                domainArr.push("null");
                for(i = 0; i < arr.length; i++){
                    domainArr.push(""+arr[i]);
                }
                // console.log(domainArr);


                // scale =   {
                //             "domain": domainArr,//legendValues,//[ 'null','very-low-','low-','moderate-','high-','very-high-' ],
                //             "range": colorArr,//["orange", "gray", "blue", "green", "red", "pink", "purple"]//colorArr,
                //         }

                colorArrAttr = this.colorLookupForList( attribute1["name"], attribute1["listOfLabels"].split(",") );//domainArr );//attribute["listOfLabels"].split(",") );//domainArr );  
                colorArrAttr.splice(0,0,"lightgray");

                scale =   {
                            "domain": domainArr,//this.getAttributeList(attribute2),//attribute2["listOfLabels"].split(","),//domainArr,//legendValues,//[ 'null','very-low-','low-','moderate-','high-','very-high-' ],
                            "range": colorArrAttr//colorArr,//["orange", "gray", "blue", "green", "red", "pink", "purple"]//colorArr,
                        }
            }
            if( attribute1["type"] == "ordinal"){
                //colorArr = this.getColorArrayByColorBrewerName(attribute1["color"], 5, true); //"Blues"); //this.getColorArray("pink5");
                domainArr = [];
                // domainArr.push("null");
                arr= this.getAttributeList(attribute1);
                domainArr.push("null");
                for(i = 0; i < arr.length; i++){
                    domainArr.push(""+arr[i]);
                }
                // console.log(domainArr);


              colorArrAttr = this.colorLookupForList( attribute1["name"], attribute1["listOfLabels"].split(",") );//domainArr );//attribute["listOfLabels"].split(",") );//domainArr );  

              colorArrAttr.splice(0,0,"lightgray");
                scale =   {
                            "domain": domainArr,//this.getAttributeList(attribute2),//attribute2["listOfLabels"].split(","),//domainArr,//legendValues,//[ 'null','very-low-','low-','moderate-','high-','very-high-' ],
                            "range": colorArrAttr//colorArr,//["orange", "gray", "blue", "green", "red", "pink", "purple"]//colorArr,
                        }

                // scale =   {
                //             "domain": domainArr,//legendValues,//[ 'null','very-low-','low-','moderate-','high-','very-high-' ],
                //             "range": colorArr,//["orange", "gray", "blue", "green", "red", "pink", "purple"]//colorArr,
                //         }
            }

            //domainArr = ["null", "very-low-poverty-rate", "low-poverty-rate", "moderate-poverty-rate", "high-poverty-rate", "very-high-poverty-rate"];
            
            var vegaLiteSpec = null;
            if( vegaSpecToCopy != null && copyTheSpec ){ // copy pivot
                vegaLiteSpec = loadash.cloneDeep(vegaSpecToCopy); 
                vegaLiteSpec.description = plotTitle;
                vegaLiteSpec.title.text = plotTitle;
                vegaLiteSpec.data.values = dataToVisualize;
                // vegaLiteSpec.transform[0].timeUnit = timeUnit;
                // vegaLiteSpec.transform[0].as = timeUnit;
                // vegaLiteSpec.encoding.x.timeUnit = timeUnit; 

                vegaLiteSpec.attribute1 = "county-map";//added
                vegaLiteSpec.attribute2 = attribute1["name"];//added
                vegaLiteSpec.filter = filter; //added
                vegaLiteSpec.encoding.color = {"field": attribute1["name"], "type": "nominal", "scale": scale}; 
                // vegaLiteSpec.encoding.color = { "field": attribute2["name"], "type": "nominal",  "legend": {"values": legendValues}, "scale": {"scheme": "tableau20"} };

            }
            else {
                str2 = attribute1["name"];//"poverty-rate";//; //'region';
                    vegaLiteSpec = {
                      "$schema": "https://vega.github.io/schema/vega-lite/v4.json",
                      "title": {
                                    "text": plotTitle,
                                     "anchor": "start"
                                 },
                      "width": 500,
                      "height": 300,
                      "data": 
                          
                            // {"values": dataToVisualize},

                          {
                            "url":  "https://raw.githubusercontent.com/vega/vega/master/docs/data/us-10m.json",
                            "format": {"type": "topojson", "feature": "counties"}
                          },
                      "transform": [
                        {
                          "lookup": "id",
                          "from": {
                            "data": {
                            "values": dataToVisualize
                            }, //{"url": "https://raw.githubusercontent.com/vega/vega/master/docs/data/unemployment.tsv"},
                            "key": "id",
                            "fields": [attribute1["name"]], //[attribute1["name"]]
                          }
                        }
                      ],
                      "projection": {"type": "albersUsa"},
                      "mark": "geoshape",
                      "encoding": {
                            "color": {
                                "field": attribute1["name"], 
                                "type": "nominal", 
                                "scale": scale,
                                // "scale": {
                                //     "domain": domainArr,//["null", "very-low-poverty-rate", "low-poverty-rate", "moderate-poverty-rate", "high-poverty-rate", "very-high-poverty-rate"], 
                                //     "range": colorArr,
                                // }
                            },
                        }, //"rate", "type": "quantitative"}}
                        "filter": filter,
                         "attribute1": "county-map",
                         "attribute2": attribute1["name"],
                         "type": "county-map"
                    }


                }//end else
             } //end att1 != null
             else{ //no color by.... 

                // var plotTitle = "";
                var filterName = ""; 

                for( var i = 0; i < filter.length; i++){
                    //plotTitle = plotTitle + filter[i]["vals"][0] ; //NOTE- not going to work totally.... 
                    filterName = filterName + filter[i]["vals"][0];
                    if( filter[i]["vals"].length > 1 ){
                        for( var j = 1; j < filter[i]["vals"].length; j++ ){
                            //plotTitle = plotTitle + " and ";
                            //plotTitle = plotTitle + filter[i]["vals"][j]; 
                            filterName = filterName + " and ";
                            filterName = filterName + filter[i]["vals"][j]; 

                        }
                    }
                    //plotTitle = plotTitle + ", ";
                }

                //plotTitle = plotTitle.substring(0, plotTitle.length - 2);
                //plotTitle = plotTitle + " map";

                plotTitle = this.createTitle(filter, attribute1, null);
                // plotTitle = plotTitle + " map";
                plotTitle.push("map");

                var dataToVisualize = [];
                dataToVisualize = this.retrievaPairs(filter, fipsAttr["name"], null );
                
                for(i = 0 ; i < dataToVisualize.length; i++){
                    dataToVisualize[i]["data"] = filterName;
                }


                colorByField = "data"; 
                domainArr = ["null", filterName]; 
                colorArr = ["whitesmoke", "steelblue"];

                var vegaLiteSpec = null;
                if( vegaSpecToCopy != null && copyTheSpec ){ // copy pivot
                    vegaLiteSpec = loadash.cloneDeep(vegaSpecToCopy); 
                    vegaLiteSpec.description = plotTitle;
                    vegaLiteSpec.title.text = plotTitle;
                    vegaLiteSpec.data.values = dataToVisualize;
                    // vegaLiteSpec.transform[0].timeUnit = timeUnit;
                    // vegaLiteSpec.transform[0].as = timeUnit;
                    // vegaLiteSpec.encoding.x.timeUnit = timeUnit; 

                    vegaLiteSpec.attribute2 = null;//added
                    vegaLiteSpec.attribute1 = "count-map"//attribute1["name"];//added
                    vegaLiteSpec.filter = filter; //added

                    // vegaLiteSpec.encoding.color = { "field": attribute2["name"], "type": "nominal",  "legend": {"values": legendValues}, "scale": {"scheme": "tableau20"} };

                }
                else {
                    vegaLiteSpec = {
                      "$schema": "https://vega.github.io/schema/vega-lite/v4.json",
                      "title": {
                                    "text": plotTitle,
                                     "anchor": "start"
                                 },
                      "width": 500,
                      "height": 300,
                      "data": 
                          
                            // {"values": dataToVisualize},

                          {
                            "url":  "https://raw.githubusercontent.com/vega/vega/master/docs/data/us-10m.json",
                            "format": {"type": "topojson", "feature": "counties"}
                          },
                      "transform": [
                        {
                          "lookup": "id",
                          "from": {
                            "data": {
                            "values": dataToVisualize
                            }, //{"url": "https://raw.githubusercontent.com/vega/vega/master/docs/data/unemployment.tsv"},
                            "key": "id",
                            "fields": ["data"],//[attribute1["name"]], //[attribute1["name"]]
                          }
                        }
                      ],
                      "projection": {"type": "albersUsa"},
                      "mark": "geoshape",
                      "encoding": {
                            "color": {
                                "field": colorByField,//attribute1["name"], 
                                "type": "nominal", 
                                "scale": {
                                    "domain": domainArr,//["null", "very-low-poverty-rate", "low-poverty-rate", "moderate-poverty-rate", "high-poverty-rate", "very-high-poverty-rate"], 
                                    "range": colorArr,
                                }
                            },
                        }, //"rate", "type": "quantitative"}}
                        "filter": filter,
                         "attribute1": "county-map",
                         "attribute2": null,
                         "type": "county-map"
                    }
                }



        }

        console_debug2("---------------------------------");
        console_debug2("---------------------------------");
        console_debug2(JSON.stringify(vegaLiteSpec));
        console_debug2("---------------------------------");
        console_debug2("---------------------------------");


        return vegaLiteSpec;

    },

    getColorArray: function(color){

        if(color == "blue"){
            return ["whitesmoke", '#eff3ff','#bdd7e7','#6baed6','#3182bd','#08519c']; //blue
        } 
        if( color == "purple"){
            return ["whitesmoke", '#f2f0f7','#cbc9e2','#9e9ac8','#756bb1','#54278f']; //purple
        }
        if( color == "green"){
            return ["whitesmoke", '#edf8e9','#bae4b3','#74c476','#31a354','#006d2c']; //green
        }
        if( color == "red"){
            return ["whitesmoke", '#fee5d9','#fcae91','#fb6a4a','#de2d26','#a50f15']; //green
        }
        if( color == "orange"){
            return ["whitesmoke", '#feedde','#fdbe85','#fd8d3c','#e6550d','#a63603']; //green
        }


        if(color == "green2"){
            return ["whitesmoke", '#edf8fb','#b2e2e2','#66c2a4','#2ca25f','#006d2c']; //blue
        } 
        if( color == "purple2"){
            return ["whitesmoke", '#edf8fb','#b3cde3','#8c96c6','#8856a7','#810f7c']; //purple
        }
        if( color == "ygb"){
            return ["whitesmoke", '#f0f9e8','#bae4bc','#7bccc4','#43a2ca','#0868ac']; //green
        }
        if( color == "b2"){
            return ["whitesmoke", '#f1eef6','#bdc9e1','#74a9cf','#2b8cbe','#045a8d']; //green
        }
        if( color == "pink"){
            return ["whitesmoke", '#fcc5c0','#fa9fb5','#f768a1','#c51b8a','#7a0177']; //green  ['#feebe2','#fcc5c0','#fa9fb5','#f768a1','#c51b8a','#7a0177']
        }
        if( color == "pink5"){
            return ['#fcc5c0','#fa9fb5','#f768a1','#c51b8a','#7a0177']; //green  #fcc5c0
        }
        if( color == "yg"){
            return ["whitesmoke", '#ffffcc','#c2e699','#78c679','#31a354','#006837']; //green
        }
        if( color == "yb"){
            return ["whitesmoke", '#ffffcc','#a1dab4','#41b6c4','#2c7fb8','#253494']; //green
        }


    },


    getColorArrayByColorBrewerName: function(colorBrewerName, length, includeNull){
        len = length;

        console.log("color array: " + colorBrewerName + " length " + length + " include null " + includeNull);


        var initialArr;
        if( includeNull ){
            len = length + 1;
            initialArr = loadash.cloneDeep( colorbrewer[colorBrewerName][len] );
            console.log(initialArr);
            initialArr.splice(0, 1, "lightgray"); //insert light gray at the front
        } else{
            len = length + 1;
            initialArr = loadash.cloneDeep( colorbrewer[colorBrewerName][len] );
            console.log(initialArr);
            initialArr.splice(0, 1); //no replacement
        }
        console.log(initialArr);
        return initialArr; //colorbrewer[colorBrewerName][5];
    },


    getColorArrayByColorBrewerName2: function(colorBrewerName, length, includeNull){
        len = length;

        console.log("color array: " + colorBrewerName + " length " + length + " include null " + includeNull);


        var initialArr;
        if( includeNull ){
            len = length + 1;
            initialArr = loadash.cloneDeep( colorbrewer[colorBrewerName][len] );
            console.log(initialArr);
            initialArr.splice(0, 1, "lightgray"); //insert light gray at the front
        } else{
            len = length + 1;
            initialArr = loadash.cloneDeep( colorbrewer[colorBrewerName][len] );
            console.log(initialArr);
            initialArr.splice(0, 1); //no replacement
        }
        console.log(initialArr);
        return initialArr; //colorbrewer[colorBrewerName][5];
    },


    parseDateTime: function(xValue, horizontalAxis){

        if( horizontalAxis == "year")
            return xValue;
        if( horizontalAxis == "month" ){
            if(xValue == "january"){
                return "1";
            }
            if(xValue == "february"){
                return "2";
            }
            if(xValue == "march"){
                return "3";
            }
            if(xValue == "april"){
                return "4";
            }
            if(xValue == "may"){
                return "5";
            }
            if(xValue == "june"){
                return "6";
            }
            if(xValue == "july"){
                return "7";
            }
            if(xValue == "august"){
                return "8";
            }
            if(xValue == "september"){
                return "9";
            }
            if(xValue == "october"){
                return "10";
            }
            if(xValue == "november"){
                return "11";
            }
            if(xValue == "december"){
                return "12";
            }
        }

        if( horizontalAxis == "hours" ){

            if(xValue == "1")
                return "December 1, 1995 01:00:00";
            if(xValue == "2")
                return "December 1, 1995 02:00:00";
            if(xValue == "3")
                return "December 1, 1995 03:00:00";
            if(xValue == "4")
                return "December 1, 1995 04:00:00";
            if(xValue == "5")
                return "December 1, 1995 05:00:00";
            if(xValue == "6")
                return "December 1, 1995 06:00:00";
            if(xValue == "7")
                return "December 1, 1995 07:00:00";
            if(xValue == "8")
                return "December 1, 1995 08:00:00";
            if(xValue == "9")
                return "December 1, 1995 09:00:00";
            if(xValue == "10")
                return "December 1, 1995 10:00:00";
            if(xValue == "11")
                return "December 1, 1995 11:00:00";
            if(xValue ==  "12")
                return "December 1, 1995 12:00:00";
            if(xValue == "13")
                return "December 1, 1995 13:00:00";
            if(xValue == "14")
                return "December 1, 1995 14:00:00";
            if(xValue == "15")
                return "December 1, 1995 15:00:00";
            if(xValue == "16")
                return "December 1, 1995 16:00:00";
            if(xValue == "17")
                return "December 1, 1995 17:00:00";
            if(xValue == "18")
                return "December 1, 1995 18:00:00";
            if(xValue == "19")
                return "December 1, 1995 19:00:00";
            if(xValue == "20")
                return "December 1, 1995 20:00:00";
            if(xValue == "21")
                return "December 1, 1995 21:00:00";
            if(xValue == "22")
                return "December 1, 1995 22:00:00";
            if(xValue == "23")
                return "December 1, 1995 23:00:00";
            if(xValue ==  "0")
                return "December 1, 1995 24:00:00";
            
        }
        if( horizontalAxis == "day" ){
            if(xValue == "sunday")
                return "January 27, 2019 12:00:00";
            if(xValue == "monday")
                return "January 28, 2019 12:00:00";
            if(xValue == "tuesday")
                return "January 29, 2019 12:00:00";
            if(xValue == "wednesday")
                return "January 30, 2019 12:00:00";
            if(xValue == "thursday")
                return "January 31, 2019 12:00:00";
            if(xValue == "friday")
                return "February 1, 2019 12:00:00";
            if(xValue == "saturday")
                return "February 2, 2019 12:00:00";
        }



    },




    getAttribute: function(attribute){
        if(attribute == "all")
            return {"name": "all", "list": ["all"], "type": "all", "plotType": "all"};

        for(var i= 0; i< this.attributeAndValues.length; i++){
            // console.log(attribute)
            if(  this.attributeAndValues[i]["name"] == attribute["name"]){
                return this.attributeAndValues[i];
            }
        }
        return null; 
    },


    ////////////////////------ DATA RETRIEVAL -----///////////////////////

    retrieveValue: function( filter, targetAttrs, targetVals  ){

        theDataset = this.datasets[ this.filenames[0] ]; //'data/covid19_2/counties-cdc-v3.csv' ];

    	console_debug("-------------------");
        console_debug("-------------------");
        console_debug("-------------------");
        console_debug("RETRIEVE VALUE");
        console_debug("filter by " + filter);
        console_debug("target attrs " + targetAttrs);
        console_debug("target vals " + targetVals);

         var count = 0; 
        for(var i = 0; i < theDataset.length; i++){
            matchFilter = true; 
            for(var j = 0; j < filter.length; j++){
            	f = filter[j];

            	m_i = false; 
            	for( var k = 0 ; k < f["vals"].length; k++ ){
            		m_i = m_i || this.isAMatch(theDataset[i], f["attr"], f["vals"][k] );
            	}


                matchFilter = matchFilter && m_i; //(match0 || match1); //this.isAMatch(this.theDataset[i], f["attr"], f["vals"][0] ); 

            }
            if( matchFilter ){

                var matchTarget = true; 
                for( var k = 0; k < targetAttrs.length; k++ ){
                    matchTarget = matchTarget && this.isAMatch(theDataset[i], targetAttrs[k], targetVals[k])
                }
                if( matchTarget ){
                    count++; 
                    //console.log( theDataset[i] );
                }
            }
        }

        return count;
    },

    retrievaPairs: function(filter, attr1, attr2 ){

        // console.log(this.theDataset);

        theDataset = this.datasets[  this.filenames[0] ];//'data/covid19_2/counties-cdc-v3.csv' ]; 

        pairsArr = [];

        if( attr2 == null ){
            attr2 = "data";
        }

        for(var i = 0; i < theDataset.length; i++){
            matchFilter = true; 
            for(var j = 0; j < filter.length; j++){
                f = filter[j];

                m_i = false; 
                for( var k = 0 ; k < f["vals"].length; k++ ){
                    m_i = m_i || this.isAMatch(theDataset[i], f["attr"], f["vals"][k] );
                }


                matchFilter = matchFilter && m_i; //(match0 || match1); //this.isAMatch(this.theDataset[i], f["attr"], f["vals"][0] ); 

            }
            if( matchFilter ){
                // console.log(i);
                // console.log(this.theDataset[i]);
                str = attr1;
                str2 = attr2;//'region';
                a1 = theDataset[i][str];//attr1];
                a2 =  theDataset[i][str2];

                // console.log(a1);
                obj = {};
                obj["id"] = a1;
                obj[attr2] = a2;

                // pairsArr.push( obj);

                // obj = { "id": a1, str2:  a2 };

                // strObj = JSON.stringify(obj)
                pairsArr.push( obj );

            }
        }
        return pairsArr;
    },

    //hard coded for covid data... =( 
    retrieveSum: function( filter, targetAttrs, targetVals, sumAttr){

        mainDataFile = 'data/covid19_2/cases.csv';
        secondDataFile = 'data/covid19_2/counties.csv';
        theMainDataset = this.datasets[ mainDataFile ]; 
        theSecondDataset = this.datasets[ secondDataFile ];

        sum = 0;

        // console.log("-------------------");
        // console.log("-------------------");
        // console.log("-------------------");
        // console.log("RETRIEVE SUM");
        // console.log("filter by " + filter);
        // console.log("target attrs " + targetAttrs);
        // console.log("target attrs " + targetAttrs.length);

        // console.log("target vals " + targetVals);
        // console.log(theMainDataset.length);

        for(var i = 0; i < theMainDataset.length; i++){
            matchFilter = true;
            //console.log(theMainDataset[i]["county-map"]);

            var secondaryDatasetRow = this.getSecondaryDatasetRow( "county-map", theMainDataset[i]["county-map"], secondDataFile ); 
            // var secondaryDatasetRow = secondaryDatasetRows[0];

            // console.log("secondary dataset row")
            // console.log(secondaryDatasetRow);
            for(var j = 0; j < filter.length; j++){
                f = filter[j];

                m_i = false; 
                for( var k = 0 ; k < f["vals"].length; k++ ){
                    m_i = m_i || this.isAMatch(secondaryDatasetRow, f["attr"], f["vals"][k]);  //this.isAMatch(theDataset[i], f["attr"], f["vals"][k] );
                }


                matchFilter = matchFilter && m_i; //(match0 || match1); //this.isAMatch(this.theDataset[i], f["attr"], f["vals"][0] ); 

            }
            if( matchFilter ){
                // console.log("MATCH FILTER!!");
                // console.log(targetAttrs);
                // console.log(targetVals);
                var matchTarget = true; 
                for( var k = 0; k < targetAttrs.length; k++ ){

                    //which dataset
                    attr = this.getAttributeFromName(targetAttrs[k]);
                    datasetName = attr["datafile"];
                    // console.log(datasetName);
                    // console.log(mainDataFile);
                    if(datasetName == mainDataFile)
                        matchTarget = matchTarget && this.isAMatch(theMainDataset[i], targetAttrs[k], targetVals[k]);
                    else
                        matchTarget = matchTarget && this.isAMatch(secondaryDatasetRow, targetAttrs[k], targetVals[k]);

                    // matchTarget = matchTarget && this.isAMatch(secondaryDatasetRow, targetAttrs[k], targetVals[k]);
                }
                if( matchTarget ){
                                    //console.log("MATCH TARGET!!");

                    sum = sum + parseInt( theMainDataset[i]["new-cases"]);
                    // console.log( theDataset[i] );
                }

            }
        }

        return sum;

    },

    isAMatch: function(d, attr, val){
        if(val == "all")
            return true; 
        return d[attr] == val; 
    },

    getSecondaryDatasetRow: function (idField, idToCheck, secondaryDatasetFilename){

        idx = this.hashFromIdToIdx[idToCheck][secondaryDatasetFilename]; 
        if( idx == null )
            return [];

        return this.datasets[secondaryDatasetFilename][idx];    
        // for(i =0; i < theSecondaryDataset.length; i++){
        //     // console.log("  id1 " + theSecondaryDataset[i][idField] );
        //     // console.log( "id2 " + idToCheck );
        //     if( theSecondaryDataset[i][idField] == idToCheck){//found the id
        //         return theSecondaryDataset[i];
        //     }
        // }

    },

    aggregateFilters: function(filterAttributes, filterValues ){

        console_debug("aggregating filters together");

        newAggregatedFilterList = [] ; 
        for( i = 0; i < filterAttributes.length; i++ ){ //for all filter attributes
            encountered = false; 
            for( j = 0; j < newAggregatedFilterList.length; j++){
                if( newAggregatedFilterList[j]["attr"] == filterAttributes[i] ){
                    encountered = true;
                    newAggregatedFilterList[j]["vals"].push( filterValues[i] ); 
                }
            }
            if( !encountered ){
                newAggregatedFilterList.push( { "attr": filterAttributes[i], "vals": [filterValues[i] ] }); 
            }
        }

        console_debug("aggregated filter");
        console_debug(newAggregatedFilterList); 

        return newAggregatedFilterList;
    },




};



var debug = false; ;

function console_debug(string){
    if(debug)
        console.log(string); 
};


var debug2 = false;

function console_debug2(string){
    if(debug2)
        console.log(string); 
};

var debug3 = true;
function console_debug3(string){
    if(debug3)
        console.log(string);
}